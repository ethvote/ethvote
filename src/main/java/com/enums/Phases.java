package com.enums;

public enum Phases {

    PKI_PHASE(0),
    KEY_DERIVATION_PHASE(1),
    VOTING_PHASE(2),
    DECRYPTION_PHASE(3),
    PUBLISH_PHASE(4);

    private final int phaseNumber;

    Phases(final int phaseNumber) {
        this.phaseNumber = phaseNumber;
    }

    public int getNumber() {
        return phaseNumber;
    }
}
