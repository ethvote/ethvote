package com.enums;

public enum Role {

    ADMINISTRATOR("Administrator"),
    VOTING_ADMINISTRATOR("Voting_Administrator"),
    REGISTRAR("Registrar");

    private final String roleName;

    Role(final String roleName) {
        this.roleName = roleName;
    }

    public String getRoleName() {
        return this.roleName;
    }
}
