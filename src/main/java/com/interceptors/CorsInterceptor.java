package com.interceptors;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
@Qualifier("CorsInterceptor")
public class CorsInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(
            HttpServletRequest request,
            HttpServletResponse response,
            Object handler) throws Exception {
        //Additional headers have to be passed for Cross Requests Response Preflight
        addIfNotAlreadyThere(response,"Access-Control-Allow-Origin", "*");
        addIfNotAlreadyThere(response,"Access-Control-Allow-Methods","*");
        addIfNotAlreadyThere(response,"Access-Control-Allow-Headers", "Authorization, Content-Type");
        addIfNotAlreadyThere(response,"Access-Control-Expose-Headers", "RefreshToken");
        return true;
    }

    private void addIfNotAlreadyThere(HttpServletResponse response, String name, String value) {
        if(!response.containsHeader(name)) {
            response.addHeader(name,value);
        }
    }
}
