package com.models.singlevotingvoter;

import javax.persistence.Embeddable;
import javax.persistence.Lob;

@Embeddable
public class SecretShareEntity {

    private Long fromVoterId;

    private String fromVoterName;

    @Lob
    private String secretShare;

    @Lob
    private String signature;

    public SecretShareEntity setSecretShare(String secretShare) {
        this.secretShare = secretShare;
        return this;
    }

    public String getSecretShare() {
        return secretShare;
    }

    public SecretShareEntity setFromVoterId(long fromVoterId) {
        this.fromVoterId = fromVoterId;
        return this;
    }

    public Long getFromVoterId() {
        return fromVoterId;
    }

    public String getFromVotername() {
        return fromVoterName;
    }

    public SecretShareEntity setFromVotername(String fromVoterName) {
        this.fromVoterName = fromVoterName;
        return this;
    }

    public String getSignature() {
        return signature;
    }

    public SecretShareEntity setSignature(String signature) {
        this.signature = signature;
        return this;
    }
}
