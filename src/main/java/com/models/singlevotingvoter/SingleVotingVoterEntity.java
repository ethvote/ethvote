package com.models.singlevotingvoter;

import com.models.voting.VotingEntity;
import com.models.user.UserEntity;

import javax.persistence.*;
import java.util.*;

@Entity(name="SingleVotingVoterEntity")
@Table
@NamedEntityGraphs({
        @NamedEntityGraph(
                name = "SingleVotingVoterEntity.commitments",
                attributeNodes = @NamedAttributeNode("commitments")),
        @NamedEntityGraph(
                name = "SingleVotingVoterEntity.secretShares",
                attributeNodes = @NamedAttributeNode("secretShares")),
        @NamedEntityGraph(
                name = "SingleVotingVoterEntity.zeroKnowledgeProofs",
                attributeNodes = @NamedAttributeNode("zeroKnowledgeProofs")
        )
})
public class SingleVotingVoterEntity {

    @EmbeddedId
    private SingleVotingVoterEntityId id;

    @ManyToOne
    @MapsId("userId")
    private UserEntity userEntity;

    @ManyToOne
    @MapsId("votingId")
    private VotingEntity votingEntity;

    private boolean canVote = false;

    private boolean isTrustee = false;

    private boolean createdSecretShares = false;

    private boolean voted = false;

    private boolean participatedInDecryption = false;

    private Long voterNumber;

    @ElementCollection(fetch = FetchType.LAZY)
    private List<SecretShareEntity> secretShares = new ArrayList<>();

    @ElementCollection(fetch = FetchType.LAZY)
    private List<CommitmentEntity> commitments = new ArrayList<>();

    @ElementCollection(fetch = FetchType.LAZY)
    private Map<Integer,VoteNIZKPEntity> zeroKnowledgeProofs = new HashMap<>();

    @Lob
    private String alpha;

    @Lob
    private String beta;

    @Lob
    private String voteSignature;

    @Lob
    private String decryptionFactor;

    @Lob
    private String decryptionFactorSignature;

    @Embedded
    private DecryptionNIZKPEntity decryptionNIZKPEntity;

    @Lob
    private String publicKey;

    private boolean createdOwnKey;

    private SingleVotingVoterEntity() {
    }

    public SingleVotingVoterEntity(UserEntity userEntity, VotingEntity votingEntity) {
        this.userEntity = userEntity;
        this.votingEntity = votingEntity;
        this.id = new SingleVotingVoterEntityId(userEntity.getId(),votingEntity.getId());
        this.decryptionNIZKPEntity = new DecryptionNIZKPEntity();
    }

    public UserEntity getUserEntity() {
        return userEntity;
    }

    public VotingEntity getVotingEntity() {
        return votingEntity;
    }

    public List<SecretShareEntity> getSecretShares() {
        return secretShares;
    }

    public SingleVotingVoterEntity addSecretShare(SecretShareEntity secretShare) {
        this.secretShares.add(secretShare);
        return this;
    }

    public List<CommitmentEntity> getCommitments() {
        return commitments;
    }

    public SingleVotingVoterEntity addCommitments(CommitmentEntity commitment) {
        this.commitments.add(commitment);
        return this;
    }

    public Long getVoterNumber() {
        return voterNumber;
    }

    public SingleVotingVoterEntity setVoterNumber(Long voterNumber) {
        this.voterNumber = voterNumber;
        return this;
    }

    public boolean createdSecretShares() {
        return createdSecretShares;
    }

    public SingleVotingVoterEntity setCreatedSecretSharesToTrue() {
        this.createdSecretShares = true;
        return this;
    }

    public boolean getVoted() {
        return voted;
    }

    public SingleVotingVoterEntity setVotedToTrue() {
        this.voted = true;
        return this;
    }

    public boolean participatedInDecryption() {
        return participatedInDecryption;
    }

    public SingleVotingVoterEntity setParticipatedInDecryptionToTrue() {
        this.participatedInDecryption = true;
        return this;
    }

    public boolean getCanVote() {
        return canVote;
    }

    public SingleVotingVoterEntity setCanVote(boolean canVote) {
        this.canVote = canVote;
        return this;
    }

    public boolean getIsTrustee() {
        return isTrustee;
    }

    public SingleVotingVoterEntity setIsTrustee(boolean isTrustee) {
        this.isTrustee = isTrustee;
        return this;
    }

    public String getAlpha() {
        return alpha;
    }

    public SingleVotingVoterEntity setAlpha(String alpha) {
        this.alpha = alpha;
        return this;
    }

    public String getBeta() {
        return beta;
    }

    public SingleVotingVoterEntity setBeta(String beta) {
        this.beta = beta;
        return this;
    }

    public String getDecryptionFactor() {
        return decryptionFactor;
    }

    public SingleVotingVoterEntity setDecryptionFactor(String decryptionFactor) {
        this.decryptionFactor = decryptionFactor;
        return this;
    }

    public SingleVotingVoterEntity addNonInteractiveZeroKnowledgeProof(VoteNIZKPEntity proof) {
        Integer forMessageIndex = proof.getMessageIndex();
        zeroKnowledgeProofs.put(forMessageIndex,proof);
        return this;
    }

    public Map<Integer,VoteNIZKPEntity> getNonInteractiveZeroKnowledgeProof() {
        return zeroKnowledgeProofs;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public SingleVotingVoterEntity setPublicKey(String publicKey) {
        this.publicKey = publicKey;
        return this;
    }

    public DecryptionNIZKPEntity getDecryptionNIZKProof() {
        return decryptionNIZKPEntity;
    }

    public SingleVotingVoterEntity setDecryptionNIZKProof(DecryptionNIZKPEntity decryptionNIZKPEntity) {
        this.decryptionNIZKPEntity = decryptionNIZKPEntity;
        return this;
    }

    public String getVoteSignature() {
        return voteSignature;
    }

    public SingleVotingVoterEntity setVoteSignature(String voteSignature) {
        this.voteSignature = voteSignature;
        return this;
    }

    public String getDecryptionFactorSignature() {
        return decryptionFactorSignature;
    }

    public SingleVotingVoterEntity setDecryptionFactorSignature(String decryptionFactorSignature) {
        this.decryptionFactorSignature = decryptionFactorSignature;
        return this;
    }

    public boolean getCreatedOwnKey() {
        return createdOwnKey;
    }

    public SingleVotingVoterEntity setCreatedOwnKeyToTrue() {
        this.createdOwnKey = true;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SingleVotingVoterEntity that = (SingleVotingVoterEntity) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
