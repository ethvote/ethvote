package com.models.user;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<UserEntity,Long>, UserRepositoryCustom  {

    Optional<UserEntity> findOneByUsername(String username);

    Optional<UserEntity> findOneByUsernameAndEmail(String name, String email);

    List<UserEntity> findByUsernameIn(List<String> usernames);

    Optional<UserEntity> findOneById(Long id);
}
