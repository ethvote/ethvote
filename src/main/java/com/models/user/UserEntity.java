package com.models.user;

import com.models.createdvotings.CreatedVotingsEntity;
import com.models.role.RolesEntity;
import com.models.singlevotingvoter.SingleVotingVoterEntity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity(name="UserEntity")
@Table
@NamedEntityGraphs({
        @NamedEntityGraph(name = "UserEntity.participatingVotings",
                attributeNodes = @NamedAttributeNode("votings")),
        @NamedEntityGraph(
                name = "UserEntity.roles",
                attributeNodes = @NamedAttributeNode("roles")),
        @NamedEntityGraph(
                name = "UserEntity.createdVotings",
                attributeNodes = @NamedAttributeNode("createdVotings")),
        @NamedEntityGraph(
                name = "UserEntity.involvedVotings",
                attributeNodes = {
                        @NamedAttributeNode("votings"),
                        @NamedAttributeNode("createdVotings"),
                })
})
public class UserEntity {

    @Id
    @GeneratedValue
    private Long id;

    private String username;

    private String email;

    private String externalId;

    @Lob
    private String privateKey;

    @Lob
    private String publicKey;

    private Long numberOfVotingsWithTrusteeRole = 0L;

    @ElementCollection(fetch = FetchType.LAZY)
    @OrderColumn
    private List<SingleVotingVoterEntity> votings = new ArrayList<>();

    @Embedded
    private RolesEntity roles = new RolesEntity();

    @ElementCollection(fetch = FetchType.LAZY)
    private List<CreatedVotingsEntity> createdVotings = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public UserEntity setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public UserEntity setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public UserEntity setExternalId(String externalId) {
        this.externalId = externalId;
        return this;
    }

    public String getExternalId() {
        return externalId;
    }

    public UserEntity setPublicKey(String publicKey) {
        this.publicKey = publicKey;
        return this;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public UserEntity setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
        return this;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public UserEntity addVoting(SingleVotingVoterEntity entity) {
        votings.add(entity);
        return this;
    }

    public List<SingleVotingVoterEntity> getVotings() {
        return votings;
    }

    public UserEntity setRoles(RolesEntity roles) {
        this.roles = roles;
        return this;
    }

    public RolesEntity getRoles() {
        return roles;
    }

    public List<CreatedVotingsEntity> getCreatedVotings() {
        return createdVotings;
    }

    public UserEntity addCreatedVoting(CreatedVotingsEntity createdVoting) {
        this.createdVotings.add(createdVoting);
        return this;
    }

    public Long getNumberOfVotingsWithTrusteeRole() {
        return numberOfVotingsWithTrusteeRole;
    }

    public UserEntity setNumberOfVotingsWithTrusteeRole(Long numberOfVotingsWithTrusteeRole) {
        this.numberOfVotingsWithTrusteeRole = numberOfVotingsWithTrusteeRole;
        return this;
    }
}
