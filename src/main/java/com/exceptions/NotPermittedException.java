package com.exceptions;

public class NotPermittedException extends IllegalVoteException {

    public NotPermittedException(String message) {
        super(message);
    }
}
