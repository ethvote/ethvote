package com.exceptions;

public class VotingClosedException extends IllegalVoteException {
    public VotingClosedException(String message) {
        super(message);
    }
}
