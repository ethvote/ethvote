package com.exceptions;

public class VoterInVotingException extends DeleteVoterException {

    public VoterInVotingException(String message) {
        super(message);
    }
}
