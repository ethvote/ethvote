package com.exceptions;

public class NoSuchVoterException extends DeleteVoterException {

    public NoSuchVoterException(String message) {
        super(message);
    }
}
