package com.exceptions;

public class PKIClosedException extends IllegalVoteException {

    public PKIClosedException(String message) {
        super(message);
    }
}
