package com.exceptions;

public class NoSuchVotingException extends IllegalVoteException {

    public NoSuchVotingException(String message) {
        super(message);
    }
}
