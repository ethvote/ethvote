package com.exceptions.parser;

public class OptionWithoutNameException extends ParserException {
    public OptionWithoutNameException(String message) {
        super(message);
    }
}
