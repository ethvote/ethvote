package com.exceptions.parser;

public class IllegalCoefficientValueException extends ParserException {
    public IllegalCoefficientValueException(String message) {
        super(message);
    }
}
