package com.exceptions.parser;

public class MissingFieldException extends ParserException {
    public MissingFieldException(String message) {
        super(message);
    }

    public MissingFieldException(String message, Exception e) {
        super(message,e);
    }
}
