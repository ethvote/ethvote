package com.exceptions.parser;

public class TooFewCommitmentsException extends ParserException {
    public TooFewCommitmentsException(String message) {
        super(message);
    }
}
