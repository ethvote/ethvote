package com.exceptions.parser;

public class TooManyCommitmentsException extends ParserException{
    public TooManyCommitmentsException(String message) {
        super(message);
    }
}
