package com.controller;

import com.controller.data.request.PublicKeyBody;
import com.data.voter.SingleVoter;
import com.data.voter.Voters;
import com.data.voter.VotersToCreate;
import com.enums.Role;
import com.exceptions.DeleteVoterException;
import com.exceptions.NoSuchVoterException;
import com.exceptions.parser.ParserException;
import com.services.context.RequestContextHolder;
import com.services.data.keys.KeyService;
import com.services.data.roles.RoleService;
import com.services.data.voters.VotersService;
import com.services.parser.VoterParser;
import com.services.token.JwtTokenCreatorService;
import com.strings.Strings;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
public class VoterController {

    private final VotersService votersService;

    private final RequestContextHolder requestContextHolder;

    private final RoleService roleService;

    private final JwtTokenCreatorService jwtTokenCreatorService;

    private final KeyService keyService;

    @Autowired
    public VoterController(VotersService votersService,
                           RequestContextHolder requestContextHolder,
                           RoleService roleService,
                           KeyService keyService,
                           JwtTokenCreatorService jwtTokenCreatorService) {
        this.votersService = votersService;
        this.keyService = keyService;
        this.requestContextHolder = requestContextHolder;
        this.roleService = roleService;
        this.jwtTokenCreatorService = jwtTokenCreatorService;
    }

    @GetMapping("/api/currentVoterToken")
    public ResponseEntity getCurrentVoter() {
        Optional<SingleVoter> optSingleVoter = requestContextHolder.getCurrentVoter();
        if(!optSingleVoter.isPresent()) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("The current voter could not be determined");
        }

        SingleVoter singleVoter = optSingleVoter.get();

        String isRegistered;

        if(singleVoter.getId() != null) {
            isRegistered = "Yes";
        }
        else {
            isRegistered = "No";
        }

        JSONObject jsonObject = new JSONObject()
                .put("voterName", singleVoter.getName())
                .put("registered", isRegistered);
        return ResponseEntity.status(HttpStatus.OK)
                .body(jsonObject.toString());
    }

    @GetMapping("/api/voters/{voterId}/publicKey")
    public ResponseEntity getKey(@PathVariable Long voterId) {
        Optional<String> key = votersService.getPublicKeyOfVoter(voterId);
        if(key.isPresent()) {
            JSONObject keyObject = new JSONObject()
                .put("publicKey",key.get());
            return ResponseEntity.status(HttpStatus.OK)
                    .body(keyObject.toString());
        }
        else {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(String.format("The key for user '%d' does not exist",voterId));
        }
    }

    @GetMapping("/api/voters")
    public ResponseEntity getAllVoters(
            @RequestParam("onlyIfKeySpecified") boolean onlyIfKeySpecified) {
        Voters voters = null;
        if(onlyIfKeySpecified) {
            voters = votersService.getAllVotersWherePublicKeyIsSpecified();
        }
        else {
            voters = votersService.getAllVoters();
        }
        return ResponseEntity.status(HttpStatus.OK)
                .body(voters.toJson().toString());
    }


    @PostMapping("/api/voters")
    public ResponseEntity saveAllVoters(
            @RequestBody String jsonData) {
        VotersToCreate voters = null;

        Optional<SingleVoter> optCurrentVoter = requestContextHolder.getCurrentVoter();

        if(!optCurrentVoter.isPresent()) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN)
                    .body(Strings.VOTER_NOT_KNOWN.toString());
        }

        SingleVoter singleVoter = optCurrentVoter.get();
        if(     !roleService.hasRole(singleVoter.getId(), Role.ADMINISTRATOR) &&
                !roleService.hasRole(singleVoter.getId(), Role.REGISTRAR)) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN)
                    .body("Only administrators can add new voters");
        }

        try {
            voters = VoterParser.parse(jsonData);
        } catch (ParserException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(e.getMessage());
        }

        List<String> voterNames = voters.getVoters().stream().map(SingleVoter::getName).collect(Collectors.toList());
        Voters alreadyStoredVoters = votersService.getAllVotersByVoternames(voterNames);
        if(!alreadyStoredVoters.getVoters().isEmpty()) {
            return ResponseEntity.status(HttpStatus.CONFLICT)
                    .body(alreadyStoredVoters.toJson().toString());
        }

        Voters createdVoters = votersService.createVoters(new Voters().setVoters(voters.getVoters()),voters.doCreateKeys());
        if(createdVoters.getVoters().size() == voterNames.size()) {
            return ResponseEntity.status(HttpStatus.CREATED)
                    .body(createdVoters.toJson().toString());
        }
        else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(Strings.SERVER_SIDE_FAILURE.toString());
        }
    }

    @PutMapping("/api/voters/{voterId}/publicKey")
    public ResponseEntity storeKey(
            @PathVariable Long voterId,
            @RequestBody PublicKeyBody keyBody,
            @RequestHeader HttpHeaders headers) {

        Optional<SingleVoter> optVoter = requestContextHolder.getCurrentVoter();
        if(!optVoter.isPresent()) {
            return ResponseEntity
                    .status(HttpStatus.FORBIDDEN)
                    .body(Strings.VOTER_NOT_KNOWN.toString());
        }

        Long currentVoterId = optVoter.get().getId();
        String currentVoterName = optVoter.get().getName();
        if(!voterId.equals(currentVoterId)) {
            return ResponseEntity
                    .status(HttpStatus.UNAUTHORIZED)
                    .body(String.format("Voter '%s' has no permission to set the public key of voter with id ''%d",currentVoterName,voterId));
        }

        String key = keyBody.getPublicKey();

        if(key==null) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body("No key specified");
        }

        keyService.setPublicKeyOfVoter(voterId,key);

        Optional<String> optToken = jwtTokenCreatorService.createTokenForUser(voterId);
        if(!optToken.isPresent()) {
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Could not update key");
        }

        String token = optToken.get();
        JSONObject returnToken = new JSONObject()
                .put("token",token);

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(returnToken.toString());

    }

    @DeleteMapping("/api/voters/{voterId}")
    public ResponseEntity deleteVoter(
            @PathVariable Long voterId,
            @RequestHeader HttpHeaders headers) {

        Optional<SingleVoter> optCurrentVoter = requestContextHolder.getCurrentVoter();
        if(!optCurrentVoter.isPresent()) {
            return ResponseEntity
                    .status(HttpStatus.FORBIDDEN)
                    .body(Strings.VOTER_NOT_KNOWN.toString());
        }

        SingleVoter singleVoter = optCurrentVoter.get();
        if(!roleService.hasRole(singleVoter.getId(), Role.ADMINISTRATOR)) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN)
                    .body("Only administrators can delete voters");
        }

        try {
            votersService.deleteVoter(voterId);
        }
        catch (NoSuchVoterException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body(e.getMessage());
        }
        catch (DeleteVoterException e) {
            return ResponseEntity.status(HttpStatus.CONFLICT)
                    .body(e.getMessage());
        }

        return ResponseEntity.status(HttpStatus.OK)
                .body("The voter was deleted");

    }

    @GetMapping("/api/voters/{voterId}/privateKey")
    public ResponseEntity getPrivateKey(@PathVariable Long voterId) {
        Optional<SingleVoter> optVoter = requestContextHolder.getCurrentVoter();
        if(!optVoter.isPresent()) {
            return ResponseEntity
                    .status(HttpStatus.FORBIDDEN)
                    .body(Strings.VOTER_NOT_KNOWN.toString());
        }

        if(!optVoter.get().getId().equals(voterId)) {
            return ResponseEntity
                    .status(HttpStatus.FORBIDDEN)
                    .body(String.format("No permission to read the private key of the voter '%d'",voterId));
        }

        Optional<String> optionalKey = this.keyService.getPrivateKey(voterId);
        if(optionalKey.isPresent()) {
            JSONObject jsonObject = new JSONObject()
                    .put("privateKey",optionalKey.get());

            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(jsonObject.toString());
        }
        else {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body("Private key is not known");
        }

    }

    @PostMapping("/api/registerCurrentVoter")
    public ResponseEntity registerCurrentVoter() {
        Optional<SingleVoter> optCurrentVoter = requestContextHolder.getCurrentVoter();
        if(!optCurrentVoter.isPresent() || !optCurrentVoter.get().getUniqueId().isPresent()) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN)
                    .body(Strings.VOTER_NOT_KNOWN.toString());
        }

        SingleVoter singleVoter = optCurrentVoter.get();

        Optional<String> optUniqueId = singleVoter.getUniqueId();
        if(!optUniqueId.isPresent()) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Did not get unique id field of the user from shibboleth.");
        }

        Optional<SingleVoter> optSavedVoter = votersService.addVoter(
                singleVoter.getName(),
                singleVoter.getEmail(),
                optUniqueId.get(),
                true);

        if(!optSavedVoter.isPresent()) {
            return ResponseEntity.status(HttpStatus.CONFLICT)
                    .body("The voter is already registered");
        }

        Long id = optSavedVoter.get().getId();
        boolean votingAdminAdded = this.roleService.addRoleToVoter(id,Role.VOTING_ADMINISTRATOR);
        boolean registrarAdded = this.roleService.addRoleToVoter(id,Role.REGISTRAR);

        if(votingAdminAdded && registrarAdded) {
            return ResponseEntity.status(HttpStatus.CREATED)
                    .body("Voter was registered");
        }

        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body("Internal server error");
    }
}
