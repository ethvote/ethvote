package com.controller.data.request;

public class VoteBody {

    private String voter;
    private String votedOption;

    public String getVoter() {
        return voter;
    }

    public String getVotedOption() {
        return votedOption;
    }
}
