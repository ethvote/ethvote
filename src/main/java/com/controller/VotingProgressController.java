package com.controller;

import com.data.votingvoterprogress.VotingVoterProgress;
import com.services.data.votingvoterprogress.VotingVoterProgressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
public class VotingProgressController {

    private final VotingVoterProgressService votingVoterProgressService;

    @Autowired
    public VotingProgressController(VotingVoterProgressService votingVoterProgressService) {
        this.votingVoterProgressService = votingVoterProgressService;
    }

    @GetMapping("/api/votings/{id}/progress/voters/{voterId}")
    public ResponseEntity getProgress(
            @PathVariable Long id,
            @PathVariable Long voterId)  {

        long votingId = id;
        Optional<VotingVoterProgress> optionalVotingVoterProgress = votingVoterProgressService.getVotingProgressForVoter(votingId,voterId);
        if(!optionalVotingVoterProgress.isPresent()) {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body("Not found");
        }
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(optionalVotingVoterProgress.get().toJson().toString());
    }
}
