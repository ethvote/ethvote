package com.utils;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class StringUtils {

    private StringUtils() {
    }

    public static List<String> stringToList(String s) {
        String ret = s;
        ret = ret.replace("[","")
                .replace("]","")
                .replace(", ",",");
        if(ret.isEmpty()) {
            return Collections.emptyList();
        }
        return Arrays.asList(ret.split(","));
    }

    public static String listToString(List<String> list) {
        return list.toString();
    }
}
