package com.services.parser;

import com.data.roles.Roles;
import com.enums.Role;
import com.exceptions.parser.MissingFieldException;
import com.exceptions.parser.NoSuchRoleException;
import com.exceptions.parser.ParserException;
import com.services.conversion.RoleConverter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Optional;

public class RoleParser {

    private RoleParser() {
    }

    public static Roles parse(String jsonString) throws ParserException {
        JSONArray jsonArray = parseWithException(jsonString);
        return convertToEnum(jsonArray);
    }

    private static JSONArray parseWithException(String jsonString) throws MissingFieldException {
        JSONArray jsonArray;

        try {
            JSONObject jsonObj = new JSONObject(jsonString);
            jsonArray = jsonObj.getJSONArray("roles");
        } catch (JSONException e) {
            throw new MissingFieldException("Field \"roles\" is missing in "+jsonString,e);
        }
        return jsonArray;
    }

    private static Roles convertToEnum(JSONArray jsonArray) throws NoSuchRoleException {
        Roles roles = new Roles();
        for(Object role : jsonArray.toList()) {
            String roleName = role.toString();
            Optional<Role> optRole = RoleConverter.toEnum(roleName);
            if(!optRole.isPresent()) {
                throw new NoSuchRoleException("The role \""+role+"\" does not exist.");
            }
            roles.addRole(optRole.get());
        }

        return roles;
    }

}
