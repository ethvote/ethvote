package com.services.parser;

import com.data.decryptionfactor.SingleDecryptionFactor;
import com.exceptions.parser.MissingFieldException;
import com.exceptions.parser.NoContentException;
import com.exceptions.parser.ParserException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class SingleDecryptionFactorParser {

    private SingleDecryptionFactorParser() {
    }

    public static SingleDecryptionFactor parse(String jsonString) throws ParserException {
        SingleDecryptionFactor singleDecryptionFactor = parseWithException(jsonString);
        validate(singleDecryptionFactor,jsonString);
        return singleDecryptionFactor;
    }

    private static SingleDecryptionFactor parseWithException(String jsonString) throws NoContentException {
        SingleDecryptionFactor singleDecryptionFactor = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            singleDecryptionFactor = mapper.readValue(jsonString, SingleDecryptionFactor.class);
        } catch (IOException e) {
            throw new NoContentException("No content in string \""+jsonString+"\"",e);
        }
        return singleDecryptionFactor;
    }

    private static void validate(SingleDecryptionFactor commitmentFromVoter, String s) throws ParserException {

        if(commitmentFromVoter.getDecryptionFactor()==null) {
            throw new MissingFieldException("Missing decryption factor field in \""+s+"\"");
        }
        if(commitmentFromVoter.getSignature()==null) {
            throw new MissingFieldException("Missing signature field in \""+s+"\"");
        }

    }
}
