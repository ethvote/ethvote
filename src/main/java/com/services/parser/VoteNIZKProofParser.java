package com.services.parser;

import com.data.proofs.SingleVoteNIZKProof;
import com.data.proofs.VoteNIZKProofs;
import com.exceptions.parser.MissingFieldException;
import com.exceptions.parser.NoContentException;
import com.exceptions.parser.ParserException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class VoteNIZKProofParser {

    private VoteNIZKProofParser() {
    }

    public static VoteNIZKProofs parse(String jsonString) throws ParserException {
        VoteNIZKProofs voteNIZKProofs = parseWithException(jsonString);
        validateProofs(voteNIZKProofs,jsonString);
        return voteNIZKProofs;
    }

    private static VoteNIZKProofs parseWithException(String jsonString) throws NoContentException {
        VoteNIZKProofs voteNIZKProofs = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            voteNIZKProofs = mapper.readValue(jsonString, VoteNIZKProofs.class);
        } catch (IOException e) {
            throw new NoContentException("No content in string \""+jsonString+"\"",e);
        }
        return voteNIZKProofs;
    }

    private static void validateProofs(VoteNIZKProofs voteNIZKProofs, String s) throws MissingFieldException {
        for(SingleVoteNIZKProof singleVoteNIZKProof : voteNIZKProofs.getSingleVoteNIZKProofs()) {
            validateSingleProof(singleVoteNIZKProof,s);
        }
    }

    private static void validateSingleProof(SingleVoteNIZKProof singleVoteNIZKProof, String s) throws MissingFieldException {
        if(singleVoteNIZKProof.getMessageIndex()==null) {
            throw new MissingFieldException("Missing message index number field in \""+s+"\"");
        }

        if(singleVoteNIZKProof.getA()==null) {
            throw new MissingFieldException("Missing A number field in \""+s+"\"");
        }

        if(singleVoteNIZKProof.getB()==null) {
            throw new MissingFieldException("Missing B number field in \""+s+"\"");
        }

        if(singleVoteNIZKProof.getC()==null) {
            throw new MissingFieldException("Missing C number field in \""+s+"\"");
        }

        if(singleVoteNIZKProof.getR()==null) {
            throw new MissingFieldException("Missing R number field in \""+s+"\"");
        }

        if(singleVoteNIZKProof.getSignature()==null) {
            throw new MissingFieldException("Missing Signature field in \""+s+"\"");
        }
    }

}
