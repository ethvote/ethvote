package com.services.hash;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
@Qualifier("HashService")
public class HashServiceImpl implements HashService {

    @Override
    public String createSHA256Hash(String message) {
        return org.apache.commons.codec.digest.DigestUtils.sha256Hex(message);
    }
}
