package com.services.token;

import java.util.Optional;

public interface JwtTokenCreatorService {

    Optional<String> createTokenForUser(Long voterId);

    Optional<String> createTokenForUser(String voterName);
}
