package com.services.token;

import com.configs.Params;
import com.data.voter.SingleVoter;
import com.services.data.roles.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Qualifier("JwtTokenCreatorService")
public class JwtTokenCreatorServiceImpl implements JwtTokenCreatorService {

    private final JwtTokenCoderService jwtTokenCoderService;

    private final RoleService roleService;

    @Autowired
    public JwtTokenCreatorServiceImpl(
            JwtTokenCoderService jwtTokenCoderService,
            RoleService roleService) {
        this.jwtTokenCoderService = jwtTokenCoderService;
        this.roleService = roleService;
    }

    @Override
    public Optional<String> createTokenForUser(Long voterId) {
        Optional<SingleVoter> votersOptional = roleService.getVoterWithRoles(voterId);
        return createTokenForUser(votersOptional);
    }

    @Override
    public Optional<String> createTokenForUser(String voterName) {
        Optional<SingleVoter> votersOptional = roleService.getVoterWithRoles(voterName);
        return createTokenForUser(votersOptional);
    }

    private Optional<String> createTokenForUser(Optional<SingleVoter> votersOptional) {
        if(!votersOptional.isPresent()) {
            return Optional.empty();
        }
        return createTokenForUser(votersOptional.get());
    }

    private Optional<String> createTokenForUser(SingleVoter voter) {
        long expirationDate = java.time.Instant.now().getEpochSecond() + Params.TOKEN_EXPIRATION_INTERVAL;

        String token = null;

        Optional<String> optPublicKey = voter.getPublicKey();

        if(optPublicKey.isPresent()) {
            token = jwtTokenCoderService.createToken(
                    voter.getId(),
                    voter.getName(),
                    optPublicKey.get(),
                    expirationDate,
                    voter.getRoles(),
                    voter.getNumberOfVotingsWithTrusteeRole());
        }
        else {
            token = jwtTokenCoderService.createToken(
                    voter.getId(),
                    voter.getName(),
                    expirationDate,
                    voter.getRoles(),
                    voter.getNumberOfVotingsWithTrusteeRole());
        }

        return Optional.of(token);
    }

}
