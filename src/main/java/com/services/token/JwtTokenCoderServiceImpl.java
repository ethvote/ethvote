package com.services.token;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.data.roles.Roles;
import com.data.voter.SingleVoter;
import com.enums.Role;
import com.services.secrets.SecretsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import static com.auth0.jwt.algorithms.Algorithm.HMAC512;


@Service
@Qualifier("JwtTokenCoderService")
public class JwtTokenCoderServiceImpl implements JwtTokenCoderService {

    private final SecretsService secretsService;

    private static final String VOTER_ID = "subId";
    private static final String VOTER_NAME = "sub";
    private static final String VOTER_ROLES = "subRoles";
    private static final String PUBLIC_KEY_IDENTIFIER = "publicKey";
    private static final String EXPIRATION_TIME = "expirationTime";
    private static final String NUMBER_OF_VOTINGS_WITH_TRUSTEE_ROLE = "numberOfVotingsWithTrusteeRole";

    @Autowired
    public JwtTokenCoderServiceImpl(SecretsService secretsService) {
        this.secretsService = secretsService;
    }

    @Override
    public String createToken(Long voterId, String voterName, Long expirationTime, Roles roles, Long numberOfVotingsWithTrusteeRole) {
        LinkedHashMap<String, String> fields = new LinkedHashMap<>();
        fields.put(VOTER_ID,""+voterId);
        fields.put(VOTER_NAME,voterName);
        fields.put(NUMBER_OF_VOTINGS_WITH_TRUSTEE_ROLE,""+numberOfVotingsWithTrusteeRole);
        return createToken(fields,expirationTime,roles);
    }

    @Override
    public String createToken(Long voterId, String voterName, String userPublicKey, Long expirationTime, Roles roles, Long numberOfVotingsWithTrusteeRole) {
        LinkedHashMap<String, String> fields = new LinkedHashMap<>();
        fields.put(VOTER_ID,""+voterId);
        fields.put(VOTER_NAME,voterName);
        fields.put(PUBLIC_KEY_IDENTIFIER,userPublicKey);
        fields.put(NUMBER_OF_VOTINGS_WITH_TRUSTEE_ROLE,""+numberOfVotingsWithTrusteeRole);
        return createToken(fields,expirationTime,roles);
    }

    private String createToken(LinkedHashMap<String,String> fields, Long expirationTime, Roles roles) {
        byte[] signingKey = secretsService.getJwtSigningKey();
        JWTCreator.Builder builder = JWT.create();
        for(Map.Entry<String, String> entry : fields.entrySet()) {
            builder.withClaim(entry.getKey(),entry.getValue());
        }
        builder.withClaim(EXPIRATION_TIME,expirationTime);

        List<String> rolesString = roles.getRoles().stream().map(Role::getRoleName).collect(Collectors.toList());
        builder.withArrayClaim(VOTER_ROLES,rolesString.toArray(new String[0]));

        return builder.sign(HMAC512(signingKey));

    }

    @Override
    public Optional<SingleVoter> getVoterFromToken(String token) {
        DecodedJWT jwt = checkSignature(token);
        if(jwt==null) {
            return Optional.empty();
        }

        return extractVoter(jwt);
    }

    private DecodedJWT checkSignature(String token) {
        byte[] signingKey = secretsService.getJwtSigningKey();
        DecodedJWT jwt;

        try {
            JWTVerifier verifier = JWT.require(HMAC512(signingKey)).build();
            jwt = verifier.verify(token);
        }
        catch(JWTVerificationException exception) {
            return null;
        }

        return jwt;
    }

    private Optional<SingleVoter> extractVoter(DecodedJWT decodedJWT) {
        String voterName = decodedJWT.getClaim(VOTER_NAME).asString();
        String publicKey = decodedJWT.getClaim(PUBLIC_KEY_IDENTIFIER).asString();

        if(voterName == null) {
            return Optional.empty();
        }

        SingleVoter voter = new SingleVoter().setName(voterName);

        String voterIdString = decodedJWT.getClaim(VOTER_ID).asString();

        if(voterIdString!=null) {
            voter.setId(Long.valueOf(voterIdString));
        }

        if(publicKey!=null) {
            voter.setPublicKey(publicKey);
        }

        return Optional.of(voter);
    }


}
