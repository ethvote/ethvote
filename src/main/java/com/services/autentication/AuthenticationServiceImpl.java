package com.services.autentication;

import com.services.environment.EnvironmentService;
import com.services.secrets.SecretsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
@Qualifier("AuthenticationService")
public class AuthenticationServiceImpl implements AuthenticationService {

    private static final String ADMIN = "admin";

    private final SecretsService secretsService;
    private final EnvironmentService environmentService;

    @Autowired
    public AuthenticationServiceImpl(SecretsService secretsService,
                                     EnvironmentService environmentService) {
        this.secretsService = secretsService;
        this.environmentService = environmentService;
    }

    @Override
    public boolean authenticate(String username, String password) {
        if(environmentService.isDemoEnvironment()) {
            return authenticateDemo(username, password);
        }
        else {
            return authenticateProduction(username, password);
        }
    }

    private boolean authenticateDemo(String username, String password) {
        return !username.equals(ADMIN) || password.equals(ADMIN);
    }

    private boolean authenticateProduction(String username, String password) {
        String adminPassword = secretsService.getAdminPassword();
        return username.equals(ADMIN) && password.equals(adminPassword);
    }
}
