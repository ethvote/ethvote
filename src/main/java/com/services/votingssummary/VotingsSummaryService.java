package com.services.votingssummary;

import org.json.JSONObject;

import java.util.Optional;

public interface VotingsSummaryService {

    Optional<JSONObject> createVotingsSummary(long votingId);
}
