package com.services.mail;

import java.util.List;

public interface MailService {

    boolean sendMail(List<String> to, String subject, String message);

    boolean sendMail(String to, String subject, String message);
}
