package com.services.data.vote;

import com.data.dataforkeyderivation.DataForKeyDerivation;
import com.data.decryptionfactor.DecryptionFactors;
import com.data.decryptionfactor.SingleDecryptionFactor;
import com.data.votes.SingleVote;
import com.data.votes.Votes;
import com.data.votings.EligibleVoterWithKey;
import com.data.votings.VotingCalculationParameters;
import com.enums.Phases;
import com.exceptions.*;
import com.models.decryption.DecryptionRepository;
import com.models.singlevotingvoter.SingleVotingVoterEntity;
import com.models.singlevotingvoter.SingleVotingVoterRepository;
import com.models.voting.VotingCalculationParametersEntity;
import com.models.voting.VotingEntity;
import com.models.voting.VotingsRepository;
import com.services.conversion.VotingsConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Qualifier("VoteService")
public class VoteServiceImpl implements VoteService {

    private final VotingsRepository votingsRepository;

    private final SingleVotingVoterRepository singleVotingVoterRepository;

    private final DecryptionRepository decryptionRepository;

    @Autowired
    public VoteServiceImpl(VotingsRepository votingsRepository,
                           SingleVotingVoterRepository singleVotingVoterRepository,
                           DecryptionRepository decryptionRepository) {
        this.votingsRepository = votingsRepository;
        this.singleVotingVoterRepository = singleVotingVoterRepository;
        this.decryptionRepository = decryptionRepository;
    }

    public boolean setVoteForVoter(long voterId, long votingId, SingleVote singleVote) throws IllegalVoteException {
        Optional<SingleVotingVoterEntity> optSingleVotingVoterEntity = singleVotingVoterRepository.findOne(voterId,votingId);
        if(!optSingleVotingVoterEntity.isPresent()) {
            throw new NoSuchVotingException("The voting could not be found.");
        }

        SingleVotingVoterEntity singleVotingVoterEntity = optSingleVotingVoterEntity.get();

        if(!singleVotingVoterEntity.getCanVote()) {
            throw new NotPermittedException("You are not allowed to vote in this voting.");
        }

        if(singleVotingVoterEntity.getVoted()) {
            throw new DuplicateException("You already voted.");
        }

        if(singleVotingVoterEntity.getVotingEntity().getPhaseNumber()!=Phases.VOTING_PHASE.getNumber()) {
            throw new VotingClosedException("The voting administrator already closed the vote.");
        }

        long currentTime = System.currentTimeMillis();
        long votingEndTime = singleVotingVoterEntity.getVotingEntity().getTimeVotingIsFinished();

        if(votingEndTime != -1 && votingEndTime < currentTime) {
            throw new VotingClosedException("The time for this voting is over.");
        }

        singleVotingVoterEntity.setAlpha(singleVote.getAlpha());
        singleVotingVoterEntity.setBeta(singleVote.getBeta());
        singleVotingVoterEntity.setVoteSignature(singleVote.getSignature());
        singleVotingVoterEntity.setVotedToTrue();

        singleVotingVoterRepository.saveAndFlush(singleVotingVoterEntity);
        return true;
    }

    @Override
    public Votes getAllVotesForVoting(long votingId) {
        Optional<VotingEntity> optVoting = votingsRepository.findOneByIdWithEligibleVotersFetched(votingId);
        if(!optVoting.isPresent()) {
            //TODO: Handle error case
            throw new UnsupportedOperationException();
        }

        VotingEntity votingEntity = optVoting.get();

        Votes votes = new Votes();
        for(SingleVotingVoterEntity singleVotingVoterEntity : votingEntity.getAllEligibleVoters()) {
            if(!singleVotingVoterEntity.getVoted()) {
                continue;
            }
            SingleVote singleVote = new SingleVote()
                    .setFromVoterId(singleVotingVoterEntity.getUserEntity().getId())
                    .setFromVoterName(singleVotingVoterEntity.getUserEntity().getUsername())
                    .setAlpha(singleVotingVoterEntity.getAlpha())
                    .setBeta(singleVotingVoterEntity.getBeta())
                    .setSignature(singleVotingVoterEntity.getVoteSignature());
            votes.addVote(singleVote);
        }
        return votes;
    }

    @Override
    public Optional<DataForKeyDerivation> getDataForKeyDerivation(Long votingId) {
        Optional<VotingEntity> optVoting = votingsRepository.findOneByIdWithEligibleVotersFetched(votingId);

        if(!optVoting.isPresent()) {
            return Optional.empty();
        }

        VotingEntity votingEntity = optVoting.get();

        VotingCalculationParametersEntity votingCalculationParametersEntity = votingEntity.getCalculationParametersEntity();
        VotingCalculationParameters votingCalculationParameters = VotingsConverter.toData(votingCalculationParametersEntity);

        DataForKeyDerivation dataForKeyDerivation = new DataForKeyDerivation()
                .setNumberOfEligibleVoters(votingEntity.getNumberOfEligibleVoters())
                .setSecurityThreshold(votingEntity.getSecurityThreshold())
                .setVotingCalculationParameters(votingCalculationParameters)
                .setVotingAdministratorId(votingEntity.getVotingAdministrator().getUserEntity().getId());

        List<SingleVotingVoterEntity> eligibleVoters = votingEntity.getAllEligibleVoters();
        for(SingleVotingVoterEntity voter : eligibleVoters) {
            EligibleVoterWithKey eligibleVoter = VotingsConverter.toData(voter);
            dataForKeyDerivation.addEligibleVoter(eligibleVoter);
        }

        return Optional.of(dataForKeyDerivation);
    }

    @Override
    public boolean setDecryptionFactor(long voterId, long votingId, SingleDecryptionFactor singleDecryptionFactor) throws IllegalVoteException {
        Optional<SingleVotingVoterEntity> optSingleVotingVoterEntity = singleVotingVoterRepository.findOne(voterId,votingId);
        if(!optSingleVotingVoterEntity.isPresent()) {
            throw new NoSuchVotingException("The voting could not be found.");
        }

        SingleVotingVoterEntity singleVotingVoterEntity = optSingleVotingVoterEntity.get();
        if(singleVotingVoterEntity.getDecryptionFactor() != null) {
            throw new DuplicateException("You already provided your decryption factor.");
        }

        if(singleVotingVoterEntity.getVotingEntity().getPhaseNumber()!=Phases.DECRYPTION_PHASE.getNumber()) {
            throw new DecryptionClosedException("The time for decryption is over.");
        }

        int updatedRows = decryptionRepository.setDecryption(votingId,voterId,singleDecryptionFactor.getDecryptionFactor(),singleDecryptionFactor.getSignature());
        return updatedRows == 1;
    }

    @Override
    public DecryptionFactors getAllDecryptionFactors(long votingId) {
        Optional<VotingEntity> optVotingEntity = votingsRepository.findOneByIdWithEligibleVotersFetched(votingId);
        if(!optVotingEntity.isPresent()) {
            //TODO: 19.11.2018 Handle error case
            throw new UnsupportedOperationException();
        }

        VotingEntity votingEntity = optVotingEntity.get();
        DecryptionFactors decryptionFactors = new DecryptionFactors();
        for(SingleVotingVoterEntity singleVotingVoterEntity : votingEntity.getAllEligibleVoters()) {
            if(singleVotingVoterEntity.getDecryptionFactor()==null) {
                continue;
            }

            SingleDecryptionFactor singleDecryptionFactor = new SingleDecryptionFactor()
                    .setFromVoterId(singleVotingVoterEntity.getUserEntity().getId())
                    .setFromVoterName(singleVotingVoterEntity.getUserEntity().getUsername())
                    .setFromVoterPosition(singleVotingVoterEntity.getVoterNumber())
                    .setDecryptionFactor(singleVotingVoterEntity.getDecryptionFactor())
                    .setSignature(singleVotingVoterEntity.getDecryptionFactorSignature());

            decryptionFactors.addDecryptionFactor(singleDecryptionFactor);
        }

        return decryptionFactors;
    }

}
