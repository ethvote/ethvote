package com.services.data.roles;

import com.data.roles.Roles;
import com.data.voter.SingleVoter;
import com.data.voter.Voters;
import com.enums.Role;

import java.util.List;
import java.util.Optional;

public interface RoleService {

    Optional<SingleVoter> getVoterWithRoles(long voterId);

    Optional<SingleVoter> getVoterWithRoles(String voterName);

    boolean hasRole(long voterId, Role role);

    boolean addRoleToVoter(long voterId, Role role);

    boolean deleteRoleFromVoter(long voterId, Role role);

    Voters getAllVotersWithTheirRoles();

    Voters getAllVotersByIdWithTheirRoles(List<Long> ids);

    Roles getAllAvailableRoles();
}
