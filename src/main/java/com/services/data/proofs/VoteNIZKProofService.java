package com.services.data.proofs;

import com.data.proofs.VoteNIZKProofs;
import com.exceptions.IllegalVoteException;

import java.util.List;
import java.util.Optional;

public interface VoteNIZKProofService {

    Optional<List<VoteNIZKProofs>> getNIZKProofs(long votingId);

    boolean setNIZKProofs(long votingId, long voterId, VoteNIZKProofs voteNIZKProofs) throws IllegalVoteException;


}
