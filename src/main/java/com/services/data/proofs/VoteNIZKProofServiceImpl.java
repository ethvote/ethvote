package com.services.data.proofs;

import com.data.proofs.VoteNIZKProofs;
import com.enums.Phases;
import com.exceptions.DuplicateException;
import com.exceptions.IllegalVoteException;
import com.exceptions.NoSuchVotingException;
import com.exceptions.VotingClosedException;
import com.models.singlevotingvoter.SingleVotingVoterEntity;
import com.models.singlevotingvoter.SingleVotingVoterRepository;
import com.models.singlevotingvoter.VoteNIZKPEntity;
import com.models.voting.VotingEntity;
import com.models.voting.VotingsRepository;
import com.services.conversion.VoteNIZKProofsConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
@Qualifier("VoteNIZKProofService")
public class VoteNIZKProofServiceImpl implements VoteNIZKProofService {

    private final SingleVotingVoterRepository singleVotingVoterRepository;

    private final VotingsRepository votingsRepository;

    @Autowired
    public VoteNIZKProofServiceImpl(SingleVotingVoterRepository singleVotingVoterRepository,
                                    VotingsRepository votingsRepository) {
        this.singleVotingVoterRepository = singleVotingVoterRepository;
        this.votingsRepository = votingsRepository;
    }

    @Override
    public Optional<List<VoteNIZKProofs>> getNIZKProofs(long votingId) {
        Optional<VotingEntity> optVoting = votingsRepository.findOneByIdWithEligibleVotersFetched(votingId);
        if(!optVoting.isPresent()) {
            return Optional.empty();
        }

        List<VoteNIZKProofs> res = new ArrayList();

        VotingEntity votingEntity = optVoting.get();
        for(SingleVotingVoterEntity singleVotingVoterEntity : votingEntity.getAllEligibleVoters()) {
            if(!singleVotingVoterEntity.getVoted()) {
                continue;
            }

            Collection<VoteNIZKPEntity> proofs = singleVotingVoterEntity.getNonInteractiveZeroKnowledgeProof().values();
            VoteNIZKProofs voteNIZKProofs = VoteNIZKProofsConverter.toData(proofs);
            voteNIZKProofs.setFromVoterId(singleVotingVoterEntity.getUserEntity().getId());
            voteNIZKProofs.setFromVoterName(singleVotingVoterEntity.getUserEntity().getUsername());

            res.add(voteNIZKProofs);
        }
        return Optional.of(res);
    }

    @Override
    public boolean setNIZKProofs(long votingId, long voterId, VoteNIZKProofs voteNIZKProofs) throws IllegalVoteException {
        Optional<SingleVotingVoterEntity> optSingleVotingVoterEntity = singleVotingVoterRepository.findOne(voterId,votingId);
        if(!optSingleVotingVoterEntity.isPresent()) {
            throw new NoSuchVotingException("The voting could not be found.");
        }

        SingleVotingVoterEntity singleVotingVoterEntity = optSingleVotingVoterEntity.get();
        if(!singleVotingVoterEntity.getNonInteractiveZeroKnowledgeProof().isEmpty()) {
            throw new DuplicateException("You already specified the zero knowledge proofs for voting.");
        }

        if(singleVotingVoterEntity.getVotingEntity().getPhaseNumber()!=Phases.VOTING_PHASE.getNumber()) {
            throw new VotingClosedException("The voting administrator already closed the vote.");
        }

        long currentTime = System.currentTimeMillis();
        long votingEndTime = singleVotingVoterEntity.getVotingEntity().getTimeVotingIsFinished();

        if(votingEndTime != -1 && votingEndTime < currentTime) {
            throw new VotingClosedException("The time for this voting is over.");
        }

        List<VoteNIZKPEntity> voteNIZKPEntities = VoteNIZKProofsConverter.toEntity(voteNIZKProofs);

        for(VoteNIZKPEntity proofEntity : voteNIZKPEntities) {
            singleVotingVoterEntity.addNonInteractiveZeroKnowledgeProof(proofEntity);
        }

        singleVotingVoterRepository.saveAndFlush(singleVotingVoterEntity);
        return true;
    }
}
