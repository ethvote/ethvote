package com.services.data.votings;

import com.data.votingprogress.VotingProgress;
import com.data.votings.NewlyCreatedVoting;
import com.data.votings.Voting;
import com.data.votings.Votings;
import com.data.votingsforvoterwithprogress.VotingsForVoterWithProgress;
import com.exceptions.NoSuchVotingException;
import org.json.JSONObject;

import java.util.Optional;

public interface VotingsDataService {

    Votings getAllVotings();

    Votings getAllVotingsByVoterId(Long userId);

    VotingsForVoterWithProgress getAllVotingsByVoterIdAndPhaseNumber(Long id, Integer phaseNumber);

    Votings getAllVotingsByVotingAdministratorId(Long id);

    Optional<VotingProgress> getVotingProgress(Long votingId);

    Optional<Voting> addVoting(NewlyCreatedVoting voting);

    Optional<Voting> setPhaseNumber(long votingId, int phaseNumber);

    Optional<Voting> setVotingSummary(long votingId, JSONObject votingSummary, String transactionId);

    Optional<JSONObject> getVotingSummary(long votingId);

    Optional<Voting> setTimeVotingIsFinished(long votingId, long endTime);

    boolean deleteVoting(Long votingId) throws NoSuchVotingException;
}
