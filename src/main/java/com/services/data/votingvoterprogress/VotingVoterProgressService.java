package com.services.data.votingvoterprogress;

import com.data.votingvoterprogress.VotingVoterProgress;

import java.util.Optional;

public interface VotingVoterProgressService {

    Optional<VotingVoterProgress> getVotingProgressForVoter(Long votingId, Long voterId);
}
