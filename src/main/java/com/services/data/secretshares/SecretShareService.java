package com.services.data.secretshares;

import com.data.secretshares.SecretSharesForVoter;
import com.data.secretshares.SecretSharesFromVoter;
import com.exceptions.IllegalVoteException;

public interface SecretShareService {

    void addSecretShares(long votingsId, SecretSharesFromVoter shares) throws IllegalVoteException;

    SecretSharesForVoter getSharesForUser(long votingsId, long userId, String username);
}
