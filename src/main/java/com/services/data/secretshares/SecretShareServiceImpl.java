package com.services.data.secretshares;

import com.data.secretshares.SecretSharesForVoter;
import com.data.secretshares.SecretSharesFromVoter;
import com.data.secretshares.SingleSecretShareForVoter;
import com.data.secretshares.SingleSecretShareFromVoter;
import com.exceptions.DuplicateException;
import com.exceptions.IllegalVoteException;
import com.exceptions.NoSuchVotingException;
import com.models.singlevotingvoter.SecretShareEntity;
import com.models.singlevotingvoter.SingleVotingVoterEntity;
import com.models.singlevotingvoter.SingleVotingVoterRepository;
import com.services.conversion.SecretShareForVoterConverter;
import com.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Qualifier("SecretShareService")
public class SecretShareServiceImpl implements SecretShareService{

    private SingleVotingVoterRepository singleVotingVoterRepository;

    @Autowired
    public SecretShareServiceImpl(SingleVotingVoterRepository singleVotingVoterRepository) {
        this.singleVotingVoterRepository = singleVotingVoterRepository;
    }


    @Override
    public void addSecretShares(long votingsId, SecretSharesFromVoter shares) throws IllegalVoteException {
        long fromUserId = shares.getFromVoterId();
        String fromUserName = shares.getFromVoterName();

        for(SingleSecretShareFromVoter share : shares.getAllShares()) {
            long forUserId = share.getForVoterId();
            Optional<SingleVotingVoterEntity> optEntity = singleVotingVoterRepository.findOneByUserAndVotingWithSecretSharesFetched(forUserId,votingsId);
            if(!optEntity.isPresent()) {
                throw new NoSuchVotingException("The voting was not found.");
            }

            String secretShareValues = StringUtils.listToString(share.getSecretShare());
            String signature = share.getSignature();

            SecretShareEntity secretShareEntity = new SecretShareEntity()
                    .setSecretShare(secretShareValues)
                    .setFromVoterId(fromUserId)
                    .setFromVotername(fromUserName)
                    .setSignature(signature);

            SingleVotingVoterEntity singleVotingVoterEntity = optEntity.get();

            for(SecretShareEntity savedShare : singleVotingVoterEntity.getSecretShares()) {
                if(savedShare.getFromVoterId() == fromUserId) {
                    throw new DuplicateException("You already provided the secret shares.");
                }
            }

            singleVotingVoterEntity.addSecretShare(secretShareEntity);
            singleVotingVoterRepository.saveAndFlush(singleVotingVoterEntity);
        }

        Optional<SingleVotingVoterEntity> optOwnEntity = singleVotingVoterRepository.findOneByUserAndVotingWithSecretSharesFetched(fromUserId,votingsId);
        if(!optOwnEntity.isPresent()) {
            throw new NoSuchVotingException("The voting was not found.");
        }
        SingleVotingVoterEntity ownEntity = optOwnEntity.get();
        ownEntity.setCreatedSecretSharesToTrue();
        singleVotingVoterRepository.saveAndFlush(ownEntity);
    }

    @Override
    public SecretSharesForVoter getSharesForUser(long votingsId, long userId, String username) {
        Optional<SingleVotingVoterEntity> optSingleVotingVoterEntity = singleVotingVoterRepository.findOneByUserAndVotingWithSecretSharesFetched(userId,votingsId);
        if(!optSingleVotingVoterEntity.isPresent()) {
            //TODO: 31.10.2018 handle error case
            throw new UnsupportedOperationException();
        }

        SingleVotingVoterEntity singleVotingVoterEntity = optSingleVotingVoterEntity.get();
        long voterPosition = singleVotingVoterEntity.getVoterNumber();
        SecretSharesForVoter secretSharesForUser = new SecretSharesForVoter(userId,username,voterPosition);
        for(SecretShareEntity secretShareEntity : singleVotingVoterEntity.getSecretShares()) {
            SingleSecretShareForVoter singleSecretShareForVoter = SecretShareForVoterConverter.toData(secretShareEntity);
            secretSharesForUser.addSingleSecretShare(singleSecretShareForVoter);
        }
        return secretSharesForUser;
    }
}
