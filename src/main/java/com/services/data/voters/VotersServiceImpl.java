package com.services.data.voters;

import com.data.voter.SingleVoter;
import com.data.voter.Voters;
import com.exceptions.DeleteVoterException;
import com.exceptions.NoSuchVoterException;
import com.exceptions.VoterInVotingException;
import com.models.user.UserRepository;
import com.models.user.UserEntity;
import com.services.conversion.VoterConverter;
import com.services.crypto.CryptoService;
import com.utils.data.RsaKeyPair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
@Qualifier("VotersService")
public class VotersServiceImpl implements VotersService {

    private UserRepository userRepository;

    @Autowired
    public VotersServiceImpl(UserRepository keysRepository) {
        this.userRepository = keysRepository;
    }

    @Override
    public boolean createVoter(String votername) {
        if(userRepository.findOneByUsername(votername).isPresent()) {
            return false;
        }

        UserEntity userEntity = new UserEntity().setUsername(votername);
        userRepository.saveAndFlush(userEntity);

        return true;

    }

    @Override
    public Voters createVoters(Voters voters) {
        return createVoters(voters, false);
    }

    @Override
    public Voters createVoters(Voters voters, boolean generateKey) {
        List<UserEntity> userEntitiesToStore = new ArrayList<>();

        for(SingleVoter singleVoter : voters.getVoters()) {
            UserEntity userEntity = new UserEntity()
                    .setUsername(singleVoter.getName())
                    .setEmail(singleVoter.getEmail());

            if(generateKey) {
                RsaKeyPair rsaKeyPair = CryptoService.generateKey();
                userEntity.setPublicKey(rsaKeyPair.getPublicKey());
                userEntity.setPrivateKey(rsaKeyPair.getPrivateKey());
            }

            userEntitiesToStore.add(userEntity);
        }

        Iterable<UserEntity> savedEntities = userRepository.saveAll(userEntitiesToStore);
        List<SingleVoter> savedEntitiesList = new ArrayList<>();
        savedEntities.forEach(e -> savedEntitiesList.add(toData(e)));
        return new Voters().setVoters(savedEntitiesList);
    }

    @Override
    public Optional<SingleVoter> getVoter(String voterName) {
        Optional<UserEntity> optionalUserEntity = userRepository.findOneByUsername(voterName);
        if(!optionalUserEntity.isPresent()) {
            return Optional.empty();
        }
        return Optional.of(toData(optionalUserEntity.get()));
    }

    @Override
    public Optional<String> getPublicKeyOfVoter(Long voterId) {
        Optional<UserEntity> userEntity = userRepository.findOneById(voterId);
        if(userEntity.isPresent() && userEntity.get().getPublicKey()!=null) {
            return Optional.of(userEntity.get().getPublicKey());
        }

        return Optional.empty();
    }

    @Override
    public Voters getAllVoters() {
        Iterable<UserEntity> entities = userRepository.findAll();
        List<SingleVoter> allVoters = StreamSupport.stream(entities.spliterator(), false)
                .map(VoterConverter::toData)
                .collect(Collectors.toList());

        return new Voters().setVoters(allVoters);
    }

    @Override
    public Voters getAllVotersByVoternames(List<String> voternames) {
        List<UserEntity> userEntities = userRepository.findByUsernameIn(voternames);
        List<SingleVoter> voters = userEntities.stream().map(this::toData).collect(Collectors.toList());
        return new Voters().setVoters(voters);
    }

    @Override
    public boolean deleteVoter(Long voterId) throws DeleteVoterException {
       Optional<UserEntity> optUserEntity = userRepository.findOneByIdWithInvolvedVotingsFetched(voterId);
       if(!optUserEntity.isPresent()) {
           throw new NoSuchVoterException(String.format("The voter with id '%d' does not exist",voterId));
       }

       UserEntity userEntity = optUserEntity.get();
       if(!userEntity.getCreatedVotings().isEmpty()) {
           throw new VoterInVotingException("This voter has created some votings which are open and can therefore not be deleted");
       }
       if(!userEntity.getVotings().isEmpty()) {
           throw new VoterInVotingException("This voter is still involved in open votings and can therefore not be deleted");
       }

       userRepository.delete(userEntity);
       return true;
    }

    @Override
    public Optional<SingleVoter> upsertVotersData(String externalId, String voterName, String email) {
        Optional<UserEntity> optUserEntity = userRepository.findOneByExternalIdOrUsernameAndEmail(externalId, voterName, email);
        if(!optUserEntity.isPresent()) {
            return Optional.empty();
        }

        UserEntity userEntity = optUserEntity.get();
        if(userEntity.getExternalId() == null) {
            userEntity.setExternalId(externalId);
            userEntity = userRepository.saveAndFlush(userEntity);
        }

        if(!userEntity.getEmail().equals(email)) {
            userEntity.setEmail(email);
            userEntity = userRepository.saveAndFlush(userEntity);
        }

        return Optional.of(VoterConverter.toData(userEntity));
    }

    @Override
    public Optional<SingleVoter> addVoter(String voterName, String email, boolean generateKey) {
        Optional<UserEntity> optionalUserEntity = userRepository.findOneByUsernameAndEmail(voterName,email);
        if(optionalUserEntity.isPresent()) {
            return Optional.empty();
        }

        UserEntity userEntity = new UserEntity()
                .setUsername(voterName)
                .setEmail(email);

        return addVoter(userEntity,generateKey);
    }

    @Override
    public Optional<SingleVoter> addVoter(String voterName, String email, String externalId, boolean generateKey) {
        Optional<UserEntity> optionalUserEntity = userRepository.findOneByExternalIdOrUsernameAndEmail(externalId,voterName,email);
        if(optionalUserEntity.isPresent()) {
            return Optional.empty();
        }

        UserEntity userEntity = new UserEntity()
                .setUsername(voterName)
                .setEmail(email)
                .setExternalId(externalId);

        return addVoter(userEntity,generateKey);

    }

    private Optional<SingleVoter> addVoter(UserEntity userEntity, boolean generateKey) {
        if(generateKey) {
            RsaKeyPair rsaKeyPair = CryptoService.generateKey();
            userEntity.setPublicKey(rsaKeyPair.getPublicKey());
            userEntity.setPrivateKey(rsaKeyPair.getPrivateKey());
        }

        UserEntity savedEntity = userRepository.saveAndFlush(userEntity);
        return Optional.of(toData(savedEntity));
    }

    @Override
    public Voters getAllVotersWherePublicKeyIsSpecified() {
        return new Voters().setVoters(
                getAllVoters()
                .getVoters()
                .stream()
                .filter(u -> u.getPublicKey().isPresent())
                .collect(Collectors.toList())
        );
    }

    private SingleVoter toData(UserEntity userEntity) {
        return VoterConverter.toData(userEntity);
    }
}
