package com.data.votingsforvoterwithprogress;

import com.data.JsonMappable;
import com.data.votings.VotingOfficials;
import com.data.votingvoterprogress.VotingVoterProgress;
import com.data.votings.Voting;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class VotingsForVoterWithProgress implements JsonMappable {

    private List<Voting> votings;

    private List<VotingVoterProgress> votingsVoterProgress;

    private List<VotingOfficials> votingOfficials;

    public VotingsForVoterWithProgress() {
        votings = new ArrayList<>();
        votingsVoterProgress = new ArrayList<>();
        votingOfficials = new ArrayList<>();
    }

    public VotingsForVoterWithProgress(List<Voting> votings, List<VotingVoterProgress> votingsVoterProgress, List<VotingOfficials> votingOfficials) {
        this.votingsVoterProgress = votingsVoterProgress;
        this.votings = votings;
        this.votingOfficials = votingOfficials;
    }

    @Override
    public JSONObject toJson() {
        return new JSONObject()
                .put("votings", new JSONArray(votings
                        .stream()
                        .map(Voting::toJson)
                        .collect(Collectors.toList())))
                .put("votingsVoterProgress",new JSONArray(votingsVoterProgress
                        .stream()
                        .map(VotingVoterProgress::toJson)
                        .collect(Collectors.toList())))
                .put("votingOfficials",new JSONArray(votingOfficials
                        .stream()
                        .map(VotingOfficials::toJson)
                        .collect(Collectors.toList())));
    }
}
