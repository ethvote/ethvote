package com.data.dataforkeyderivation;

import com.data.JsonMappable;
import com.data.votings.EligibleVoter;
import com.data.votings.EligibleVoterWithKey;
import com.data.votings.VotingCalculationParameters;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class DataForKeyDerivation implements JsonMappable {

    private List<EligibleVoterWithKey> eligibleVoters;

    private VotingCalculationParameters votingCalculationParameters;

    private long securityThreshold;

    private long numberOfEligibleVoters;

    private long votingAdministratorId;

    public DataForKeyDerivation() {
        eligibleVoters = new ArrayList<>();
    }

    public DataForKeyDerivation addEligibleVoter(EligibleVoterWithKey eligibleVoters) {
        this.eligibleVoters.add(eligibleVoters);
        return this;
    }

    public List<EligibleVoterWithKey> getEligibleVoters() {
        return this.eligibleVoters;
    }

    public DataForKeyDerivation setVotingCalculationParameters(VotingCalculationParameters votingCalculationParameters) {
        this.votingCalculationParameters = votingCalculationParameters;
        return this;
    }

    public DataForKeyDerivation setSecurityThreshold(long securityThreshold) {
        this.securityThreshold = securityThreshold;
        return this;
    }

    public DataForKeyDerivation setNumberOfEligibleVoters(long numberOfEligibleVoters) {
        this.numberOfEligibleVoters = numberOfEligibleVoters;
        return this;
    }

    public boolean containsVoterId(Long id) {
        return eligibleVoters.stream()
                .map(EligibleVoter::getId)
                .collect(Collectors.toList())
                .contains(id);
    }

    public long getVotingAdministratorId() {
        return votingAdministratorId;
    }

    public DataForKeyDerivation setVotingAdministratorId(long votingAdministratorId) {
        this.votingAdministratorId = votingAdministratorId;
        return this;
    }

    @Override
    public JSONObject toJson() {
        return new JSONObject()
                .put("numberOfEligibleVoters",numberOfEligibleVoters)
                .put("securityThreshold",securityThreshold)
                .put("calculationParameters",votingCalculationParameters.toJson())
                .put("eligibleVoters", new JSONArray(
                        eligibleVoters.stream()
                                .map(EligibleVoterWithKey::toJson)
                                .collect(Collectors.toList())
                ));
    }
}