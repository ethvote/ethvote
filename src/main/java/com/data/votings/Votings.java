package com.data.votings;

import com.data.JsonMappable;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class Votings implements JsonMappable {

    private List<Voting> votingsList;

    public Votings() {
        votingsList = new LinkedList<>();
    }

    public Votings addVoting(Voting voting) {
        this.votingsList.add(voting);
        return this;
    }

    public List<Voting> getVotings() {
        return this.votingsList;
    }

    @Override
    public JSONObject toJson() {
        return new JSONObject().put("votings",new JSONArray(votingsList
                .stream()
                .map(Voting::toJson)
                .collect(Collectors.toList())));
    }
}
