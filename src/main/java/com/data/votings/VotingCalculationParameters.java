package com.data.votings;

import com.data.JsonMappable;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.json.JSONObject;

public class VotingCalculationParameters implements JsonMappable {

    @JsonProperty("primeP")
    private String primeP;

    @JsonProperty("primeQ")
    private String primeQ;

    @JsonProperty("generatorG")
    private String generatorG;

    public String getPrimeP() {
        return primeP;
    }

    public VotingCalculationParameters setPrimeP(String primeP) {
        this.primeP = primeP;
        return this;
    }

    public String getPrimeQ() {
        return primeQ;
    }

    public VotingCalculationParameters setPrimeQ(String primeQ) {
        this.primeQ = primeQ;
        return this;
    }

    public String getGeneratorG() {
        return generatorG;
    }

    public VotingCalculationParameters setGeneratorG(String generatorG) {
        this.generatorG = generatorG;
        return this;
    }

    @Override
    public JSONObject toJson() {

        return new JSONObject()
                .put("primeP",this.primeP)
                .put("primeQ",this.primeQ)
                .put("generatorG",this.generatorG);
    }

}


