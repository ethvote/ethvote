package com.data.votings;

import com.data.JsonMappable;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class Voting<T extends Voting> implements JsonMappable {

    private long id;

    @JsonProperty("title")
    private String title;

    @JsonProperty("description")
    private String description;

    @JsonProperty("options")
    private List<VotingOption> votingsOptions;

    private int phase;

    private long timeVotingIsFinished = -1;

    private String transactionId;

    private String votingsSummary;


    public Voting() {
        votingsOptions = new LinkedList<>();
    }

    public T setId(Long id) {
        this.id = id;
        return (T)this;
    }

    public Long getId() {
        return this.id;
    }

    public T setTitle(String title) {
        this.title = title;
        return (T)this;
    }

    public String getTitle() {
        return this.title;
    }

    public T setDescription(String description) {
        this.description = description;
        return (T)this;
    }

    public String getDescription() {
        return this.description;
    }

    public T addVotingOption(VotingOption option) {
        votingsOptions.add(option);
        return (T)this;
    }

    public List<VotingOption> getVotingOptions() {
        return this.votingsOptions;
    }

    public int getPhase() {
        return phase;
    }

    public T setPhase(int phase) {
        this.phase = phase;
        return (T)this;
    }

    public long getTimeVotingIsFinished() {
        return timeVotingIsFinished;
    }

    public T setTimeVotingIsFinished(long timeVotingIsFinished) {
        this.timeVotingIsFinished = timeVotingIsFinished;
        return (T)this;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public T setTransactionId(String transactionId) {
        this.transactionId = transactionId;
        return (T)this;
    }

    public String getVotingsSummary() {
        return votingsSummary;
    }

    public T setVotingsSummary(String votingsSummary) {
        this.votingsSummary = votingsSummary;
        return (T)this;
    }

    @Override
    public JSONObject toJson() {
        return new JSONObject()
                .put("id",id)
                .put("title",getTitle())
                .put("description",getDescription())
                .put("phase",getPhase())
                .put("timeVotingIsFinished",getTimeVotingIsFinished())
                .put("options",new JSONArray(
                        votingsOptions.stream()
                        .map(VotingOption::toJson)
                        .collect(Collectors.toList())
                ))
                .put("transactionId",transactionId)
                .put("votingsSummary",votingsSummary);
    }
}
