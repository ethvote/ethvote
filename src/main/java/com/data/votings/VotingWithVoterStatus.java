package com.data.votings;

import org.json.JSONObject;

public class VotingWithVoterStatus extends Voting<VotingWithVoterStatus> {

    private boolean createdSecretShares = false;

    private boolean voted = false;

    public VotingWithVoterStatus setCreatedSecretShares(boolean createdSecretShares) {
        this.createdSecretShares = createdSecretShares;
        return this;
    }

    public VotingWithVoterStatus setVoted(boolean voted) {
        this.voted = voted;
        return this;
    }

    @Override
    public JSONObject toJson() {
        return super.toJson()
                .put("createdSecretShares",createdSecretShares)
                .put("voted",voted);
    }
}
