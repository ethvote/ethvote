package com.data.secretshares;

import com.data.JsonMappable;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SecretSharesForVoter implements JsonMappable {

    private final long forVoterId;

    private final String forVoterName;

    private final long forVoterPosition;

    private List<SingleSecretShareForVoter> allShares;

    public SecretSharesForVoter(long forUserId, String forUsername, long forVoterPosition) {
        this.forVoterId = forUserId;
        this.forVoterName = forUsername;
        this.forVoterPosition = forVoterPosition;
        allShares = new ArrayList<>();
    }

    public long getForVoterId() {
        return forVoterId;
    }

    public String getForVoterName() {
        return forVoterName;
    }

    public SecretSharesForVoter addSingleSecretShare(SingleSecretShareForVoter share) {
        allShares.add(share);
        return this;
    }

    public long getForVoterPosition() {
        return forVoterPosition;
    }

    public List<SingleSecretShareForVoter> getAllShares() {
        return allShares;
    }

    @Override
    public JSONObject toJson() {
        return new JSONObject()
                .put("forVoterId", forVoterId)
                .put("forVoterName", forVoterName)
                .put("forVoterPosition", forVoterPosition)
                .put("secretShares",new JSONArray(
                        allShares.stream()
                                .map(SingleSecretShareForVoter::toJson)
                                .collect(Collectors.toList())
                ));
    }
}
