package com.data.voter;

import com.data.JsonMappable;
import com.data.roles.Roles;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.json.JSONObject;

import java.util.Optional;

public class SingleVoter implements JsonMappable {

    private Long id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("email")
    private String email;

    private long numberOfVotingWithTrusteeRole;

    private Optional<String> publicKey = Optional.empty();

    private Roles roles = new Roles();

    private Optional<String> uniqueId = Optional.empty();

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {return email; }

    public Optional<String> getPublicKey() {
        return publicKey;
    }

    public Roles getRoles() {
        return roles;
    }

    public Optional<String> getUniqueId() {
        return  uniqueId;
    }

    public long getNumberOfVotingsWithTrusteeRole() {
        return numberOfVotingWithTrusteeRole;
    }

    public SingleVoter setRoles(Roles roles) {
        this.roles = roles;
        return this;
    }

    public SingleVoter setPublicKey(String publicKey) {
        this.publicKey = Optional.of(publicKey);
        return this;
    }

    public SingleVoter setId(Long id) {
        this.id = id;
        return this;
    }

    public SingleVoter setName(String name) {
        this.name = name;
        return this;
    }

    public SingleVoter setEmail(String email) {
        this.email = email;
        return this;
    }

    public SingleVoter setUniqueId(String id) {
        this.uniqueId = Optional.of(id);
        return this;
    }

    public SingleVoter setNumberOfVotingsWithTrusteeRole(long numberOfVotingWithTrusteeRole) {
        this.numberOfVotingWithTrusteeRole = numberOfVotingWithTrusteeRole;
        return this;
    }


    @Override
    public JSONObject toJson() {
        JSONObject jsonObject = new JSONObject()
                .put("id",id)
                .put("name",name);

        if(publicKey.isPresent()) {
            jsonObject.put("publicKey", publicKey.get());
        }

        return jsonObject;
    }

    public JSONObject toJsonWithRoles() {
        return toJson()
            .put("roles",roles.toJson().get("roles"));
    }
}
