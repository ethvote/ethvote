package com.data.proofs;

import com.data.JsonMappable;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.json.JSONObject;

public class DecryptionNIZKProof implements JsonMappable {

    @JsonProperty("a")
    private String a;

    @JsonProperty("b")
    private String b;

    @JsonProperty("r")
    private String r;

    @JsonProperty("signature")
    private String signature;

    public DecryptionNIZKProof setA(String a) {
        this.a = a;
        return this;
    }

    public DecryptionNIZKProof setB(String b) {
        this.b = b;
        return this;
    }

    public DecryptionNIZKProof setR(String r) {
        this.r = r;
        return this;
    }

    public DecryptionNIZKProof setSignature(String signature) {
        this.signature = signature;
        return this;
    }

    public String getA() {
        return a;
    }

    public String getB() {
        return b;
    }

    public String getR() {
        return r;
    }

    public String getSignature() {
        return signature;
    }

    @Override
    public JSONObject toJson() {
        return new JSONObject()
                .put("a",a)
                .put("b",b)
                .put("r",r)
                .put("signature",signature);
    }
}
