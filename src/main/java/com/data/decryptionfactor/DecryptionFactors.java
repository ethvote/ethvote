package com.data.decryptionfactor;

import com.data.JsonMappable;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class DecryptionFactors implements JsonMappable {

    private List<SingleDecryptionFactor> decryptionFactorList = new ArrayList<>();

    public DecryptionFactors addDecryptionFactor(SingleDecryptionFactor singleDecryptionFactor) {
        decryptionFactorList.add(singleDecryptionFactor);
        return this;
    }

    public List<SingleDecryptionFactor> getAllDecryptionFactors() {
        return decryptionFactorList;
    }

    @Override
    public JSONObject toJson() {
        return new JSONObject()
                .put("decryptionFactors",new JSONArray(
                        decryptionFactorList.stream()
                                .map(SingleDecryptionFactor::toJson)
                                .collect(Collectors.toList())
                ));
    }
}
