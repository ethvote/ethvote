package com.data.commitments;

import com.data.JsonMappable;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Commitments implements JsonMappable {

    List<CommitmentFromVoter> commitmentFromVoters;

    public Commitments() {
        commitmentFromVoters = new ArrayList<>();
    }

    public Commitments addCommitmentsFromVoter(CommitmentFromVoter commitmentFromVoter) {
        this.commitmentFromVoters.add(commitmentFromVoter);
        return this;
    }

    @Override
    public JSONObject toJson() {
        return new JSONObject()
                .put("commitments",new JSONArray(
                        commitmentFromVoters.stream()
                                .map(CommitmentFromVoter::toJson)
                                .collect(Collectors.toList())
                ));
    }
}
