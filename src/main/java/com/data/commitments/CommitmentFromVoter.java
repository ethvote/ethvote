package com.data.commitments;

import com.data.JsonMappable;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CommitmentFromVoter implements JsonMappable {

    @JsonProperty("numberOfCommitments")
    private Long numberOfCommitments;

    @JsonProperty("commitments")
    private List<SingleCommitment> singleCommitments;

    private long fromVoterId;

    private String fromVoterName;

    private long fromVoterPosition;

    public CommitmentFromVoter() {
        singleCommitments = new ArrayList<>();
    }

    public Long getNumberOfCommitments() {
        return numberOfCommitments;
    }

    public void setNumberOfCommitments(long numberOfCommitments) {
        this.numberOfCommitments = numberOfCommitments;
    }

    public List<SingleCommitment> getSingleCommitmentList() {
        return singleCommitments;
    }

    public CommitmentFromVoter addCommitments(SingleCommitment commitment) {
        singleCommitments.add(commitment);
        return this;
    }

    public CommitmentFromVoter setFromVoterId(long fromVoterId) {
        this.fromVoterId = fromVoterId;
        return this;
    }

    public CommitmentFromVoter setFromVoterName(String fromVoterName) {
        this.fromVoterName = fromVoterName;
        return this;
    }

    public CommitmentFromVoter setFromVoterPosition(long fromVoterPosition) {
        this.fromVoterPosition = fromVoterPosition;
        return this;
    }

    @Override
    public JSONObject toJson() {
        return new JSONObject()
                .put("fromVoterId",fromVoterId)
                .put("fromVoterName",fromVoterName)
                .put("fromVoterPosition",fromVoterPosition)
                .put("numberOfCommitments",numberOfCommitments)
                .put("commitmentsFromVoter",new JSONArray(
                        singleCommitments.stream()
                                .map(SingleCommitment::toJson)
                                .collect(Collectors.toList())
                ));
    }
}
