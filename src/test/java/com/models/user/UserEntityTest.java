package com.models.user;

import com.models.role.RolesEntity;
import com.models.voting.VotingEntity;
import com.models.voting.VotingsRepository;
import com.models.singlevotingvoter.SingleVotingVoterEntity;
import com.models.singlevotingvoter.SingleVotingVoterRepository;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class UserEntityTest {

    private final String possibleKey =
            "-----BEGIN PUBLIC KEY-----"+
            "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjgfr/ECWA112ofXVJnvv"+
            "TeP/izroEa/ym7nczQ5Qozd3dTxsWvGDAgo9Q05zTwkTkIKAz0vnbx/Y+9I8NpJc"+
            "pWtL71ofsJ44dpMjd7Jzd94zROkd7SuHg4NGgo9DSaMQ8gMVRdhCfT4YhL4QjpZA"+
            "NuAm5s5iBTVanNveQ9lV+co/7yvTMa6M//c0i+c/cqej2Q6efh+c95/twFDDCiSX"+
            "GB/tZbHciwD5UyqcncQA9Vnrg8/tr/X6B/jN3cGMYcHHS0PRpiAeO31gBoMzFV3s"+
            "6YDgrIVMw7V80dwprGwZlb21gleyQykRnzqN25RIgkyNyunrS7czZ4NyBvqUFCMk"+
            "wwIDAQAB"+
            "-----END PUBLIC KEY---";

    private final String possiblePrivateKey =
            "-----BEGIN RSA PRIVATE KEY-----\n" +
            "MIIEowIBAAKCAQEAzX43pXt34v+ih9MuTWXNwjAn9riZNL74jVxgHoAvhOT9GbgF\n" +
            "lEdM9ZmFslOHXt69xfSXWQzs87DEisFxEBAAdv9ktt/2Sytm5If07yp811I8iaev\n" +
            "Zu2jsI9T3AdG4I/WbU0FA7kPrELBg0ncaU/P9iP8PdUraqSgGSiT0lDjjHHkO1jQ\n" +
            "nON5zXHiCjtngq7Y9MVZqQQYS/90uRjbMjnKIHCZKu9VPDI8vADe9qjkWPrLiD7Q\n" +
            "GQhThbzYx8UhdsMYCZZvgKiPRcaC3QtzuyNhgdpnzIw/j0/u0lIf6prPBgXnO8VV\n" +
            "atJT/kg3gUE2n1roBI6CUCtNJf9ZB1z1iyQ/bwIDAQABAoIBAB4kcKrUQ2p83003\n" +
            "c8LETJwP7poY9iwlA8DPNzAyKFuWL+7DrgR+jZzhrJlS+aKNCgmNTl1bG9obvfCx\n" +
            "MvxNWewam/OGmDzq0bhy8uwuxMoxf2dyZ98Gu6yMkZtO8UgZATXeMX6cI4mRRIvA\n" +
            "Bqs8mv96xCduaznE39ELlodSoyt4o/BaoBa6YI98xVVKvOBo7dCgtdPAmlRkSazq\n" +
            "JclTlCj07oIoukO5KElJOjFL9b+dxDhbD/gtXq64QyLuyOWEiCVwwT8k+/pmT+di\n" +
            "FVAiBI2a0jTIzoLmZ3OBOZZB4jOhjmOr4GFvxxS8FAgk+jblEBJSbKsukFH/dxi9\n" +
            "KZA944kCgYEA8bJWUGlE3Ga+nLhwMUsIhAHU0i5QqQwUrPk0rblyswlPWzER+PQm\n" +
            "ZGReC5yF0SWZl6fTE+sDnav/Wpap1VxlovUyZ1gYegrIQR2+7VwpYy8x6a3ffUW4\n" +
            "cNNTYEX93Rvg8btZ5CTrm/fHt33iTXB3916YGuk1l4PNVO5tf5chBCcCgYEA2adm\n" +
            "5L0gD4vHQvBaKrInUDmdg/BArt+oyfsqdUWTciIoH1p31utCkv+6xWbV5SxDvaGQ\n" +
            "8Sm1ZpDXdzEYbP8ZA3Vc0+9a6Z9K1kfSuOZ/VaGH+A5ypyi+6KOPbX+VqstDqJeY\n" +
            "GrHOiusFFHqqJ1Y9C0Sn9Mmj9rq0i6qAjlA9D3kCgYB1oW4QxKsW+YpbjAhOdU43\n" +
            "sQx7Hr9zBsYpSUAyLMd+C1Hr9/3fRDRLN+3tiMg3+iy+epdoYkDoRAWLeTg+9zEg\n" +
            "FmXplh6ThxvfVTLPIsOh2h5YtJ7IwV36y18Qw//B1QetnpkpSC7DQdyZmVLCjbJV\n" +
            "Wlx8/HNwwlz0sPXp2wvekQKBgHyu+DOa4A3nzetb0ttYmUrM5p4ZeIR1HbUvS+BI\n" +
            "TjH4vTFQ2wvkU4vwMWc6BNg25ktwjZ6JfSBXYwcluc9ex06C4f2KxXLFzNI57M39\n" +
            "RP6h/UktxaJaphqQ7+4TQ4LG1Gh1By5ro11TSuKzblwAfBHDb8+731DNHfZQYzqj\n" +
            "XodJAoGBAL0NDRcwqvq9LOO5lvoj1R3cVq/y7EPRRezlj3kFzMLfyiM4RkZ5AFvU\n" +
            "4XfE/E9t6iz1nl1noGvV7D7ZyNEbD7iALRaaBynZpiZNqfuG0G/CbZ7GckAuyInm\n" +
            "YN+L9XoN5wK6uw4fx6I5PV8Uo6ndALKf2xDGBPW7BL7TzigyirWu\n" +
            "-----END RSA PRIVATE KEY-----";

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private VotingsRepository votingsRepository;

    @Autowired
    private SingleVotingVoterRepository singleVotingVoterRepository;

    @Test
    public void savingUserWithoutKeyWorks() {
        Long numberOfVotingWithTrusteeRole = 27L;
        String userName = "user";
        UserEntity userEntity = new UserEntity()
                .setUsername(userName)
                .setNumberOfVotingsWithTrusteeRole(numberOfVotingWithTrusteeRole);

        Long id = userRepository.save(userEntity).getId();
        Optional<UserEntity> entityOptional = userRepository.findById(id);
        Assert.assertEquals(userName,entityOptional.get().getUsername());
        Assert.assertEquals(null,entityOptional.get().getPublicKey());
        Assert.assertEquals(numberOfVotingWithTrusteeRole,entityOptional.get().getNumberOfVotingsWithTrusteeRole());
    }

    @Test
    public void savingUserWithEmailWorks() {
        String userName = "user";
        String email = "user@mail";
        UserEntity userEntity = new UserEntity().setUsername(userName).setEmail(email);
        Long id = userRepository.save(userEntity).getId();
        Optional<UserEntity> entityOptional = userRepository.findById(id);
        Assert.assertEquals(userName,entityOptional.get().getUsername());
        Assert.assertEquals(email,entityOptional.get().getEmail());
        Assert.assertEquals(null,entityOptional.get().getPublicKey());
    }

    @Test
    public void savingLargeKeyWorks() {
        String userName = "user";

        UserEntity userEntity = new UserEntity().setUsername(userName);
        userRepository.save(userEntity);

        userEntity.setPublicKey(possibleKey);
        Long id = userRepository.save(userEntity).getId();

        Optional<UserEntity> entityOptional = userRepository.findById(id);
        Assert.assertEquals(userName,entityOptional.get().getUsername());
        Assert.assertEquals(possibleKey,entityOptional.get().getPublicKey());
    }

    @Test
    public void savingLargePrivateKeyWorks() {
        String userName = "user";

        UserEntity userEntity = new UserEntity().setUsername(userName);
        userRepository.save(userEntity);

        userEntity.setPrivateKey(possiblePrivateKey);
        Long id = userRepository.save(userEntity).getId();

        Optional<UserEntity> entityOptional = userRepository.findById(id);
        Assert.assertEquals(userName,entityOptional.get().getUsername());
        Assert.assertEquals(possiblePrivateKey,entityOptional.get().getPrivateKey());
    }

    @Test
    @Transactional
    public void addVotingEntityWorks() {
        VotingEntity votingEntity1 = new VotingEntity()
                .setTitle("Voting One");
        VotingEntity votingEntity2 = new VotingEntity()
                .setTitle("Voting Two");
        VotingEntity votingEntity3 = new VotingEntity()
                .setTitle("Voting Three");
        votingsRepository.save(votingEntity1);
        votingsRepository.save(votingEntity2);
        votingsRepository.save(votingEntity3);

        UserEntity userEntity = new UserEntity();
        userRepository.save(userEntity);

        SingleVotingVoterEntity voting1 = new SingleVotingVoterEntity(userEntity,votingEntity1);
        singleVotingVoterRepository.save(voting1);
        SingleVotingVoterEntity voting2 = new SingleVotingVoterEntity(userEntity,votingEntity2);
        singleVotingVoterRepository.save(voting2);
        SingleVotingVoterEntity voting3 = new SingleVotingVoterEntity(userEntity,votingEntity3);
        singleVotingVoterRepository.save(voting3);

        userEntity
                .setUsername("John Doe")
                .addVoting(voting1)
                .addVoting(voting2)
                .addVoting(voting3);
        userRepository.save(userEntity);

        Assert.assertEquals(Arrays.asList("Voting One","Voting Two","Voting Three"),userRepository
                .findOneByUsername("John Doe")
                .get()
                .getVotings()
                .stream()
                .map(s -> s.getVotingEntity().getTitle())
                .collect(Collectors.toList())
        );
    }

    @Test
    @Transactional
    public void roleCanBeStored() {
        UserEntity userEntity = new UserEntity()
                .setUsername("dummy")
                .setRoles(new RolesEntity().setAdministratorRole(true));
        Long id = userRepository.save(userEntity).getId();

        UserEntity entityAfterStorage = userRepository.findOneById(id).get();
        Assert.assertTrue(entityAfterStorage.getRoles().hasAdministratorRole());
        Assert.assertFalse(entityAfterStorage.getRoles().hasVotingAdministratorRole());
        Assert.assertFalse(entityAfterStorage.getRoles().hasRegistrarRole());
    }
}
