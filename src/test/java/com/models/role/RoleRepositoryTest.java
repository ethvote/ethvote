package com.models.role;

import com.models.user.UserEntity;
import com.models.user.UserRepository;
import org.assertj.core.util.Lists;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class RoleRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Test
    public void setRoleOfVoterNotThereReturnsFalse() {
        Long notThere = 1L;
        int updatedRows = roleRepository.updateAdminRole(notThere,true);
        Assert.assertEquals(0,updatedRows);
    }

    @Test
    public void setAdministratorRoleWorks() {
        UserEntity userEntity = new UserEntity();
        Long id = userRepository.save(userEntity).getId();
        UserEntity savedEntity = userRepository.findOneById(id).get();
        Assert.assertFalse(savedEntity.getRoles().hasAdministratorRole());
        int updatedRows = roleRepository.updateAdminRole(id,true);
        savedEntity = userRepository.findOneById(id).get();
        Assert.assertTrue(savedEntity.getRoles().hasAdministratorRole());
        Assert.assertEquals(1,updatedRows);
    }

    @Test
    public void removeAdministratorRoleWorks() {
        UserEntity userEntity = new UserEntity();
        Long id = userRepository.save(userEntity).getId();
        UserEntity savedEntity = userRepository.findOneById(id).get();
        Assert.assertFalse(savedEntity.getRoles().hasAdministratorRole());
        int updatedRows = roleRepository.updateAdminRole(id,false);
        savedEntity = userRepository.findOneById(id).get();
        Assert.assertFalse(savedEntity.getRoles().hasAdministratorRole());
        Assert.assertEquals(1,updatedRows);
    }

    @Test
    public void setVotingAdministratorRoleWorks() {
        UserEntity userEntity = new UserEntity();
        Long id = userRepository.save(userEntity).getId();
        UserEntity savedEntity = userRepository.findOneById(id).get();
        Assert.assertFalse(savedEntity.getRoles().hasVotingAdministratorRole());
        int updatedRows = roleRepository.updateVotingAdminRole(id,true);
        savedEntity = userRepository.findOneById(id).get();
        Assert.assertTrue(savedEntity.getRoles().hasVotingAdministratorRole());
        Assert.assertEquals(1,updatedRows);
    }

    @Test
    public void removeVotingAdministratorRoleWorks() {
        UserEntity userEntity = new UserEntity();
        Long id = userRepository.save(userEntity).getId();
        UserEntity savedEntity = userRepository.findOneById(id).get();
        Assert.assertFalse(savedEntity.getRoles().hasVotingAdministratorRole());
        int updatedRows = roleRepository.updateVotingAdminRole(id,false);
        savedEntity = userRepository.findOneById(id).get();
        Assert.assertFalse(savedEntity.getRoles().hasVotingAdministratorRole());
        Assert.assertEquals(1,updatedRows);
    }

    @Test
    public void setRegistrarRoleWorks() {
        UserEntity userEntity = new UserEntity();
        Long id = userRepository.save(userEntity).getId();
        UserEntity savedEntity = userRepository.findOneById(id).get();
        Assert.assertFalse(savedEntity.getRoles().hasRegistrarRole());
        int updatedRows = roleRepository.updateRegistrarRoleRole(id,true);
        savedEntity = userRepository.findOneById(id).get();
        Assert.assertTrue(savedEntity.getRoles().hasRegistrarRole());
        Assert.assertEquals(1,updatedRows);
    }

    @Test
    public void removeRegistrarRoleWorks() {
        UserEntity userEntity = new UserEntity();
        Long id = userRepository.save(userEntity).getId();
        UserEntity savedEntity = userRepository.findOneById(id).get();
        Assert.assertFalse(savedEntity.getRoles().hasRegistrarRole());
        int updatedRows = roleRepository.updateRegistrarRoleRole(id,false);
        savedEntity = userRepository.findOneById(id).get();
        Assert.assertFalse(savedEntity.getRoles().hasRegistrarRole());
        Assert.assertEquals(1,updatedRows);

    }
}
