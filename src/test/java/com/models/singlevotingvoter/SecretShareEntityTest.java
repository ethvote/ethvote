package com.models.singlevotingvoter;

import com.models.voting.VotingEntity;
import com.models.voting.VotingsRepository;
import com.models.user.UserEntity;
import com.models.user.UserRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class SecretShareEntityTest {

    @Autowired
    SingleVotingVoterRepository votingVoterRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    VotingsRepository votingsRepository;

    @Test
    public void savingLargeDataWorks() {
        UserEntity userEntity = new UserEntity();
        VotingEntity votingEntity = new VotingEntity();

        long userId = userRepository.save(userEntity).getId();
        long votingId = votingsRepository.save(votingEntity).getId();
        String secretShare = "156145153415645311864844231489484823123416464564132121864564123184864351556464894364818646453527156417614989279647984748914718922784115614515341564531186484423148948482312341646456413212186456412318486435155646489436481864645352715641761498927964798474891471892278411561451534156453118648442314894848231234164645641321218645641231848643515564648943648186464535271564176149892796479847489147189227841156145153415645311864844231489484823123416464564132121864564123184864351556464894364818646453527156417614989279647984748914718922784115614515341564531186484423148948482312341646456413212186456412318486435155646489436481864645352715641761498927964798474891471892278411561451534156453118648442314894848231234164645641321218645641231848643515564648943648186464535271564176149892796479847489147189227841156145153415645311864844231489484823123416464564132121864564123184864351556464894364818646453527156417614989279647984748914718922784115614515341564531186484423148948482312341646456413212186456412318486435155646489436481864645352715641761498927964798474891471892278411561451534156453118648442314894848231234164645641321218645641231848643515564648943648186464535271564176149892796479847489147189227841";
        String signature = "MxOGqShzqQcIaoeMWVTuepBTQzJNWkr9qfFYNThz7QhUFEqlLdhxjzBWliKIMS9wv6n2jlor4rpInabLfco+N2REXREMCX2QjLz/hAQW459++lMltpD748hCZHE5smB4yDsghNgeTcYSzj+1rwmQSdyo+omz14JDlHWByp4xFRoyHauuNpCRY4ll2yToNdFU7mBGvqDmoXpK3St+jWPVXMqe5IAmQKjQTGUUBw+YNzY4Kr6qVoT3XXdEIcwu/q0gwoKj0Cmwi2AS7NgjSptZVsmz3pkAmRHlFrRkB6HmTIBW+3f+zw6CAc994KqTr+feoPdkGZDvqKF4T4coC4U+9A==";
        Long fromVoterId = 007L;
        String fromVoterName = "James Bond";
        SingleVotingVoterEntity entity = new SingleVotingVoterEntity(userEntity,votingEntity)
                .addSecretShare(new SecretShareEntity().setSecretShare(secretShare).setFromVoterId(fromVoterId).setFromVotername(fromVoterName).setSignature(signature));

        votingVoterRepository.save(entity);

        Optional<SingleVotingVoterEntity> optEntity = votingVoterRepository.findOneByUserAndVotingWithSecretSharesFetched(userId,votingId);
        Assert.assertEquals(secretShare,optEntity.get().getSecretShares().get(0).getSecretShare());
        Assert.assertEquals(fromVoterId,optEntity.get().getSecretShares().get(0).getFromVoterId());
        Assert.assertEquals(fromVoterName,optEntity.get().getSecretShares().get(0).getFromVotername());
        Assert.assertEquals(signature,optEntity.get().getSecretShares().get(0).getSignature());
    }
}
