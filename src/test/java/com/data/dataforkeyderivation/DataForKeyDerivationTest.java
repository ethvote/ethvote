package com.data.dataforkeyderivation;

import com.data.votings.EligibleVoterWithKey;
import com.data.votings.VotingCalculationParameters;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.when;

public class DataForKeyDerivationTest {

    @Test
    public void returnValidJsonObjectIfKeyIsThere() {
        Long securityThreshold = 27L;
        Long numberOfEligibleVoters = 3L;

        JSONObject fakeVotingParameterObject = new JSONObject();
        VotingCalculationParameters parameters = Mockito.mock(VotingCalculationParameters.class);
        when(parameters.toJson()).thenReturn(fakeVotingParameterObject);

        DataForKeyDerivation dataForKeyDerivation = new DataForKeyDerivation()
                .setNumberOfEligibleVoters(numberOfEligibleVoters)
                .setSecurityThreshold(securityThreshold)
                .setVotingCalculationParameters(parameters)
                .addEligibleVoter(new EligibleVoterWithKey())
                .addEligibleVoter(new EligibleVoterWithKey())
                .addEligibleVoter(new EligibleVoterWithKey());

        JSONObject object = dataForKeyDerivation.toJson();
        Assert.assertEquals(3,object.getJSONArray("eligibleVoters").length());
        Assert.assertEquals(securityThreshold,Long.valueOf(object.getLong("securityThreshold")));
        Assert.assertEquals(fakeVotingParameterObject,object.getJSONObject("calculationParameters"));
        Assert.assertEquals(numberOfEligibleVoters,Long.valueOf(object.getLong("numberOfEligibleVoters")));

    }

    @Test
    public void containsVoterQueryWorks() {
        Long existingId = 1L;
        Long notExistingId = 27L;

        DataForKeyDerivation dataForKeyDerivation = new DataForKeyDerivation()
                .addEligibleVoter(new EligibleVoterWithKey().setId(0L))
                .addEligibleVoter(new EligibleVoterWithKey().setId(existingId))
                .addEligibleVoter(new EligibleVoterWithKey().setId(2L));

        Assert.assertTrue(dataForKeyDerivation.containsVoterId(existingId));
        Assert.assertFalse(dataForKeyDerivation.containsVoterId(notExistingId));
    }
}

