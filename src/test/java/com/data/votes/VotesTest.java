package com.data.votes;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;


public class VotesTest {

    @Test
    public void returnValidJsonObject() {
        SingleVote singleVote1 = new SingleVote().setAlpha("alpha1");
        SingleVote singleVote2 = new SingleVote().setAlpha("alpha2");

        Votes votes = new Votes()
                .addVote(singleVote1)
                .addVote(singleVote2);

        JSONObject object = votes.toJson();
        Assert.assertEquals(2,object.getLong("numberOfVotes"));
        Assert.assertEquals(2,object.getJSONArray("votes").length());
        Assert.assertEquals("alpha1",object.getJSONArray("votes").getJSONObject(0).get("alpha"));
        Assert.assertEquals("alpha2",object.getJSONArray("votes").getJSONObject(1).get("alpha"));
    }

}
