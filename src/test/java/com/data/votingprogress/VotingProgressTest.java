package com.data.votingprogress;

import com.data.voter.SingleVoter;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

public class VotingProgressTest {

    @Test
    public void returnValidJsonObject() {
        SingleVoterProgress singleVoterProgress1 = new SingleVoterProgress()
                .setVoter(new SingleVoter().setName("foo"))
                .setPosition(1L)
                .setIsTrustee(false)
                .setCanVote(false);
        SingleVoterProgress singleVoterProgress2 = new SingleVoterProgress()
                .setVoter(new SingleVoter().setName("bar"))
                .setPosition(2L)
                .setIsTrustee(true)
                .setCanVote(true);
        SingleVoterProgress singleVoterProgress3 = new SingleVoterProgress()
                .setVoter(new SingleVoter().setName("baz"))
                .setPosition(3L)
                .setIsTrustee(false)
                .setCanVote(true);

        VotingProgress votingProgress = new VotingProgress()
                .setVotersProgressList(Arrays.asList(singleVoterProgress1,singleVoterProgress2,singleVoterProgress3))
                .setPhaseNumber(73)
                .setTransactionId("simpleTransactionId");

        Assert.assertEquals("{\"phaseNumber\":73,\"votersProgress\":[{\"participatedInDecryption\":false,\"participatedInVoting\":false,\"usesCustomKey\":false,\"voterName\":\"foo\",\"position\":1,\"isTrustee\":false,\"participatedInKeyGeneration\":false,\"canVote\":false},{\"participatedInDecryption\":false,\"participatedInVoting\":false,\"usesCustomKey\":false,\"voterName\":\"bar\",\"position\":2,\"isTrustee\":true,\"participatedInKeyGeneration\":false,\"canVote\":true},{\"participatedInDecryption\":false,\"participatedInVoting\":false,\"usesCustomKey\":false,\"voterName\":\"baz\",\"position\":3,\"isTrustee\":false,\"participatedInKeyGeneration\":false,\"canVote\":true}],\"transactionId\":\"simpleTransactionId\"}",votingProgress.toJson().toString());
    }
}
