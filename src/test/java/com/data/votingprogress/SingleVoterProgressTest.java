package com.data.votingprogress;

import com.data.voter.SingleVoter;
import org.junit.Assert;
import org.junit.Test;

public class SingleVoterProgressTest {

    @Test
    public void returnValidJsonObject() {
        SingleVoterProgress singleVoterProgress = new SingleVoterProgress()
                .setPosition(42L)
                .setVoter(new SingleVoter().setName("foo"))
                .setParticipatedInKeyGeneration(true)
                .setParticipatedInVoting(false)
                .setParticipatedInDecryption(false)
                .setIsTrustee(true)
                .setCanVote(true)
                .setCreatedCustomKey(true);

        Assert.assertEquals("{\"participatedInDecryption\":false,\"participatedInVoting\":false,\"usesCustomKey\":true,\"voterName\":\"foo\",\"position\":42,\"isTrustee\":true,\"participatedInKeyGeneration\":true,\"canVote\":true}",singleVoterProgress.toJson().toString());
    }
}
