package com.data.votings;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

public class NewlyCreatedVotingTest {

    @Test
    public void returnValidJsonObject() {
        long id = 27;
        String title = "title";
        String description = "desc";
        int securityThreshold = 27;
        int numberOfEligibleVoters = 42;
        String op1Name = "foo";
        String op2Name = "bar";
        String op3Name = "baz";
        String username1 = "username1";
        String username2 = "username2";
        String username3 = "username3";
        String primeP = "31";
        String primeQ = "37";
        String generatorG = "2";
        int phase = 0;

        VotingOption op1 = new VotingOption().setOptionName(op1Name);
        VotingOption op2 = new VotingOption().setOptionName(op2Name);
        VotingOption op3 = new VotingOption().setOptionName(op3Name);

        EligibleVoter p1 = new EligibleVoter().setName(username1);
        EligibleVoter p2 = new EligibleVoter().setName(username2);
        EligibleVoter p3 = new EligibleVoter().setName(username3);

        VotingCalculationParameters votingCalculationParameters = new VotingCalculationParameters()
                .setPrimeP(primeP)
                .setPrimeQ(primeQ)
                .setGeneratorG(generatorG);

        NewlyCreatedVoting voting = new NewlyCreatedVoting()
                .setId(id)
                .setTitle(title)
                .setDescription(description)
                .setSecurityThreshold(27)
                .setNumberOfEligibleVoters(42)
                .setPhase(phase)
                .setVotingCalculationParameters(votingCalculationParameters)
                .addVotingOption(op1)
                .addVotingOption(op2)
                .addVotingOption(op3)
                .addParticipatingVoter(p1)
                .addParticipatingVoter(p2)
                .addParticipatingVoter(p3);

        JSONObject object = voting.toJson();
        JSONArray options = object.getJSONArray("options");
        JSONArray keys = object.getJSONArray("eligibleVoters");
        JSONObject calcParameters = object.getJSONObject("calculationParameters");

        Assert.assertEquals(id,object.getInt("id"));
        Assert.assertEquals(title,object.getString("title"));
        Assert.assertEquals(description,object.getString("description"));
        Assert.assertEquals(numberOfEligibleVoters,object.getInt("numberOfEligibleVoters"));
        Assert.assertEquals(securityThreshold,object.getInt("securityThreshold"));
        Assert.assertEquals(phase,object.getInt("phase"));
        Assert.assertEquals(op1Name,options.getJSONObject(0).getString("optionName"));
        Assert.assertEquals(op2Name,options.getJSONObject(1).getString("optionName"));
        Assert.assertEquals(op3Name,options.getJSONObject(2).getString("optionName"));
        Assert.assertEquals(username1,keys.getJSONObject(0).getString("name"));
        Assert.assertEquals(username2,keys.getJSONObject(1).getString("name"));
        Assert.assertEquals(username3,keys.getJSONObject(2).getString("name"));

        Assert.assertEquals(primeP,calcParameters.getString("primeP"));
        Assert.assertEquals(primeQ,calcParameters.getString("primeQ"));
        Assert.assertEquals(generatorG,calcParameters.getString("generatorG"));
    }
}
