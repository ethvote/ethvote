package com.data.votings;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

public class VotingOfficialsTest {

    @Test
    public void returnValidJsonObject() {
        Long votingId = 27L;
        String votingAdminName = "votingAdmin";
        String trusteeOne = "trustOne";
        String trusteeTwo = "trustTwo";
        String trusteeThree = "trustThree";

        VotingOfficials votingOfficials = new VotingOfficials()
                .setVotingId(votingId)
                .setVotingAdministrator(votingAdminName)
                .addTrustee(trusteeOne)
                .addTrustee(trusteeTwo)
                .addTrustee(trusteeThree);

        JSONObject jsonObject = votingOfficials.toJson();

        Assert.assertEquals(votingId.longValue(),jsonObject.getLong("votingId"));
        Assert.assertEquals(votingAdminName,jsonObject.getString("votingAdministrator"));
        Assert.assertEquals(trusteeOne,jsonObject.getJSONArray("trustees").getString(0));
        Assert.assertEquals(trusteeTwo,jsonObject.getJSONArray("trustees").getString(1));
        Assert.assertEquals(trusteeThree,jsonObject.getJSONArray("trustees").getString(2));
    }
}
