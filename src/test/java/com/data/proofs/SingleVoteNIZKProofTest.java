package com.data.proofs;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

public class SingleVoteNIZKProofTest {

    @Test
    public void returnValidJsonObject() {
        Integer messageIndex = 27;
        String a = "a";
        String b = "b";
        String c = "c";
        String r = "r";

        SingleVoteNIZKProof proof = new SingleVoteNIZKProof()
                .setMessageIndex(messageIndex)
                .setA(a)
                .setB(b)
                .setC(c)
                .setR(r);

        JSONObject jsonObject = proof.toJson();

        Assert.assertEquals(messageIndex,jsonObject.get("messageIndex"));
        Assert.assertEquals(a,jsonObject.get("a"));
        Assert.assertEquals(b,jsonObject.get("b"));
        Assert.assertEquals(c,jsonObject.get("c"));
        Assert.assertEquals(r,jsonObject.get("r"));
    }
}
