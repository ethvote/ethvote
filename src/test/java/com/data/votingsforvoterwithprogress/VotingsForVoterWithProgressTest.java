package com.data.votingsforvoterwithprogress;

import com.data.votings.VotingOfficials;
import com.data.votingvoterprogress.VotingVoterProgress;
import com.data.votings.Voting;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class VotingsForVoterWithProgressTest {

    @Test
    public void returnValidJsonObject() {
        String title = "special title";
        Long votingId = 27L;

        List<Voting> votingList = Arrays.asList(new Voting().setTitle(title));
        List<VotingVoterProgress> votingsVoterProgress = Arrays.asList(new VotingVoterProgress().setVotingId(votingId));
        List<VotingOfficials> votingOfficialsList = Arrays.asList();

        VotingsForVoterWithProgress votingsForVoterWithProgress = new VotingsForVoterWithProgress(votingList, votingsVoterProgress,votingOfficialsList);
        JSONObject jsonObject = votingsForVoterWithProgress.toJson();

        JSONArray votingJsonArray = jsonObject.getJSONArray("votings");
        JSONArray votingProgressJsonArray = jsonObject.getJSONArray("votingsVoterProgress");

        Assert.assertEquals(1,votingJsonArray.length());
        Assert.assertEquals(1,votingProgressJsonArray.length());

        Assert.assertEquals(title,votingJsonArray.getJSONObject(0).get("title"));
        Assert.assertEquals(votingId,votingProgressJsonArray.getJSONObject(0).get("votingId"));
    }
}
