package com.data.roles;

import com.enums.Role;
import org.junit.Assert;
import org.junit.Test;

public class RolesTest {

    @Test
    public void returnValidJsonObject() {
        Roles roles = new Roles();
        roles.addRole(Role.ADMINISTRATOR);
        roles.addRole(Role.REGISTRAR);

        Assert.assertEquals("{\"roles\":[\"Administrator\",\"Registrar\"]}",roles.toJson().toString());
    }

}
