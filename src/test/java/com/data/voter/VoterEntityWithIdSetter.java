package com.data.voter;

import com.models.user.UserEntity;

public class VoterEntityWithIdSetter extends UserEntity {

    private Long id;

    public VoterEntityWithIdSetter setId(Long id) {
        this.id = id;
        return this;
    }

    @Override
    public Long getId() {
        return id;
    }
}
