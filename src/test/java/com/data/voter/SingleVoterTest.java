package com.data.voter;

import com.data.roles.Roles;
import com.enums.Role;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

public class SingleVoterTest {

    @Test
    public void returnValidJsonObjectIfKeyNotThere() {
        long id = 27;
        String name = "dummyName";

        SingleVoter voter = new SingleVoter()
                .setName(name)
                .setId(id);

        JSONObject object = voter.toJson();

        Assert.assertEquals(id,object.getLong("id"));
        Assert.assertEquals(name,object.getString("name"));
    }

    @Test
    public void returnValidJsonObjectIfKeyIsThere() {
        long id = 27;
        String name = "dummyName";
        String key = "key";

        SingleVoter voter = new SingleVoter()
                .setName(name)
                .setId(id)
                .setPublicKey(key);

        JSONObject object = voter.toJson();

        Assert.assertEquals(id,object.getLong("id"));
        Assert.assertEquals(name,object.getString("name"));
        Assert.assertEquals(key,object.getString("publicKey"));
    }

    @Test
    public void returnValidJsonWithRoles() {
        long id = 27;
        String name = "dummyName";

        Roles roles = new Roles()
                .addRole(Role.ADMINISTRATOR)
                .addRole(Role.REGISTRAR);

        SingleVoter voter = new SingleVoter().setName(name).setRoles(roles).setId(id);
        JSONObject object = voter.toJsonWithRoles();

        Assert.assertEquals(id,object.getLong("id"));
        Assert.assertEquals(name,object.getString("name"));
        Assert.assertEquals(2,object.getJSONArray("roles").length());
        Assert.assertTrue(object.getJSONArray("roles").toList().contains(Role.ADMINISTRATOR.getRoleName()));
        Assert.assertTrue(object.getJSONArray("roles").toList().contains(Role.REGISTRAR.getRoleName()));
        Assert.assertEquals("{\"roles\":[\"Administrator\",\"Registrar\"],\"name\":\"dummyName\",\"id\":27}",object.toString());
    }

}
