package com.service.crypto;

import com.services.crypto.CryptoService;
import com.utils.data.RsaKeyPair;
import org.junit.Assert;
import org.junit.Test;

public class CryptoServiceTest {

    @Test
    public void shouldGenerateKeyPair() {
        RsaKeyPair keyPair = CryptoService.generateKey();
        Assert.assertTrue(keyPair.getPublicKey().startsWith("-----BEGIN PUBLIC KEY-----"));
        Assert.assertTrue(keyPair.getPrivateKey().startsWith("-----BEGIN PRIVATE KEY-----"));
    }
}
