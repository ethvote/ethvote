package com.service.data.commitments;

import com.data.commitments.CommitmentFromVoter;
import com.data.commitments.Commitments;
import com.data.commitments.SingleCommitment;
import com.data.voter.VoterEntityWithIdSetter;
import com.enums.Phases;
import com.exceptions.IllegalVoteException;
import com.exceptions.VotingClosedException;
import com.models.voting.VotingEntity;
import com.models.voting.VotingsRepository;
import com.models.singlevotingvoter.CommitmentEntity;
import com.models.singlevotingvoter.SingleVotingVoterEntity;
import com.models.singlevotingvoter.SingleVotingVoterRepository;
import com.models.user.UserEntity;
import com.models.user.UserRepository;
import com.services.data.commitments.CommitmentService;
import com.services.data.commitments.CommitmentServiceImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class CommitmentServiceImplTest {

    private SingleVotingVoterRepository mockSingleShareRepository;

    private VotingsRepository mockVotingsRepository;

    private CommitmentService commitmentService;

    @Autowired
    private SingleVotingVoterRepository realRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private VotingsRepository votingsRepository;


    @Before
    public void setup() {
        realRepository.deleteAll();
        userRepository.deleteAll();
        votingsRepository.deleteAll();

        mockSingleShareRepository = Mockito.mock(SingleVotingVoterRepository.class);
        mockVotingsRepository = Mockito.mock(VotingsRepository.class);
        commitmentService = new CommitmentServiceImpl(mockSingleShareRepository,mockVotingsRepository);
    }

    @After
    public void tearDown() {
        realRepository.deleteAll();
        userRepository.deleteAll();
        votingsRepository.deleteAll();
    }

    @Test
    (expected = IllegalVoteException.class)
    public void throwsExceptionIfVotingNotThere() throws IllegalVoteException {
        long votingId = 42;
        long userId = 27;

        when(mockSingleShareRepository
                .findOneByUserAndVotingWithCommitmentsFetched(userId,votingId))
                .thenReturn(Optional.empty());

        commitmentService.setCommitmentsForUser(votingId,userId, new CommitmentFromVoter());
    }

    @Test
    (expected = IllegalVoteException.class)
    public void throwsExceptionIfCommitmentsAlreadyStored() throws IllegalVoteException {
        long votingId = 42;
        long userId = 27;

        UserEntity userEntity = new UserEntity();
        VotingEntity votingEntity = new VotingEntity().setPhaseNumber(Phases.KEY_DERIVATION_PHASE.getNumber());

        when(mockSingleShareRepository
                .findOneByUserAndVotingWithCommitmentsFetched(userId,votingId))
                .thenReturn(Optional.of(
                        new SingleVotingVoterEntity(userEntity,votingEntity)
                        .addCommitments(new CommitmentEntity())
                ));

        commitmentService.setCommitmentsForUser(votingId,userId, new CommitmentFromVoter());
    }

    @Test
    (expected = VotingClosedException.class)
    public void throwsExceptionKeyGenerationPhaseIsOver() throws IllegalVoteException {
        long votingId = 42;
        long userId = 27;

        UserEntity userEntity = new UserEntity();
        VotingEntity votingEntity = new VotingEntity().setPhaseNumber(Phases.VOTING_PHASE.getNumber());

        when(mockSingleShareRepository
                .findOneByUserAndVotingWithCommitmentsFetched(userId,votingId))
                .thenReturn(Optional.of(
                        new SingleVotingVoterEntity(userEntity,votingEntity)
                ));

        commitmentService.setCommitmentsForUser(votingId,userId, new CommitmentFromVoter());
    }

    @Test
    public void savingCommitmentsInvokesTheRightCalls() throws IllegalVoteException {
        long votingId = 42;
        long userId = 27;

        String commitment1 = "c1";
        String commitment2 = "c2";
        String commitment3 = "c3";

        long commitmentsNumber1 = 0;
        long commitmentsNumber2 = 1;
        long commitmentsNumber3 = 2;

        String signature1 = "signature1";
        String signature2 = "signature2";
        String signature3 = "signature3";

        SingleVotingVoterEntity e1 = Mockito.mock(SingleVotingVoterEntity.class);
        when(e1.getVotingEntity()).thenReturn(new VotingEntity().setPhaseNumber(Phases.KEY_DERIVATION_PHASE.getNumber()));

        when(mockSingleShareRepository
                .findOneByUserAndVotingWithCommitmentsFetched(userId,votingId))
                .thenReturn(Optional.of(e1));

        SingleCommitment singleCommitment1 = new SingleCommitment()
                .setCommitment(commitment1)
                .setCoefficientNumber(commitmentsNumber1)
                .setSignature(signature1);
        SingleCommitment singleCommitment2 = new SingleCommitment()
                .setCommitment(commitment2)
                .setCoefficientNumber(commitmentsNumber2)
                .setSignature(signature2);
        SingleCommitment singleCommitment3 = new SingleCommitment()
                .setCommitment(commitment3)
                .setCoefficientNumber(commitmentsNumber3)
                .setSignature(signature3);

        CommitmentFromVoter commitmentFromVoter = new CommitmentFromVoter()
                .addCommitments(singleCommitment1)
                .addCommitments(singleCommitment2)
                .addCommitments(singleCommitment3);

        commitmentService.setCommitmentsForUser(votingId,userId, commitmentFromVoter);

        ArgumentCaptor<CommitmentEntity> captor = ArgumentCaptor.forClass(CommitmentEntity.class);

        verify(e1,times(3)).addCommitments(captor.capture());

        Assert.assertEquals(Arrays.asList(commitment1,commitment2,commitment3),captor.getAllValues()
                .stream()
                .map(CommitmentEntity::getCommitment)
                .collect(Collectors.toList()));

        Assert.assertEquals(Arrays.asList(commitmentsNumber1,commitmentsNumber2,commitmentsNumber3),captor.getAllValues()
                .stream()
                .map(CommitmentEntity::getForCoefficientNumber)
                .collect(Collectors.toList()));

        Assert.assertEquals(Arrays.asList(signature1,signature2,signature3),captor.getAllValues()
                .stream()
                .map(CommitmentEntity::getSignature)
                .collect(Collectors.toList()));

        verify(mockSingleShareRepository,times(1)).saveAndFlush(e1);
    }

    @Test
    public void getCommitmentsFromUserWorks() {
        long votingId = 42;
        long userId = 007;

        String commitment1 = "c1";
        String commitment2 = "c2";
        String commitment3 = "c3";

        String signature1 = "signature1";
        String signature2 = "signature2";
        String signature3 = "signature3";

        long commitmentsNumber1 = 0;
        long commitmentsNumber2 = 1;
        long commitmentsNumber3 = 2;

        UserEntity userEntity = new VoterEntityWithIdSetter().setId(userId).setUsername("u1");
        VotingEntity votingEntity = new VotingEntity();

        SingleVotingVoterEntity entity = new SingleVotingVoterEntity(userEntity,votingEntity).setVoterNumber(0L);

        entity.addCommitments(new CommitmentEntity().setCommitment("c1").setForCoefficientNumber(commitmentsNumber1).setSignature(signature1));
        entity.addCommitments(new CommitmentEntity().setCommitment("c2").setForCoefficientNumber(commitmentsNumber2).setSignature(signature2));
        entity.addCommitments(new CommitmentEntity().setCommitment("c3").setForCoefficientNumber(commitmentsNumber3).setSignature(signature3));


        when(mockSingleShareRepository.findOneByUserAndVotingWithCommitmentsFetched(userId,votingId))
                .thenReturn(Optional.of(entity));

        CommitmentFromVoter commitmentFromVoter = commitmentService.getCommitmentsFromUser(votingId,userId);
        Assert.assertEquals(Arrays.asList(commitment1,commitment2,commitment3), commitmentFromVoter
                .getSingleCommitmentList()
                .stream()
                .map(s -> s.getCommitment())
                .collect(Collectors.toList()));

        Assert.assertEquals(Arrays.asList(commitmentsNumber1,commitmentsNumber2,commitmentsNumber3), commitmentFromVoter
                .getSingleCommitmentList()
                .stream()
                .map(s -> s.getCoefficientNumber())
                .collect(Collectors.toList()));

        Assert.assertEquals(Arrays.asList(signature1,signature2,signature3), commitmentFromVoter
                .getSingleCommitmentList()
                .stream()
                .map(s -> s.getSignature())
                .collect(Collectors.toList()));
    }

    @Test
    public void getAllCommitmentsWorks() {
        long votingId = 42;

        UserEntity userEntity1 = new VoterEntityWithIdSetter().setId(0L).setUsername("v1");
        UserEntity userEntity2 = new VoterEntityWithIdSetter().setId(1L).setUsername("v2");
        UserEntity userEntity3 = new VoterEntityWithIdSetter().setId(2L).setUsername("v3");

        VotingEntity votingEntity = new VotingEntity();

        SingleVotingVoterEntity entity1 = new SingleVotingVoterEntity(userEntity1,votingEntity).setVoterNumber(0L);
        SingleVotingVoterEntity entity2 = new SingleVotingVoterEntity(userEntity2,votingEntity).setVoterNumber(1L);
        SingleVotingVoterEntity entity3 = new SingleVotingVoterEntity(userEntity3,votingEntity).setVoterNumber(2L);

        CommitmentEntity commitmentEntity1 = new CommitmentEntity().setCommitment("c1").setForCoefficientNumber(0).setSignature("signature1");
        CommitmentEntity commitmentEntity2 = new CommitmentEntity().setCommitment("c2").setForCoefficientNumber(0).setSignature("signature2");
        CommitmentEntity commitmentEntity3 = new CommitmentEntity().setCommitment("c1").setForCoefficientNumber(1).setSignature("signature3");

        entity1.addCommitments(commitmentEntity1).setCreatedSecretSharesToTrue();
        entity3.addCommitments(commitmentEntity2).addCommitments(commitmentEntity3).setCreatedSecretSharesToTrue();

        votingEntity.addEligibleVoter(entity1).addEligibleVoter(entity2).addEligibleVoter(entity3);

        when(mockVotingsRepository.findOneByIdWithEligibleVotersWithCommitmentsFetched(votingId))
                .thenReturn(Optional.of(votingEntity));

        //userEntity2 has no secret shares specified, therefore he doesn't appear here
        Commitments commitments = commitmentService.getAllCommitmentsForVoting(votingId);
        Assert.assertEquals(
                "{\"commitments\":[{\"numberOfCommitments\":1,\"fromVoterPosition\":0,\"fromVoterId\":0,\"commitmentsFromVoter\":[{\"signature\":\"signature1\",\"commitment\":\"c1\",\"coefficientNumber\":0}],\"fromVoterName\":\"v1\"},{\"numberOfCommitments\":2,\"fromVoterPosition\":2,\"fromVoterId\":2,\"commitmentsFromVoter\":[{\"signature\":\"signature2\",\"commitment\":\"c2\",\"coefficientNumber\":0},{\"signature\":\"signature3\",\"commitment\":\"c1\",\"coefficientNumber\":1}],\"fromVoterName\":\"v3\"}]}",
                commitments.toJson().toString()
        );
    }

    @Test
    public void testWithDatabase() throws IllegalVoteException {
        String commitment1 = "c1";
        String commitment2 = "c2";
        String commitment3 = "c3";

        long commitmentsNumber1 = 0;
        long commitmentsNumber2 = 1;
        long commitmentsNumber3 = 2;

        String signature1 = "signature1";
        String signature2 = "signature2";
        String signature3 = "signature3";

        SingleCommitment singleCommitment1 = new SingleCommitment()
                .setCommitment(commitment1)
                .setCoefficientNumber(commitmentsNumber1)
                .setSignature(signature1);
        SingleCommitment singleCommitment2 = new SingleCommitment()
                .setCommitment(commitment2)
                .setCoefficientNumber(commitmentsNumber2)
                .setSignature(signature2);
        SingleCommitment singleCommitment3 = new SingleCommitment()
                .setCommitment(commitment3)
                .setCoefficientNumber(commitmentsNumber3)
                .setSignature(signature3);

        CommitmentFromVoter commitmentFromVoterToSave = new CommitmentFromVoter()
                .addCommitments(singleCommitment1)
                .addCommitments(singleCommitment2)
                .addCommitments(singleCommitment3);

        commitmentService = new CommitmentServiceImpl(realRepository,votingsRepository);

        UserEntity e1 = new UserEntity().setUsername("u1");
        long userId1 = userRepository.save(e1).getId();

        VotingEntity votingEntity = new VotingEntity().setPhaseNumber(Phases.KEY_DERIVATION_PHASE.getNumber());
        long votingsId = votingsRepository.save(votingEntity).getId();

        realRepository.save(new SingleVotingVoterEntity(e1,votingEntity).setVoterNumber(0L));

        commitmentService.setCommitmentsForUser(votingsId,userId1, commitmentFromVoterToSave);
        CommitmentFromVoter commitmentFromVoter = commitmentService.getCommitmentsFromUser(votingsId,userId1);

        Assert.assertEquals(Arrays.asList(commitment1,commitment2,commitment3), commitmentFromVoter
                .getSingleCommitmentList()
                .stream()
                .map(s -> s.getCommitment())
                .collect(Collectors.toList()));

        Assert.assertEquals(Arrays.asList(commitmentsNumber1,commitmentsNumber2,commitmentsNumber3), commitmentFromVoter
                .getSingleCommitmentList()
                .stream()
                .map(s -> s.getCoefficientNumber())
                .collect(Collectors.toList()));

        Assert.assertEquals(Arrays.asList(signature1,signature2,signature3), commitmentFromVoter
                .getSingleCommitmentList()
                .stream()
                .map(s -> s.getSignature())
                .collect(Collectors.toList()));
    }
}
