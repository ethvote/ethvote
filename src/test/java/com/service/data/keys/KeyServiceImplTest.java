package com.service.data.keys;

import com.data.voter.VoterEntityWithIdSetter;
import com.enums.Phases;
import com.exceptions.IllegalVoteException;
import com.exceptions.NoSuchVotingException;
import com.exceptions.PKIClosedException;
import com.models.singlevotingvoter.SingleVotingVoterEntity;
import com.models.singlevotingvoter.SingleVotingVoterRepository;
import com.models.user.UserEntity;
import com.models.user.UserRepository;
import com.models.voting.VotingEntity;
import com.services.data.keys.KeyService;
import com.services.data.keys.KeyServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.Optional;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class KeyServiceImplTest {

    private KeyService keyService;

    private UserRepository mockUserRepository;

    private SingleVotingVoterRepository mockSingleVotingVoterRepository;

    private Long voterId = 9L;
    private Long votingId = 27L;
    private String voterName = "dummy";
    private String voterKey = "specialKey";

    @Before
    public void setup() {
        this.mockUserRepository = Mockito.mock(UserRepository.class);
        this.mockSingleVotingVoterRepository = Mockito.mock(SingleVotingVoterRepository.class);

        this.keyService = new KeyServiceImpl(mockUserRepository,mockSingleVotingVoterRepository);
    }

    @Test
    public void getPrivateKeyReturnsEmptyIfUserNotKnown() {
        when(mockUserRepository.findOneById(voterId)).thenReturn(Optional.empty());

        Optional<String> privateKey = keyService.getPrivateKey(voterId);

        Assert.assertFalse(privateKey.isPresent());
    }

    @Test
    public void getPrivateKeyReturnsEmptyIfKeyIsNotKnown() {
        UserEntity userEntity = new UserEntity();
        when(mockUserRepository.findOneById(voterId)).thenReturn(Optional.of(userEntity));

        Optional<String> privateKey = keyService.getPrivateKey(voterId);

        Assert.assertFalse(privateKey.isPresent());
    }

    @Test
    public void getPrivateKeyWorksIfKeyIsThere() {
        String privateKeyValue = "specialKey";
        UserEntity userEntity = new UserEntity().setPrivateKey(privateKeyValue);
        when(mockUserRepository.findOneById(voterId)).thenReturn(Optional.of(userEntity));

        Optional<String> privateKey = keyService.getPrivateKey(voterId);

        Assert.assertTrue(privateKey.isPresent());
        Assert.assertEquals(privateKeyValue,privateKey.get());
    }

    @Test
    public void saveKeyWorksIfVoterThere() {
        when(mockUserRepository.findOneById(voterId)).thenReturn(Optional.of(new VoterEntityWithIdSetter().setId(voterId).setUsername(voterName).setPrivateKey("oldKey")));

        keyService.setPublicKeyOfVoter(voterId, voterKey);

        ArgumentCaptor<UserEntity> entityArgumentCaptor = ArgumentCaptor.forClass(UserEntity.class);
        verify(mockUserRepository,times(1)).saveAndFlush(entityArgumentCaptor.capture());
        Assert.assertEquals(voterName,entityArgumentCaptor.getValue().getUsername());
        Assert.assertEquals(voterKey,entityArgumentCaptor.getValue().getPublicKey());
        Assert.assertNull(entityArgumentCaptor.getValue().getPrivateKey());
    }

    @Test
    public void saveKeyReturnsFalseIfVoterNotThere() {
        when(mockUserRepository.findOneByUsername(voterName)).thenReturn(Optional.empty());
        Assert.assertFalse(keyService.setPublicKeyOfVoter(voterId, voterKey));
    }

    @Test
    (expected = PKIClosedException.class)
    public void saveKeyThrowsExceptionIfWrongPhase() throws IllegalVoteException {
        int wrongPhase = 27;
        SingleVotingVoterEntity mockSingleVotingVoterEntity = Mockito.mock(SingleVotingVoterEntity.class);
        when(mockSingleVotingVoterRepository.findOne(this.voterId,this.votingId)).thenReturn(Optional.of(mockSingleVotingVoterEntity));
        when(mockSingleVotingVoterEntity.getVotingEntity()).thenReturn(new VotingEntity().setPhaseNumber(wrongPhase));

        boolean successful = keyService.setPublicKeyOfVoterInVoting(this.voterId,this.votingId,voterKey);

        verify(mockSingleVotingVoterEntity,times(1)).setPublicKey(this.voterKey);
        verify(mockSingleVotingVoterRepository,times(1)).saveAndFlush(mockSingleVotingVoterEntity);
        Assert.assertTrue(successful);
    }

    @Test
    public void saveKeyWorksForVotingWorksIfThereAndRightPhase() throws IllegalVoteException {
        SingleVotingVoterEntity mockSingleVotingVoterEntity = Mockito.mock(SingleVotingVoterEntity.class);
        when(mockSingleVotingVoterRepository.findOne(this.voterId,this.votingId)).thenReturn(Optional.of(mockSingleVotingVoterEntity));
        when(mockSingleVotingVoterEntity.getVotingEntity()).thenReturn(new VotingEntity().setPhaseNumber(Phases.PKI_PHASE.getNumber()));
        when(mockSingleVotingVoterEntity.setPublicKey(this.voterKey)).thenReturn(mockSingleVotingVoterEntity);

        boolean successful = keyService.setPublicKeyOfVoterInVoting(this.voterId,this.votingId,voterKey);

        verify(mockSingleVotingVoterEntity,times(1)).setPublicKey(this.voterKey);
        verify(mockSingleVotingVoterEntity,times(1)).setCreatedOwnKeyToTrue();
        verify(mockSingleVotingVoterRepository,times(1)).saveAndFlush(mockSingleVotingVoterEntity);
        Assert.assertTrue(successful);
    }

    @Test
    (expected = NoSuchVotingException.class)
    public void saveKeyForVotingsReturnsFalseIfVoterNotThere() throws IllegalVoteException {
        when(mockSingleVotingVoterRepository.findOne(this.voterId,this.votingId)).thenReturn(Optional.empty());
        keyService.setPublicKeyOfVoterInVoting(this.voterId,this.votingId,voterKey);
    }

    @Test
    public void getPublicKeyForVoterInVotingReturnsEmptyIfNotThere() {
        when(mockSingleVotingVoterRepository.findOne(voterId,votingId)).thenReturn(Optional.empty());
        Assert.assertFalse(keyService.getPublicKeyOfVoterInVoting(voterId,votingId).isPresent());
    }

    @Test
    public void getPublicKeyForVoterInVotingReturnsDataIfThere() {
        String publicKey = "publicKey";
        SingleVotingVoterEntity singleVotingVoterEntity = Mockito.mock(SingleVotingVoterEntity.class);
        when(singleVotingVoterEntity.getPublicKey()).thenReturn(publicKey);
        when(mockSingleVotingVoterRepository.findOne(voterId,votingId)).thenReturn(Optional.of(singleVotingVoterEntity));

        Optional<String> optKey = keyService.getPublicKeyOfVoterInVoting(voterId,votingId);
        Assert.assertTrue(optKey.isPresent());
        Assert.assertEquals(publicKey,optKey.get());
    }
}
