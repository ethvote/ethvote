package com.service.data.secretshares;

import com.data.secretshares.SecretSharesForVoter;
import com.data.secretshares.SecretSharesFromVoter;
import com.data.secretshares.SingleSecretShareFromVoter;
import com.exceptions.DuplicateException;
import com.exceptions.IllegalVoteException;
import com.exceptions.NoSuchVotingException;
import com.models.voting.VotingEntity;
import com.models.voting.VotingsRepository;
import com.models.singlevotingvoter.SecretShareEntity;
import com.models.singlevotingvoter.SingleVotingVoterEntity;
import com.models.singlevotingvoter.SingleVotingVoterRepository;
import com.models.user.UserEntity;
import com.models.user.UserRepository;
import com.services.data.secretshares.SecretShareService;
import com.services.data.secretshares.SecretShareServiceImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class SecretShareImplServiceTest {

    private SingleVotingVoterRepository mockSingleShareRepository;

    private SecretShareService secretShareService;

    @Autowired
    private SingleVotingVoterRepository realRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private VotingsRepository votingsRepository;


    @Before
    public void setup() {
        realRepository.deleteAll();
        userRepository.deleteAll();
        votingsRepository.deleteAll();

        mockSingleShareRepository = Mockito.mock(SingleVotingVoterRepository.class);
        secretShareService = new SecretShareServiceImpl(mockSingleShareRepository);
    }

    @After
    public void tearDown() {
        realRepository.deleteAll();
        userRepository.deleteAll();
        votingsRepository.deleteAll();
    }

    @Test
    (expected = IllegalVoteException.class)
    public void savingThrowsExceptionIfVotingNotFound() throws IllegalVoteException {
        long votingsId = 27;
        long fromVoterId = 42;

        long forVoterId = 89;

        when(mockSingleShareRepository
                .findOneByUserAndVotingWithSecretSharesFetched(forVoterId,votingsId))
                .thenReturn(Optional.empty());

        secretShareService.addSecretShares(votingsId,
                new SecretSharesFromVoter().setFromVoterId(fromVoterId)
                .addSingleSecretShare(new SingleSecretShareFromVoter().setForVoterId(forVoterId))
        );
    }

    @Test
    (expected = DuplicateException.class)
    public void savingThrowsExceptionAlreadySavedShares() throws IllegalVoteException {
        long votingsId = 27;
        long fromVoterId = 42;
        long forVoterId = 89;

        SingleVotingVoterEntity e = new SingleVotingVoterEntity(new UserEntity(), new VotingEntity())
                .addSecretShare(new SecretShareEntity().setFromVoterId(fromVoterId));

        when(mockSingleShareRepository
                .findOneByUserAndVotingWithSecretSharesFetched(forVoterId,votingsId))
                .thenReturn(Optional.of(e));

        secretShareService.addSecretShares(votingsId,
                new SecretSharesFromVoter().setFromVoterId(fromVoterId)
                        .addSingleSecretShare(new SingleSecretShareFromVoter()
                                .setForVoterId(forVoterId)
                                .setSecretShare(Arrays.asList()))
        );
    }

    @Test
    (expected = NoSuchVotingException.class)
    public void savingThrowsNoExceptionIfNotAlreadyStored() throws IllegalVoteException {
        long votingsId = 27;
        long fromVoterId = 42;
        long forVoterId = 89;

        long fromOther = 1;

        SingleVotingVoterEntity e = new SingleVotingVoterEntity(new UserEntity(), new VotingEntity())
                .addSecretShare(new SecretShareEntity().setFromVoterId(fromOther));

        when(mockSingleShareRepository
                .findOneByUserAndVotingWithSecretSharesFetched(forVoterId,votingsId))
                .thenReturn(Optional.of(e));

        secretShareService.addSecretShares(votingsId,
                new SecretSharesFromVoter().setFromVoterId(fromVoterId)
                        .addSingleSecretShare(new SingleSecretShareFromVoter()
                                .setForVoterId(forVoterId)
                                .setSecretShare(Arrays.asList()))
        );
    }

    @Test
    public void savingSecretSharesInvokesTheRightCalls() throws IllegalVoteException {
        long votingsId = 42;

        SingleVotingVoterEntity ownEntity = Mockito.mock(SingleVotingVoterEntity.class);
        SingleVotingVoterEntity e2 = Mockito.mock(SingleVotingVoterEntity.class);
        SingleVotingVoterEntity e3 = Mockito.mock(SingleVotingVoterEntity.class);

        long fromUserId = 1;
        String fromUsername = "Yoda";

        long forUserId1 = 1;
        String forUserName1 = "Luke";

        long forUserId2 = 2;
        String forUserName2 = "Annakin";

        long forUserId3 = 3;
        String forUserName3 = "Lea";

        List<String> share1 = Arrays.asList("s1","s2","s3");
        List<String> share2 = Arrays.asList("s4","s5","s6");
        List<String> share3 = Arrays.asList("s7","s8","s9");

        String signature1 = "sign1";
        String signature2 = "sign2";
        String signature3 = "sign3";

        when(mockSingleShareRepository
                .findOneByUserAndVotingWithSecretSharesFetched(fromUserId,votingsId))
                .thenReturn(Optional.of(ownEntity));

        when(mockSingleShareRepository
                .findOneByUserAndVotingWithSecretSharesFetched(forUserId2,votingsId))
                .thenReturn(Optional.of(e2));

        when(mockSingleShareRepository
                .findOneByUserAndVotingWithSecretSharesFetched(forUserId3,votingsId))
                .thenReturn(Optional.of(e3));

        SecretSharesFromVoter secretSharesFromVoter = new SecretSharesFromVoter().setFromVoterId(fromUserId).setFromVoterName(fromUsername)
                .addSingleSecretShare(new SingleSecretShareFromVoter().setForVoterId(forUserId1).setForVoterName(forUserName1).setSecretShare(share1).setSignature(signature1))
                .addSingleSecretShare(new SingleSecretShareFromVoter().setForVoterId(forUserId2).setForVoterName(forUserName2).setSecretShare(share2).setSignature(signature2))
                .addSingleSecretShare(new SingleSecretShareFromVoter().setForVoterId(forUserId3).setForVoterName(forUserName3).setSecretShare(share3).setSignature(signature3));

        secretShareService.addSecretShares(votingsId, secretSharesFromVoter);

        ArgumentCaptor<SecretShareEntity> captor = ArgumentCaptor.forClass(SecretShareEntity.class);

        verify(ownEntity,times(1)).addSecretShare(captor.capture());
        verify(e2,times(1)).addSecretShare(captor.capture());
        verify(e3,times(1)).addSecretShare(captor.capture());
        Assert.assertEquals(Arrays.asList(share1.toString(),share2.toString(),share3.toString()),captor.getAllValues()
                .stream()
                .map(SecretShareEntity::getSecretShare)
                .collect(Collectors.toList()));

        Assert.assertEquals(Arrays.asList(1L,1L,1L),captor.getAllValues()
                .stream()
                .map(SecretShareEntity::getFromVoterId)
                .collect(Collectors.toList()));

        Assert.assertEquals(Arrays.asList(fromUsername,fromUsername,fromUsername),captor.getAllValues()
                .stream()
                .map(SecretShareEntity::getFromVotername)
                .collect(Collectors.toList()));

        Assert.assertEquals(Arrays.asList(signature1,signature2,signature3),captor.getAllValues()
                .stream()
                .map(SecretShareEntity::getSignature)
                .collect(Collectors.toList()));

        verify(mockSingleShareRepository,times(2)).saveAndFlush(ownEntity);
        verify(mockSingleShareRepository,times(1)).saveAndFlush(e2);
        verify(mockSingleShareRepository,times(1)).saveAndFlush(e3);
        verify(ownEntity,times(1)).setCreatedSecretSharesToTrue();
    }

    @Test
    public void getSecretSharesInvokesTheRightCalls() {
        String voter1 = "foo";
        String voter2 = "bar";
        String voter3 = "baz";

        List<String> share1 = Arrays.asList("s1","s2","s3");
        List<String> share2 = Arrays.asList("s4","s5","s6");
        List<String> share3 = Arrays.asList("s7","s8","s9");

        String signature1 = "sign1";
        String signature2 = "sign2";
        String signature3 = "sign3";

        long voterId1 = 1;
        long voterId2 = 2;
        long voterId3 = 3;

        long votingId = 42;
        String username = "James Bond";
        long userId = 007;
        long voterPosition = 27;

        UserEntity userEntity = new UserEntity();
        VotingEntity votingEntity = new VotingEntity();

        SingleVotingVoterEntity entity = new SingleVotingVoterEntity(userEntity,votingEntity);
        entity.addSecretShare(new SecretShareEntity().setFromVoterId(voterId1).setFromVotername(voter1).setSecretShare(share1.toString()).setSignature(signature1));
        entity.addSecretShare(new SecretShareEntity().setFromVoterId(voterId2).setFromVotername(voter2).setSecretShare(share2.toString()).setSignature(signature2));
        entity.addSecretShare(new SecretShareEntity().setFromVoterId(voterId3).setFromVotername(voter3).setSecretShare(share3.toString()).setSignature(signature3));
        entity.setVoterNumber(voterPosition);

        when(mockSingleShareRepository.findOneByUserAndVotingWithSecretSharesFetched(userId,votingId))
                .thenReturn(Optional.of(entity));

        SecretSharesForVoter secretSharesForUser = secretShareService.getSharesForUser(votingId,userId,username);
        Assert.assertEquals(userId,secretSharesForUser.getForVoterId());
        Assert.assertEquals(voterPosition,secretSharesForUser.getForVoterPosition());
        Assert.assertEquals(Arrays.asList(share1,share2,share3),secretSharesForUser
                .getAllShares()
                .stream()
                .map(s -> s.getSecretShare())
                .collect(Collectors.toList()));

        Assert.assertEquals(Arrays.asList(voter1,voter2,voter3),secretSharesForUser
                .getAllShares()
                .stream()
                .map(s -> s.getFromVoterName())
                .collect(Collectors.toList()));

        Assert.assertEquals(Arrays.asList(voterId1,voterId2,voterId3),secretSharesForUser
                .getAllShares()
                .stream()
                .map(s -> s.getFromVoterId())
                .collect(Collectors.toList()));

        Assert.assertEquals(Arrays.asList(voterId1,voterId2,voterId3),secretSharesForUser
                .getAllShares()
                .stream()
                .map(s -> s.getFromVoterId())
                .collect(Collectors.toList()));

        Assert.assertEquals(Arrays.asList(signature1,signature2,signature3),secretSharesForUser
                .getAllShares()
                .stream()
                .map(s -> s.getSignature())
                .collect(Collectors.toList()));
    }

    @Test
    public void testWithDatabase() throws IllegalVoteException {
        secretShareService = new SecretShareServiceImpl(realRepository);

        UserEntity e1 = new UserEntity();
        UserEntity e2 = new UserEntity();
        UserEntity e3 = new UserEntity();
        UserEntity e4 = new UserEntity();

        long userId1 = userRepository.save(e1).getId();
        long userId2 = userRepository.save(e2).getId();
        long userId3 = userRepository.save(e3).getId();
        long userId4 = userRepository.save(e4).getId();

        String signature1 = "sign1";
        String signature2 = "sign2";
        String signature3 = "sign3";

        VotingEntity votingEntity = new VotingEntity();
        long votingsId = votingsRepository.save(votingEntity).getId();

        realRepository.save(new SingleVotingVoterEntity(e1,votingEntity).setVoterNumber(0L));
        realRepository.save(new SingleVotingVoterEntity(e2,votingEntity).setVoterNumber(1L));
        realRepository.save(new SingleVotingVoterEntity(e3,votingEntity).setVoterNumber(2L));
        realRepository.save(new SingleVotingVoterEntity(e4,votingEntity).setVoterNumber(3L));

        secretShareService.addSecretShares(votingsId,new SecretSharesFromVoter().setFromVoterId(userId1).setFromVoterName("James Bond")
                .addSingleSecretShare(new SingleSecretShareFromVoter().setForVoterId(userId2).setForVoterName("foo").setSecretShare(Arrays.asList("s1","s2","s3")).setSignature(signature1))
                .addSingleSecretShare(new SingleSecretShareFromVoter().setForVoterId(userId3).setForVoterName("bar").setSecretShare(Arrays.asList("s4","s5","s6")).setSignature(signature2))
                .addSingleSecretShare(new SingleSecretShareFromVoter().setForVoterId(userId4).setForVoterName("baz").setSecretShare(Arrays.asList("s7","s8","s9")).setSignature(signature3))
        );

        SecretSharesForVoter shares = secretShareService.getSharesForUser(votingsId,userId2,"foo");
        Assert.assertEquals(1L,shares.getForVoterPosition());
        Assert.assertEquals(userId2,shares.getForVoterId());
        Assert.assertEquals("foo",shares.getForVoterName());
        Assert.assertEquals("foo",shares.getForVoterName());
        Assert.assertEquals(1,shares.getAllShares().size());
        Assert.assertEquals(Arrays.asList("s1","s2","s3"),shares.getAllShares().get(0).getSecretShare());
        Assert.assertEquals(userId1,shares.getAllShares().get(0).getFromVoterId());
        Assert.assertEquals("James Bond",shares.getAllShares().get(0).getFromVoterName());
    }

}
