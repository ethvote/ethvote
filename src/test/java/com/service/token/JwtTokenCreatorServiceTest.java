package com.service.token;

import com.configs.Params;
import com.data.roles.Roles;
import com.data.voter.SingleVoter;
import com.enums.Role;
import com.services.data.roles.RoleService;
import com.services.token.JwtTokenCoderService;
import com.services.token.JwtTokenCreatorService;
import com.services.token.JwtTokenCreatorServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.util.*;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

public class JwtTokenCreatorServiceTest {

    Long voterId = 27L;

    String voterName = "user";

    String key = "key";

    Roles roles = new Roles().addRole(Role.REGISTRAR).addRole(Role.ADMINISTRATOR);

    Long numberOfVotingsWithTrusteeRole = 42L;

    private RoleService mockRoleService;

    private JwtTokenCoderService mockJwtTokenCoderService;

    private JwtTokenCreatorService jwtTokenCreatorService;

    @Before
    public void setup() {
        mockJwtTokenCoderService = Mockito.mock(JwtTokenCoderService.class);
        mockRoleService = Mockito.mock(RoleService.class);
        jwtTokenCreatorService = new JwtTokenCreatorServiceImpl(mockJwtTokenCoderService,mockRoleService);
    }

    @Test
    public void createTokenForUserByIdWithoutKeyWorks() {
        long minExpirationTime = java.time.Instant.now().getEpochSecond() + Params.TOKEN_EXPIRATION_INTERVAL;
        SingleVoter voter = new SingleVoter()
                .setName(voterName)
                .setPublicKey(key)
                .setId(voterId)
                .setRoles(roles)
                .setNumberOfVotingsWithTrusteeRole(numberOfVotingsWithTrusteeRole);

        when(mockRoleService.getVoterWithRoles(voterId)).thenReturn(Optional.of(voter));

        ArgumentCaptor<Long> argumentCaptor = ArgumentCaptor.forClass(Long.class);
        String dummyToken = "tokenWithKey";
        when(mockJwtTokenCoderService.createToken(eq(voterId),eq(voterName),eq(key),argumentCaptor.capture(),eq(roles),eq(numberOfVotingsWithTrusteeRole))).thenReturn(dummyToken);
        String retToken = jwtTokenCreatorService.createTokenForUser(voterId).get();
        Assert.assertEquals(dummyToken,retToken);
        //Timing test with exact times are dangerous, therefore a 5 second backup is added in the test
        Assert.assertTrue(argumentCaptor.getValue().longValue()>minExpirationTime-5);
    }

    @Test
    public void createTokenForUserByIdWithKeyWorks() {
        long minExpirationTime = java.time.Instant.now().getEpochSecond() + Params.TOKEN_EXPIRATION_INTERVAL;
        SingleVoter voterWithoutKey = new SingleVoter().setId(voterId).setName(voterName);
        SingleVoter voter = new SingleVoter()
                .setRoles(roles)
                .setId(voterWithoutKey.getId())
                .setName(voterWithoutKey.getName())
                .setNumberOfVotingsWithTrusteeRole(numberOfVotingsWithTrusteeRole);

        when(mockRoleService.getVoterWithRoles(voterId)).thenReturn(Optional.of(voter));

        ArgumentCaptor<Long> argumentCaptor = ArgumentCaptor.forClass(Long.class);
        String dummyToken = "tokenWithoutKey";
        when(mockJwtTokenCoderService.createToken(eq(voterId),eq(voterName),argumentCaptor.capture(),eq(roles),eq(numberOfVotingsWithTrusteeRole))).thenReturn(dummyToken);
        String retToken = jwtTokenCreatorService.createTokenForUser(voterId).get();
        Assert.assertEquals(dummyToken,retToken);
        //Timing test with exact times are dangerous, therefore a 5 second backup is added in the test
        Assert.assertTrue(argumentCaptor.getValue().longValue()>minExpirationTime-5);
    }

    @Test
    public void createTokenForUserByIdWorks() {
        long minExpirationTime = java.time.Instant.now().getEpochSecond() + Params.TOKEN_EXPIRATION_INTERVAL;
        SingleVoter voter = new SingleVoter()
                .setRoles(roles)
                .setId(voterId)
                .setName(voterName)
                .setNumberOfVotingsWithTrusteeRole(numberOfVotingsWithTrusteeRole);

        when(mockRoleService.getVoterWithRoles(voterId)).thenReturn(Optional.of(voter));

        ArgumentCaptor<Long> argumentCaptor = ArgumentCaptor.forClass(Long.class);
        String dummyToken = "tokenWithoutKey";
        when(mockJwtTokenCoderService.createToken(eq(voterId),eq(voterName),argumentCaptor.capture(),eq(roles),eq(numberOfVotingsWithTrusteeRole))).thenReturn(dummyToken);
        String retToken = jwtTokenCreatorService.createTokenForUser(voterId).get();
        Assert.assertEquals(dummyToken,retToken);
        //Timing test with exact times are dangerous, therefore a 5 second backup is added in the test
        Assert.assertTrue(argumentCaptor.getValue().longValue()>minExpirationTime-5);
    }

    @Test
    public void createTokeForUserByUsernameWorks() {
        long minExpirationTime = java.time.Instant.now().getEpochSecond() + Params.TOKEN_EXPIRATION_INTERVAL;
        SingleVoter voter = new SingleVoter()
                .setRoles(roles)
                .setId(voterId)
                .setName(voterName)
                .setNumberOfVotingsWithTrusteeRole(numberOfVotingsWithTrusteeRole);

        when(mockRoleService.getVoterWithRoles(voterName)).thenReturn(Optional.of(voter));

        ArgumentCaptor<Long> argumentCaptor = ArgumentCaptor.forClass(Long.class);
        String dummyToken = "tokenWithoutKey";
        when(mockJwtTokenCoderService.createToken(eq(voterId),eq(voterName),argumentCaptor.capture(),eq(roles),eq(numberOfVotingsWithTrusteeRole))).thenReturn(dummyToken);
        String retToken = jwtTokenCreatorService.createTokenForUser(voterName).get();
        Assert.assertEquals(dummyToken,retToken);
        //Timing test with exact times are dangerous, therefore a 5 second backup is added in the test
        Assert.assertTrue(argumentCaptor.getValue().longValue()>minExpirationTime-5);
    }

}
