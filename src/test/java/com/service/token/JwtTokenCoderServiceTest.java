package com.service.token;

import com.data.roles.Roles;
import com.data.voter.SingleVoter;

import com.enums.Role;
import com.services.secrets.SecretsService;
import com.services.token.JwtTokenCoderService;
import com.services.token.JwtTokenCoderServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import java.util.Optional;

import static org.mockito.Mockito.when;

public class JwtTokenCoderServiceTest {

    private final String key = "veryVerySecret";

    private JwtTokenCoderService tokenService;

    private SecretsService mockSecretsService;

    private long expirationTime = 42L;

    private long numberOfVotingWithTrusteeRole = 27L;

    @Before
    public void setup() {
        mockSecretsService = Mockito.mock(SecretsService.class);
        when(mockSecretsService.getJwtSigningKey()).thenReturn(key.getBytes());
        tokenService = new JwtTokenCoderServiceImpl(mockSecretsService);
    }

    @Test
    public void rightTokenCreatedForUser() {
        Long voterId = 42L;
        String expected = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWJJZCI6IjQyIiwic3ViIjoieW9kYSIsImV4cGlyYXRpb25UaW1lIjo0MiwibnVtYmVyT2ZWb3RpbmdzV2l0aFRydXN0ZWVSb2xlIjoiMjciLCJzdWJSb2xlcyI6WyJSZWdpc3RyYXIiXX0.0GA_1PWEGkvsjqqQUPCp2L3N5g8mvBlids9Be4k3lj2cHsOlBFXYyfSpR1EXm9b29k8cQlBUbhzczOuFQPIzoQ";
        String username = "yoda";
        String returnedToken = tokenService.createToken(voterId,username,expirationTime, new Roles().addRole(Role.REGISTRAR),numberOfVotingWithTrusteeRole);
        Assert.assertEquals(expected,returnedToken);
    }

    @Test
    public void rightTokenCreatedForUserWithKey() {
        Long userId = 42L;
        String expected = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWJJZCI6IjQyIiwic3ViIjoieW9kYSIsImV4cGlyYXRpb25UaW1lIjo0MiwicHVibGljS2V5Ijoic29tZUR1bW15IiwibnVtYmVyT2ZWb3RpbmdzV2l0aFRydXN0ZWVSb2xlIjoiMjciLCJzdWJSb2xlcyI6WyJBZG1pbmlzdHJhdG9yIl19.uAnVEy2xLFJza3hNFfpddYum_yTeeLUWWY7-6KMDtI2YmLeThNEanK7XizZgdd9XSA6gUlo5ekXF2v1EDwyJrw";
        String username = "yoda";
        String publicKey = "someDummy";
        String returnedToken = tokenService.createToken(userId,username,publicKey,expirationTime, new Roles().addRole(Role.ADMINISTRATOR),numberOfVotingWithTrusteeRole);
        Assert.assertEquals(expected,returnedToken);
    }

    @Test
    public void rightTokenCreatedForUserWithMultipleRoles() {
        Long userId = 42L;
        String expected = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWJJZCI6IjQyIiwic3ViIjoieW9kYSIsImV4cGlyYXRpb25UaW1lIjo0MiwicHVibGljS2V5Ijoic29tZUR1bW15IiwibnVtYmVyT2ZWb3RpbmdzV2l0aFRydXN0ZWVSb2xlIjoiMjciLCJzdWJSb2xlcyI6WyJBZG1pbmlzdHJhdG9yIiwiUmVnaXN0cmFyIl19.w8VDLCUoayypBjM40hjzMUdkYNE3EWsVSyZO8Np_FE4hhwaFX0IZSn55ezS5sUfTtlSRjgqxYwaU0y1wFf8YVg";
        String username = "yoda";
        String publicKey = "someDummy";
        String returnedToken = tokenService.createToken(userId,username,publicKey,expirationTime, new Roles().addRole(Role.ADMINISTRATOR).addRole(Role.REGISTRAR),numberOfVotingWithTrusteeRole);
        Assert.assertEquals(expected,returnedToken);
    }

    @Test
    public void getUserFromInvalidTokenShouldReturnEmpty() {
        Long voterId = 42L;
        String username = "luke";
        String wrongKey = "notVerySecret";
        String token = tokenService.createToken(voterId,username,0L,new Roles(),numberOfVotingWithTrusteeRole);
        when(mockSecretsService.getJwtSigningKey()).thenReturn(wrongKey.getBytes());
        Optional<SingleVoter> voter = tokenService.getVoterFromToken(token);
        Assert.assertFalse(voter.isPresent());
    }

    @Test
    public void validTokenWithNoUserNameShouldReturnEmptyUser() {
        String tokenWithoutUserName = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJvdGhlckZpZWxkIjoid2hhdGV2ZXIifQ.NO31wes_np1YjX0kr6oPrNvvYsQ7d700avumlpbD_6R5LKvc66PT1HU5Zo34uiZGKnHbQU69QuoHRH82KT3Y_Q";
        Optional<SingleVoter> voter = tokenService.getVoterFromToken(tokenWithoutUserName);
        Assert.assertFalse(voter.isPresent());
    }

    @Test
    public void getUserFromValidTokenShouldWork() {
        Long voterId = 42L;
        String username = "luke";
        String token = tokenService.createToken(voterId,username,0L,new Roles(),numberOfVotingWithTrusteeRole);
        Optional<SingleVoter> voter = tokenService.getVoterFromToken(token);
        Assert.assertTrue(voter.isPresent());
        Assert.assertEquals(voterId,voter.get().getId());
        Assert.assertEquals(username,voter.get().getName());
        Assert.assertFalse(voter.get().getPublicKey().isPresent());
    }

    @Test
    public void getUserWithKeyFromValidTokenShouldWork() {
        Long voterId = 42L;
        String username = "luke";
        String publicKey = "key";
        String token = tokenService.createToken(voterId,username,publicKey,0L,new Roles(),numberOfVotingWithTrusteeRole);
        Optional<SingleVoter> voter = tokenService.getVoterFromToken(token);
        Assert.assertTrue(voter.isPresent());
        Assert.assertEquals(voterId,voter.get().getId());
        Assert.assertEquals(username,voter.get().getName());
        Assert.assertEquals(publicKey,voter.get().getPublicKey().get());
    }



}
