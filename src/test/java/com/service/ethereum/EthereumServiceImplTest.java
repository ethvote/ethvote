package com.service.ethereum;

import com.services.ethereum.EthereumService;
import com.services.ethereum.EthereumServiceImpl;
import com.services.secrets.FakeSecretsService;
import com.services.secrets.SecretsService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

public class EthereumServiceImplTest {

    EthereumService ethereumService;

    SecretsService secretsService;

    @Before
    public void setup() {
        secretsService = new FakeSecretsService();
        ethereumService = new EthereumServiceImpl(secretsService);
    }

    @Test
    public void sendEthereumWorks() {
        String message = "0x215ad4d3e53c63990da43d22add6cceba165616d542a02daa89cdd8774f44724";
        Optional<String> transactionId = ethereumService.sendMessageToTestNetwork(message);
        Assert.assertTrue(transactionId.isPresent());
    }
}
