package com.service.parser;


import com.data.proofs.VoteNIZKProofs;
import com.exceptions.parser.MissingFieldException;
import com.exceptions.parser.ParserException;
import com.services.parser.VoteNIZKProofParser;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class VoteNIZKProofParserTest {

    private JSONObject validJsonObject;

    @Before
    public void setup() {
        JSONObject o1 = new JSONObject()
                .put("a","a1")
                .put("b", "b1")
                .put("c", "c1")
                .put("r", "r1")
                .put("signature", "sign1")
                .put("messageIndex",0);

        JSONObject o2 = new JSONObject()
                .put("a","a2")
                .put("b", "b2")
                .put("c", "c2")
                .put("r", "r2")
                .put("r", "r2")
                .put("signature", "sign2")
                .put("messageIndex",1);

        validJsonObject = new JSONObject()
                .put("proofs",new JSONArray()
                        .put(o1)
                        .put(o2));
    }

    @Test
    public void canParseValidJson() throws ParserException {
        VoteNIZKProofs proofs = VoteNIZKProofParser.parse(validJsonObject.toString());

        Assert.assertEquals(2,proofs.getSingleVoteNIZKProofs().size());

        Assert.assertEquals(0,proofs.getSingleVoteNIZKProofs().get(0).getMessageIndex().intValue());
        Assert.assertEquals("a1",proofs.getSingleVoteNIZKProofs().get(0).getA());
        Assert.assertEquals("b1",proofs.getSingleVoteNIZKProofs().get(0).getB());
        Assert.assertEquals("c1",proofs.getSingleVoteNIZKProofs().get(0).getC());
        Assert.assertEquals("r1",proofs.getSingleVoteNIZKProofs().get(0).getR());
        Assert.assertEquals("sign1",proofs.getSingleVoteNIZKProofs().get(0).getSignature());

        Assert.assertEquals(1,proofs.getSingleVoteNIZKProofs().get(1).getMessageIndex().intValue());
        Assert.assertEquals("a2",proofs.getSingleVoteNIZKProofs().get(1).getA());
        Assert.assertEquals("b2",proofs.getSingleVoteNIZKProofs().get(1).getB());
        Assert.assertEquals("c2",proofs.getSingleVoteNIZKProofs().get(1).getC());
        Assert.assertEquals("r2",proofs.getSingleVoteNIZKProofs().get(1).getR());
        Assert.assertEquals("r2",proofs.getSingleVoteNIZKProofs().get(1).getR());
        Assert.assertEquals("sign2",proofs.getSingleVoteNIZKProofs().get(1).getSignature());
    }

    @Test
    (expected = MissingFieldException.class)
    public void missingMessageIndexFieldThrowsException() throws ParserException {
        validJsonObject.getJSONArray("proofs").getJSONObject(0).remove("messageIndex").toString();
        String stringWithoutMessageField = validJsonObject.toString();
        VoteNIZKProofParser.parse(stringWithoutMessageField);
    }

    @Test
    (expected = MissingFieldException.class)
    public void missingAFieldThrowsException() throws ParserException {
        validJsonObject.getJSONArray("proofs").getJSONObject(0).remove("a").toString();
        String stringWithoutAField = validJsonObject.toString();
        VoteNIZKProofParser.parse(stringWithoutAField);
    }

    @Test
    (expected = MissingFieldException.class)
    public void missingBFieldThrowsException() throws ParserException {
        validJsonObject.getJSONArray("proofs").getJSONObject(0).remove("b").toString();
        String stringWithoutBField = validJsonObject.toString();
        VoteNIZKProofParser.parse(stringWithoutBField);
    }

    @Test
    (expected = MissingFieldException.class)
    public void missingCFieldThrowsException() throws ParserException {
        validJsonObject.getJSONArray("proofs").getJSONObject(0).remove("c").toString();
        String stringWithoutCField = validJsonObject.toString();
        VoteNIZKProofParser.parse(stringWithoutCField);
    }

    @Test
    (expected = MissingFieldException.class)
    public void missingRFieldThrowsException() throws ParserException {
        validJsonObject.getJSONArray("proofs").getJSONObject(0).remove("r").toString();
        String stringWithoutRField = validJsonObject.toString();
        VoteNIZKProofParser.parse(stringWithoutRField);
    }

    @Test
    (expected = MissingFieldException.class)
    public void missingSignatureFieldThrowsException() throws ParserException {
        validJsonObject.getJSONArray("proofs").getJSONObject(0).remove("signature").toString();
        String stringWithoutSignatureField = validJsonObject.toString();
        VoteNIZKProofParser.parse(stringWithoutSignatureField);
    }

}
