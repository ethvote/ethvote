package com.service.parser;

import com.data.voter.VotersToCreate;
import com.exceptions.parser.MissingFieldException;
import com.exceptions.parser.ParserException;
import com.services.parser.VoterParser;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

public class VoterParserTest {

    @Test
    (expected = ParserException.class)
    public void invalidJsonThrowsException() throws ParserException {
        String invalidJson = "invalid";
        VoterParser.parse(invalidJson);
    }

    @Test
    (expected = ParserException.class)
    public void parseErrorIfCreateKeysFieldIsMissing() throws ParserException {
        String validJson = "{\"voters\":[]}";
        VotersToCreate voters = VoterParser.parse(validJson);
        Assert.assertEquals(Arrays.asList(),voters.getVoters());
        Assert.assertFalse(voters.doCreateKeys());
    }

    @Test
    public void canParserEmptyListWithCreateKeysFalse() throws ParserException {
        String validJson = "{\"voters\":[], \"createKeys\": \"false\"}";
        VotersToCreate voters = VoterParser.parse(validJson);
        Assert.assertEquals(Arrays.asList(),voters.getVoters());
        Assert.assertFalse(voters.doCreateKeys());
    }

    @Test
    public void canParserEmptyListWithCreateKeysTrue() throws ParserException {
        String validJson = "{\"voters\":[], \"createKeys\": \"true\"}";
        VotersToCreate voters = VoterParser.parse(validJson);
        Assert.assertEquals(Arrays.asList(),voters.getVoters());
        Assert.assertTrue(voters.doCreateKeys());
    }

    @Test
    public void validVoterCanBeParsed() throws ParserException {
        String validJson = "{\"voters\":[{\"name\":\"foo\", \"email\":\"bar@baz\"}], \"createKeys\": \"false\"}";
        VotersToCreate voters = VoterParser.parse(validJson);
        Assert.assertEquals(1,voters.getVoters().size());
        Assert.assertEquals("foo",voters.getVoters().get(0).getName());
        Assert.assertEquals("bar@baz",voters.getVoters().get(0).getEmail());
    }

    @Test
    public void multipleValidVotersCanBeParsed() throws ParserException {
        String validJson = "{\"voters\":[" +
                "{\"name\":\"foo\", \"email\":\"foo@mail\"}," +
                "{\"name\":\"bar\", \"email\":\"bar@mail\"}," +
                "{\"name\":\"baz\", \"email\":\"baz@mail\"}]," +
                "\"createKeys\": \"false\"}";
        VotersToCreate voters = VoterParser.parse(validJson);
        Assert.assertEquals(3,voters.getVoters().size());
        Assert.assertEquals("foo",voters.getVoters().get(0).getName());
        Assert.assertEquals("bar",voters.getVoters().get(1).getName());
        Assert.assertEquals("baz",voters.getVoters().get(2).getName());

        Assert.assertEquals("foo@mail",voters.getVoters().get(0).getEmail());
        Assert.assertEquals("bar@mail",voters.getVoters().get(1).getEmail());
        Assert.assertEquals("baz@mail",voters.getVoters().get(2).getEmail());
    }

    @Test
    (expected = MissingFieldException.class)
    public void missingNameFieldThrowsException() throws ParserException {
        String validJson = "{\"voters\":[{\"email\":\"foo@mail\"}]}";
        VoterParser.parse(validJson);
    }

    @Test
    (expected = MissingFieldException.class)
    public void missingEmailFieldThrowsException() throws ParserException {
        String validJson = "{\"voters\":[{\"name\":\"foo\"}]}";
        VoterParser.parse(validJson);
    }
}
