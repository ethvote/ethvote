package com.service.parser;

import com.data.secretshares.SecretSharesFromVoter;
import com.data.secretshares.SingleSecretShareFromVoter;
import com.exceptions.parser.MissingFieldException;
import com.exceptions.parser.ParserException;
import com.services.parser.SecretShareParser;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.stream.Collectors;

public class SecretShareParserTest {

    private JSONObject validJsonObject;

    @Before
    public void setup() {
        JSONArray shares1 = new JSONArray().put("share1.1").put("share1.2");
        JSONArray shares2 = new JSONArray().put("share2.1").put("share2.2");
        JSONArray shares3 = new JSONArray().put("share3.1").put("share3.2");

        JSONArray jsonCommitmentsArray = new JSONArray();
        jsonCommitmentsArray.put(new JSONObject().put("secretShare",shares1).put("forVoterId",0).put("forVoterName","foo").put("signature","sign1"));
        jsonCommitmentsArray.put(new JSONObject().put("secretShare",shares2).put("forVoterId",1).put("forVoterName","bar").put("signature","sign2"));
        jsonCommitmentsArray.put(new JSONObject().put("secretShare",shares3).put("forVoterId",2).put("forVoterName","baz").put("signature","sign3"));

        validJsonObject = new JSONObject()
                .put("secretShares",jsonCommitmentsArray)
                .put("fromVoterId",0)
                .put("fromVoterName","Me");
    }

    @Test
    public void canParseValidJson() throws ParserException {
        SecretSharesFromVoter secretSharesFromVoter = SecretShareParser.parse(validJsonObject.toString());
        Assert.assertEquals(0, secretSharesFromVoter.getFromVoterId().longValue());
        Assert.assertEquals("Me", secretSharesFromVoter.getFromVoterName());
        Assert.assertEquals(Arrays.asList(0L,1L,2L), secretSharesFromVoter.getAllShares()
                        .stream()
                        .map(SingleSecretShareFromVoter::getForVoterId)
                        .collect(Collectors.toList()));

        Assert.assertEquals(Arrays.asList("foo","bar","baz"), secretSharesFromVoter.getAllShares()
                .stream()
                .map(SingleSecretShareFromVoter::getForVoterName)
                .collect(Collectors.toList()));

        Assert.assertEquals(Arrays.asList(Arrays.asList("share1.1","share1.2"),Arrays.asList("share2.1","share2.2"),Arrays.asList("share3.1","share3.2")), secretSharesFromVoter.getAllShares()
                .stream()
                .map(SingleSecretShareFromVoter::getSecretShare)
                .collect(Collectors.toList()));
    }

    @Test(expected = MissingFieldException.class)
    public void missingSecretShares() throws ParserException {
        validJsonObject.remove("secretShares");
        SecretShareParser.parse(validJsonObject.toString());

    }

    @Test(expected = MissingFieldException.class)
    public void missingFromUserId() throws ParserException {
        validJsonObject.remove("fromVoterId");
        SecretShareParser.parse(validJsonObject.toString());

    }

    @Test(expected = MissingFieldException.class)
    public void missingFromUsername() throws ParserException {
        validJsonObject.remove("fromVoterName");
        SecretShareParser.parse(validJsonObject.toString());
    }

    @Test(expected = MissingFieldException.class)
    public void missingSecretShareValue() throws ParserException {
        validJsonObject.getJSONArray("secretShares").getJSONObject(0).remove("secretShare");
        SecretShareParser.parse(validJsonObject.toString());
    }

    @Test(expected = MissingFieldException.class)
    public void missingSecretShareForUserIdValue() throws ParserException {
        validJsonObject.getJSONArray("secretShares").getJSONObject(0).remove("forVoterId");
        SecretShareParser.parse(validJsonObject.toString());
    }

    @Test(expected = MissingFieldException.class)
    public void missingSecretShareForUserNameValue() throws ParserException {
        validJsonObject.getJSONArray("secretShares").getJSONObject(0).remove("forVoterName");
        SecretShareParser.parse(validJsonObject.toString());
    }

    @Test(expected = MissingFieldException.class)
    public void missingSecretShareSignatureValue() throws ParserException {
        validJsonObject.getJSONArray("secretShares").getJSONObject(0).remove("signature");
        SecretShareParser.parse(validJsonObject.toString());
    }

}
