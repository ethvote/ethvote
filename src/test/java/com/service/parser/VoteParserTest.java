package com.service.parser;

import com.data.votes.SingleVote;
import com.exceptions.parser.MissingFieldException;
import com.exceptions.parser.ParserException;
import com.services.parser.VoteParser;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class VoteParserTest {

    private JSONObject validJsonObject;

    private final Long fromVoterId = 27L;
    private final String fromVoterName = "James";
    private final String alpha = "12345561351861";
    private final String beta = "231985612894561";
    private final String signature = "signature";

    @Before
    public void setup() {
        validJsonObject = new JSONObject()
                .put("fromVoterId",fromVoterId)
                .put("fromVoterName",fromVoterName)
                .put("alpha",alpha)
                .put("beta",beta)
                .put("signature",signature);
    }

    @Test
    public void validJsonCanBeParsed() throws ParserException {
        String validVote = validJsonObject.toString();
        SingleVote singleVote = VoteParser.parse(validVote);
        Assert.assertEquals(fromVoterId, singleVote.getFromVoterId());
        Assert.assertEquals(fromVoterName, singleVote.getFromVoterName());
        Assert.assertEquals(alpha, singleVote.getAlpha());
        Assert.assertEquals(beta, singleVote.getBeta());
    }

    @Test(expected = MissingFieldException.class)
    public void throwsExceptionIfFromVoterIdFieldIsMissing() throws ParserException {
        validJsonObject.remove("fromVoterId");
        VoteParser.parse(validJsonObject.toString());
    }

    @Test(expected = MissingFieldException.class)
    public void throwsExceptionFromVoterNameFieldIsMissing() throws ParserException {
        validJsonObject.remove("fromVoterName");
        VoteParser.parse(validJsonObject.toString());
    }

    @Test(expected = MissingFieldException.class)
    public void throwsExceptionIfAlphaFieldIsMissing() throws ParserException {
        validJsonObject.remove("alpha");
        VoteParser.parse(validJsonObject.toString());
    }

    @Test(expected = MissingFieldException.class)
    public void throwsExceptionIfBetaFieldIsMissing() throws ParserException {
        validJsonObject.remove("beta");
        VoteParser.parse(validJsonObject.toString());
    }

    @Test(expected = MissingFieldException.class)
    public void throwsExceptionIfSignatureFieldIsMissing() throws ParserException {
        validJsonObject.remove("signature");
        VoteParser.parse(validJsonObject.toString());
    }
}
