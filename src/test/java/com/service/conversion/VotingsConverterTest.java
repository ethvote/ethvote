package com.service.conversion;

import com.data.voter.VoterEntityWithIdSetter;
import com.data.votings.*;
import com.models.createdvotings.CreatedVotingsEntity;
import com.models.helper.VotingEntityWithSetters;
import com.models.singlevotingvoter.SingleVotingVoterEntity;
import com.models.user.UserEntity;
import com.models.voting.VotingCalculationParametersEntity;
import com.models.voting.VotingEntity;
import com.models.voting.VotingOptionEntity;
import com.services.conversion.VotingsConverter;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class VotingsConverterTest {

    private final String title = "title";
    private final String description = "description";
    private final Long id = 42L;
    private final Long eligibleVoters = 87L;
    private final Long securityThreshold = 27L;
    private final String transactionId = "transactionId";
    private final String votingsSummary = "votingsSummary";

    private final Long timeVotingIsFinished = 5L;

    private final int phaseNumber = 7;
    private final int numberOfEligibleVoters = 5;
    private final int phase = 42;

    private final int numberOfOptions = 3;

    private final String primeP = "31";
    private final String primeQ = "37";
    private final String generatorG = "4";

    private final String username1 = "username1";
    private final String username2 = "username2";
    private final String username3 = "username3";
    private final String username4 = "username4";
    private final String username5 = "username5";

    @Test
    public void optionEntityToOption() {
        String optionName = "optName";
        Long optionPrimeNumber = 31L;
        VotingOptionEntity votingOptionEntity = new VotingOptionEntity()
                .setOptionName(optionName)
                .setOptionPrimeNumber(optionPrimeNumber);
        VotingOption votingOption =  VotingsConverter.toData(votingOptionEntity);
        Assert.assertEquals(optionName,votingOption.getOptionName());
        Assert.assertEquals(optionPrimeNumber,votingOption.getOptionPrimeNumber());
    }

    @Test
    public void votingEntityToVoting() {
        VotingEntity votingEntity = createValidVotingEntity();
        Voting voting =  VotingsConverter.toData(votingEntity);
        Assert.assertEquals(id,voting.getId());
        Assert.assertEquals(title,voting.getTitle());
        Assert.assertEquals(description,voting.getDescription());
        Assert.assertEquals(numberOfOptions,voting.getVotingOptions().size());
        Assert.assertEquals(phase,voting.getPhase());
        Assert.assertEquals(timeVotingIsFinished.longValue(),voting.getTimeVotingIsFinished());
        Assert.assertEquals(transactionId,voting.getTransactionId());
    }

    @Test
    public void votingEntityToVotingWithVoterStatusSecretSharesCreationFalse() {
        VotingEntity votingEntity = createValidVotingEntity();
        VotingWithVoterStatus votingWithVoterStatus = VotingsConverter.toDataWithVotingStatus(votingEntity,false,false);
        Assert.assertEquals(id,votingWithVoterStatus.getId());
        Assert.assertEquals(title,votingWithVoterStatus.getTitle());
        Assert.assertEquals(description,votingWithVoterStatus.getDescription());
        Assert.assertEquals(numberOfOptions,votingWithVoterStatus.getVotingOptions().size());
        Assert.assertEquals(phase,votingWithVoterStatus.getPhase());
        Assert.assertEquals(false,votingWithVoterStatus.toJson().getBoolean("createdSecretShares"));
        Assert.assertEquals(false,votingWithVoterStatus.toJson().getBoolean("voted"));
    }

    @Test
    public void votingEntityToVotingWithVoterStatusSecretSharesCreationTrue() {
        VotingEntity votingEntity = createValidVotingEntity();
        VotingWithVoterStatus votingWithVoterStatus = VotingsConverter.toDataWithVotingStatus(votingEntity,true,true);
        Assert.assertEquals(id,votingWithVoterStatus.getId());
        Assert.assertEquals(title,votingWithVoterStatus.getTitle());
        Assert.assertEquals(description,votingWithVoterStatus.getDescription());
        Assert.assertEquals(numberOfOptions,votingWithVoterStatus.getVotingOptions().size());
        Assert.assertEquals(phase,votingWithVoterStatus.getPhase());
        Assert.assertEquals(true,votingWithVoterStatus.toJson().getBoolean("createdSecretShares"));
        Assert.assertEquals(true,votingWithVoterStatus.toJson().getBoolean("voted"));
    }

    @Test
    public void votingsIterableToVotings() {
        String titleOne = "t1";
        String titleTwo = "t2";
        String titleThree = "t3";
        String titleFour = "t4";

        List<VotingEntity> votingEntities = new ArrayList<>();
        votingEntities.add(createValidVotingEntity().setTitle(titleOne));
        votingEntities.add(createValidVotingEntity().setTitle(titleTwo));
        votingEntities.add(createValidVotingEntity().setTitle(titleThree));
        votingEntities.add(createValidVotingEntity().setTitle(titleFour));

        Votings votings = VotingsConverter.toData(votingEntities);
        Assert.assertEquals(4,votings.getVotings().size());
        Assert.assertEquals(titleOne,votings.getVotings().get(0).getTitle());
        Assert.assertEquals(titleTwo,votings.getVotings().get(1).getTitle());
        Assert.assertEquals(titleThree,votings.getVotings().get(2).getTitle());
        Assert.assertEquals(titleFour,votings.getVotings().get(3).getTitle());
    }

    @Test
    public void optionToOptionEntity() {
        String optionName = "dummy";
        long primeNumber = 29L;
        VotingOption votingOption = new VotingOption()
                .setOptionName(optionName)
                .setOptionPrimeNumber(primeNumber);
        VotingOptionEntity votingOptionEntity = VotingsConverter.toEntity(votingOption);
        Assert.assertEquals(optionName,votingOptionEntity.getOptionName());
        Assert.assertEquals(primeNumber,votingOptionEntity.getOptionPrimeNumber());
    }

    @Test
    public void votingCalcParametersToVotingCalcParametersEntity() {
        String primeP = "31";
        String primeQ = "37";
        String generatorG = "4";

        VotingCalculationParameters calculationParameters = new VotingCalculationParameters()
                .setPrimeP(primeP)
                .setPrimeQ(primeQ)
                .setGeneratorG(generatorG);

        VotingCalculationParametersEntity calculationParametersEntity = VotingsConverter.toEntity(calculationParameters);

        Assert.assertEquals(primeP,calculationParametersEntity.getPrimeP());
        Assert.assertEquals(primeQ,calculationParametersEntity.getPrimeQ());
        Assert.assertEquals(generatorG,calculationParametersEntity.getGeneratorG());
    }

    @Test
    public void votingCalcParametersEntityToVotingCalcParameters() {
        String primeP = "31";
        String primeQ = "37";
        String generatorG = "4";

        VotingCalculationParametersEntity calculationParametersEntity = new VotingCalculationParametersEntity()
                .setPrimeP(primeP)
                .setPrimeQ(primeQ)
                .setGeneratorG(generatorG);

        VotingCalculationParameters calculationParameters = VotingsConverter.toData(calculationParametersEntity);

        Assert.assertEquals(primeP,calculationParameters.getPrimeP());
        Assert.assertEquals(primeQ,calculationParameters.getPrimeQ());
        Assert.assertEquals(generatorG,calculationParameters.getGeneratorG());
    }

    @Test
    public void singleVotingVoterEntityToParticipatingVoterWithKey() {
        String key = "userKey";
        String name1 = "name1";
        long id = 42;
        long position = 27;

        UserEntity voter1 = new VoterEntityWithIdSetter()
                .setId(id)
                .setUsername(name1);
        VotingEntityWithSetters votingEntity = new VotingEntityWithSetters();
        SingleVotingVoterEntity singleVotingVoterEntity = new SingleVotingVoterEntity(voter1,votingEntity)
                .setVoterNumber(position)
                .setPublicKey(key)
                .setIsTrustee(true)
                .setCanVote(true);


        EligibleVoterWithKey eligibleVoterWithKey = VotingsConverter.toData(singleVotingVoterEntity);
        Assert.assertEquals(name1,eligibleVoterWithKey.toJson().getString("name"));
        Assert.assertEquals(key,eligibleVoterWithKey.toJson().getString("publicKey"));
        Assert.assertEquals(position,eligibleVoterWithKey.toJson().getLong("position"));
        Assert.assertEquals(id,eligibleVoterWithKey.toJson().getLong("id"));
        Assert.assertTrue(eligibleVoterWithKey.toJson().getBoolean("canVote"));
        Assert.assertTrue(eligibleVoterWithKey.toJson().getBoolean("isTrustee"));
    }

    @Test
    public void newlyCreatedVotingToVotingEntity() {

        VotingCalculationParameters calculationParameters = new VotingCalculationParameters()
                .setPrimeP(primeP)
                .setPrimeQ(primeQ)
                .setGeneratorG(generatorG);

        VotingOption v1 = new VotingOption();
        VotingOption v2 = new VotingOption();
        VotingOption v3 = new VotingOption();
        NewlyCreatedVoting voting = new NewlyCreatedVoting()
                .setId(id)
                .setTitle(title)
                .setDescription(description)
                .addVotingOption(v1)
                .addVotingOption(v2)
                .addVotingOption(v3)
                .setNumberOfEligibleVoters(eligibleVoters)
                .setSecurityThreshold(securityThreshold)
                .setPhase(phaseNumber)
                .setVotingCalculationParameters(calculationParameters)
                .addParticipatingVoter(new EligibleVoter().setName("user1"))
                .addParticipatingVoter(new EligibleVoter().setName("user1"))
                .addParticipatingVoter(new EligibleVoter().setName("user1"));

        VotingEntity votingEntity =  VotingsConverter.toEntity(voting);
        Assert.assertEquals(title,votingEntity.getTitle());
        Assert.assertEquals(description,votingEntity.getDescription());
        Assert.assertEquals(3,votingEntity.getVotingOptions().size());
        Assert.assertEquals(securityThreshold,votingEntity.getSecurityThreshold());
        Assert.assertEquals(eligibleVoters,votingEntity.getNumberOfEligibleVoters());
        Assert.assertEquals(phaseNumber,votingEntity.getPhaseNumber());
        Assert.assertEquals(primeP,votingEntity.getCalculationParametersEntity().getPrimeP());
        Assert.assertEquals(primeQ,votingEntity.getCalculationParametersEntity().getPrimeQ());
        Assert.assertEquals(generatorG,votingEntity.getCalculationParametersEntity().getGeneratorG());
    }

    @Test
    public void toVotingOfficialsWorks() {
        VotingEntity votingEntity = new VotingEntity();

        UserEntity userEntity1 = new UserEntity().setUsername("u1");
        UserEntity userEntity2 = new UserEntity().setUsername("u2");
        UserEntity userEntity3 = new UserEntity().setUsername("u3");

        SingleVotingVoterEntity singleVotingVoterEntity1 = new SingleVotingVoterEntity(userEntity1,votingEntity).setIsTrustee(false);
        SingleVotingVoterEntity singleVotingVoterEntity2 = new SingleVotingVoterEntity(userEntity2,votingEntity).setIsTrustee(true);
        SingleVotingVoterEntity singleVotingVoterEntity3 = new SingleVotingVoterEntity(userEntity3,votingEntity).setIsTrustee(true);

        CreatedVotingsEntity createdVotingsEntity = new CreatedVotingsEntity(userEntity3,votingEntity);

        votingEntity
                .addEligibleVoter(singleVotingVoterEntity1)
                .addEligibleVoter(singleVotingVoterEntity2)
                .addEligibleVoter(singleVotingVoterEntity3)
                .setVotingAdministrator(createdVotingsEntity);

        VotingOfficials votingOfficials = VotingsConverter.toVotingOfficials(votingEntity);

        Assert.assertEquals("u3",votingOfficials.getVotingAdministratorName());
        Assert.assertEquals(Arrays.asList("u2","u3"),votingOfficials.getTrusteeNames());
    }

    private VotingEntity createValidVotingEntity() {
        VotingOptionEntity vE1 = new VotingOptionEntity().setOptionName("o1").setOptionPrimeNumber(2L);
        VotingOptionEntity vE2 = new VotingOptionEntity().setOptionName("o2").setOptionPrimeNumber(3L);
        VotingOptionEntity vE3 = new VotingOptionEntity().setOptionName("o3").setOptionPrimeNumber(5L);

        UserEntity user1 = new UserEntity().setUsername(username1);
        UserEntity user2 = new UserEntity().setUsername(username2);
        UserEntity user3 = new UserEntity().setUsername(username3);
        UserEntity user4 = new UserEntity().setUsername(username4);
        UserEntity user5 = new UserEntity().setUsername(username5);

        VotingCalculationParametersEntity calcEntity = new VotingCalculationParametersEntity()
                .setPrimeP(primeP)
                .setPrimeQ(primeQ)
                .setGeneratorG(generatorG);

        VotingEntity votingEntity = new VotingEntityWithSetters()
                .setId(id)
                .setTitle(title)
                .setDescription(description)
                .setNumberOfEligibleVoters(numberOfEligibleVoters)
                .setSecurityThreshold(securityThreshold)
                .setCalculationParametersEntity(calcEntity)
                .setPhaseNumber(phase)
                .setTimeVotingIsFinished(timeVotingIsFinished)
                .addVotingOption(vE1)
                .addVotingOption(vE2)
                .addVotingOption(vE3)
                .setTransactionId(transactionId)
                .setVotingSummary(votingsSummary);

        SingleVotingVoterEntity voter1 = new SingleVotingVoterEntity(user1,votingEntity);
        SingleVotingVoterEntity voter2 = new SingleVotingVoterEntity(user2,votingEntity);
        SingleVotingVoterEntity voter3 = new SingleVotingVoterEntity(user3,votingEntity);
        SingleVotingVoterEntity voter4 = new SingleVotingVoterEntity(user4,votingEntity);
        SingleVotingVoterEntity voter5 = new SingleVotingVoterEntity(user5,votingEntity);

        votingEntity.addEligibleVoter(voter1);
        votingEntity.addEligibleVoter(voter2);
        votingEntity.addEligibleVoter(voter3);
        votingEntity.addEligibleVoter(voter4);
        votingEntity.addEligibleVoter(voter5);

        return votingEntity;
    }
}
