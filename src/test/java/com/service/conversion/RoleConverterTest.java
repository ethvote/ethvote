package com.service.conversion;

import com.data.roles.Roles;
import com.enums.Role;
import com.models.role.RolesEntity;
import com.services.conversion.RoleConverter;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;


public class RoleConverterTest {

    Role admin = Role.ADMINISTRATOR;
    Role votingAdmin = Role.VOTING_ADMINISTRATOR;
    Role registrar = Role.REGISTRAR;

    @Test
    public void multipleRolesWorks() {
        Role expected1 = Role.ADMINISTRATOR;
        Role expected2 = Role.VOTING_ADMINISTRATOR;

        RolesEntity rolesEntity = new RolesEntity()
                .setAdministratorRole(true)
                .setVotingAdministratorRole(true);

        Roles roles = RoleConverter.toData(rolesEntity);

        Assert.assertEquals(Arrays.asList(expected1,expected2),roles.getRoles());
    }

    @Test
    public void administratorRoleIsConverted() {
        RolesEntity rolesEntity = new RolesEntity()
                .setAdministratorRole(true);

        Roles roles = RoleConverter.toData(rolesEntity);

        Assert.assertEquals(Arrays.asList(admin),roles.getRoles());
    }

    @Test
    public void votingAdministratorRoleIsConverted() {
        RolesEntity rolesEntity = new RolesEntity()
                .setVotingAdministratorRole(true);

        Roles roles = RoleConverter.toData(rolesEntity);

        Assert.assertEquals(Arrays.asList(votingAdmin),roles.getRoles());
    }

    @Test
    public void registrarRoleIsConverted() {
        RolesEntity rolesEntity = new RolesEntity()
                .setRegistrarRole(true);

        Roles roles = RoleConverter.toData(rolesEntity);

        Assert.assertEquals(Arrays.asList(registrar),roles.getRoles());
    }
}
