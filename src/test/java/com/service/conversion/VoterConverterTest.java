package com.service.conversion;

import com.data.voter.SingleVoter;

import com.models.user.UserEntity;
import com.services.conversion.VoterConverter;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.when;

public class VoterConverterTest {

    private final String username = "Yoda";
    private final Long id = 27L;
    private final String publicKey = "key";
    private final long numberOfVotingWithTrusteeRole = 42;

    @Test
    public void conversionToDataWithKeysWorks() {
        UserEntity votingEntity = Mockito.mock(UserEntity.class);
        when(votingEntity.getId()).thenReturn(id);
        when(votingEntity.getUsername()).thenReturn(username);
        when(votingEntity.getPublicKey()).thenReturn(publicKey);
        SingleVoter voter = VoterConverter.toData(votingEntity);
        Assert.assertEquals(username,voter.getName());
        Assert.assertEquals(publicKey,voter.getPublicKey().get());
        Assert.assertEquals(id,voter.getId());
        Assert.assertTrue(voter.getPublicKey().isPresent());
        Assert.assertEquals(publicKey, voter.getPublicKey().get());
    }

    @Test
    public void conversionToDataWorksWithSomeFieldsUnspecified() {
        UserEntity votingEntity = new UserEntity().setUsername(username);
        SingleVoter voter = VoterConverter.toData(votingEntity);
        Assert.assertEquals(username,voter.getName());
        Assert.assertNull(voter.getId());
        Assert.assertFalse(voter.getUniqueId().isPresent());
    }

    @Test
    public void conversionToDataWorksWithAllFieldsSpecified() {
        UserEntity votingEntity = Mockito.mock(UserEntity.class);
        when(votingEntity.getId()).thenReturn(id);
        when(votingEntity.getUsername()).thenReturn(username);
        when(votingEntity.getExternalId()).thenReturn("id");
        when(votingEntity.getNumberOfVotingsWithTrusteeRole()).thenReturn(numberOfVotingWithTrusteeRole);
        SingleVoter voter = VoterConverter.toData(votingEntity);
        Assert.assertEquals(username,voter.getName());
        Assert.assertEquals(id,voter.getId());
        Assert.assertFalse(voter.getPublicKey().isPresent());
        Assert.assertTrue(voter.getUniqueId().isPresent());
        Assert.assertEquals(numberOfVotingWithTrusteeRole,voter.getNumberOfVotingsWithTrusteeRole());
    }
}
