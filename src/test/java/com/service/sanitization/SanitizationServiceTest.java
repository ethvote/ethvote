package com.service.sanitization;

import com.services.sanitization.SanitizationService;
import org.junit.Assert;
import org.junit.Test;

public class SanitizationServiceTest {

    @Test
    public void nullStringShouldNotPass() {
        Assert.assertFalse(SanitizationService.hasNoSpecialCharacters(null));
    }

    @Test
    public void invalidCharactersShouldNotPass() {
        char[] potentiallyDangerous = { '(',')',
                                        '{', '}',
                                        '[',']',
                                        '/','\\',
                                        '<','>',
                                        '+', '-',
                                        '"', '\'',
                                        '%', ';',
                                        '&'};

        for(char c : potentiallyDangerous) {
            Assert.assertFalse(SanitizationService.hasNoSpecialCharacters(""+c));
        }
    }

    @Test
    public void validEntryShouldPass() {
        String validString = "adf asjk0f07asd";
        Assert.assertTrue(SanitizationService.hasNoSpecialCharacters(validString));
    }

    @Test
    public void invalidEntryShouldNotPass() {
        String validString = "adf(asjk0f07asd";
        Assert.assertFalse(SanitizationService.hasNoSpecialCharacters(validString));
    }
}
