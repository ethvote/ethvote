package com.controller;

import com.data.commitments.CommitmentFromVoter;
import com.data.commitments.Commitments;
import com.data.commitments.SingleCommitment;
import com.service.context.FakeRequestContextHolder;
import com.services.context.RequestContextHolder;
import com.services.data.commitments.CommitmentService;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
@ActiveProfiles("test")
public class CommitmentControllerTest {

    private RequestContextHolder requestContextHolder;

    @MockBean
    private CommitmentService commitmentService;

    private ControllerTestHelper helper;

    @Before()
    public void setup(){
        requestContextHolder = new FakeRequestContextHolder();
        helper = new ControllerTestHelper(requestContextHolder)
                .setController(new CommitmentController(commitmentService,requestContextHolder));
    }

    @Test
    public void userTriesToSpecifyCommitmentsOfOtherPermissionDenied() throws Exception {
        long votingsId = 1;
        long voterId = 4;
        long differentVoterId = 5;

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.POST)
                .setUrl("/api/votings/"+votingsId+"/commitments/voters/"+voterId)
                .setVoterId(differentVoterId)
                .setController(new CommitmentController(commitmentService,requestContextHolder))
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.FORBIDDEN.value(),response.getStatus());
    }

    @Test
    public void invalidData() throws Exception {
        long votingsId = 1;
        long voterId = 4;

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.POST)
                .setUrl("/api/votings/"+votingsId+"/commitments/voters/"+voterId)
                .setVoterId(voterId)
                .setContent("dummyContent")
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.BAD_REQUEST.value(),response.getStatus());

    }

    @Test
    public void validDataShouldStore() throws Exception {
        Long votingsId = 1L;
        Long voterId = 4L;

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.POST)
                .setUrl("/api/votings/"+votingsId+"/commitments/voters/"+voterId)
                .setVoterId(voterId)
                .setContent(createValidInput())
                .doAuthenticatedRequest();

        verify(commitmentService,times(1)).setCommitmentsForUser(eq(votingsId),eq(voterId),any());
        Assert.assertEquals(HttpStatus.CREATED.value(),response.getStatus());
    }

    @Test
    public void getCommitmentFromVoterWorks() throws Exception {
        Long votingsId = 1L;
        Long voterId = 4L;

        CommitmentFromVoter commitmentFromVoter = new CommitmentFromVoter()
                .addCommitments(new SingleCommitment().setCommitment("c1").setCoefficientNumber(1).setSignature("sign1"))
                .addCommitments(new SingleCommitment().setCommitment("c2").setCoefficientNumber(2).setSignature("sign2"))
                .addCommitments(new SingleCommitment().setCommitment("c3").setCoefficientNumber(3).setSignature("sign3"))
                .setFromVoterId(voterId)
                .setFromVoterName("u")
                .setFromVoterPosition(0L);

        when(commitmentService.getCommitmentsFromUser(votingsId,voterId)).thenReturn(commitmentFromVoter);

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.GET)
                .setUrl("/api/votings/"+votingsId+"/commitments/voters/"+voterId)
                .setVoterId(voterId)
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.OK.value(),response.getStatus());
        Assert.assertEquals("{\"fromVoterPosition\":0,\"fromVoterId\":4,\"commitmentsFromVoter\":[{\"signature\":\"sign1\",\"commitment\":\"c1\",\"coefficientNumber\":1},{\"signature\":\"sign2\",\"commitment\":\"c2\",\"coefficientNumber\":2},{\"signature\":\"sign3\",\"commitment\":\"c3\",\"coefficientNumber\":3}],\"fromVoterName\":\"u\"}",
                response.getContentAsString());
    }

    @Test
    public void getAllCommitmentWorks() throws Exception {
        Long votingsId = 1L;
        Long voterId = 4L;

        Commitments commitments = Mockito.mock(Commitments.class);
        JSONObject jsonObject = Mockito.mock(JSONObject.class);
        when(jsonObject.toString()).thenReturn("dummyJson");
        when(commitments.toJson()).thenReturn(jsonObject);

        when(commitmentService.getAllCommitmentsForVoting(votingsId)).thenReturn(commitments);

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.GET)
                .setUrl("/api/votings/"+votingsId+"/commitments")
                .setVoterId(voterId)
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.OK.value(),response.getStatus());
        Assert.assertEquals("dummyJson", response.getContentAsString());
    }


    private String createValidInput() {
        JSONArray jsonCommitmentsArray = new JSONArray();
        jsonCommitmentsArray.put(new JSONObject().put("commitment","c1").put("coefficientNumber",0L).put("signature","sign1"));
        jsonCommitmentsArray.put(new JSONObject().put("commitment","c2").put("coefficientNumber",1L).put("signature","sign2"));
        jsonCommitmentsArray.put(new JSONObject().put("commitment","c3").put("coefficientNumber",2L).put("signature","sign3"));

        JSONObject validJsonObject = new JSONObject()
                .put("commitments",jsonCommitmentsArray)
                .put("numberOfCommitments",3);

        return validJsonObject.toString();
    }

}
