package com.controller;

import com.data.voter.SingleVoter;

import com.services.context.RequestContextHolder;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;


import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

public class ControllerTestHelper {

    private RequestContextHolder requestContextHolder;

    private SingleVoter voter = null;

    private String url = null;

    String content  = null;

    private Long voterId = null;

    private String voterName = null;

    private HttpMethod method = null;

    private Object controller = null;

    public ControllerTestHelper(RequestContextHolder requestContextHolder) {
        this.requestContextHolder = requestContextHolder;
    }

    public ControllerTestHelper setUser(SingleVoter voter) {
        this.voter = voter;
        return this;
    }

    public ControllerTestHelper setUrl(String url) {
        this.url = url;
        return this;
    }

    public ControllerTestHelper setVoterId(Long voterId) {
        this.voterId = voterId;
        return this;
    }

    public ControllerTestHelper setContent(String content) {
        this.content = content;
        return this;
    }

    public ControllerTestHelper setVoterName(String voterName) {
        this.voterName = voterName;
        return this;
    }

    public ControllerTestHelper setMethod(HttpMethod method) {
        this.method = method;
        return this;
    }

    public ControllerTestHelper setController(Object controller) {
        this.controller = controller;
        return this;
    }

    public MockHttpServletResponse doAuthenticatedRequest() throws Exception {
        setDefaultValues();
        requestContextHolder.setCurrentVoter(voter);
        return doRequest();
    }

    public MockHttpServletResponse doUnauthenticatedRequest() throws Exception {
        setDefaultValues();
        return doRequest();
    }

    private MockHttpServletResponse doRequest() throws Exception {
        MockMvc mockMvc = MockMvcBuilders
                .standaloneSetup(controller)
                .build();

        switch (method) {
            case GET:
                return getRequest(mockMvc,url);
            case HEAD:
                return headRequest(mockMvc,url);
            case POST:
                return postRequest(mockMvc,url,content);
            case PUT:
                return putRequest(mockMvc,url,content);
            case PATCH:
                return patchRequest(mockMvc,url);
            case DELETE:
                return deleteRequest(mockMvc,url);
            case OPTIONS:
                return optionsRequest(mockMvc,url);
        }
        return null;
    }

    private MockHttpServletResponse getRequest(MockMvc mockMvc, String url) throws Exception {
        MvcResult result = mockMvc.perform(
                get(url)
        ).andReturn();
        return result.getResponse();
    }

    private MockHttpServletResponse headRequest(MockMvc mockMvc, String url) throws Exception {
        MvcResult result = mockMvc.perform(
                head(url)
        ).andReturn();
        return result.getResponse();
    }

    private MockHttpServletResponse postRequest(MockMvc mockMvc, String url, String content) throws Exception {
        MvcResult result = mockMvc.perform(
                post(url)
                        .content(content)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andReturn();
        return result.getResponse();
    }

    private MockHttpServletResponse putRequest(MockMvc mockMvc, String url, String content) throws Exception {
        MvcResult result = mockMvc.perform(
                put(url)
                .content(content)
                .contentType(MediaType.APPLICATION_JSON)
        ).andReturn();
        return result.getResponse();
    }

    private MockHttpServletResponse patchRequest(MockMvc mockMvc, String url) throws Exception {
        MvcResult result = mockMvc.perform(
                patch(url)
        ).andReturn();
        return result.getResponse();
    }

    private MockHttpServletResponse deleteRequest(MockMvc mockMvc, String url) throws Exception {
        MvcResult result = mockMvc.perform(
                delete(url)
        ).andReturn();
        return result.getResponse();
    }

    private MockHttpServletResponse optionsRequest(MockMvc mockMvc, String url) throws Exception {
        MvcResult result = mockMvc.perform(
                options(url)
        ).andReturn();
        return result.getResponse();
    }

    private void setDefaultValues() {
        if(voter==null) {
            if(voterId == null) {
                voterId = 0L;
            }
            if(voterName == null) {
                voterName = "dummyName";
            }
            voter = new SingleVoter().setName(voterName).setId(voterId);
        }

        if(content == null) {
            content = "dummyContent";
        }
    }



}
