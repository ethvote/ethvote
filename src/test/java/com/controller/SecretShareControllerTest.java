package com.controller;

import com.data.secretshares.SecretSharesForVoter;
import com.data.secretshares.SingleSecretShareForVoter;
import com.data.voter.SingleVoter;

import com.service.context.FakeRequestContextHolder;
import com.services.context.RequestContextHolder;
import com.services.data.secretshares.SecretShareService;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
@ActiveProfiles("test")
public class SecretShareControllerTest {

    private RequestContextHolder requestContextHolder;

    @MockBean
    private SecretShareService secretShareService;

    private ControllerTestHelper helper;

    private SecretShareController secretShareController;

    @Before()
    public void setup(){
        requestContextHolder = new FakeRequestContextHolder();
        secretShareController = new SecretShareController(secretShareService,requestContextHolder);
        helper = new ControllerTestHelper(requestContextHolder)
        .setController(secretShareController);
    }

    @Test
    public void invalidContentReturnsError() throws Exception {
        long votingsId = 1;

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.POST)
                .setUrl("/api/votings/"+votingsId+"/secretShares")
                .setContent("invalidContent")
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.BAD_REQUEST.value(),response.getStatus());
    }

    @Test
    public void userTriesToSpecifySecretShareWithOtherUsernamePermissionDenied() throws Exception {
        long votingsId = 1;
        String actualVoterName = "dummy";
        Long actualId = 27L;

        String pretendedVoterName = "other";

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.POST)
                .setUrl("/api/votings/"+votingsId+"/secretShares")
                .setContent(createValidInput(actualVoterName,actualId))
                .setVoterName(pretendedVoterName)
                .setVoterId(actualId)
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.FORBIDDEN.value(),response.getStatus());
    }

    @Test
    public void userTriesToSpecifySecretShareWithOtherUserIdPermissionDenied() throws Exception {
        long votingsId = 1;
        String actualVoterName = "dummy";
        Long actualId = 27L;

        Long pretendedId = 42L;

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.POST)
                .setUrl("/api/votings/"+votingsId+"/secretShares")
                .setContent(createValidInput(actualVoterName,pretendedId))
                .setVoterName(actualVoterName)
                .setVoterId(actualId)
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.FORBIDDEN.value(),response.getStatus());
    }

    @Test
    public void savingWorks() throws Exception {
        long votingsId = 1;
        String actualVoterName = "dummy";
        Long actualId = 27L;

        SingleVoter currentVoter = new SingleVoter().setName(actualVoterName).setId(actualId);

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.POST)
                .setUrl("/api/votings/"+votingsId+"/secretShares")
                .setContent(createValidInput(actualVoterName,actualId))
                .setUser(currentVoter)
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.CREATED.value(),response.getStatus());
        verify(secretShareService,times(1)).addSecretShares(eq(votingsId),any());
    }

    @Test
    public void getSecretSharesWorks() throws Exception {
        Long votingsId = 27L;
        String forUsername = "John";
        Long voterId = 4L;
        Long voterPosition = 42L;

        String signature1 = "sign1";
        String signature2 = "sign2";
        String signature3 = "sign3";


        SecretSharesForVoter secretSharesForUser = new SecretSharesForVoter(voterId,forUsername,voterPosition)
                .addSingleSecretShare(new SingleSecretShareForVoter(0L,"u0",Arrays.asList("s0","s1","s2"),signature1))
                .addSingleSecretShare(new SingleSecretShareForVoter(1L,"u1",Arrays.asList("s3","s4","s5"),signature2))
                .addSingleSecretShare(new SingleSecretShareForVoter(2L,"u2",Arrays.asList("s6","s7","s8"),signature3));

        when(secretShareService.getSharesForUser(eq(votingsId),eq(voterId),anyString())).thenReturn(secretSharesForUser);

        SingleVoter user = new SingleVoter().setName("u").setId(voterId);
        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.GET)
                .setUrl("/api/votings/"+votingsId+"/secretShares/forVoters/"+voterId)
                .setUser(user)
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.OK.value(),response.getStatus());
        Assert.assertEquals("{\"forVoterId\":4,\"forVoterName\":\"John\",\"forVoterPosition\":42,\"secretShares\":[{\"secretShare\":[\"s0\",\"s1\",\"s2\"],\"signature\":\"sign1\",\"fromVoterId\":0,\"fromVoterName\":\"u0\"},{\"secretShare\":[\"s3\",\"s4\",\"s5\"],\"signature\":\"sign2\",\"fromVoterId\":1,\"fromVoterName\":\"u1\"},{\"secretShare\":[\"s6\",\"s7\",\"s8\"],\"signature\":\"sign3\",\"fromVoterId\":2,\"fromVoterName\":\"u2\"}]}",response.getContentAsString());
    }

    private String createValidInput(String fromUsername, Long fromUserId) {
        JSONArray secretShare1 = new JSONArray().put("s1").put("s2").put("s3");
        JSONArray secretShare2 = new JSONArray().put("s4").put("s5").put("s6");
        JSONArray secretShare3 = new JSONArray().put("s7").put("s8").put("s9");

        JSONArray secretSharesArray = new JSONArray();
        secretSharesArray.put(new JSONObject().put("secretShare",secretShare1).put("forVoterName","user0").put("forVoterId",0L).put("signature","sign1"));
        secretSharesArray.put(new JSONObject().put("secretShare",secretShare2).put("forVoterName","user1").put("forVoterId",1L).put("signature","sign2"));
        secretSharesArray.put(new JSONObject().put("secretShare",secretShare3).put("forVoterName","user2").put("forVoterId",2L).put("signature","sign3"));

        JSONObject validJsonObject = new JSONObject()
                .put("fromVoterName",fromUsername)
                .put("fromVoterId",fromUserId)
                .put("secretShares",secretSharesArray);
        return validJsonObject.toString();
    }
}
