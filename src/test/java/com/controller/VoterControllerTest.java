package com.controller;

import com.data.voter.*;
import com.enums.Role;
import com.exceptions.NoSuchVoterException;
import com.exceptions.VoterInVotingException;
import com.service.context.FakeRequestContextHolder;
import com.services.context.RequestContextHolder;
import com.services.data.keys.KeyService;
import com.services.data.roles.RoleService;
import com.services.data.voters.VotersService;
import com.services.token.JwtTokenCreatorService;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
@ActiveProfiles("test")
public class VoterControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private VotersService votersService;

    @MockBean
    private JwtTokenCreatorService mockTokenCreatorService;

    private Long voterId = 27L;

    private String voterName = "someVoter";

    private String voterMail = "voter@mail";

    private String voterKey = "someKey";

    private RequestContextHolder requestContextHolder;

    private ControllerTestHelper helper;

    private VoterController voterController;

    private RoleService roleService;

    private KeyService keyService;

    @Before
    public void setup() {
        mockTokenCreatorService = Mockito.mock(JwtTokenCreatorService.class);
        requestContextHolder = new FakeRequestContextHolder();
        roleService = Mockito.mock(RoleService.class);
        keyService = Mockito.mock(KeyService.class);
        voterController = new VoterController(votersService,requestContextHolder,roleService,keyService, mockTokenCreatorService);
        helper = new ControllerTestHelper(requestContextHolder)
                .setController(voterController);
    }

    @Test
    public void getTokenWorksRegistered() throws Exception {
        SingleVoter singleVoter = new SingleVoter().setName("John Doe").setId(1L);
        requestContextHolder.setCurrentVoter(singleVoter);

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.GET)
                .setUrl("/api/currentVoterToken")
                .doUnauthenticatedRequest();

        Assert.assertEquals(HttpStatus.OK.value(),response.getStatus());
        Assert.assertEquals("{\"voterName\":\"John Doe\",\"registered\":\"Yes\"}",response.getContentAsString());
    }

    @Test
    public void getTokenWorksNotRegistered() throws Exception {
        SingleVoter singleVoter = new SingleVoter().setName("John Doe");
        requestContextHolder.setCurrentVoter(singleVoter);

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.GET)
                .setUrl("/api/currentVoterToken")
                .doUnauthenticatedRequest();

        Assert.assertEquals(HttpStatus.OK.value(),response.getStatus());
        Assert.assertEquals("{\"voterName\":\"John Doe\",\"registered\":\"No\"}",response.getContentAsString());
    }

    @Test
    public void getKeyWorksIfThere() throws Exception {
        when(votersService.getPublicKeyOfVoter(voterId)).thenReturn(Optional.of(voterKey));

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.GET)
                .setUrl("/api/voters/"+ voterId +"/publicKey")
                .doAuthenticatedRequest();

        JSONObject expected = new JSONObject()
                .put("publicKey", voterKey);

        Assert.assertEquals(HttpStatus.OK.value(),response.getStatus());
        Assert.assertEquals(expected.toString(),response.getContentAsString());
    }

    @Test
    public void getAllVotersWorks() throws Exception {
        List<SingleVoter> voters = Arrays.asList(
                new SingleVoter().setName("u1").setId(0L),
                new SingleVoter().setName("u2").setId(1L),
                new SingleVoter().setName("u3").setId(2L)
        );

        when(votersService.getAllVoters()).thenReturn(new Voters().setVoters(voters));

        String expected = "{\"voters\":[{\"name\":\"u1\",\"id\":0},{\"name\":\"u2\",\"id\":1},{\"name\":\"u3\",\"id\":2}]}";

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.GET)
                .setUrl("/api/voters?onlyIfKeySpecified=false")
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.OK.value(),response.getStatus());
        Assert.assertEquals(expected,response.getContentAsString());
    }

    @Test
    public void getAllVotersWhereKeysAreSpecifiedWorks() throws Exception {
        List<SingleVoter> voters = Arrays.asList(
                new SingleVoter().setName("u1").setId(0L),
                new SingleVoter().setName("u2").setId(1L),
                new SingleVoter().setName("u3").setId(2L)
        );

        when(votersService.getAllVotersWherePublicKeyIsSpecified()).thenReturn(new Voters().setVoters(voters));

        String expected = "{\"voters\":[{\"name\":\"u1\",\"id\":0},{\"name\":\"u2\",\"id\":1},{\"name\":\"u3\",\"id\":2}]}";

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.GET)
                .setUrl("/api/voters?onlyIfKeySpecified=true")
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.OK.value(),response.getStatus());
        Assert.assertEquals(expected,response.getContentAsString());
    }

    @Test
    public void saveReturnsPermissionDeniedIfUserIsNotKnown() throws Exception {
        String validJson = "{\"voters\":[{\"name\":\"foo\", \"email\": \"foo@mail\"}," +
                "{\"name\":\"bar\", \"email\": \"bar@mail\"}," +
                "{\"name\":\"baz\", \"email\": \"baz@mail\"}], \"createKeys\": \"false\"}";

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.POST)
                .setUrl("/api/voters")
                .setContent(validJson)
                .doUnauthenticatedRequest();

        Assert.assertEquals(HttpStatus.FORBIDDEN.value(),response.getStatus());
    }

    @Test
    public void saveReturnsConflictIfSomeVotersAreAlreadyStored() throws Exception {
        when(roleService.hasRole(voterId, Role.ADMINISTRATOR)).thenReturn(true);
        String validJson = "{\"voters\":[{\"name\":\"foo\", \"email\": \"foo@mail\"}," +
                "{\"name\":\"bar\", \"email\": \"bar@mail\"}," +
                "{\"name\":\"baz\", \"email\": \"baz@mail\"}], \"createKeys\": \"false\"}";
        when(votersService.getAllVotersByVoternames(Arrays.asList("foo","bar","baz")))
                .thenReturn(new Voters().setVoters(
                        Arrays.asList(
                            new SingleVoter().setName("foo"),
                            new SingleVoter().setName("baz")
                                )));

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.POST)
                .setUrl("/api/voters")
                .setContent(validJson)
                .setVoterId(voterId)
                .doAuthenticatedRequest();

        Assert.assertEquals("" +
                "{\"voters\":[{\"name\":\"foo\"}," +
                "{\"name\":\"baz\"}]}",response.getContentAsString());
        Assert.assertEquals(HttpStatus.CONFLICT.value(),response.getStatus());
    }

    @Test
    public void saveVoterWorksNotGenerateKey() throws Exception {
        when(roleService.hasRole(voterId, Role.ADMINISTRATOR)).thenReturn(true);
        String validJsonWithGenerateKeyFalse = "{\"voters\":[{\"name\":\"foo\", \"email\": \"foo@mail\"},{\"name\":\"bar\", \"email\": \"bar@mail\"},{\"name\":\"baz\", \"email\": \"baz@mail\"}], \"createKeys\": \"false\"}";

        when(votersService.getAllVotersByVoternames(Arrays.asList("foo","bar","baz")))
                .thenReturn(new Voters());

        when(votersService.createVoters(any(),eq(false))).thenReturn(new Voters());

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.POST)
                .setUrl("/api/voters")
                .setContent(validJsonWithGenerateKeyFalse)
                .setVoterId(voterId)
                .doAuthenticatedRequest();

        verify(votersService,times(1)).createVoters(any(),eq(false));
    }

    @Test
    public void saveVoterWorksDoGenerateKey() throws Exception {
        when(roleService.hasRole(voterId, Role.ADMINISTRATOR)).thenReturn(true);
        String validJsonWithGenerateKeyFalse = "{\"voters\":[{\"name\":\"foo\", \"email\": \"foo@mail\"},{\"name\":\"bar\", \"email\": \"bar@mail\"},{\"name\":\"baz\", \"email\": \"baz@mail\"}], \"createKeys\": \"true\"}";

        when(votersService.getAllVotersByVoternames(Arrays.asList("foo","bar","baz")))
                .thenReturn(new Voters());

        when(votersService.createVoters(any(),eq(true))).thenReturn(new Voters());

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.POST)
                .setUrl("/api/voters")
                .setContent(validJsonWithGenerateKeyFalse)
                .setVoterId(voterId)
                .doAuthenticatedRequest();

        verify(votersService,times(1)).createVoters(any(),eq(true));
    }

    @Test
    public void registrarCanAddVoters() throws Exception {
        when(roleService.hasRole(voterId, Role.REGISTRAR)).thenReturn(true);
        String validJsonWithGenerateKeyFalse = "{\"voters\":[{\"name\":\"foo\", \"email\": \"foo@mail\"},{\"name\":\"bar\", \"email\": \"bar@mail\"},{\"name\":\"baz\", \"email\": \"baz@mail\"}], \"createKeys\": \"true\"}";

        when(votersService.getAllVotersByVoternames(Arrays.asList("foo","bar","baz")))
                .thenReturn(new Voters());

        when(votersService.createVoters(any(),eq(true))).thenReturn(new Voters());

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.POST)
                .setUrl("/api/voters")
                .setContent(validJsonWithGenerateKeyFalse)
                .setVoterId(voterId)
                .doAuthenticatedRequest();

        verify(votersService,times(1)).createVoters(any(),eq(true));
    }

    @Test
    public void saveVotersWorks() throws Exception {
        when(roleService.hasRole(voterId, Role.ADMINISTRATOR)).thenReturn(true);
        String validJson = "{\"voters\":[{\"name\":\"foo\", \"email\": \"foo@mail\"},{\"name\":\"bar\", \"email\": \"bar@mail\"},{\"name\":\"baz\", \"email\": \"baz@mail\"}], \"createKeys\": \"true\"}";

        when(votersService.getAllVotersByVoternames(Arrays.asList("foo","bar","baz")))
                .thenReturn(new Voters());
        Voters voters = new Voters().setVoters(
                Arrays.asList(
                        new SingleVoter().setName("foo").setId(1L),
                        new SingleVoter().setName("bar").setId(2L),
                        new SingleVoter().setName("baz").setId(3L))
        );

        ArgumentCaptor<Voters> captor = ArgumentCaptor.forClass(Voters.class);
        ArgumentCaptor<Boolean> booleanCaptor = ArgumentCaptor.forClass(Boolean.class);

        when(votersService.createVoters(captor.capture(),booleanCaptor.capture())).thenReturn(voters);

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.POST)
                .setUrl("/api/voters")
                .setContent(validJson)
                .setVoterId(voterId)
                .doAuthenticatedRequest();

        Voters returnedVoters = captor.getValue();
        Assert.assertEquals("foo", returnedVoters.getVoters().get(0).getName());
        Assert.assertEquals("bar", returnedVoters.getVoters().get(1).getName());
        Assert.assertEquals("baz", returnedVoters.getVoters().get(2).getName());

        Assert.assertEquals(HttpStatus.CREATED.value(),response.getStatus());
        Assert.assertEquals(voters.toJson().toString(),response.getContentAsString());

        Assert.assertTrue(booleanCaptor.getValue());
    }

    @Test
    public void onlyAdminOrRegistrarUsersCanAddVoters() throws Exception {
        when(roleService.hasRole(voterId, Role.ADMINISTRATOR)).thenReturn(false);
        when(roleService.hasRole(voterId, Role.REGISTRAR)).thenReturn(false);
        String validJson = "{\"voters\":[{\"name\":\"foo\", \"email\": \"foo@mail\"},{\"name\":\"bar\", \"email\": \"bar@mail\"},{\"name\":\"baz\", \"email\": \"baz@mail\"}]}";

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.POST)
                .setUrl("/api/voters")
                .setContent(validJson)
                .setVoterId(voterId)
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.FORBIDDEN.value(),response.getStatus());
    }

    @Test
    public void return404IfKeyNotAvailable() throws Exception {
        when(votersService.getPublicKeyOfVoter(voterId)).thenReturn(Optional.empty());

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.GET)
                .setUrl("/api/voters/"+ voterId +"/publicKey")
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.NOT_FOUND.value(),response.getStatus());
    }

    @Test
    public void putMappingReturnsErrorCodeIfSomethingWentWrong() throws Exception {
        String body = new JSONObject()
                .put("publicKey", voterKey)
                .toString();

        String token = "dummyToken";
        when(mockTokenCreatorService.createTokenForUser(voterId)).thenReturn(Optional.empty());

        SingleVoter voter = new SingleVoter().setName(voterName).setId(voterId);
        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.PUT)
                .setUrl("/api/voters/"+ voterId +"/publicKey")
                .setContent(body)
                .setUser(voter)
                .doAuthenticatedRequest();

        verify(keyService,times(1)).setPublicKeyOfVoter(voterId, voterKey);
        Assert.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(),response.getStatus());
    }

    @Test
    public void putMappingWorksIfCurrentVoterHasPermissions() throws Exception {
        String body = new JSONObject()
                .put("publicKey", voterKey)
                .toString();

        String token = "dummyToken";
        when(mockTokenCreatorService.createTokenForUser(voterId)).thenReturn(Optional.of(token));

        SingleVoter voter = new SingleVoter().setName(voterName).setId(voterId);
        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.PUT)
                .setUrl("/api/voters/"+ voterId +"/publicKey")
                .setContent(body)
                .setUser(voter)
                .doAuthenticatedRequest();

        JSONObject expected = new JSONObject()
                .put("token",token);

        verify(keyService,times(1)).setPublicKeyOfVoter(voterId, voterKey);
        Assert.assertEquals(HttpStatus.CREATED.value(),response.getStatus());
        Assert.assertEquals(expected.toString(),response.getContentAsString());
    }

    @Test
    public void putMappingFailsIfNoKeySpecified() throws Exception {
        String body = new JSONObject()
                .toString();

        SingleVoter user = new SingleVoter().setName(voterName).setId(voterId);
        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.PUT)
                .setUrl("/api/voters/"+ voterId +"/publicKey")
                .setContent(body)
                .setUser(user)
                .doAuthenticatedRequest();

        verify(keyService,times(0)).setPublicKeyOfVoter(anyLong(),anyString());
        Assert.assertEquals(HttpStatus.BAD_REQUEST.value(),response.getStatus());
    }

    @Test
    public void putMappingRejectedAsNoCurrentVoterIsSpecified() throws Exception {
        String body = new JSONObject()
                .put("publicKey", voterKey)
                .toString();

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.PUT)
                .setUrl("/api/voters/"+ voterId +"/publicKey")
                .setContent(body)
                .doUnauthenticatedRequest();

        verify(keyService,times(0)).setPublicKeyOfVoter(anyLong(),anyString());
        Assert.assertEquals(HttpStatus.FORBIDDEN.value(),response.getStatus());
    }

    @Test
    public void putMappingRejectedAsItComesFromWrongVoter() throws Exception {
        String body = new JSONObject()
                .put("publicKey", voterKey)
                .toString();

        SingleVoter voter = new SingleVoter().setName(voterName).setId(voterId);
        Long otherUserId = 200L;

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.PUT)
                .setUrl("/api/voters/"+ otherUserId +"/publicKey")
                .setContent(body)
                .setUser(voter)
                .doAuthenticatedRequest();

        verify(keyService,times(0)).setPublicKeyOfVoter(anyLong(),anyString());
        Assert.assertEquals(HttpStatus.UNAUTHORIZED.value(),response.getStatus());
    }

    @Test
    public void getPrivateKeyWithVoterNotKnownReturnsUnauthorized() throws Exception {
        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.GET)
                .setUrl("/api/voters/"+ voterId +"/privateKey")
                .doUnauthenticatedRequest();

        Assert.assertEquals(HttpStatus.FORBIDDEN.value(),response.getStatus());
    }

    @Test
    public void getPrivateKeyWithWrongVoterReturnsForbidden() throws Exception {
        Long wrongVoterId = voterId + 1;

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.GET)
                .setUrl("/api/voters/"+ voterId +"/privateKey")
                .setVoterId(wrongVoterId)
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.FORBIDDEN.value(),response.getStatus());
    }

    @Test
    public void getPrivateKeyReturnsNotFoundIfNotThere() throws Exception {
        when(keyService.getPrivateKey(voterId)).thenReturn(Optional.empty());

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.GET)
                .setUrl("/api/voters/"+ voterId +"/privateKey")
                .setVoterId(voterId)
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.NOT_FOUND.value(),response.getStatus());
    }

    @Test
    public void getPrivateKeyWorksIfThere() throws Exception {
        String key = "veryVerySecret";
        when(keyService.getPrivateKey(voterId)).thenReturn(Optional.of(key));

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.GET)
                .setUrl("/api/voters/"+ voterId +"/privateKey")
                .setVoterId(voterId)
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.OK.value(),response.getStatus());
        Assert.assertEquals("{\"privateKey\":\"veryVerySecret\"}",response.getContentAsString());
    }

    @Test
    public void deleteVoterReturnsForbiddenIfCurrentVoterIsNotKnown() throws Exception {
        Long voterId = 27L;

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.DELETE)
                .setUrl("/api/voters/"+ voterId)
                .doUnauthenticatedRequest();

        Assert.assertEquals(HttpStatus.FORBIDDEN.value(),response.getStatus());
    }

    @Test
    public void deleteVoterReturnsForbiddenIfCurrentVoterIsNotAdmin() throws Exception {
        Long voterId = 27L;

        when(roleService.hasRole(voterId,Role.ADMINISTRATOR)).thenReturn(false);

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.DELETE)
                .setUrl("/api/voters/"+ voterId)
                .setVoterId(voterId)
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.FORBIDDEN.value(),response.getStatus());
    }

    @Test
    public void deleteVoterReturnsNotFoundIfNotThere() throws Exception {
        Long voterId = 27L;

        when(roleService.hasRole(voterId,Role.ADMINISTRATOR)).thenReturn(true);
        when(votersService.deleteVoter(voterId)).thenThrow(new NoSuchVoterException("dummy"));

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.DELETE)
                .setUrl("/api/voters/"+ voterId)
                .setVoterId(voterId)
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.NOT_FOUND.value(),response.getStatus());
    }

    @Test
    public void deleteVoterReturnsConflictIfStillInvolvedInVotings() throws Exception {
        Long voterId = 27L;

        when(roleService.hasRole(voterId,Role.ADMINISTRATOR)).thenReturn(true);
        when(votersService.deleteVoter(voterId)).thenThrow(new VoterInVotingException("dummy"));

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.DELETE)
                .setUrl("/api/voters/"+ voterId)
                .setVoterId(voterId)
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.CONFLICT.value(),response.getStatus());
    }

    @Test
    public void deleteVoterReturnsOkIfSuccessful() throws Exception {
        Long voterId = 27L;

        when(roleService.hasRole(voterId,Role.ADMINISTRATOR)).thenReturn(true);
        when(votersService.deleteVoter(voterId)).thenReturn(true);

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.DELETE)
                .setUrl("/api/voters/"+ voterId)
                .setVoterId(voterId)
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.OK.value(),response.getStatus());
    }

    @Test
    public void registerCurrentVoterIsForbiddenIfUserIsNotKnown() throws Exception {
        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.POST)
                .setUrl("/api/registerCurrentVoter")
                .doUnauthenticatedRequest();

        Assert.assertEquals(HttpStatus.FORBIDDEN.value(),response.getStatus());
    }

    @Test
    public void registerCurrentVoterPropagatesErrorVoterAddingFailed() throws Exception {
        Long voterId = 27L;
        String uniqueId = "uniqueId";

        when(votersService.addVoter(voterName,voterMail,uniqueId,true))
                .thenReturn(Optional.empty());

        when(roleService.addRoleToVoter(eq(voterId),any())).thenReturn(false);

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.POST)
                .setUrl("/api/registerCurrentVoter")
                .setUser(new SingleVoter().setId(voterId).setName(voterName).setEmail(voterMail).setUniqueId(uniqueId))
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.CONFLICT.value(),response.getStatus());
    }

    @Test
    public void registerCurrentVoterPropagatesErrorRoleAddingFailed() throws Exception {
        Long voterId = 27L;
        String uniqueId = "uniqueId";

        when(votersService.addVoter(voterName,voterMail,uniqueId,true))
                .thenReturn(Optional.of(new SingleVoter().setId(voterId)));

        when(roleService.addRoleToVoter(eq(voterId),any())).thenReturn(false);

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.POST)
                .setUrl("/api/registerCurrentVoter")
                .setUser(new SingleVoter().setId(voterId).setName(voterName).setEmail(voterMail).setUniqueId(uniqueId))
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(),response.getStatus());
    }

    @Test
    public void registerCurrentVoterWorks() throws Exception {
        Long voterId = 27L;
        String uniqueId = "uniqueId";

        when(votersService.addVoter(voterName,voterMail,uniqueId,true))
                .thenReturn(Optional.of(new SingleVoter().setId(voterId)));

        when(roleService.addRoleToVoter(eq(voterId),any())).thenReturn(true);

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.POST)
                .setUrl("/api/registerCurrentVoter")
                .setUser(new SingleVoter().setId(voterId).setName(voterName).setEmail(voterMail).setUniqueId(uniqueId))
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.CREATED.value(),response.getStatus());

        verify(votersService,times(1)).addVoter(voterName,voterMail,uniqueId,true);
        verify(roleService,times(1)).addRoleToVoter(voterId,Role.VOTING_ADMINISTRATOR);
        verify(roleService,times(1)).addRoleToVoter(voterId,Role.REGISTRAR);
    }
}
