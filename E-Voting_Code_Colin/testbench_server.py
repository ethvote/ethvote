from bbserver import BBserver
import json

# clear receipts
stack = []
with open("blockchain.json", "w") as outfile:
    json.dump(stack, outfile, indent=2, ensure_ascii=False)

bbserver_key_enc = open("bbserver_key.pem", "rb").read().decode("utf-8")
admin_pubkey_enc = open("admin_pubkey.pem", "rb").read().decode("utf-8")

BBserver(admin_pubkey_enc, "localhost", 1234, bbserver_key_enc).start()
