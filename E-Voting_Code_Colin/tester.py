from voter import Voter
from admin import Admin
import nacl.public
import nacl.signing
import nacl.encoding
import time

voter_sign_sk = [nacl.signing.SigningKey.generate() for _ in range(5)]
voter_sign_pk = [voter_sign_sk[i].verify_key.encode(encoder=nacl.encoding.HexEncoder) for i in range(5)]
voter_enc_sk = [nacl.public.PrivateKey.generate() for _ in range(5)]
voter_enc_pk = [voter_enc_sk[i].public_key.encode(encoder=nacl.encoding.HexEncoder) for i in range(5)]

bbserver_pubkey_enc = open("bbserver_pubkey.pem", "rb").read()
bbserver_pubkey = nacl.signing.VerifyKey(bbserver_pubkey_enc, encoder=nacl.encoding.HexEncoder)
admin_key_enc = open("admin_key.pem", "rb").read()
admin_key = nacl.signing.SigningKey(admin_key_enc, encoder=nacl.encoding.HexEncoder)
#####
# better in different script for 2 consoles.

# # clear receipts
# stack = []
# with open("blockchain.json", "w") as outfile:
#     json.dump(stack, outfile, indent=2, ensure_ascii=False)
#
# bbserver_key_enc = open("bbserver_key.pem", "rb").read().decode("utf-8")
# admin_pubkey_enc = open("admin_pubkey.pem", "rb").read().decode("utf-8")
#
# BBserver(admin_pubkey_enc, "localhost", 1234, bbserver_key_enc).start()

######

voterlist = {"1": {"nr": 1, "voterid": 101, "voter_sign_pk": voter_sign_pk[0].decode("utf-8"), "voter_enc_pk": voter_enc_pk[0].decode("utf-8")},
             "2": {"nr": 2, "voterid": 102, "voter_sign_pk": voter_sign_pk[1].decode("utf-8"), "voter_enc_pk": voter_enc_pk[1].decode("utf-8")},
             "3": {"nr": 3, "voterid": 103, "voter_sign_pk": voter_sign_pk[2].decode("utf-8"), "voter_enc_pk": voter_enc_pk[2].decode("utf-8")},
             "4": {"nr": 4, "voterid": 104, "voter_sign_pk": voter_sign_pk[3].decode("utf-8"), "voter_enc_pk": voter_enc_pk[3].decode("utf-8")},
             "5": {"nr": 5, "voterid": 105, "voter_sign_pk": voter_sign_pk[4].decode("utf-8"), "voter_enc_pk": voter_enc_pk[4].decode("utf-8")}}

votersraw = [{"voterid": 101, "voter_sign_pk": voter_sign_pk[0].decode("utf-8"), "voter_enc_pk": voter_enc_pk[0].decode("utf-8")},
             {"voterid": 102, "voter_sign_pk": voter_sign_pk[1].decode("utf-8"), "voter_enc_pk": voter_enc_pk[1].decode("utf-8")},
             {"voterid": 103, "voter_sign_pk": voter_sign_pk[2].decode("utf-8"), "voter_enc_pk": voter_enc_pk[2].decode("utf-8")},
             {"voterid": 104, "voter_sign_pk": voter_sign_pk[3].decode("utf-8"), "voter_enc_pk": voter_enc_pk[3].decode("utf-8")},
             {"voterid": 105, "voter_sign_pk": voter_sign_pk[4].decode("utf-8"), "voter_enc_pk": voter_enc_pk[4].decode("utf-8")}]

voterkeys = {"1": {"nr": 1, "voterid": 101, "voter_sign_sk": voter_sign_sk[0], "voter_enc_sk": voter_enc_sk[0]},
             "2": {"nr": 2, "voterid": 102, "voter_sign_sk": voter_sign_sk[1], "voter_enc_sk": voter_enc_sk[1]},
             "3": {"nr": 3, "voterid": 103, "voter_sign_sk": voter_sign_sk[2], "voter_enc_sk": voter_enc_sk[2]},
             "4": {"nr": 4, "voterid": 104, "voter_sign_sk": voter_sign_sk[3], "voter_enc_sk": voter_enc_sk[3]},
             "5": {"nr": 5, "voterid": 105, "voter_sign_sk": voter_sign_sk[4], "voter_enc_sk": voter_enc_sk[4]}}

settings = {"electionid": 1001,
            "mode": "ranking",
            "textoptions": ["Bill", "George", "Barack", "Donald"],
            "options": [3, 5, 7, 11],
            "thresholdfraction": 0.5,
            "p": 28716815405345534032228544684235121829153323576430273906239248066501109681081095482194640997947027327149006330209055428612405406253196014870389934056590195399279558272617767483077102977259847066561843493358656139196765129185122465953728669668583598998494439657361387793116204089330636288316420706672177731839476502118343902901125144385735823681347926912646221667290995212333474752079398825273425171467762551123619207828507902399268897366555422679142176315445851610341991658982829917860964935603100343440819635298746715726162980203283304336517566155298947636309868835883059537925411180058670877248758180567274996927399,
            "q": 14358407702672767016114272342117560914576661788215136953119624033250554840540547741097320498973513663574503165104527714306202703126598007435194967028295097699639779136308883741538551488629923533280921746679328069598382564592561232976864334834291799499247219828680693896558102044665318144158210353336088865919738251059171951450562572192867911840673963456323110833645497606166737376039699412636712585733881275561809603914253951199634448683277711339571088157722925805170995829491414958930482467801550171720409817649373357863081490101641652168258783077649473818154934417941529768962705590029335438624379090283637498463699,
            "g": 7911278167574398209492278367805015119193768492095299059364431787359756855870180333223692421336497268677193194123944583406964537607007951786048344205372365977693017710012714272903592385659410956391763371143276350802717770865015738202759101535915055887800836488847838126349897625416278819155783865396357541981471410081540213267246708492247805553381992203965146042302754281320419340173189512382237222200119781406590062666527200613976753470814679029625923321989615185102654180601474661279171019547882651527047872984625876498284091043943383003407714231853651023731708729609821186519693844212648947945706060485440198077980,
            "h": 25821554451330136806850547111468913430990586224299507309550020492668974103529772720619205691812349716943584764357187071552400567064341707902681469746533979177070754950011351489242735855740355247535897925564395699726953447712595753909498656212814265215991799243796731587905433974878208148752063540658084018823807648888534794174499184581975275369978808363244664151007227764672489140209931766792167214990387683110789521602429808193916015488806532536001436363452082210001730631209570843820375882091644016153612149523393889600100364718834716018859593474828721646837217044597054331024308075105941504671990535758465535983781}
# only needed for additive homomorphism:
# h = 25821554451330136806850547111468913430990586224299507309550020492668974103529772720619205691812349716943584764357187071552400567064341707902681469746533979177070754950011351489242735855740355247535897925564395699726953447712595753909498656212814265215991799243796731587905433974878208148752063540658084018823807648888534794174499184581975275369978808363244664151007227764672489140209931766792167214990387683110789521602429808193916015488806532536001436363452082210001730631209570843820375882091644016153612149523393889600100364718834716018859593474828721646837217044597054331024308075105941504671990535758465535983781

admin = Admin(settings, votersraw, bbserver_pubkey_enc.decode("utf-8"), admin_key_enc.decode("utf-8"))
admin.sendsetupmessage()
admin.sendvoterlist()
time.sleep(1)

##
voters = [Voter(voterlist[i]["voterid"], voterkeys[i]["voter_sign_sk"], voterkeys[i]["voter_enc_sk"]) for i in voterlist.keys()]

for i in range(5):
    voters[i].round1()


# reinstantiate all the voters so they don't need to store any data.
voters = [Voter(voterlist[i]["voterid"], voterkeys[i]["voter_sign_sk"], voterkeys[i]["voter_enc_sk"]) for i in voterlist.keys()]

# voters[0]._vote = [3, 5, 7, 11]
# voters[1]._vote = [5, 7, 11, 3]
# voters[2]._vote = [5, 3, 11, 7]
# voters[3]._vote = [11, 5, 7, 3]
# voters[4]._vote = [7, 3, 5, 11]

textvotes = [["Bill", "George", "Barack", "Donald"],
             ["George", "Barack", "Donald", "Bill"],
             ["George", "Bill", "Donald", "Barack"],
             ["Donald", "George", "Barack", "Bill"],
             ["Barack", "Bill", "George", "Donald"]]

time.sleep(3)
for i in range(5):
    voters[i].round2(textvotes[i])
time.sleep(3)

# reinstantiate all the voters so they don't need to store any data.
voters = [Voter(voterlist[i]["voterid"], voterkeys[i]["voter_sign_sk"], voterkeys[i]["voter_enc_sk"]) for i in voterlist.keys()]

for i in range(3):
    if i == 1:
        voters[i].round3(True)
    else:
        voters[i].round3()
time.sleep(3)

# reinstantiate all the voters so they don't need to store any data.
voters = [Voter(voterlist[i]["voterid"], voterkeys[i]["voter_sign_sk"], voterkeys[i]["voter_enc_sk"]) for i in voterlist.keys()]
voters[1].round4()



