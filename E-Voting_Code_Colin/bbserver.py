import socket
import nacl.signing
import nacl.encoding
import nacl.hash
import nacl.exceptions
import base64
import threading
import json
import io
import time
from utils import NIZKProof, ORProof


class BBserver:

    def __init__(self, admin_pkey, host="localhost", port=1234, skey=None):
        self._admin_pkey = nacl.signing.VerifyKey(admin_pkey.encode("utf-8"), encoder=nacl.encoding.HexEncoder)
        if skey is None:
            self._bbserver_skey = nacl.signing.SigningKey.generate()
            self._bbserver_skey_hex = self._bbserver_skey.encode(encoder=nacl.encoding.HexEncoder).decode("utf-8")
            self._bbserver_pkey = self._bbserver_skey.verify_key
            self._bbserver_pkey_hex = self._bbserver_pkey.encode(encoder=nacl.encoding.HexEncoder).decode("utf-8")
        else:
            self._bbserver_skey = nacl.signing.SigningKey(skey.encode("utf-8"), encoder=nacl.encoding.HexEncoder)
            self._bbserver_skey_hex = skey
            self._bbserver_pkey = self._bbserver_skey.verify_key
            self._bbserver_pkey_hex = self._bbserver_pkey.encode(encoder=nacl.encoding.HexEncoder).decode("utf-8")

        self._questions = 4  # amount of questions on the ballot (int)
        self._database = []
        self._unconfirmed_messages = []  # messages that have not yet been posted to the blockchain
        self._lastreceipt = 0  # todo: index of last processed receipt in blockchain. search from there for new receipts

        self._sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._host = host
        self._port = port
        self._sock.bind((self._host, self._port))

    def get_bbserver_pkey(self):
        return self._bbserver_pkey_hex

    def set_bbserver_skey(self, key):
        """
        :param key: bbserver_skey as hex encoded str
        """
        self._bbserver_skey = nacl.signing.SigningKey(key.encode("utf-8"), encoder=nacl.encoding.HexEncoder)
        self._bbserver_skey_hex = key
        self._bbserver_pkey = self._bbserver_skey.verify_key
        self._bbserver_pkey_hex = self._bbserver_pkey.encode(encoder=nacl.encoding.HexEncoder).decode("utf-8")

    @staticmethod
    def _appendmessage(message):
        with open("msgstack.json") as infile:
            stack = json.load(infile)
        if not stack:
            lastnr = 0
        else:
            lastnr = stack[-1]["nr"]
        message["nr"] = lastnr + 1
        print(message)
        stack.append(message)
        with open("msgstack.json", "w") as outfile:
            json.dump(stack, outfile, indent=2, ensure_ascii=False)

    @staticmethod
    def clearmessagestack():
        stack = []
        with open("msgstack.json", "w") as outfile:
            json.dump(stack, outfile, indent=2, ensure_ascii=False)

    def _verify_receipt(self, receipt):
        """
        verifies if the signature of a message receipt is correct
        :param receipt: hexstring of hash+signature
        :return: True if receipt is valid, False otherwise
        """
        if len(receipt) != 192:
            return False
        h_hex, sig_hex = receipt[:64], receipt[64:]
        h = h_hex.encode("utf-8")
        sig = bytes.fromhex(sig_hex)
        try:
            self._bbserver_skey.verify_key.verify(h, sig)
            return True
        except (ValueError, TypeError, nacl.exceptions.BadSignatureError):
            return False

    def _findnewreceipts(self):
        """
        searches blockchain for new hash/signature pairs
        todo: implement for real blockchain. could search e.g. by userids of voters
        :return: list of (verified) hash+signature hexstrings
        """
        with open("blockchain.json") as infile:
            blockchainstack = json.load(infile)
        receipts = blockchainstack[self._lastreceipt:]
        self._lastreceipt += len(receipts)
        receipts = [i for i in receipts if self._verify_receipt(i)]
        return receipts

    def _confirm_messages(self):
        """
        keeps searching for new receipts and appends new confirmed messages to messagestack
        """
        while 1:
            receipts = self._findnewreceipts()
            for receipt in receipts:
                h_hex = receipt[:64]
                h = h_hex.encode("utf-8")
                found = False
                for message in self._unconfirmed_messages:
                    if h == message["h"]:
                        self._unconfirmed_messages.remove(message)
                        message.pop("h")
                        if message["type"] == "setup" and message["origin"] == "admin":
                            self.clearmessagestack()
                        # todo: pointer to receipt in blockchain
                        self._appendmessage(message)
                        found = True
                        break
                if not found:
                    print("\x1b[6;30;41m" + "Receipt without message!" + "\x1b[0m")
            time.sleep(.1)

    def writemessages(self):
        print("starting writemessages")
        t = threading.Thread(target=self._confirm_messages)
        t.start()

    def start(self):
        self.writemessages()
        self.startlistening()

    def _builddatabase(self, msg):
        """
        Builds voter database from messages. If the server only acts as a bulletin board, only voterlist is relevant.
        Rest is just in case the server wants to run tests/verify proofs.
        :return: database (dict)
        """
        dbase = self._database

        if msg["type"] == "voterlist":
            dbase = json.loads(msg["content"])

        elif msg["type"] == "round1":
            dbase[str(msg["origin"])]["commits"] = json.loads(msg["content"])[1]

        elif msg["type"] == "round2":
            dbase[str(msg["origin"])]["ballot"] = json.loads(msg["content"])["ballot"]
            dbase[str(msg["origin"])]["encryptionproof"] = []
            for i in range(self._questions):
                dbase[str(msg["origin"])]["encryptionproof"].append(
                    ORProof.from_dict(json.loads(msg["content"])["proof"][0][i]))
            dbase[str(msg["origin"])]["prodproof"] = NIZKProof.from_dict(json.loads(msg["content"])["proof"][1])

        elif msg["type"] == "round3":
            dbase[str(msg["origin"])]["partdec"] = json.loads(msg["content"])["dec"]
            dbase[str(msg["origin"])]["partdecproof"] = []
            for i in range(self._questions):
                dbase[str(msg["origin"])]["partdecproof"].append(
                    NIZKProof.from_dict(json.loads(msg["content"])["proof"][i]))

        return dbase

    def startlistening(self):
        print("server started and listening")
        threading.Thread(target=self._listen).start()

    def _listen(self):
        self._sock.listen(5)
        while 1:
            clientsocket, address = self._sock.accept()
            clientsocket.settimeout(60)
            threading.Thread(target=self._conversation, args=(clientsocket, address)).start()

    def _conversation(self, client, address):
        chunksize = 1024
        while 1:
            buf = client.recv(chunksize)
            l = []
            while buf:
                print("Receiving...")
                l.append(buf.decode("utf-8"))
                buf = client.recv(chunksize)
            message = "".join(l)
            print("Done Receiving")
            message_dec = json.loads(message)
            message_dec_nosig = message_dec
            # verify signature of received message
            recsig = message_dec_nosig.pop("signature")
            recsig = base64.b64decode(recsig)
            if message_dec_nosig["origin"] == "admin":
                sig_verify_key = self._admin_pkey
            else:
                sig_verify_key = nacl.signing.VerifyKey(self._database[str(message_dec_nosig["origin"])]
                                                        ["voter_sign_pk"], encoder=nacl.encoding.HexEncoder)
            h = nacl.hash.sha256(json.dumps(message_dec_nosig).encode("utf-8"))
            try:
                sig_verify_key.verify(h, recsig)
            except (ValueError, TypeError, nacl.exceptions.BadSignatureError):
                print("Invalid Signature!")

            h = nacl.hash.sha256(message.encode("utf-8"))
            message_dec["h"] = h
            self._unconfirmed_messages.append(message_dec)  # Possibly need to synchronize
            self._database = self._builddatabase(message_dec_nosig)  # Possibly need to synchronize _builddatabase

            # sign (full) message and send signature back
            sendsig = self._bbserver_skey.sign(h).signature
            sendsig_b64 = base64.b64encode(sendsig)

            print('Sending...')
            file = io.BytesIO(sendsig_b64)
            sbuf = file.read(chunksize)
            while sbuf:
                print('Sending...')
                client.send(sbuf)
                sbuf = file.read(chunksize)
            print("Done Sending")
            client.close()
            break
