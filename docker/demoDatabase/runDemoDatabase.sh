sudo docker stop demoDatabaseContainer
sudo docker rm demoDatabaseContainer

sudo docker stop demoDatabaseTestContainer
sudo docker rm demoDatabaseTestContainer

sudo docker rmi mysql

sudo docker volume ls -qf dangling=true
sudo docker volume rm $(sudo docker volume ls -qf dangling=true)

sudo docker run -d \
      -p 3316:3306 \
     --name demoDatabaseContainer \
     -e MYSQL_ROOT_PASSWORD=root123 \
     -e MYSQL_DATABASE=evoting_app_db \
     -e MYSQL_USER=app_user \
     -e MYSQL_PASSWORD=test123 \
        mysql:latest

        sudo docker run -d \
              -p 3317:3306 \
             --name demoDatabaseTestContainer \
             -e MYSQL_ROOT_PASSWORD=root123 \
             -e MYSQL_DATABASE=evoting_test_db \
             -e MYSQL_USER=app_user \
             -e MYSQL_PASSWORD=test123 \
                mysql:latest

sleep 10

