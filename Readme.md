INSTALLATION GUIDE:

====Step 0: Install Java (if not already installed)====

====Step 1: Install docker====

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable
sudo apt-get update
apt-cache policy docker-ce
sudo apt-get install -y docker-ce

To test if it worked, run
docker run hello-world

====Step 2: Install Apache HTTP server====

Depends on the operating system,

https://httpd.apache.org/

provides good manuals

====Step 3: Install Shibboleth====

Good instructions can be found at

https://www.switch.ch/aai/guides/sp/installation/

====Step 4: Configure Shibboleth====

Follow the tutorial on

https://www.switch.ch/aai/guides/sp/configuration/

The configuration files can be found in this repository at configFiles/shibboleth

====Step 5: Configure Apache====

In order that the apache configuration works, some modules have to be activated. This can be done
by executing the following statements:

sudo a2enmod proxy
sudo a2enmod proxy_http
sudo a2enmod proxy_ajp
sudo a2enmod shib

Additionally, to turn on ssl, run:

sudo a2ensite default-ssl

The configuration files can be found at

/configFiles/shibboleth

Make sure the firewall is adapted to your needs, to test if the tool works withour firewall you can
execute the statement

sudo ufw disable

and then start to build your configuration.

GRADLE TASKS:

The following commands can be used to setup the components

===Production version (version which relies on shibboleth)===

sudo ./gradlew buildProdFrontend

    Runs the production frontend on the local machine (angular has to be installed on the machine)

sudo ./gradlew runProdDockerFrontend

    Runs the production frontend in a docker container (no further setup required)

sudo ./gradlew runProdBackend

    Runs the production frontend in a docker container (no further setup required)

sudo ./gradlew runProdDockerBackend

    Runs the production backend in a docker container (no further setup required)

sudo ./gradlew runDockerProdDatabase

   Runs the production database in a docker container (no further setup required)

===Demo version (version with fake authentication service)===

sudo ./gradlew buildDemoFrontend

    Runs the demonstration frontend on the local machine (angular has to be installed on the machine)

sudo ./gradlew runDemoDockerFrontend

    Runs the demonstration frontend in a docker container (no further setup required)

sudo ./gradlew runDemoBackend

    Runs the demonstration frontend in a docker container (no further setup required)

sudo ./gradlew runDemoDockerBackend

    Runs the demonstration backend in a docker container (no further setup required)

sudo ./gradlew runDockerDemoDatabase

   Runs the demonstration database in a docker container (no further setup required)

sudp ./gradlew sonarqube

    Runs the sonar static analyzer (needs instance of sonarqube to run on the machine)


RUN ETHVote

sudo ./gradlew runDockerProdDatabase
sudo ./gradlew runProdDockerBackend
sudo ./gradlew runProdDockerFrontend


ADDITIONAL COMMANDS:

To tear down all docker containers, one can use

sudo docker kill $(docker ps -q)
sudo docker rm $(docker ps -a -q)
sudo docker rmi -f $(docker images -q)
