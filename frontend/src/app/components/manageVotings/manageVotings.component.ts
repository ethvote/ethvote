import {Component, OnInit, ViewChild} from '@angular/core';
import {VotingsDataService} from '../../services/communication/votingsData/votingsData.service';
import {Maybe} from '../../utils/maybe.utils';
import {VotingsArray} from '../../models/votings/votingsArray.json.model';
import {VotingOverview} from '../../models/votings/votingOverview.json.model';
import {VotingProgressService} from '../../services/communication/votingProgress/votingProgress.service';
import {MatDialog, MatDialogConfig, MatExpansionPanel, MatMenuTrigger, MatSnackBar} from '@angular/material';
import {VotingProgress} from '../../models/votings/votingProgress.json.model';
import {StartVotingComponent} from '../startVoting/startVoting.component';
import {ActivatedRoute} from '@angular/router';
import {DecryptionFactors} from '../../models/decryptionFactors/decryptionFactors.model';
import {LagrangePolynom} from '../../data/lagrangePolynom';
import {DecryptionFactorsService} from '../../services/communication/decryptionFactors/decryptionFactors.service';
import {VoteService} from '../../services/communication/vote/vote.service';
import {LagrangeService} from '../../services/lagrange/lagrange.service';
import {MathService} from '../../services/math/math.service';
import {PrimeFactorizationService} from '../../services/primeFactorization/primeFactorization.service';
import {VotingOption} from '../../models/votings/votingOption.json.model';
import {ShowResultsComponent} from '../dialogs/showResults/showResults.component';
import {SingleVoterProgress} from '../../models/votings/singleVoterProgress';
import {Pair} from '../../utils/pair.utils';
import {VotingSummaryJsonParser} from '../../services/jsonParser/votingSummaryJsonParser.service';
import {VotingSummary} from '../../models/votingSummary/votingSummary.json.model';
import {Votes} from '../../models/vote/votes.json.model';
import {DataForKeyCalculation} from '../../models/dataForKeyCalculation/dataForKeyCalculation.model';
import {VotingCalculationParameters} from '../../models/votings/votingsCalculationParameters.json.model';
import {SingleDecryptionFactor} from '../../models/decryptionFactors/singleDecryptionFactor.model';
import {SingleVote} from '../../models/vote/singleVote.json.model';
import {VoteHelperService} from '../../services/vote/voteHelper.service';

@Component({
  selector: 'app-manage-votings',
  templateUrl: './manageVotings.component.html',
  styleUrls: ['./manageVotings.component.css']
})
export class ManageVotingsComponent implements OnInit {

  @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;

  showSpinner = false;
  spinnerDescription = 'Publishing to ethereum network ...';

  votings: VotingOverview[] = [];
  votingProgress: SingleVoterProgress[] = [];

  votersWhichParticipatedInKeyGenerationNumber = 0;
  votersWhichVotedNumber = 0;
  votersWhichParticipatedInDecryptionNumber = 0;

  securityThreshold = -1;
  transactionId = '';
  viewTransactionUrl = '';

  votingId = -1;
  votingResult: number[];

  phaseNumberOfSelected = -1;
  endTimeOfSelected = -1;

  votingOptionsOfSelected: VotingOption[];

  isExpansionPanelOpen = false;

  notifyTrusteesOnKeyGenerationStarts =  true;
  notifyTrusteesOnDecryptionStarts =  true;

  constructor(private votingsDataService: VotingsDataService,
              private votingProgressService: VotingProgressService,
              private decryptionFactorService: DecryptionFactorsService,
              private voteService: VoteService,
              private voteHelperService: VoteHelperService,
              private lagrangeService: LagrangeService,
              private mathService: MathService,
              private primeFactorizationService: PrimeFactorizationService,
              private votingSummaryJsonParser: VotingSummaryJsonParser,
              private dialog: MatDialog,
              private snackBar: MatSnackBar,
              private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.updateData(undefined);
  }

  updateData(votingIdToInspect: number) {
    this.votingsDataService.getCreatedVotings().subscribe(data => this.onCreatedVotingsReceived(data, votingIdToInspect));
  }


  onCreatedVotingsReceived(data: Maybe<VotingsArray>, votingId: Number) {
    this.showSpinner = false;
    this.votings = data.getData().getVotings();

    if (votingId !== undefined) {
      for (const voting of this.votings) {
        if (voting.getId() === +votingId) {

          this.onSelectVoting(voting);
          break;
        }
      }
    }

  }

  onExpandPanel(matExpansionPanel: MatExpansionPanel, event: Event): void {
    if (matExpansionPanel._getExpandedState() === 'expanded') {
      event.stopPropagation();

      matExpansionPanel.close();
      this.trigger.openMenu();
    }
  }

  onSelectVoting(voting: VotingOverview) {
    this.isExpansionPanelOpen = true;

    this.votingProgressService.getVotingProgress(voting.getId().valueOf()).subscribe(
        data => this.onVotingProgressDataReceived(data, voting)
    );
  }

  onVotingProgressDataReceived(data: Maybe<VotingProgress>, voting: VotingOverview) {
    this.securityThreshold = data.getData().getSecurityThreshold();
    this.transactionId = data.getData().getTransactionId();
    this.viewTransactionUrl = 'https://ropsten.etherscan.io/tx/' + this.transactionId;

    this.votingProgress = data.getData().getVotersProgress();
    this.votingId = voting.getId().valueOf();

    this.phaseNumberOfSelected = voting.getPhaseNumber();
    this.endTimeOfSelected = voting.getTimeVotingIsFinished();
    this.votingOptionsOfSelected = voting.getOptions();

    this.votingResult = new Array(this.votingOptionsOfSelected.length);

    this.votersWhichParticipatedInKeyGenerationNumber = 0;
    this.votersWhichVotedNumber = 0;
    this.votersWhichParticipatedInDecryptionNumber = 0;

    for (const singleVoterProgress of this.votingProgress) {
      if (singleVoterProgress.getParticipatedInKeyGeneration()) {
        this.votersWhichParticipatedInKeyGenerationNumber++;
      }
      if (singleVoterProgress.getParticipatedInVoting()) {
        this.votersWhichVotedNumber++;
      }
      if (singleVoterProgress.getParticipatedInDecryption()) {
        this.votersWhichParticipatedInDecryptionNumber++;
      }
    }

    if (this.phaseNumberOfSelected === 4) {
      this.votingsDataService.getVotingSummary(this.votingId).subscribe(text => this.onVotingSummaryReceived(text));
    }

  }

  onVotingSummaryReceived(maybeVotingSummary: Maybe<string>) {
    const summaryText = maybeVotingSummary.getData();
    const maybeSummary: Maybe<VotingSummary> = this.votingSummaryJsonParser.parseJson(summaryText);
    const votingSummary: VotingSummary = maybeSummary.getData();

    const votingCalculationParameters: VotingCalculationParameters = votingSummary.getMetaData().getVotingCalculationParameters();

    const allVotes: SingleVote[] = [];
    for (const singleVote of votingSummary.getVotePhase()) {
      allVotes.push(singleVote.getVote());
    }

    const decryptionFactors: SingleDecryptionFactor[] = [];
    for (const decryptionFactor of votingSummary.getDecryptionFactors()) {
      decryptionFactors.push(decryptionFactor.getDecryptionFactor());
    }

    this.votingResult = this.voteHelperService.calculateVotingOutCome(votingCalculationParameters, allVotes, decryptionFactors, this.votingOptionsOfSelected.length);
  }

  onStartKeyGenerationClicked() {
    this.votingsDataService.setPhaseNumber(this.votingId, 1, this.notifyTrusteesOnKeyGenerationStarts).subscribe(d => this.updateData(this.votingId));
  }

  onStartVotingClicked() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.width = '360px';
    dialogConfig.data = {
      votingId: this.votingId
    };

    const dialogRef = this.dialog.open(StartVotingComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => this.onStartVotingDialogClosed(result));
  }

  onStartVotingDialogClosed(data) {
    if (data === undefined) {
      return;
    }

    const voting: VotingOverview = data['voting'];
    const phaseNumber = data['phaseNumber'];

    if (voting !== undefined && phaseNumber === 2) {
      this.phaseNumberOfSelected = 2;
      this.updateData(voting.getId().valueOf());
    }
  }

  onCreatedVotingEvent(newVotingId: number) {
    this.votingsDataService.getCreatedVotings().subscribe(data => this.onCreatedVotingsReceived(data, newVotingId));
  }

  closeVoting() {
    this.votingsDataService.setPhaseNumber(this.votingId, 3, this.notifyTrusteesOnDecryptionStarts).subscribe(d => this.updateData(this.votingId));
  }

  // counting

  count() {
    this.decryptionFactorService.getAllDecryptionFactors(this.votingId).subscribe(d => this.onDecryptionFactorsFetched(d));
  }

  onDecryptionFactorsFetched(decryptionFactor: Maybe<DecryptionFactors>) {
    this.voteService.getDataForKeyCalculation(this.votingId).subscribe(data => this.onDataFetched(data, decryptionFactor));
  }

  onDataFetched(dataForKeyCalculation: Maybe<DataForKeyCalculation>, decFactors: Maybe<DecryptionFactors>) {
    this.voteService.getVotes(this.votingId).subscribe(votes => this.onComplete(votes, dataForKeyCalculation, decFactors));
  }

  onComplete(votes: Maybe<Votes>, dataForKeyCalculation: Maybe<DataForKeyCalculation>, decFactors: Maybe<DecryptionFactors>) {
    const votingCalculationParameters: VotingCalculationParameters = dataForKeyCalculation.getData().getCalculationParameters();
    const votesArray = votes.getData().getVotes();
    const decryptionFactors = decFactors.getData().getDecryptionFactors();

    const voteResult = this.voteHelperService.calculateVotingOutCome(votingCalculationParameters, votesArray, decryptionFactors, this.votingOptionsOfSelected.length);

    this.openDialog(this.votingOptionsOfSelected, voteResult);
  }

  openDialog(options: VotingOption[], result: number[]) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      votingOptions: options,
      result: result,
    };

    this.dialog.open(ShowResultsComponent, dialogConfig);
  }

  // publish voting

  publishVoting() {
    this.showSpinner = true;
    this.votingsDataService.setPhaseNumber(this.votingId, 4, false).subscribe(d => this.updateData(this.votingId));
  }

  deleteVoting(votingId: number) {
    this.votingsDataService.deleteVoting(votingId).subscribe(data => this.onDeleteVotingReturned(data));
  }

  onDeleteVotingReturned(response: Pair<boolean, string>) {
    if (response.getFirst()) {
      this.showSuccess(response.getSecond());
      this.isExpansionPanelOpen = false;
      this.updateData(undefined);
    }
    else {
      this.showFailure(response.getSecond());
    }
  }

  private showSuccess(successMessage: string) {
    this.snackBar.open(successMessage, '', {
      duration: 2000,
      panelClass: ['green-snackbar']
    });
  }

  private showFailure(failureMessage: string) {
    this.snackBar.open(failureMessage, '', {
      duration: 2000,
      panelClass: ['red-snackbar']
    });
  }

}
