import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {VoterArray} from '../../../models/voters/voterArray.model';

@Component({
  selector: 'app-add-voter-failed-dialog',
  templateUrl: './addVoterFailed.component.html',
  styleUrls: ['./addVoterFailed.component.css']
})
export class AddVoterFailedComponent implements OnInit {

  alreadyExistingVoters: VoterArray;

  constructor(public dialogRef: MatDialogRef<AddVoterFailedComponent>,
              @Inject(MAT_DIALOG_DATA) private data: { alreadyExistingVoters: VoterArray } ) {
    this.alreadyExistingVoters = data.alreadyExistingVoters;
  }

  ngOnInit() {
  }

  deleteExistingVotersFromList() {
    this.dialogRef.close('delete');
  }

}
