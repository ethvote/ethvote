import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {VotingOverview} from '../../../models/votings/votingOverview.json.model';
import {VotingOfficials} from '../../../models/votings/votingOfficials.json.model';

@Component({
  selector: 'app-add-voter-failed-dialog',
  templateUrl: './votingInformation.component.html',
  styleUrls: ['./votingInformation.component.css']
})
export class VotingInformationComponent implements OnInit {

  voting: VotingOverview;
  votingOfficials: VotingOfficials;

  constructor(public dialogRef: MatDialogRef<VotingInformationComponent>,
              @Inject(MAT_DIALOG_DATA) private data: { voting: VotingOverview, votingOfficials: VotingOfficials } ) {
    this.voting = data.voting;
    this.votingOfficials = data.votingOfficials;
  }

  ngOnInit() {
    console.log(this.voting);
    console.log(this.votingOfficials);
  }

}
