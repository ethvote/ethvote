import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';
import {VotingOption} from '../../../models/votings/votingOption.json.model';

@Component({
  selector: 'app-show-results',
  templateUrl: './showResults.component.html',
  styleUrls: ['./showResults.component.css']
})
export class ShowResultsComponent implements OnInit {

  votingOptions: VotingOption[];
  result: number[];

  constructor(@Inject(MAT_DIALOG_DATA) private data: { votingOptions: VotingOption[], result: number[] } ) {
    this.votingOptions = data.votingOptions;
    this.result = data.result;
  }

  ngOnInit() {
    console.log(this.data.votingOptions);
    console.log(this.data.result);
  }

}
