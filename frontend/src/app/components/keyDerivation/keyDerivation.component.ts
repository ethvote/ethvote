import {Component, OnInit} from '@angular/core';
import {VotingsDataService} from '../../services/communication/votingsData/votingsData.service';
import {VotingOverview} from '../../models/votings/votingOverview.json.model';
import {Maybe} from '../../utils/maybe.utils';
import {VoteService} from '../../services/communication/vote/vote.service';
import {DataForKeyCalculation} from '../../models/dataForKeyCalculation/dataForKeyCalculation.model';
import {EligibleVoterWithKey} from '../../models/dataForKeyCalculation/eligibleVoterWithKey.model';
import {Polynom} from '../../data/polynom';
import {SingleCommitment} from '../../models/commitments/singleCommitment.model';
import {CommitmentFromVoter} from '../../models/commitments/commitmentFromVoter.model';
import {SecretSharesFromVoter} from '../../models/secretShares/secretSharesFromVoter.model';
import {SingleSecretShareFromVoter} from '../../models/secretShares/singleSecretShareFromVoter.model';
import {forkJoin, Observable, of} from 'rxjs';
import {MathService} from '../../services/math/math.service';
import {CurrentUserService} from '../../services/currentUser/currentUser.service';
import {VoteHelperService} from '../../services/vote/voteHelper.service';
import {CommitmentsService} from '../../services/communication/commitments/commitments.service';
import {SecretSharesService} from '../../services/communication/secretShares/secretShares.service';
import {MatDialog, MatDialogConfig, MatSnackBar, MatTableDataSource} from '@angular/material';
import {VotingsWithProgressArray} from '../../models/votings/votingsWithProgressArray.json.model';
import {VotingVoterProgress} from '../../models/votings/votingVoterProgress';
import {Pair} from '../../utils/pair.utils';
import {VotingInformationComponent} from '../dialogs/votingInformation/votingInformation.component';
import {flatMap} from 'rxjs/operators';
import {PrivateKeyService} from '../../services/privateKey/privateKey.service';
import {CryptoService} from '../../services/crypto/crypto.service';
import {VotingOfficials} from '../../models/votings/votingOfficials.json.model';

@Component({
  selector: 'app-key-derivation',
  templateUrl: './keyDerivation.component.html',
  styleUrls: ['./keyDerivation.component.css']
})
export class KeyDerivationComponent implements OnInit {

  showSpinner = false;
  spinnerDescription = '';

  displayedVotings: Array<VotingOverview> = [];
  statusMap: { [id: number]: VotingVoterProgress; } = {};
  votingOfficialsMap: { [id: number]: VotingOfficials; } = {};
  textMap: { [id: number]: String; } = {};

  displayedColumns: string[] = ['Title', 'Description', 'Generate'];
  dataSource: MatTableDataSource<VotingOverview>;

  constructor(private votingsDataService: VotingsDataService,
              private voteService: VoteService,
              private mathService: MathService,
              private currentUserService: CurrentUserService,
              private voteHelperService: VoteHelperService,
              private commitmentService: CommitmentsService,
              private secretSharesService: SecretSharesService,
              private privateKeyService: PrivateKeyService,
              private cryptoService: CryptoService,
              private dialog: MatDialog,
              private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.dataSource = new MatTableDataSource();
    this.votingsDataService.getCurrentVotings(1).subscribe(data => this.onVotingsDataReceived(data));
  }

  onVotingsDataReceived(votings: Maybe<VotingsWithProgressArray>) {
    this.displayedVotings = votings.getData().getVotings();
    this.dataSource.data = votings.getData().getVotings();

    for (const singleVoting of this.displayedVotings) {
      let text: String = singleVoting.getDescription();
      text += '\n\nPossible choices:\n\n';

      const optionTextArray = singleVoting.getOptions().map(v => v.getOptionName());
      const optionText = optionTextArray.join(' | ');
      text += optionText;

      this.textMap[singleVoting.getId().valueOf()] = text;
    }

    this.evaluateVotingsProgressData(votings.getData().getVotingsVoterProgress());
    this.evaluateVotingOfficialsData(votings.getData().getVotingOfficials());
  }

  evaluateVotingsProgressData(progress: VotingVoterProgress[]) {
    for (const singleProgress of progress) {
      this.statusMap[singleProgress.getVotingId().valueOf()] = singleProgress;
    }
  }

  evaluateVotingOfficialsData(votingOfficials: VotingOfficials[]) {
    for (const votingOffical of votingOfficials) {
      this.votingOfficialsMap[votingOffical.getVotingId().valueOf()] = votingOffical;
    }
  }

  onGenerateKeyClicked(votingId: number) {
    this.spinnerDescription = 'Loading data ...';
    this.showSpinner = true;

    const currentVoterId = this.currentUserService.getUserId().getData();
    const result = this.voteService.getDataForKeyCalculation(votingId)
        .pipe(flatMap(maybeKey => this.getPrivateKey(maybeKey, currentVoterId)))
        .pipe(flatMap(data => this.calculateSharesAndSecrets(data)))
        .pipe(flatMap(data => this.sendData(data, votingId, currentVoterId)));

    result.subscribe(res => this.showMessage(res));
  }

  private showMessage(res: Pair<boolean, string>) {
    this.showSpinner = false;

    if (res.getFirst()) {
      this.showSuccessfulSnackBar('Sending key generation data worked');
    }
    else {
      this.showErrorSnackBar(res.getSecond());
    }

    this.votingsDataService.getCurrentVotings(1).subscribe(data => this.onVotingsDataReceived(data));
  }

  private getPrivateKey(maybeKeyCalculationsData: Maybe<DataForKeyCalculation>, currentVoterId: number): Observable<Maybe<Pair<DataForKeyCalculation, string>>> {
    if (maybeKeyCalculationsData.isEmpty()) {
      return of(Maybe.fromEmpty());
    }

    this.spinnerDescription = 'Loading key ...';

    const keyCalculationsData = maybeKeyCalculationsData.getData();

    const ret = this.privateKeyService.getPrivateKey(keyCalculationsData, currentVoterId).pipe(flatMap(res => this.aggregate(keyCalculationsData, res)));
    return ret;
  }

  private aggregate(data: DataForKeyCalculation, maybePrivateKey: Maybe<string>): Observable<Maybe<Pair<DataForKeyCalculation, string>>> {
    if (maybePrivateKey.isEmpty()) {
      return of(Maybe.fromEmpty());
    }
    return of(Maybe.fromData(new Pair(data, maybePrivateKey.getData())));
  }

  private calculateSharesAndSecrets(data: Maybe<Pair<DataForKeyCalculation, string>>): Observable<Maybe<Pair<CommitmentFromVoter, SecretSharesFromVoter>>> {
    if (data.isEmpty()) {
      return of(Maybe.fromEmpty());
    }

    this.spinnerDescription = 'Calculating secret shares key ...';

    const keyCalculationsData = data.getData().getFirst();
    const privateKey = data.getData().getSecond();

    const securityThreshold: number = keyCalculationsData.getSecurityThreshold();
    const primeQ: string = keyCalculationsData.getCalculationParameters().getPrimeQ();

    const polynom: Polynom = this.mathService.generateRandomPolynomial(securityThreshold, primeQ);
    const commitments: CommitmentFromVoter = this.calculateCommitments(keyCalculationsData, polynom, privateKey);
    const secretShares: SecretSharesFromVoter = this.calculateSecretShares(keyCalculationsData, polynom, privateKey);

    return of(Maybe.fromData(new Pair(commitments, secretShares)));
  }

  private sendData(data: Maybe<Pair<CommitmentFromVoter, SecretSharesFromVoter>>, votingId: number, currentVoterId: number): Observable<Pair<boolean, string>> {
    if (data.isEmpty()) {
      return of(new Pair(false, 'Data to send could not be calculated.'));
    }

    this.spinnerDescription = 'Sending shares ...';

    const observableResponseFromStoreCommitments = this.commitmentService.storeCommitments(votingId, currentVoterId, data.getData().getFirst());
    const observableResponseFromSecretShares = this.secretSharesService.storeSecretShares(votingId, currentVoterId, data.getData().getSecond());

    const ret = forkJoin(
        observableResponseFromStoreCommitments,
        observableResponseFromSecretShares,
    ).pipe(flatMap(res => this.evaluate(res)));
    return ret;
  }

  private evaluate(responses: any[]): Observable<Pair<boolean, string>> {
    const responseFromCommitments: Pair<boolean, string> = responses[0];
    const responseFromSecretShares: Pair<boolean, string> = responses[1];

    if (!responseFromCommitments.getFirst()) {
      return of(new Pair(false, responseFromCommitments.getSecond()));
    }
    if (!responseFromSecretShares.getFirst()) {
      return of(new Pair(false, responseFromSecretShares.getSecond()));
    }
    return of(new Pair(true, null));
  }

  showMoreInformation(voting: VotingOverview) {
    const dialogConfig = new MatDialogConfig();

    const votingOfficials: VotingOfficials = this.votingOfficialsMap[voting.getId().valueOf()];

    dialogConfig.width = '90vw';
    dialogConfig.maxHeight = '80vh';
    dialogConfig.data = {
      voting: voting,
      votingOfficials: votingOfficials
    };

    this.dialog.open(VotingInformationComponent, dialogConfig);

  }

  private calculateCommitments(keyCalculationsData: DataForKeyCalculation, polynom: Polynom, privateKey: string): CommitmentFromVoter {
    const securityThreshold = keyCalculationsData.getSecurityThreshold();
    const generatorG = keyCalculationsData.getCalculationParameters().getGeneratorG();
    const primeP = keyCalculationsData.getCalculationParameters().getPrimeP();

    const singleCommitments: SingleCommitment[] = new Array(securityThreshold);

    for (let i = 0; i < securityThreshold; i++) {
      const coefficientNumber = i;
      const commitmentValue = this.mathService.powerWithModulo(generatorG, polynom.getCoefficients()[coefficientNumber], primeP);
      const messageToSign = coefficientNumber + ',' + commitmentValue;
      const signature = this.cryptoService.signSHA256(privateKey, messageToSign).getData();

      const singleCommitment = new SingleCommitment()
          .setCoefficientNumber(coefficientNumber)
          .setCommitment(commitmentValue)
          .setSignature(signature);

      singleCommitments[i] = singleCommitment;
    }

    const commitments: CommitmentFromVoter = new CommitmentFromVoter().setCommitments(singleCommitments);
    return commitments;
  }

  private calculateSecretShares(keyCalculationsData: DataForKeyCalculation, polynom: Polynom, privateKey: string): SecretSharesFromVoter {
    const numberOfEligibleVoters = keyCalculationsData.getNumberOfEligibleVoters();

    const currentVoterName = this.currentUserService.getVoterName().getData();
    const currentVoterId = this.currentUserService.getUserId().getData();

    const secretShares: SecretSharesFromVoter = new SecretSharesFromVoter(currentVoterId, currentVoterName);
    const eligibleVoters: EligibleVoterWithKey[] = keyCalculationsData.getEligibleVoters();

    for (let i = 0; i < numberOfEligibleVoters; i++) {
      const eligibleVoter: EligibleVoterWithKey = eligibleVoters[i];

      const voterId = eligibleVoter.getId();
      const voterName = eligibleVoter.getName();
      const voterPublicKey = eligibleVoter.getPublicKey();
      const isTrustee = eligibleVoter.getIsTrustee();

      if (!isTrustee) {
        continue;
      }

      const positionToEvaluatePolynomial = i + 1;
      const polynomValue = this.mathService.evaluatePolynomial(polynom, '' + positionToEvaluatePolynomial);

      const encryptedValues = this.voteHelperService.encryptPolynomPoint(polynomValue, voterPublicKey);
      const concattedValue = encryptedValues.join(',');
      const signature = this.cryptoService.signSHA256(privateKey, concattedValue).getData();

      const singleSecretShare = new SingleSecretShareFromVoter(voterId, voterName, encryptedValues, signature);
      secretShares.addSecretShare(singleSecretShare);
    }
    return secretShares;
  }

  private showSuccessfulSnackBar(message: string) {
    this.snackBar.open(message, '', {
      duration: 2000,
      panelClass: ['green-snackbar']
    });
  }

  private showErrorSnackBar(message: string) {
    this.snackBar.open(message, '', {
      duration: 2000,
      panelClass: ['red-snackbar']
    });
  }

}
