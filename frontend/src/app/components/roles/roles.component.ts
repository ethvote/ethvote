import {Component, OnInit, ViewChild} from '@angular/core';
import {RolesService} from '../../services/communication/roles/roles.service';
import {Maybe} from '../../utils/maybe.utils';
import {Roles} from '../../models/roles/roles.model';
import {forkJoin, Observable} from 'rxjs';
import {VotersWithRoles} from '../../models/roles/votersWithRoles.model';
import {MatDialog, MatDialogConfig, MatPaginator, MatSnackBar, MatTableDataSource} from '@angular/material';
import {AddVoterComponent} from '../addVoter/addVoter.component';
import {SingleVoterWithRoles} from '../../models/roles/singleVoterWithRoles.model';
import {VotersService} from '../../services/communication/voters/voters.service';
import {Pair} from '../../utils/pair.utils';
import {CurrentUserService} from '../../services/currentUser/currentUser.service';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.css']
})
export class RolesComponent implements OnInit {
  backup: SingleVoterWithRoles[];

  displayedColumns: string[];
  dataSource: MatTableDataSource<SingleVoterWithRoles>;
  isCurrentUserAdmin: boolean;
  isCurrentUserRegistrar: boolean;

  userDidModification = false;

  constructor(private rolesService: RolesService,
              private votersService: VotersService,
              private snackBar: MatSnackBar,
              private currentUserService: CurrentUserService,
              private dialog: MatDialog) { }

  ngOnInit() {
    this.isCurrentUserAdmin = this.currentUserService.isCurrentUserAdmin();
    this.isCurrentUserRegistrar = this.currentUserService.isCurrentUserRegistrar();

    if (this.isCurrentUserAdmin) {
      this.displayedColumns = ['Name', 'Voting_Administrator', 'Registrar', 'Administrator', 'Delete'];
    }
    else {
      this.displayedColumns = ['Name'];
    }
    this.dataSource = new MatTableDataSource();
    this.updateView();
  }

  updateView() {
    const availableRolesObservable = this.rolesService.getAllAvailableRoles();
    const userWithRolesObservable = this.rolesService.getAllVotersWithRoles();

    forkJoin(availableRolesObservable, userWithRolesObservable).subscribe(data => this.onRolesLoaded(data[0], data[1]));
  }

  onRolesLoaded(maybeRoles: Maybe<Roles>, maybeVoterWithRoles: Maybe<VotersWithRoles>) {
    const votersArray = maybeVoterWithRoles.getData().getVoters();
    votersArray.sort((v1, v2) => v1.getId() - v2.getId());

    this.backup = this.deepCopy(votersArray);
    this.dataSource.data = votersArray;
  }


  deepCopy(voters: SingleVoterWithRoles[]): SingleVoterWithRoles[] {
    const ret = [];
    for (const voter of voters) {
      ret.push(new SingleVoterWithRoles()
          .setId(voter.getId())
          .setName(voter.getName())
          .setRoles(voter.getRoles().slice())
      );
    }
    return ret;
  }

  reset() {
    for (let i = 0; i < this.dataSource.data.length; i++) {
      const voter = this.dataSource.data[i];
      const backupVoter = this.backup[i];

      voter.setRoles(backupVoter.getRoles().slice());
    }
    this.updateDidModificationStatus();
  }

  onChangeAdminRole(index: number, newValue: boolean) {
    this.dataSource.data[index].modifyAdminRole(newValue);
    this.updateDidModificationStatus();
  }

  onChangeVotingAdminRole(index: number, newValue: boolean) {
    this.dataSource.data[index].modifyVotingAdminRole(newValue);
    this.updateDidModificationStatus();
  }

  onChangeRegistrarRole(index: number, newValue: boolean) {
    this.dataSource.data[index].modifyRegistrarRole(newValue);
    this.updateDidModificationStatus();
  }

  updateDidModificationStatus() {
    this.userDidModification = (this.calculateModifications().length !== 0);
  }

  calculateModifications(): Observable<boolean>[] {
    const allVoters = this.dataSource.data;

    const allResponses: Observable<boolean>[] = [];

    for (let i = 0; i < allVoters.length; i++) {
      const id = allVoters[i].getId();

      const oldAdminRole = this.backup[i].isAdministrator();
      const newAdminRole = allVoters[i].isAdministrator();

      const oldVotingAdminRole = this.backup[i].isVotingAdministrator();
      const newVotingAdminRole = allVoters[i].isVotingAdministrator();

      const oldRegistrarRole = this.backup[i].isRegistrar();
      const newRegistrarRole = allVoters[i].isRegistrar();

      if (!oldAdminRole && newAdminRole) {
        allResponses.push(this.rolesService.addRole(id, 'Administrator'));
      }
      if (oldAdminRole && !newAdminRole) {
        allResponses.push(this.rolesService.removeRole(id, 'Administrator'));
      }

      if (!oldVotingAdminRole && newVotingAdminRole) {
        allResponses.push(this.rolesService.addRole(id, 'Voting_Administrator'));
      }
      if (oldVotingAdminRole && !newVotingAdminRole) {
        allResponses.push(this.rolesService.removeRole(id, 'Voting_Administrator'));
      }

      if (!oldRegistrarRole && newRegistrarRole) {
        allResponses.push(this.rolesService.addRole(id, 'Registrar'));
      }
      if (oldRegistrarRole && !newRegistrarRole) {
        allResponses.push(this.rolesService.removeRole(id, 'Registrar'));
      }
    }

    return allResponses;
  }

  save() {
    const allResponses: Observable<boolean>[] = this.calculateModifications();

    if (allResponses.length === 0) {
      return;
    }

    forkJoin(allResponses).subscribe(data => this.onRequestsReturned(data));
  }

  private onRequestsReturned(responses: any[]) {
    for (const result of responses) {
      if (result === false) {
        this.showFailure('There was a failure');
        return;
      }
    }
    this.showSuccess('Successfully done ' + responses.length + ' role change(s).');
    this.backup = this.deepCopy(this.dataSource.data);
    this.updateDidModificationStatus();
  }

  addVoter() {
    const dialogConfig = new MatDialogConfig();
    const dialogRef = this.dialog.open(AddVoterComponent, { panelClass: 'custom-dialog-container' });

    dialogRef.afterClosed().subscribe(result => {
      this.updateView();
    });
  }

  deleteVoter(voterId: number) {
    this.votersService.deleteVoter(voterId).subscribe(data => this.onDeleteVoterReturned(data));
  }

  onDeleteVoterReturned(response: Pair<boolean, string>) {
    if (response.getFirst()) {
      this.showSuccess(response.getSecond());
      this.updateView();
    }
    else {
      this.showFailure(response.getSecond());
    }
  }

  private showSuccess(successMessage: string) {
    this.snackBar.open(successMessage, '', {
      duration: 2000,
      panelClass: ['green-snackbar']
    });
  }

  private showFailure(failureMessage: string) {
    this.snackBar.open(failureMessage, '', {
      duration: 2000,
      panelClass: ['red-snackbar']
    });
  }


}
