import {Injectable} from '@angular/core';
import {VoteHelperService} from './voteHelper.service';
import {Helper} from '../../utils/helper.utils';
import {CryptoService} from '../crypto/crypto.service';
import {DataForKeyCalculation} from '../../models/dataForKeyCalculation/dataForKeyCalculation.model';
import {SecretSharesForVoter} from '../../models/secretShares/secretSharesForVoter.model';
import {Commitments} from '../../models/commitments/commitments.model';
import {CommitmentFromVoter} from '../../models/commitments/commitmentFromVoter.model';
import {SingleSecretShareForVoter} from '../../models/secretShares/singleSecretShareForVoter';
import {MathService} from '../math/math.service';
import {Votes} from '../../models/vote/votes.json.model';
import {VotingCalculationParameters} from '../../models/votings/votingsCalculationParameters.json.model';
import {SingleVote} from '../../models/vote/singleVote.json.model';
import {SingleDecryptionFactor} from '../../models/decryptionFactors/singleDecryptionFactor.model';
import {LagrangePolynom} from '../../data/lagrangePolynom';
import {LagrangeService} from '../lagrange/lagrange.service';
import {PrimeFactorizationService} from '../primeFactorization/primeFactorization.service';

@Injectable()
export class VoteHelperServiceImpl implements VoteHelperService {

    constructor(private cryptoService: CryptoService,
                private mathService: MathService,
                private lagrangeService: LagrangeService,
                private primeFactorizationService: PrimeFactorizationService) {
    }

    decryptPolynomPoint(encryptedPoint: string[], privateKey: string): string {
        const decryptedValues = new Array();
        for (const encryptedValue of encryptedPoint) {
            const decryptedValue = this.cryptoService.decrypt(privateKey, encryptedValue).getData();
            decryptedValues.push(decryptedValue);
        }
        return Helper.concatString(decryptedValues);
    }

    encryptPolynomPoint(polynomValue: string, publicKey: string): string[] {
        const splittedPolynomValue = Helper.splitString(polynomValue, 200);
        const encryptedValues = new Array();
        for (let j = 0; j < splittedPolynomValue.length; j++) {
            encryptedValues.push(this.cryptoService.encrypt(publicKey, splittedPolynomValue[j]));
        }
        return encryptedValues;
    }

    validateShares(data: DataForKeyCalculation, commitments: Commitments, secretShares: SecretSharesForVoter, privateKey): [number, boolean][] {
        const sortedCommitments = commitments.getCommitments().sort(this.compareCommitments);
        const sortedSecretShares = secretShares.getSecretShares().sort(this.compareSecretShares);

        const res = new Array();

        for (let i = 0; i < sortedCommitments.length; i++) {
            const singleCommitment = sortedCommitments[i];
            const singleSecretShare = sortedSecretShares[i];

            const fromVoterId = singleSecretShare.getFromVoterId();

            if (singleCommitment.getFromVoterId() !== singleSecretShare.getFromVoterId()) {
                res.push([fromVoterId, false]);
            }
            else if (!this.validateShare(data, singleCommitment, singleSecretShare, '' + (secretShares.getForVoterPosition() + 1), privateKey)) {
                res.push([fromVoterId, false]);
            }
            else {
                res.push([fromVoterId, true]);
            }
        }
        return res;
    }

    validateShare(data: DataForKeyCalculation, commitment: CommitmentFromVoter, share: SingleSecretShareForVoter, forVoterPosition: string, privateKey: string): boolean {
        const primeP = data.getCalculationParameters().getPrimeP();
        const generatorG = data.getCalculationParameters().getGeneratorG();

        const raisedToPowerCommitments = new Array();
        for (const singleCommitment of commitment.getCommitments()) {
            const coefficientNumber = singleCommitment.getCoefficientNumber();
            const commitmentValue = singleCommitment.getCommitment();

            const exponent = this.mathService.powerWithModulo(forVoterPosition, '' + coefficientNumber, primeP);
            const poweredVal = this.mathService.powerWithModulo(commitmentValue,  exponent, primeP);
            raisedToPowerCommitments.push(poweredVal);
        }

        const calculatedValueFromCommitments = this.mathService.multiplyAllWithModulo(raisedToPowerCommitments, primeP);

        const decryptedShareValue = this.decryptPolynomPoint(share.getSecretShare(), privateKey);
        const calculatedValueFromSecretShare = this.mathService.powerWithModulo(generatorG, decryptedShareValue, primeP);

        return calculatedValueFromCommitments === calculatedValueFromSecretShare;
    }

    calculatePublicKey(data: DataForKeyCalculation, commitments: Commitments): string {
        const primeP = data.getCalculationParameters().getPrimeP();
        const allCommitmentsForCoefficientZero = new Array();
        for (const commitment of commitments.getCommitments()) {
            for (const singleCommitment of commitment.getCommitments()) {
                if (singleCommitment.getCoefficientNumber() === 0) {
                    allCommitmentsForCoefficientZero.push(singleCommitment.getCommitment());
                }
            }
        }
        return this.mathService.multiplyAllWithModulo(allCommitmentsForCoefficientZero, primeP);
    }

    encryptVote(data: DataForKeyCalculation, encryptionKey: string, vote: string): [string, string, string] {
        const primeP = data.getCalculationParameters().getPrimeP();
        const primeQ = data.getCalculationParameters().getPrimeQ();
        const generatorG = data.getCalculationParameters().getGeneratorG();

        const randomValue = this.mathService.generateRandom(primeQ);
        const randomizedKey = this.mathService.powerWithModulo(encryptionKey, randomValue, primeP);

        const alpha = this.mathService.powerWithModulo(generatorG, randomValue, primeP);
        const beta = this.mathService.multiplyWithModulo(vote, randomizedKey, primeP);

        return [alpha, beta, randomValue];
    }

    calculateSecretPartOfKey(data: DataForKeyCalculation, secretShares: SecretSharesForVoter, privateKey: string): string {
        const primeQ = data.getCalculationParameters().getPrimeQ();
        const allShares = new Array();
        for (const singleSecretShare of secretShares.getSecretShares()) {
            const decryptedShare = this.decryptPolynomPoint(singleSecretShare.getSecretShare(), privateKey);
            allShares.push(decryptedShare);
        }
        return this.mathService.addAllWithModulo(allShares, primeQ);
    }

    calculateDecryptionFactor(data: DataForKeyCalculation, secretShares: SecretSharesForVoter, votes: Votes, privateKey: string) {
        const primeP = data.getCalculationParameters().getPrimeP();
        const secretShare = this.calculateSecretPartOfKey(data, secretShares, privateKey);

        const alphas = new Array();

        for (const singleVote of votes.getVotes()) {
            const alpha = singleVote.getAlpha();
            alphas.push(alpha);
        }

        const multipliedAlphas = this.mathService.multiplyAllWithModulo(alphas, primeP);
        return this.mathService.powerWithModulo(multipliedAlphas, secretShare, primeP);
    }

    calculateVotingOutCome(votingCalculationParameters: VotingCalculationParameters, votesArray: SingleVote[], decryptionFactors: SingleDecryptionFactor[], numberOfOptions): number[] {
        const primeP = votingCalculationParameters.getPrimeP();
        const primeQ = votingCalculationParameters.getPrimeQ();

        const allPoints = decryptionFactors;
        const lagrangePolynomial = new LagrangePolynom();
        for (let i = 0; i < allPoints.length; i++) {
            const position = allPoints[i].getFromVoterPosition() + 1;
            const value = allPoints[i].getDecryptionFactor();
            lagrangePolynomial.addPoint('' + position, value);
        }

        const valueAtZero = this.lagrangeService.getValueAtPosition('0', lagrangePolynomial, primeP, primeQ);
        let beta = '1';
        for (const vote of votesArray) {
            beta = this.mathService.multiplyWithModulo(beta, vote.getBeta(), primeP);
        }

        const valueAtZeroInvers = this.mathService.modInverse(valueAtZero, primeP);
        const allVotes = this.mathService.multiplyWithModulo(beta, valueAtZeroInvers, primeP);

        const voteResult = this.primeFactorizationService.factorize(allVotes);

        while (voteResult.length < numberOfOptions) {
            voteResult.push(0);
        }
        return voteResult;
    }

    private compareCommitments(comm1: CommitmentFromVoter, comm2: CommitmentFromVoter) {
        if (comm1.getFromVoterId() < comm2.getFromVoterId()) {
            return -1;
        }
        else if (comm1.getFromVoterPosition() > comm2.getFromVoterPosition()) {
            return 1;
        }
        else {
            return 0;
        }
    }

    private compareSecretShares(share1: SingleSecretShareForVoter, share2: SingleSecretShareForVoter) {
        if (share1.getFromVoterId() < share2.getFromVoterId()) {
            return -1;
        }
        else if (share1.getFromVoterId() > share2.getFromVoterId()) {
            return 1;
        }
        else {
            return 0;
        }
    }

}
