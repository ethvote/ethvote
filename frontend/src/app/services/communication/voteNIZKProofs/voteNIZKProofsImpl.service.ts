import { map, catchError } from 'rxjs/operators';
import { HttpClient, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import {Pair} from '../../../utils/pair.utils';
import {VoteNIZKProofService} from './voteNIZKProofs.service';
import {VoteNIZKProof} from '../../../models/voteNIZKProof/voteNIZKProofs';
import {AllVoteNIZKProofs} from '../../../models/voteNIZKProof/allVoteNIZKProofs';
import {Maybe} from '../../../utils/maybe.utils';
import {VotesNIZKProofJsonParser} from '../../jsonParser/votesNIZKProofJsonParser.service';

@Injectable()
export class VoteNIZKProofServiceImpl implements VoteNIZKProofService {

    private startUrl = '/api/votings/';

    constructor(private http: HttpClient,
                private votesNIZKProofParser: VotesNIZKProofJsonParser) {
    }

    sendNIZKProofs(voteId: Number, voterId: Number, proofs: VoteNIZKProof): Observable<Pair<boolean, string>> {
        let obsResponse: Observable<HttpResponse<String>>;
        let obsReturn: Observable<Pair<boolean, string>>;

        obsResponse = this.http.post(this.startUrl + voteId + '/voteZeroKnowledgeProofs/voters/' + voterId, JSON.stringify(proofs), {
            observe: 'response',
            responseType: 'text'
        });

        obsReturn = obsResponse.pipe(
            map(data => this.checkIfNIZKProofWasSetSuccessfull(data)),
            catchError(err => this.handleNIZKProofSetError(err))
        );

        return obsReturn;
    }

    private checkIfNIZKProofWasSetSuccessfull(resp: HttpResponse<any>): Pair<boolean, string> {
        const successful = resp.status === 201;
        if (successful) {
            return new Pair<boolean, string>(true, 'Successful');
        }
        else {
            return new Pair<boolean, string>(false, 'Unknown failure');
        }
    }

    private handleNIZKProofSetError(error: HttpErrorResponse): Observable<Pair<boolean, string>> {
        return of(new Pair(false, error.error));
    }

    getNITKProofs(voteId: Number): Observable<Maybe<AllVoteNIZKProofs>> {
        let obsResponse: Observable<HttpResponse<string>>;
        let obsReturn: Observable<Maybe<AllVoteNIZKProofs>>;

        obsResponse = this.http.get(this.startUrl + voteId + '/voteZeroKnowledgeProofs', {
            observe: 'response',
            responseType: 'text'
        });

        obsReturn = obsResponse.pipe(
            map(data => this.mapToNIZKProof(data)),
            catchError(err => this.handleErrorForNIZKProof(err))
        );

        return obsReturn;
    }

    private mapToNIZKProof(resp: HttpResponse<string>): Maybe<AllVoteNIZKProofs> {
        if (resp && resp.status === 200 && resp.body) {
            return this.votesNIZKProofParser.parseJson(resp.body);
        }
        else {
            return Maybe.fromEmpty();
        }
    }

    private handleErrorForNIZKProof(error: HttpErrorResponse): Observable<Maybe<AllVoteNIZKProofs>> {
        console.log(error);
        return of(Maybe.fromEmpty());
    }
}
