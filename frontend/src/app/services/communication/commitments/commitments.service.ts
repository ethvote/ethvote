import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {CommitmentFromVoter} from '../../../models/commitments/commitmentFromVoter.model';
import {Maybe} from '../../../utils/maybe.utils';
import {Commitments} from '../../../models/commitments/commitments.model';
import {Pair} from '../../../utils/pair.utils';

@Injectable()
export abstract class CommitmentsService {

    abstract storeCommitments(votingId: Number, voterId: Number, commitments: CommitmentFromVoter): Observable<Pair<boolean, string>>;

    abstract getAllCommitments(votingId: Number): Observable<Maybe<Commitments>> ;

}
