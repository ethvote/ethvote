import {Injectable} from '@angular/core';
import {SecretSharesService} from './secretShares.service';
import {SecretSharesFromVoter} from '../../../models/secretShares/secretSharesFromVoter.model';
import {Observable, of} from 'rxjs';
import {HttpClient, HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';
import {Maybe} from '../../../utils/maybe.utils';
import {SecretSharesForVoter} from '../../../models/secretShares/secretSharesForVoter.model';
import {SecretSharesForVoterJsonParser} from '../../jsonParser/secretSharesForVoterJsonParser.service';
import {Pair} from '../../../utils/pair.utils';

@Injectable()
export class SecretSharesServiceImpl implements SecretSharesService {

    private startUrl = '/api/votings/';

    constructor(private http: HttpClient,
                private parser: SecretSharesForVoterJsonParser) {
    }

    storeSecretShares(votingId: Number, voterId: Number, secretShares: SecretSharesFromVoter): Observable<Pair<boolean, string>> {
        let obsResponse: Observable<HttpResponse<String>>;
        let obsReturn: Observable<Pair<boolean, string>>;

        obsResponse = this.http.post(
            this.startUrl + votingId + '/secretShares',
            JSON.stringify(secretShares),
            {
                observe: 'response',
                responseType: 'text'
            });

        obsReturn = obsResponse.pipe(
            map(data => this.checkIfSuccessfull(data)),
            catchError(err => this.handleError(err))
        );

        return obsReturn;
    }

    private checkIfSuccessfull(resp: HttpResponse<String>): Pair<boolean, string> {
        const successful = resp.status === 201;
        if (successful) {
            return new Pair<boolean, string>(true, 'Successful');
        }
        else {
            return new Pair<boolean, string>(false, 'Unknown failure');
        }
    }

    private handleError(error: HttpErrorResponse): Observable<Pair<boolean, string>> {
        return of(new Pair(false, error.error));
    }

    getAllSharesForVoter(votingId: Number, voterId: Number): Observable<Maybe<SecretSharesForVoter>> {
        let obsSecretSharesForVoterResponse: Observable<HttpResponse<String>>;
        let obsSecretSharesForVoter: Observable<Maybe<SecretSharesForVoter>>;

        obsSecretSharesForVoterResponse = this.http.get<String>(this.startUrl + votingId + '/secretShares/forVoters/' + voterId,
            { observe: 'response'});
        obsSecretSharesForVoter = obsSecretSharesForVoterResponse.pipe(
            map(data => this.mapToSecretSharesForVoter(data)),
            catchError(err => this.handleGetSecretSharesForVoterError(err))
        );
        return obsSecretSharesForVoter;
    }

    private mapToSecretSharesForVoter(resp: HttpResponse<String>): Maybe<SecretSharesForVoter> {
        if (resp && resp.status === 200 && resp.body) {
            return this.parser.parseJson(JSON.stringify(resp.body));
        }
        else {
            return Maybe.fromEmpty();
        }
    }

    private handleGetSecretSharesForVoterError(error: HttpErrorResponse): Observable<Maybe<SecretSharesForVoter>> {
        console.log(error);
        return of(Maybe.fromEmpty());
    }
}
