import {Injectable} from '@angular/core';
import {SecretSharesFromVoter} from '../../../models/secretShares/secretSharesFromVoter.model';
import {Observable} from 'rxjs';
import {Maybe} from '../../../utils/maybe.utils';
import {SecretSharesForVoter} from '../../../models/secretShares/secretSharesForVoter.model';
import {Pair} from '../../../utils/pair.utils';

@Injectable()
export abstract class SecretSharesService {

    abstract storeSecretShares(votingId: Number, voterId: Number, secretShares: SecretSharesFromVoter): Observable<Pair<boolean, string>>;

    abstract getAllSharesForVoter(votingId: Number, voterId: Number): Observable<Maybe<SecretSharesForVoter>>;

}
