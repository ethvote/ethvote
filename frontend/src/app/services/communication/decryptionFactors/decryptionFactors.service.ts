import {Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {Maybe} from '../../../utils/maybe.utils';
import {DecryptionFactors} from '../../../models/decryptionFactors/decryptionFactors.model';
import {SingleDecryptionFactor} from '../../../models/decryptionFactors/singleDecryptionFactor.model';
import {Pair} from '../../../utils/pair.utils';

@Injectable()
export abstract class DecryptionFactorsService {

    abstract storeDecryptionFactor(votingId: Number, voterId: Number, decryptionFactor: SingleDecryptionFactor): Observable<Pair<boolean, string>>;

    abstract getAllDecryptionFactors(votingId: Number): Observable<Maybe<DecryptionFactors>> ;

}
