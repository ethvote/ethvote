import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {VotingProgress} from '../../../models/votings/votingProgress.json.model';
import {Maybe} from '../../../utils/maybe.utils';

@Injectable({
  providedIn: 'root'
})
export abstract class VotingProgressService {

  abstract getVotingProgress(votingId: number): Observable<Maybe<VotingProgress>>;
}
