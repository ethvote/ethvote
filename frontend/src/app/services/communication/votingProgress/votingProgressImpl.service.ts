import { Injectable } from '@angular/core';
import {VotingProgressService} from './votingProgress.service';
import {Observable, of} from 'rxjs';
import {Maybe} from '../../../utils/maybe.utils';
import {VotingProgress} from '../../../models/votings/votingProgress.json.model';
import {HttpClient, HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';
import {VotingProgressJsonParser} from '../../jsonParser/votingProgressJsonParser.service';

@Injectable({
    providedIn: 'root'
})
export class VotingProgressServiceImpl implements VotingProgressService {

    constructor(private http: HttpClient,
                private votingProgressJsonParser: VotingProgressJsonParser) {
    }

    getVotingProgress(votingId: number): Observable<Maybe<VotingProgress>> {
        let obsVoterProgressResponse: Observable<HttpResponse<String>>;
        let votingProgress: Observable<Maybe<VotingProgress>>;

        obsVoterProgressResponse = this.http.get<string>('/api/votings/' + votingId + '/votingsOverview', {observe: 'response'});
        votingProgress = obsVoterProgressResponse.pipe(
            map(data => this.mapToVotingProgress(data)),
            catchError(err => this.handleErrorForVotingProgress(err))
        );

        return votingProgress;
    }

    private mapToVotingProgress(resp: HttpResponse<String>): Maybe<VotingProgress> {
        if (resp && resp.status === 200 && resp.body) {
            return this.votingProgressJsonParser.parseJson(JSON.stringify(resp.body));
        }
        else {
            return Maybe.fromEmpty();
        }
    }

    private handleErrorForVotingProgress(error: HttpErrorResponse): Observable<Maybe<VotingProgress>> {
        console.log(error);
        return of(Maybe.fromEmpty());
    }
}
