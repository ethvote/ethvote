import { Observable } from 'rxjs';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';

@Injectable()
export class UrlRewriteInterceptor implements HttpInterceptor {

    constructor() {
    }

    intercept(req: HttpRequest<any>,
        next: HttpHandler): Observable<HttpEvent<any>> {

        const rewritePrefix = '/api';

        const baseUrl = environment.API_URL;
        if (req.url.startsWith(rewritePrefix)) {
            req = req.clone({
                url: baseUrl + req.url
            });
            return next.handle(req);
        }
        return next.handle(req);
    }
}
