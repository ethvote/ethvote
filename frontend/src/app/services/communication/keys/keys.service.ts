import { UserPublicKey } from '../../../models/userPublicKey/userPublicKey.model';
import { Maybe } from '../../../utils/maybe.utils';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable()
export abstract class KeysService {

    abstract storePublicKey(voterId: number, publicKey: string): Observable<Maybe<string>>;

    abstract storePublicKeyForVoting(voterId: number, votingId: number, publicKey: string): Observable<boolean>;

    abstract getPrivateKey(voterId: number): Observable<Maybe<string>>;

    abstract getAllKeys(): Observable<UserPublicKey[]>;

    abstract getPublicKeyForVoting(voterId: number, votingId: number): Observable<Maybe<string>>;

}
