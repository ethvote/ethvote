import {map, catchError } from 'rxjs/operators';
import {Observable, of } from 'rxjs';
import {HttpClient, HttpResponse, HttpErrorResponse } from '@angular/common/http';

import {Injectable } from '@angular/core';
import {VotersService } from './voters.service';
import {VotersJsonParser} from '../../jsonParser/votersJsonParser.service';
import {Maybe} from '../../../utils/maybe.utils';
import {VoterArray} from '../../../models/voters/voterArray.model';
import {ResponseFromHttpRequest} from '../../../data/responseFromHttpRequest';
import {Pair} from '../../../utils/pair.utils';

@Injectable()
export class VotersServiceImpl implements VotersService {

  private startUrl = '/api/voters';

  constructor(private http: HttpClient,
              private votersJsonParser: VotersJsonParser) {
  }

  getAllVoters(): Observable<ResponseFromHttpRequest<VoterArray>> {
    let obsVoterResponse: Observable<HttpResponse<string>>;
    let allVoters: Observable<ResponseFromHttpRequest<VoterArray>>;

    obsVoterResponse = this.http.get(this.startUrl,
        {
          observe: 'response',
          responseType: 'text'
        });
    allVoters = obsVoterResponse.pipe(
      map(data => this.mapToVotersArray(data)),
      catchError(err => this.catchGetAllVotersError(err))
    );

    return allVoters;
  }

  saveVoters(voters: VoterArray, createKeys: boolean): Observable<ResponseFromHttpRequest<VoterArray>> {

    voters['createKeys'] = createKeys;

    const jsonString = JSON.stringify(voters, (key, value) => {
      if (value !== null) {
        return value;
      }
    });

    let obsVoterResponse: Observable<HttpResponse<string>>;
    let savedVoters: Observable<ResponseFromHttpRequest<VoterArray>>;

    obsVoterResponse = this.http.post(this.startUrl, jsonString,
        {
          observe: 'response',
          responseType: 'text'
        });
    savedVoters = obsVoterResponse.pipe(
        map(data => this.mapToVotersArray(data)),
        catchError(err => this.catchGetAllVotersError(err))
    );

    return savedVoters;
  }

  private mapToVotersArray(resp: HttpResponse<string>): ResponseFromHttpRequest<VoterArray> {
    console.log(resp);
    if (resp && resp.body) {
      const maybeVoters = this.votersJsonParser.parseJson(resp.body);
      return new ResponseFromHttpRequest(maybeVoters, resp.status);
    }

    return new ResponseFromHttpRequest(Maybe.fromEmpty(), resp.status);
  }

  private catchGetAllVotersError(error: HttpErrorResponse): Observable<ResponseFromHttpRequest<VoterArray>> {
    if (error && error.error) {
      const stringToParse = JSON.stringify(error.error);
      const maybeVoters = this.votersJsonParser.parseJson(stringToParse);
      return of(new ResponseFromHttpRequest(maybeVoters, error.status));
    }
    return of(new ResponseFromHttpRequest(Maybe.fromEmpty(), error.status));
  }

  deleteVoter(voterId: Number): Observable<Pair<boolean, string>> {
    let obsVoterResponse: Observable<HttpResponse<string>>;
    let returnMessage: Observable<Pair<boolean, string>>;

    obsVoterResponse = this.http.delete(this.startUrl + '/' + voterId,
        {
          observe: 'response',
          responseType: 'text'
        });

    returnMessage = obsVoterResponse.pipe(
        map(data => this.mapToDeleteMessage(data)),
        catchError(err => this.catchDeleteVoterError(err))
    );

    return returnMessage;
  }

  private mapToDeleteMessage(resp: HttpResponse<string>): Pair<boolean, string> {
    console.log(resp);
    if (resp && resp.status === 200 && resp.body) {
      return new Pair(true, resp.body);
    }
    return new Pair(false, resp.body);
  }

  private catchDeleteVoterError(error: HttpErrorResponse): Observable<Pair<boolean, string>> {
    return of(new Pair(false, error.error));
  }

  registerCurrentVoter(): Observable<Pair<boolean, string>> {
    let obsVoterResponse: Observable<HttpResponse<string>>;
    let returnMessage: Observable<Pair<boolean, string>>;

    obsVoterResponse = this.http.post('/api/registerCurrentVoter',
        {},
        {
          observe: 'response',
          responseType: 'text'
        });

    returnMessage = obsVoterResponse.pipe(
        map(data => this.mapToRegisterMessage(data)),
        catchError(err => this.catchRegisterVoterError(err))
    );

    return returnMessage;
  }

  private mapToRegisterMessage(resp: HttpResponse<string>): Pair<boolean, string> {
    if (resp && resp.status === 201 && resp.body) {
      return new Pair(true, resp.body);
    }
    return new Pair(false, resp.body);
  }

  private catchRegisterVoterError(error: HttpErrorResponse): Observable<Pair<boolean, string>> {
    return of(new Pair(false, error.error));
  }

}
