import { Injectable } from '@angular/core';
import { Maybe } from '../../../utils/maybe.utils';
import { User } from '../../../models/user.model';
import { AuthenticationService } from './authentication.service';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';

interface LoginResponse {
    token: string;
}

@Injectable()
export class AuthenticationServiceImpl implements AuthenticationService {

    private url = '/api/authenticate';

    constructor(private http: HttpClient) {
    }

    private mapToUser(username: string, resp: HttpResponse<LoginResponse>): Maybe<User> {
        if (resp && resp.status === 200 && resp.body.token) {
            return Maybe.fromData(new User(username, resp.body.token));
        }
        else {
            return Maybe.fromEmpty();
        }
    }

    private handleError(error: HttpErrorResponse): Observable<Maybe<User>> {
        console.log(error);
        return of(Maybe.fromEmpty());
    }

    authenticate(username: string, password: string): Observable<Maybe<User>> {
        let obsLoginResponse: Observable<HttpResponse<LoginResponse>>;
        let obsUser: Observable<Maybe<User>>;

        obsLoginResponse = this.http.post<LoginResponse>(this.url, {username: username, password: password}, {observe: 'response'});
        obsUser = obsLoginResponse.pipe(
            map(data => this.mapToUser(username, data)),
            catchError(err => this.handleError(err))
        );
        return obsUser;
    }
}

