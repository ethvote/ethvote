import { Observable } from 'rxjs';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LocalStorageService } from '../../localStorage/localStorage.service';
import { tap } from 'rxjs/operators';
import {SubscriberService} from '../../subscription/subscriber.service';

@Injectable()
export class RefreshTokenInterceptor implements HttpInterceptor {

    constructor(private storageService: LocalStorageService,
                private subscriberService: SubscriberService) {
    }

    intercept(req: HttpRequest<any>,
              next: HttpHandler): Observable<HttpEvent<any>> {

        return next.handle(req).pipe(
            tap(event => {
                if (event instanceof HttpResponse) {
                    const token = event.headers.get('RefreshToken');
                    const oldToken = this.storageService.get('token');
                    if (token !== null) {
                        this.storageService.upsert('token', token);
                    }
                    if (oldToken.isEmpty() || oldToken.getData() !== token) {
                        this.subscriberService.notifyTokenChanged();
                    }
                }
            })
        );
    }
}
