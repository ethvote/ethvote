import { Maybe } from './../../utils/maybe.utils';
import { Injectable } from '@angular/core';
import { LocalStorageService } from './localStorage.service';

@Injectable()
export class LocalStorageServiceImpl implements LocalStorageService {

    get(key: string): Maybe<string> {
        const item = sessionStorage.getItem(key);
        if (item !== null) {
            return Maybe.fromData(item);
        }
        else {
            return Maybe.fromEmpty();
        }
    }

    upsert(key: string, value: string) {
        sessionStorage.setItem(key, value);
    }

    delete(key: string): boolean {
        if (this.get(key).isEmpty()) {
            return false;
        }
        else {
            sessionStorage.removeItem(key);
            return true;
        }
    }
}
