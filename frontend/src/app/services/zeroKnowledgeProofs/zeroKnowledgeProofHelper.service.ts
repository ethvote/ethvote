import {Injectable} from '@angular/core';
import {DataForKeyCalculation} from '../../models/dataForKeyCalculation/dataForKeyCalculation.model';
import {CommitmentFromVoter} from '../../models/commitments/commitmentFromVoter.model';
import {SingleSecretShareForVoter} from '../../models/secretShares/singleSecretShareForVoter';
import {Commitments} from '../../models/commitments/commitments.model';
import {SecretSharesForVoter} from '../../models/secretShares/secretSharesForVoter.model';
import {Votes} from '../../models/vote/votes.json.model';
import {VoteNIZKProof} from '../../models/voteNIZKProof/voteNIZKProofs';
import {DecryptionNIZKProof} from '../../models/decryptionNIZKProof/decryptionNIZKProof';
import {VotingCalculationParameters} from '../../models/votings/votingsCalculationParameters.json.model';
import {AllVoteNIZKProofs} from '../../models/voteNIZKProof/allVoteNIZKProofs';
import {VotingOption} from '../../models/votings/votingOption.json.model';
import {SingleVote} from '../../models/vote/singleVote.json.model';

@Injectable()
export abstract class ZeroKnowledgeProofHelperService {

    abstract calculateVoteZeroKnowledgeProof(params: VotingCalculationParameters, alpha: string, beta: string, r: string, keyToEncrypt: string, chosenOptionNumber: number, allOptions: number[]): VoteNIZKProof;

    abstract verifyAllProofs(params: VotingCalculationParameters, votes: Votes, proofs: AllVoteNIZKProofs, votingOptions: VotingOption[], keyToEncrypt: string):  SingleVote[];

    abstract verifyVoteZeroKnowledgeProof(params: VotingCalculationParameters, alpha: string, beta: string, keyToEncrypt: string, options: number[], proof: VoteNIZKProof): boolean;

    abstract calculateDecryptionFactorZeroKnowledgeProof(params: VotingCalculationParameters, secretShares: SecretSharesForVoter, votes: Votes, privateKey: string): DecryptionNIZKProof;

}
