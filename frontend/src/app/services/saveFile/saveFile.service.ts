import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export abstract class SaveFileService {

  constructor() { }

  abstract saveFile(text: String, fileName: String);
}
