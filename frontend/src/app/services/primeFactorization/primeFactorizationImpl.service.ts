import {Injectable} from '@angular/core';
import {MathService} from '../math/math.service';
import {PrimesService} from '../primes/primes.service';

@Injectable()
export class PrimeFactorizationServiceImpl {

    constructor(private mathService: MathService,
                private primesService: PrimesService) {
    }

    factorize(number: string): number[] {
        let restNumber = number;
        let currentIndex = 0;
        let currentPrime = '' + this.primesService.getNthPrime(currentIndex);
        let currentCount = 0;

        const ret: number[] = new Array();

        do {
            while (this.mathService.mod(restNumber, currentPrime) === '0') {
                restNumber = this.mathService.divide(restNumber, currentPrime);
                currentCount++;
            }
            ret.push(currentCount);
            currentCount = 0;

            currentIndex++;
            currentPrime = '' + this.primesService.getNthPrime(currentIndex);

        } while (restNumber !== '1');

        return ret;
    }
}
