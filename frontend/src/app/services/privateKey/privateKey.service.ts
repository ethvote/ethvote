import {Injectable} from '@angular/core';
import {Maybe} from '../../utils/maybe.utils';
import {DataForKeyCalculation} from '../../models/dataForKeyCalculation/dataForKeyCalculation.model';
import {Observable} from 'rxjs';

@Injectable()
export abstract class PrivateKeyService {

    abstract getPrivateKey(data: DataForKeyCalculation, voterId: number): Observable<Maybe<string>>;

}
