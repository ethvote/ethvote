import {Maybe} from '../../utils/maybe.utils';
import {SecretPartOfKey} from '../../models/secretPartOfKey/secretPartOfKey.json.model';
import {SecretPartOfKeyParser} from './secretPartOfKeyParser.service';
import {JsonConvert, ValueCheckingMode } from 'json2typescript';

export class SecretPartOfKeyParserImpl implements SecretPartOfKeyParser {

    parseJson(jsonString: string): Maybe<SecretPartOfKey> {
        let secretPartOfKey: SecretPartOfKey;
        try {
            const jsonObj: object = JSON.parse(jsonString);
            const jsonConvert: JsonConvert = new JsonConvert();
            jsonConvert.ignorePrimitiveChecks = false;
            jsonConvert.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL;
            secretPartOfKey = jsonConvert.deserializeObject(jsonObj, SecretPartOfKey);
        } catch (e) {
            console.log(e);
            return Maybe.fromEmpty();
        }
        return Maybe.fromData(secretPartOfKey);
    }

}
