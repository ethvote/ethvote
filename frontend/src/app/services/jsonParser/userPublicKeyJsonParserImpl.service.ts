import { UserPublicKeyArray } from './../../models/userPublicKey/userPublicKeyArray.model';
import { Maybe } from './../../utils/maybe.utils';

import { JsonConvert, ValueCheckingMode } from 'json2typescript';
import { Injectable } from '@angular/core';
import { UserPublicKeyJsonParser } from './userPublicKeyJsonParser.service';

@Injectable()
export class UserPublicKeyJsonParserImpl implements UserPublicKeyJsonParser {

    parseJson(jsonString: string): Maybe<UserPublicKeyArray> {
        let userPublicKeys: UserPublicKeyArray;
        try {
        const jsonObj: object = JSON.parse(jsonString);
        const jsonConvert: JsonConvert = new JsonConvert();
        jsonConvert.ignorePrimitiveChecks = false;
        jsonConvert.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL;
        userPublicKeys = jsonConvert.deserializeObject(jsonObj, UserPublicKeyArray);
        } catch (e) {
            console.log(e);
            return Maybe.fromEmpty();
        }
        return Maybe.fromData(userPublicKeys);
    }
}
