import { Injectable } from '@angular/core';
import { Maybe } from '../../utils/maybe.utils';
import { DataForKeyCalculation } from '../../models/dataForKeyCalculation/dataForKeyCalculation.model';

@Injectable()
export abstract class DataForKeyCalculationJsonParser {

    abstract parseJson(jsonString: string): Maybe<DataForKeyCalculation>;

}
