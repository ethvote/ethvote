import {Maybe} from '../../utils/maybe.utils';
import {DecryptionFactors} from '../../models/decryptionFactors/decryptionFactors.model';

export abstract class DecryptionFactorsJsonParser {
    abstract parseJson(jsonString: string): Maybe<DecryptionFactors>;
}
