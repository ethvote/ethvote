import { Maybe } from './../../utils/maybe.utils';
import { Injectable } from '@angular/core';
import { VotingProgress} from '../../models/votings/votingProgress.json.model';

@Injectable()
export abstract class VotingProgressJsonParser {

    abstract parseJson(jsonString: string): Maybe<VotingProgress>;

}
