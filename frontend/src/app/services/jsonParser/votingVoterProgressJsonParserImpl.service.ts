import {Injectable } from '@angular/core';
import {Maybe } from '../../utils/maybe.utils';
import {JsonConvert, ValueCheckingMode } from 'json2typescript';
import {VotingVoterProgressJsonParser} from './votingVoterProgressJsonParser.service';
import {VotingVoterProgress} from '../../models/votings/votingVoterProgress';

@Injectable()
export class VotingVoterProgressJsonParserImpl implements VotingVoterProgressJsonParser {

    parseJson(jsonString: string): Maybe<VotingVoterProgress> {
        let votingProgress: VotingVoterProgress;
        try {
            const jsonObj: object = JSON.parse(jsonString);
            const jsonConvert: JsonConvert = new JsonConvert();
            jsonConvert.ignorePrimitiveChecks = false;
            jsonConvert.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL;
            votingProgress = jsonConvert.deserializeObject(jsonObj, VotingVoterProgress);
        } catch (e) {
            console.log(e);
            return Maybe.fromEmpty();
        }
        return Maybe.fromData(votingProgress);
    }
}
