import {Maybe} from '../../utils/maybe.utils';
import {JsonConvert, ValueCheckingMode } from 'json2typescript';
import {SecretSharesForVoter} from '../../models/secretShares/secretSharesForVoter.model';
import {SecretSharesForVoterJsonParser} from './secretSharesForVoterJsonParser.service';

export class SecretSharesForVoterJsonParserImpl implements SecretSharesForVoterJsonParser {

    parseJson(jsonString: string): Maybe<SecretSharesForVoter> {
        let singleSecretShareForVoter: SecretSharesForVoter;
        try {
            const jsonObj: object = JSON.parse(jsonString);
            const jsonConvert: JsonConvert = new JsonConvert();
            jsonConvert.ignorePrimitiveChecks = false;
            jsonConvert.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL;
            singleSecretShareForVoter = jsonConvert.deserializeObject(jsonObj, SecretSharesForVoter);
        } catch (e) {
            console.log(e);
            return Maybe.fromEmpty();
        }
        return Maybe.fromData(singleSecretShareForVoter);
    }

}
