import { UserPublicKeyArray } from './../../models/userPublicKey/userPublicKeyArray.model';
import { Maybe } from './../../utils/maybe.utils';


import { Injectable } from '@angular/core';

@Injectable()
export abstract class UserPublicKeyJsonParser {

    abstract parseJson(jsonString: string): Maybe<UserPublicKeyArray>;

}
