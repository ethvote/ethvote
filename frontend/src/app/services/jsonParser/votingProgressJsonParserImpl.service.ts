import {Injectable} from '@angular/core';
import {Maybe} from '../../utils/maybe.utils';
import {JsonConvert, ValueCheckingMode } from 'json2typescript';
import {VotingProgressJsonParser} from './votingProgressJsonParser.service';
import {VotingProgress} from '../../models/votings/votingProgress.json.model';

@Injectable()
export class VotingProgressJsonParserImpl implements VotingProgressJsonParser {

    parseJson(jsonString: string): Maybe<VotingProgress> {
        let votingProgress: VotingProgress;
        try {
            const jsonObj: object = JSON.parse(jsonString);
            const jsonConvert: JsonConvert = new JsonConvert();
            jsonConvert.ignorePrimitiveChecks = false;
            jsonConvert.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL;
            votingProgress = jsonConvert.deserializeObject(jsonObj, VotingProgress);
        } catch (e) {
            console.log(e);
            return Maybe.fromEmpty();
        }
        return Maybe.fromData(votingProgress);
    }
}
