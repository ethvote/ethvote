import {Injectable} from '@angular/core';
import {Maybe} from '../../utils/maybe.utils';
import {Votes} from '../../models/vote/votes.json.model';
import {VotesJsonParser} from './votesJsonParser.service';
import { JsonConvert, ValueCheckingMode } from 'json2typescript';
import {AllVoteNIZKProofs} from '../../models/voteNIZKProof/allVoteNIZKProofs';
import {VotesNIZKProofJsonParser} from './votesNIZKProofJsonParser.service';

@Injectable()
export class VotesNIZKProofJsonParserImpl implements VotesNIZKProofJsonParser {

    parseJson(jsonString: string): Maybe<AllVoteNIZKProofs> {
        let voteNIZKProof: AllVoteNIZKProofs;
        try {
            const jsonObj: object = JSON.parse(jsonString);
            const jsonConvert: JsonConvert = new JsonConvert();
            jsonConvert.ignorePrimitiveChecks = false;
            jsonConvert.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL;
            voteNIZKProof = jsonConvert.deserializeObject(jsonObj, AllVoteNIZKProofs);
        } catch (e) {
            console.log(e);
            return Maybe.fromEmpty();
        }
        return Maybe.fromData(voteNIZKProof);
    }
}
