import { Maybe } from './../../utils/maybe.utils';

import { JsonConvert, ValueCheckingMode } from 'json2typescript';
import { Injectable } from '@angular/core';
import {VotingsWithProgressJsonParser} from './votingsWithProgressJsonParser.service';
import {VotingsWithProgressArray} from '../../models/votings/votingsWithProgressArray.json.model';

@Injectable()
export class VotingsWithProgressJsonParserImpl implements VotingsWithProgressJsonParser {

    parseJson(jsonString: string): Maybe<VotingsWithProgressArray> {
        let votings: VotingsWithProgressArray;
        try {
            const jsonObj: object = JSON.parse(jsonString);
            const jsonConvert: JsonConvert = new JsonConvert();
            jsonConvert.ignorePrimitiveChecks = false;
            jsonConvert.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL;
            votings = jsonConvert.deserializeObject(jsonObj, VotingsWithProgressArray);
        } catch (e) {
            console.log(e);
            return Maybe.fromEmpty();
        }
        return Maybe.fromData(votings);
    }
}
