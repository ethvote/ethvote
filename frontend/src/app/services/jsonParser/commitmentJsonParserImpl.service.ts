import { Injectable } from '@angular/core';
import { Maybe } from '../../utils/maybe.utils';
import { JsonConvert, ValueCheckingMode } from 'json2typescript';
import {CommitmentJsonParser} from './commitmentJsonParser.service';
import {Commitments} from '../../models/commitments/commitments.model';

@Injectable()
export class CommitmentJsonParserImpl implements CommitmentJsonParser {

    parseJson(jsonString: string): Maybe<Commitments> {
        let dataForKeyCalculation: Commitments;
        try {
            const jsonObj: object = JSON.parse(jsonString);
            const jsonConvert: JsonConvert = new JsonConvert();
            jsonConvert.ignorePrimitiveChecks = false;
            jsonConvert.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL;
            dataForKeyCalculation = jsonConvert.deserializeObject(jsonObj, Commitments);
        } catch (e) {
            console.log(e);
            return Maybe.fromEmpty();
        }
        return Maybe.fromData(dataForKeyCalculation);
    }
}
