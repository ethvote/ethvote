import { UserPublicKeyJsonParserImpl } from './services/jsonParser/userPublicKeyJsonParserImpl.service';
import { UserPublicKeyJsonParser } from 'src/app/services/jsonParser/userPublicKeyJsonParser.service';
import { KeysServiceImpl } from './services/communication/keys/keysImpl.service';
import { SaveFileServiceImpl } from './services/saveFile/saveFileImpl.service';
import { CryptoServiceImpl } from './services/crypto/cyptoImpl.service';
import { CryptoService } from './services/crypto/crypto.service';
import { InsecureSecretsService } from 'src/app/services/secrets/insecureSecrets.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthorizationInterceptor } from './services/communication/authorizationInterceptor/authorizationInterceptor.service';
import { LoggedInGuard } from './guards/loggedIn.guard';
import { CurrentUserServiceImpl } from './services/currentUser/currentUserImpl.service';
import { AuthenticationService } from './services/communication/authentication/authentication.service';
import { AuthenticationServiceImpl } from './services/communication/authentication/authenticationImpl.service';
import { JwtTokenDecoderService } from './services/jwtTokenDecoder/jwtTokenDecoder.service';
import { JwtTokenDecoderServiceImpl } from './services/jwtTokenDecoder/jwtTokenDecoderImpl.service';
import { LocalStorageService } from './services/localStorage/localStorage.service';
import { LocalStorageServiceImpl } from './services/localStorage/localStorageImpl.service';
import { CurrentUserService } from './services/currentUser/currentUser.service';
import { VotingsDataService } from './services/communication/votingsData/votingsData.service';
import { VotingsDataServiceImpl } from './services/communication/votingsData/votingsDataImpl.service';
import { UrlRewriteInterceptor } from './services/communication/urlRewriteInterceptor/urlRewriteInterceptor.service';
import { VoteService } from './services/communication/vote/vote.service';
import { VoteServiceImpl } from './services/communication/vote/voteImpl.service';
import { SecretsService } from './services/secrets/secrets.service';
import { SaveFileService } from './services/saveFile/saveFile.service';
import { KeySpecifiedGuard } from './guards/keySpecified.guard';
import { DataForKeyCalculationJsonParser } from './services/jsonParser/dataForKeyCalculationJsonParser.service';
import { DataForKeyCalculationJsonParserImpl } from './services/jsonParser/dataForKeyCalculationJsonParserImpl.service';
import {MathService} from './services/math/math.service';
import {MathServiceImpl} from './services/math/mathImpl.service';
import {CommitmentsService} from './services/communication/commitments/commitments.service';
import {CommitmentsServiceImpl} from './services/communication/commitments/commitmentsImpl.service';
import {SecretSharesService} from './services/communication/secretShares/secretShares.service';
import {SecretSharesServiceImpl} from './services/communication/secretShares/secretSharesImpl.service';
import {SecretSharesForVoterJsonParser} from './services/jsonParser/secretSharesForVoterJsonParser.service';
import {SecretSharesForVoterJsonParserImpl} from './services/jsonParser/secretSharesForVoterJsonParserImpl.service';
import {CommitmentJsonParser} from './services/jsonParser/commitmentJsonParser.service';
import {CommitmentJsonParserImpl} from './services/jsonParser/commitmentJsonParserImpl.service';
import {PrimesService} from './services/primes/primes.service';
import {PrimesServiceImpl} from './services/primes/primesImpl.service';
import {VotingVoterProgressJsonParser} from './services/jsonParser/votingVoterProgressJsonParser.service';
import {VotingVoterProgressJsonParserImpl} from './services/jsonParser/votingVoterProgressJsonParserImpl.service';
import {VotingVoterProgressService} from './services/communication/votingVoterProgress/votingVoterProgress.service';
import {VotingVoterProgressServiceImpl} from './services/communication/votingVoterProgress/votingVoterProgressImpl.service';
import {VotesJsonParser} from './services/jsonParser/votesJsonParser.service';
import {VotesJsonParserImpl} from './services/jsonParser/votesJsonParserImpl.service';
import {SecretPartOfKeyParser} from './services/jsonParser/secretPartOfKeyParser.service';
import {SecretPartOfKeyParserImpl} from './services/jsonParser/secretPartOfKeyParserImpl.service';
import {DecryptionFactorsJsonParser} from './services/jsonParser/decryptionFactorsJsonParser.service';
import {DecryptionFactorsJsonParserImpl} from './services/jsonParser/decryptionFactorsJsonParserImpl.service';
import {DecryptionFactorsService} from './services/communication/decryptionFactors/decryptionFactors.service';
import {DecryptionFactorsServiceImpl} from './services/communication/decryptionFactors/decryptionFactorsImpl.service';
import {PrimeFactorizationService} from './services/primeFactorization/primeFactorization.service';
import {PrimeFactorizationServiceImpl} from './services/primeFactorization/primeFactorizationImpl.service';
import {LagrangeService} from './services/lagrange/lagrange.service';
import {LagrangeServiceImpl} from './services/lagrange/lagrangeImpl.service';
import {RefreshTokenInterceptor} from './services/communication/refreshTokenInterceptor/refreshTokenInterceptor';
import {IsAdminGuard} from './guards/isAdmin.guard';
import {RolesJsonParser} from './services/jsonParser/rolesJsonParser.service';
import {RolesJsonParserImpl} from './services/jsonParser/rolesJsonParserImpl.service';
import {RolesService} from './services/communication/roles/roles.service';
import {RolesServiceImpl} from './services/communication/roles/rolesImpl.service';
import {VoteHelperService} from './services/vote/voteHelper.service';
import {VoteHelperServiceImpl} from './services/vote/voteHelperImpl.service';
import {KeysService} from './services/communication/keys/keys.service';
import {VoterParserService} from './services/voterParser/voterParser.service';
import {VoterParserServiceImpl} from './services/voterParser/voterParserImpl.service';
import {VotersService} from './services/communication/voters/voters.service';
import {VotersServiceImpl} from './services/communication/voters/votersImpl.service';
import {VotersJsonParser} from './services/jsonParser/votersJsonParser.service';
import {VotersJsonParserImpl} from './services/jsonParser/votersJsonParserImpl.service';
import {IsVotingAdminGuard} from './guards/isVotingAdmin.guard';
import {VotingProgressJsonParser} from './services/jsonParser/votingProgressJsonParser.service';
import {VotingProgressJsonParserImpl} from './services/jsonParser/votingProgressJsonParserImpl.service';
import {VotingProgressService} from './services/communication/votingProgress/votingProgress.service';
import {VotingProgressServiceImpl} from './services/communication/votingProgress/votingProgressImpl.service';
import {VotingsJsonParser} from './services/jsonParser/votingsJsonParser.service';
import {VotingsJsonParserImpl} from './services/jsonParser/votingsJsonParserImpl.service';
import {IsRegistrarGuard} from './guards/isRegistrar.guard';
import {VotingsWithProgressJsonParser} from './services/jsonParser/votingsWithProgressJsonParser.service';
import {VotingsWithProgressJsonParserImpl} from './services/jsonParser/votingsWithProgressJsonParserImpl.service';
import {AuthenticatedVoterService} from './services/communication/authenticatedVoter/authenticatedVoter.service';
import {AuthenticatedVoterServiceImpl} from './services/communication/authenticatedVoter/authenticatedVoterImpl.service';
import {VoteNIZKProofService} from './services/communication/voteNIZKProofs/voteNIZKProofs.service';
import {VoteNIZKProofServiceImpl} from './services/communication/voteNIZKProofs/voteNIZKProofsImpl.service';
import {DecryptionNIZKProofService} from './services/communication/decryptionNIZKProof/decryptionNIZKProofs.service';
import {DecryptionNIZKProofServiceImpl} from './services/communication/decryptionNIZKProof/decryptionNIZKProofsImpl.service';
import {ZeroKnowledgeProofHelperService} from './services/zeroKnowledgeProofs/zeroKnowledgeProofHelper.service';
import {ZeroKnowledgeProofHelperServiceImpl} from './services/zeroKnowledgeProofs/zeroKnowledgeProofHelperImpl.service';
import {VotesNIZKProofJsonParser} from './services/jsonParser/votesNIZKProofJsonParser.service';
import {VotesNIZKProofJsonParserImpl} from './services/jsonParser/votesNIZKProofJsonParserImpl.service';
import {SubscriberService} from './services/subscription/subscriber.service';
import {SubscriberServiceImpl} from './services/subscription/subscriberImpl.service';
import {IsAdminOrRegistrarGuard} from './guards/isAdminOrRegistrar.guard';
import {EnvironmentService} from './services/environment/environment.service';
import {EnvironmentServiceImpl} from './services/environment/environmentImpl.service';
import {PrivateKeyService} from './services/privateKey/privateKey.service';
import {PrivateKeyServiceImpl} from './services/privateKey/privateKeyImpl.service';
import {VotingSummaryJsonParser} from './services/jsonParser/votingSummaryJsonParser.service';
import {VotingSummaryJsonParserImpl} from './services/jsonParser/votingSummaryJsonParserImpl.service';
import {IsTrusteeGuard} from './guards/isTrustee.guard';


export const providers =
  [
    // Services
    { provide: AuthenticationService, useClass: AuthenticationServiceImpl },
    { provide: JwtTokenDecoderService, useClass: JwtTokenDecoderServiceImpl },
    { provide: CurrentUserService, useClass: CurrentUserServiceImpl },
    { provide: LocalStorageService, useClass: LocalStorageServiceImpl },
    { provide: VotingsJsonParser, useClass: VotingsJsonParserImpl },
    { provide: UserPublicKeyJsonParser, useClass: UserPublicKeyJsonParserImpl },
    { provide: VotingsDataService, useClass: VotingsDataServiceImpl },
    { provide: VoteService, useClass: VoteServiceImpl },
    { provide: CryptoService, useClass: CryptoServiceImpl },
    { provide: SecretsService, useClass: InsecureSecretsService },
    { provide: SaveFileService, useClass: SaveFileServiceImpl },
    { provide: KeysService, useClass: KeysServiceImpl },
    { provide: DataForKeyCalculationJsonParser, useClass: DataForKeyCalculationJsonParserImpl },
    { provide: MathService, useClass: MathServiceImpl },
    { provide: CommitmentsService, useClass: CommitmentsServiceImpl },
    { provide: SecretSharesService, useClass: SecretSharesServiceImpl },
    { provide: SecretSharesForVoterJsonParser, useClass: SecretSharesForVoterJsonParserImpl },
    { provide: CommitmentJsonParser, useClass: CommitmentJsonParserImpl},
    { provide: PrimesService, useClass: PrimesServiceImpl},
    { provide: VotingVoterProgressJsonParser, useClass: VotingVoterProgressJsonParserImpl},
    { provide: VotingVoterProgressService, useClass: VotingVoterProgressServiceImpl},
    { provide: VotesJsonParser, useClass: VotesJsonParserImpl},
    { provide: SecretPartOfKeyParser, useClass: SecretPartOfKeyParserImpl},
    { provide: DecryptionFactorsJsonParser, useClass: DecryptionFactorsJsonParserImpl},
    { provide: DecryptionFactorsService, useClass: DecryptionFactorsServiceImpl},
    { provide: PrimeFactorizationService, useClass: PrimeFactorizationServiceImpl},
    { provide: LagrangeService, useClass: LagrangeServiceImpl},
    { provide: RolesService, useClass: RolesServiceImpl},
    { provide: RolesJsonParser, useClass: RolesJsonParserImpl},
    { provide: VoteHelperService, useClass: VoteHelperServiceImpl},
    { provide: VoterParserService, useClass: VoterParserServiceImpl},
    { provide: VotersService, useClass: VotersServiceImpl},
    { provide: VotersJsonParser, useClass: VotersJsonParserImpl},
    { provide: VotingProgressJsonParser, useClass: VotingProgressJsonParserImpl},
    { provide: VotingProgressService, useClass: VotingProgressServiceImpl},
    { provide: VotingsWithProgressJsonParser, useClass: VotingsWithProgressJsonParserImpl},
    { provide: AuthenticatedVoterService, useClass: AuthenticatedVoterServiceImpl},
    { provide: VoteNIZKProofService, useClass: VoteNIZKProofServiceImpl},
    { provide: DecryptionNIZKProofService, useClass: DecryptionNIZKProofServiceImpl},
    { provide: ZeroKnowledgeProofHelperService, useClass: ZeroKnowledgeProofHelperServiceImpl},
    { provide: VotesNIZKProofJsonParser, useClass: VotesNIZKProofJsonParserImpl},
    { provide: VotingSummaryJsonParser, useClass: VotingSummaryJsonParserImpl},
    { provide: SubscriberService, useClass: SubscriberServiceImpl},
    { provide: EnvironmentService, useClass: EnvironmentServiceImpl},
    { provide: PrivateKeyService, useClass: PrivateKeyServiceImpl},
    // Guards
    LoggedInGuard,
    KeySpecifiedGuard,
    IsAdminGuard,
    IsVotingAdminGuard,
    IsRegistrarGuard,
    IsAdminOrRegistrarGuard,
    IsTrusteeGuard,
    // Interceptors
    { provide: HTTP_INTERCEPTORS, useClass: UrlRewriteInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: AuthorizationInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: RefreshTokenInterceptor, multi: true },
  ];
