import {VoterParserService} from '../services/voterParser/voterParser.service';

export class NewVoterLine {

    private name = '';
    private email = '';

    private isAdmin = false;
    private isVotingAdmin = false;
    private isRegistrar = false;

    private codeMirror;
    private voterParserService: VoterParserService;

    private checkedForValidity = false;

    public setCodeMirror(codeMirror): NewVoterLine {
        this.codeMirror = codeMirror;
        return this;
    }

    public setParser(voterParserService: VoterParserService): NewVoterLine {
        this.voterParserService = voterParserService;
        return this;
    }

    public getName() {
        return this.name;
    }

    public setName(name: string) {
        this.name = name;
    }

    public getEmail() {
        return this.email;
    }

    public setEmail(email: string) {
        this.email = email;
    }

    public getIsAdmin() {
        return this.isAdmin;
    }

    public setAdmin(isAdmin: boolean) {
        this.isAdmin = isAdmin;
    }

    public getIsVotingAdmin() {
        return this.isVotingAdmin;
    }

    public setVotingAdmin(isVotingAdmin: boolean) {
        this.isVotingAdmin = isVotingAdmin;
    }

    public getIsRegistrar() {
        return this.isRegistrar;
    }

    public setRegistrar(isRegistrar: boolean) {
        this.isRegistrar = isRegistrar;
    }

    public update(index: number) {
        const textRepresentation = this.createTextRepresentation();

        const oldText = this.codeMirror.getLine(index);
        this.codeMirror.replaceRange(textRepresentation, {line: index, ch: 0}, {line: index, ch: oldText.length});
    }

    public createTextRepresentation() {
        const roles = [];

        if (this.getIsAdmin()) {
            roles.push('Administrator');
        }
        if (this.getIsVotingAdmin()) {
            roles.push('Voting Administrator');
        }
        if (this.getIsRegistrar()) {
            roles.push('Registrar');
        }

        let textRepresentation = this.name + '\t' + this.email;

        if (roles.length !== 0) {
            const rolesRep = '\t(' + roles.join(',') + ')';
            textRepresentation += rolesRep;
        }
        return textRepresentation;
    }

    public parseFromCodeMirror(index: number) {
        const textToParse = this.codeMirror.getLine(index);
        const maybeVoter = this.voterParserService.parseLine(textToParse);

        if (!maybeVoter.isEmpty()) {
            const voter = maybeVoter.getData();
            this.setName(voter.getName());
            this.setEmail(voter.getEmail());
            this.setAdmin(voter.isAdministrator());
            this.setVotingAdmin(voter.isVoting_Administrator());
            this.setRegistrar(voter.isRegistrar());

            this.codeMirror.setGutterMarker(index, 'breakpoints', null);
        }
        else {
            this.codeMirror.setGutterMarker(index, 'breakpoints', this.makeMarker());
        }
    }

    public isNameValid() {
        return this.getName() !== '';
    }

    public isEmailValid() {
        return this.getEmail() !== '';
    }

    private makeMarker() {
        const marker = document.createElement('div');
        marker.style.color = '#822';
        marker.innerHTML = '●';
        return marker;
    }

    public setCheckedForValidity(value: boolean) {
        this.checkedForValidity = value;
    }

    public getCheckedForValidity() {
        return this.checkedForValidity;
    }

    public isValid(): boolean {
        return this.isNameValid() && this.isEmailValid();
    }

}
