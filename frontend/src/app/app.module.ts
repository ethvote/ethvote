import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';

import { providers } from './app.providers';
import { imports } from './app.imports';
import { declarations } from './app.declarations';


@NgModule({
  declarations: declarations,
  imports: imports,
  providers: providers,
  bootstrap: [AppComponent]
})
export class AppModule { }
