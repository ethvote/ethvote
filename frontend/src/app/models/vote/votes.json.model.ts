import {JsonProperty, JsonObject } from 'json2typescript';
import {SingleVote} from './singleVote.json.model';

@JsonObject('Votes')
export class Votes {

    @JsonProperty('votes', [SingleVote])
    private votes: SingleVote[] = undefined;

    public getVotes(): SingleVote[] {
        return this.votes;
    }

}
