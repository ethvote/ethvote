import { JsonObject, JsonProperty } from 'json2typescript';

@JsonObject('Voter')
export class Voter {

    @JsonProperty('name', String)
    name: string = null;

    @JsonProperty('id', Number)
    id: number = null;

    email: string = null;

    getName(): string {
        return this.name;
    }

    getEmail(): string {
        return this.email;
    }

    getId(): number {
        return this.id;
    }

    public setName(name: string): Voter {
        this.name = name;
        return this;
    }

    public setEmail(email: string): Voter {
        this.email = email;
        return this;
    }

    public setId(id: number): Voter {
        this.id = id;
        return this;
    }

}
