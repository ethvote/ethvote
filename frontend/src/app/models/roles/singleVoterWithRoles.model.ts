import { JsonProperty, JsonObject } from 'json2typescript';

@JsonObject('SingleVoterWithRoles')
export class SingleVoterWithRoles {

    @JsonProperty('id', Number)
    private id: number = null;

    @JsonProperty('name', String)
    private name: string = null;

    @JsonProperty('roles', [String])
    private roles: string[] = new Array();

    setName(name: string): SingleVoterWithRoles {
        this.name = name;
        return this;
    }

    setRoles(roles: string[]): SingleVoterWithRoles {
        this.roles = roles;
        return this;
    }

    setId(id: number): SingleVoterWithRoles {
        this.id = id;
        return this;
    }

    getId(): number {
        return this.id;
    }

    getName(): string {
        return this.name;
    }

    getRoles(): string[] {
        return this.roles;
    }

    isAdministrator(): boolean {
        return this.roles.indexOf('Administrator') !== -1;
    }

    isVotingAdministrator(): boolean {
        return this.roles.indexOf('Voting_Administrator') !== -1;
    }

    isRegistrar(): boolean {
        return this.roles.indexOf('Registrar') !== -1;
    }

    modifyAdminRole(newValue: boolean) {
        if (newValue) {
            this.roles.push('Administrator');
        }
        else {
            this.roles = this.roles.filter(word => word !== 'Administrator');
        }
    }

    modifyVotingAdminRole(newValue: boolean) {
        if (newValue) {
            this.roles.push('Voting_Administrator');
        }
        else {
            this.roles = this.roles.filter(word => word !== 'Voting_Administrator');
        }
    }

    modifyRegistrarRole(newValue: boolean) {
        if (newValue) {
            this.roles.push('Registrar');
        }
        else {
            this.roles = this.roles.filter(word => word !== 'Registrar');
        }
    }
}
