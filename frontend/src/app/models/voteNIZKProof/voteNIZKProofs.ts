import {JsonProperty, JsonObject } from 'json2typescript';
import {SingleVoteNIZKProof} from './singleVoteNIZKProof';

@JsonObject('VoteNIZKProof')
export class VoteNIZKProof {

    @JsonProperty('fromVoterId', Number)
    private fromVoterId: number = undefined;

    @JsonProperty('fromVoterName', String)
    private fromVoterName: string = undefined;

    @JsonProperty('proofs', [SingleVoteNIZKProof])
    private proofs: SingleVoteNIZKProof[] = undefined;

    public getFromVoterId(): number {
        return this.fromVoterId;
    }

    public getFromVoterName(): string {
        return this.fromVoterName;
    }

    public setZeroKnowledgeProofs(proofs: SingleVoteNIZKProof[]): VoteNIZKProof {
        this.proofs = proofs;
        return this;
    }

    public getZeroKnowledgeProofs(): SingleVoteNIZKProof[] {
        return this.proofs;
    }

}
