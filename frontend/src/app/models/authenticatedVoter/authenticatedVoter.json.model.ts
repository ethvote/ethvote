import { JsonProperty, JsonObject } from 'json2typescript';

@JsonObject('AuthenticatedVoter')
export class AuthenticatedVoter {

    @JsonProperty('voterName', String)
    private voterName: string = undefined;

    private isRegistered = false;

    public getVoterName(): string {
        return this.voterName;
    }

    public getIsRegistered(): boolean {
        return this.isRegistered;
    }

    public setVotername(voterName: string): AuthenticatedVoter {
        this.voterName = voterName;
        return this;
    }

    public setIsRegistered(isRegistered: boolean): AuthenticatedVoter {
        this.isRegistered = isRegistered;
        return this;
    }

}
