import { JsonObject, JsonProperty } from 'json2typescript';

@JsonObject('VotingVoterProgress')
export class VotingVoterProgress {

    @JsonProperty('votingId', Number)
    private votingId: number = undefined;

    @JsonProperty('phaseNumber', Number)
    private phaseNumber: number = null;

    @JsonProperty('participatedInDecryption', Boolean)
    private participatedInDecryption: boolean = null;

    @JsonProperty('voted', Boolean)
    private voted: boolean = null;

    @JsonProperty('createdSecretShares', Boolean)
    private createdSecretShares: boolean = null;

    @JsonProperty('usesCustomKey', Boolean)
    private usesCustomKey: boolean = null;

    getVotingId(): Number {
        return this.votingId;
    }

    getPhaseNumber(): Number {
        return this.phaseNumber;
    }

    getParticipatedInDecryption(): Boolean {
        return this.participatedInDecryption;
    }

    getVoted(): Boolean {
        return this.voted;
    }

    getCreatedSecretShares(): Boolean {
        return this.createdSecretShares;
    }

    getUsesCustomKey(): Boolean {
        return this.usesCustomKey;
    }

}
