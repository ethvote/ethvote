import {VotingCalculationParameters} from './votingsCalculationParameters.json.model';
import {EligibleVoter} from './eligibleVoter.json.model';
import {Voting} from './voting.json.model';
import { JsonObject, JsonProperty } from 'json2typescript';

@JsonObject('Voting')
export class NewCreatedVoting extends Voting<NewCreatedVoting> {

    @JsonProperty('calculationParameters', VotingCalculationParameters)
    private calculationParameters: VotingCalculationParameters = null;

    @JsonProperty('securityThreshold', Number)
    private securityThreshold: number = undefined;

    @JsonProperty('numberOfEligibleVoters', Number)
    private numberOfEligibleVoters: number = undefined;

    @JsonProperty('eligibleVoters', [EligibleVoter])
    private eligibleVoters: EligibleVoter[] = null;

    setSecurityThreshold(k: number): NewCreatedVoting {
        this.securityThreshold = k;
        return this;
    }

    getSecurityThreshold(): number {
        return this.securityThreshold;
    }

    setNumberOfEligibleVoters(n: number): NewCreatedVoting {
        this.numberOfEligibleVoters = n;
        return this;
    }

    getNumberOfEligibleVoters(): number {
        return this.numberOfEligibleVoters;
    }

    setEligibleVoters(voters: EligibleVoter[]): NewCreatedVoting {
        this.eligibleVoters = voters;
        return this;
    }

    getEligibleVoters(): EligibleVoter[] {
        return this.eligibleVoters;
    }

    setVotingCalculationParameters(votingCalculationParameters: VotingCalculationParameters): NewCreatedVoting {
        this.calculationParameters = votingCalculationParameters;
        return this;
    }

    getVotingCalculationParameters(): VotingCalculationParameters {
        return this.calculationParameters;
    }
}
