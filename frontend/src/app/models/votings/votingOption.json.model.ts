import { JsonObject, JsonProperty } from 'json2typescript';

@JsonObject('Option')
export class VotingOption {

    @JsonProperty('optionName', String)
    optionName: string = null;

    @JsonProperty('optionPrimeNumber', Number)
    optionPrimeNumber: number = null;

    getOptionName(): String {
        return this.optionName;
    }

    setOptionName(optionName: string): VotingOption {
        this.optionName = optionName;
        return this;
    }

    getOptionPrimeNumber(): Number {
        return this.optionPrimeNumber;
    }

    setOptionPrimeNumber(optionPrimeNumber: number): VotingOption {
        this.optionPrimeNumber = optionPrimeNumber;
        return this;
    }

}
