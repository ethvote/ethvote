import { JsonObject, JsonProperty } from 'json2typescript';
import { Voting } from './voting.json.model';
import {VotingOverview} from './votingOverview.json.model';

@JsonObject('VotingsArray')
export class VotingsArray {

    @JsonProperty('votings', [VotingOverview])
    votings: VotingOverview[] = new Array();

    getVotings(): VotingOverview[] {
        return this.votings;
    }

    setVotings(votings: VotingOverview[]): VotingsArray  {
        this.votings = votings;
        return this;
    }

}
