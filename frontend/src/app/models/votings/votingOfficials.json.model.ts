import { JsonObject, JsonProperty } from 'json2typescript';

@JsonObject('VotingOfficials')
export class VotingOfficials {

    @JsonProperty('votingId', Number)
    private votingId: number = undefined;

    @JsonProperty('votingAdministrator', String)
    private votingAdministrator: string = undefined;

    @JsonProperty('trustees', [String])
    private trustees: string[] = undefined;

    getVotingId(): Number {
        return this.votingId;
    }

    getVotingAdministrator(): String {
        return this.votingAdministrator;
    }

    getTrustees(): String[] {
        return this.trustees;
    }

}
