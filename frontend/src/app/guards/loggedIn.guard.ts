import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import { Injectable } from '@angular/core';
import { CurrentUserService } from '../services/currentUser/currentUser.service';

@Injectable()
export class LoggedInGuard implements CanActivate {

    constructor(private currentUserService: CurrentUserService,
                private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (this.currentUserService.isCurrentUserLoggedIn()) {
            return true;
        }
        else {
            this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
            return false;
        }
    }
}
