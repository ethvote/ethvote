import { Maybe } from 'src/app/utils/maybe.utils';
import { KeySpecifiedGuard } from './../../app/guards/keySpecified.guard';
import { RouterTestingModule } from '@angular/router/testing';
import { TestBed } from '@angular/core/testing';
import { CurrentUserService } from '../../app/services/currentUser/currentUser.service';
import { NoopCurrentUserService } from '../services/currentUser/noopCurrentUser.service';
import { Router } from '@angular/router';

describe('KeySpecifiedGuard', () => {

    let noopCurrentUserService: CurrentUserService;
    let keySpecifiedGuard: KeySpecifiedGuard;

    let router: Router;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [RouterTestingModule],
        });

        router = TestBed.get(Router);

        noopCurrentUserService = new NoopCurrentUserService();
        keySpecifiedGuard = new KeySpecifiedGuard(noopCurrentUserService, router);
    });

    it('should return false if public key is not available', () => {
        spyOn(noopCurrentUserService, 'getPublicKey').and.returnValue(Maybe.fromEmpty());
        expect(keySpecifiedGuard.canActivate()).toBeFalsy();
    });

    it('should return true if public key is available', () => {
        spyOn(noopCurrentUserService, 'getPublicKey').and.returnValue(Maybe.fromData('dummy'));
        expect(keySpecifiedGuard.canActivate()).toBeTruthy();
    });

});
