import {Helper} from '../../app/utils/helper.utils';

describe('Helper', () => {

    it('should be able to spilt string shorter then one chunk', () => {
        const stringToSplit = '111';
        const splitted = Helper.splitString(stringToSplit, 5);
        expect(splitted.length).toBe(1);
        expect(splitted[0]).toBe(stringToSplit);
    });

    it('should be able to spilt string with multiple chunks last chunk shorter', () => {
        const stringToSplit = '1111122222333';
        const splitted = Helper.splitString(stringToSplit, 5);
        expect(splitted.length).toBe(3);
        expect(splitted[0]).toBe('11111');
        expect(splitted[1]).toBe('22222');
        expect(splitted[2]).toBe('333');
    });

    it('should be able to spilt string with multiple chunks', () => {
        const stringToSplit = '111112222233333';
        const splitted = Helper.splitString(stringToSplit, 5);
        expect(splitted.length).toBe(3);
        expect(splitted[0]).toBe('11111');
        expect(splitted[1]).toBe('22222');
        expect(splitted[2]).toBe('33333');
    });

    it('should be able to concat strings', () => {
        const one = '123';
        const two = '456';
        const three = '78';
        expect(Helper.concatString([one, two, three])).toBe(one + two + three);
    });

});
