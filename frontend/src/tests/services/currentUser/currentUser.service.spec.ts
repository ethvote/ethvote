import { JwtTokenDecoderServiceImpl } from './../../../app/services/jwtTokenDecoder/jwtTokenDecoderImpl.service';
import { Maybe } from './../../../app/utils/maybe.utils';
import { LocalStorageService } from '../../../app/services/localStorage/localStorage.service';
import { CurrentUserService } from '../../../app/services/currentUser/currentUser.service';
import { CurrentUserServiceImpl } from '../../../app/services/currentUser/currentUserImpl.service';
import { JwtTokenDecoderService } from 'src/app/services/jwtTokenDecoder/jwtTokenDecoder.service';

class MockStorageService extends LocalStorageService {
    get(key: string): Maybe<string> {
      return null;
    }

    upsert(key: string, value: string) {}

    delete(key: string): boolean {
        return false;
    }
  }

let storageService: LocalStorageService;
let currentUserService: CurrentUserService;
let jwtTokenDecoderService: JwtTokenDecoderService;

const currentVoterId = 42;
const currentVoterName = 'John Doe';

const tokenForJohnDoe =
`eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.\
eyJzdWJJZCI6IjQyIiwic3ViIjoiSm9obiBEb2UifQ.\
35vXIRl5oQj2UeF5l_Sm6TZc3gCy4o1pqiEHBHXyHWRRblYz0bHenUhs9jNW7VsAFlPdaqeU1Pcdql0Gybr-OQ`;

const tokenWithoutId =
`eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJKb2huIERvZSJ9.uvyq6w5K3kjiCYIk3\
hoProrOuAfjGfhoR0K4T0GPlrQ`;

const tokenWithInvalidId =
`eyJhbGciOiJIUzI1NiJ9.eyJzdWJJZCI6ImR1bW15Iiwic3ViIjoiSm9obiBEb2U\
ifQ.V780ex2fz8jKWE8WLYFm3dj-fh4-8Vap1QVRqMgNfU0`;

const publicKey =
`-----BEGIN PUBLIC KEY-----\
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDJc3q7lPV/O6F75X2qCer5HVtO\
jHzJpNLzamtXrrAVFG8pN6Hdyg4jFp51PN1OvHOLdILQD0dYIA8Czb0qs/N94i8v\
UY9ovK6ta9HW3fs8TZLnZ7slKmkOcGpnWLRdphEumLonyk3AEhU58dsh+mlJS5u+\
MtcsG+Nx1/B/3AZH/QIDAQAB\
-----END PUBLIC KEY-----`;

const tokenForJohnDoeWithPublicKey =
`eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWJJZCI6IjQyIiwic3ViIj\
oiSm9obiBEb2UiLCJwdWJsaWNLZXkiOiItLS0tLUJFR0lOIFBVQkxJQyBLRVktLS\
0tLU1JR2ZNQTBHQ1NxR1NJYjNEUUVCQVFVQUE0R05BRENCaVFLQmdRREpjM3E3bF\
BWL082Rjc1WDJxQ2VyNUhWdE9qSHpKcE5MemFtdFhyckFWRkc4cE42SGR5ZzRqRn\
A1MVBOMU92SE9MZElMUUQwZFlJQThDemIwcXMvTjk0aTh2VVk5b3ZLNnRhOUhXM2\
ZzOFRaTG5aN3NsS21rT2NHcG5XTFJkcGhFdW1Mb255azNBRWhVNThkc2grbWxKUz\
V1K010Y3NHK054MS9CLzNBWkgvUUlEQVFBQi0tLS0tRU5EIFBVQkxJQyBLRVktLS\
0tLSJ9.I2CH9QA6ZRTkkvcxjdqJO63aBqF0M2QBsJN3TS-tuAnS90WtHH6MYy6tc\
04v61tRTnOqOIu7LSWmTzpdX6YlPA`;

const tokenForAdminWithRoles =
`eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWJJZCI6IjEiLCJzdWIiOi\
JhZG1pbiIsImV4cGlyYXRpb25UaW1lIjoxNTQ1MjI1NTM5LCJwdWJsaWNLZXkiOi\
ItLS0tLUJFR0lOIFBVQkxJQyBLRVktLS0tLVxyXG5NSUlCSWpBTkJna3Foa2lHOX\
cwQkFRRUZBQU9DQVE4QU1JSUJDZ0tDQVFFQXN6b0Z6VVN6ZVk1N2R3SnpvMW1kXH\
JcbkVFSW1vb2hmT0xUb1JYd0x1cmEySHB6blN1Vm5obEN0YU1acXpyejlVWHFPZ1\
I0R2xlUVRZSzlwRGZKSXBCWHJcclxuOHhqRURIZzJpU1lYdGU5QjlrLzJNWGw5al\
A0QTZ0WjMyK25xdnFJcTN2UlQ0UWJIcWdVWjRNc2lCRXc0Ni9JZ1xyXG4vMVNvTG\
xqKy9VLzNzV1o2SWxqSmJYdnFYMXFzRkxsK3l2NktYNU9hVnhxamZLSGN1MFh0Zl\
JRT1hGeGJ3cGZJXHJcbmpVSWFkMXhJa0hhN3hySUpCdXpZUitRZWdMVTJRMHcvdF\
BjN24xMDQrTnNTaGg1OGlxVTh0VXAyTnIxMFptc3VcclxuRWNhdnUyT0JBOSt5MV\
FsREVqZWQycW9yTS9oOGFzMzVUeEVvVmVzM3dUbnJZUnV3Qmc2TzF2RmZnMUcrcF\
c0NFxyXG4zd0lEQVFBQlxyXG4tLS0tLUVORCBQVUJMSUMgS0VZLS0tLS1cclxuIi\
wic3ViUm9sZXMiOlsiQWRtaW5pc3RyYXRvciIsIlBvd2Vydm90ZXIiXX0.fzOuYC\
xuRQDP64vUVfbnFGdIzkqtWhc7U2iSF7291U8IgcaNqa93G1EkAZGO8HpS59tiCP\
AjSrhpsnuBGQVikw`;

const tokenWithRegistrarRole =
`eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWJJZCI6IjEiLCJzdWIiOi\
JhZG1pbiIsImV4cGlyYXRpb25UaW1lIjoxNTQ2MzY1Njk5LCJwdWJsaWNLZXkiOi\
ItLS0tLUJFR0lOIFBVQkxJQyBLRVktLS0tLVxyXG5NSUlCSWpBTkJna3Foa2lHOX\
cwQkFRRUZBQU9DQVE4QU1JSUJDZ0tDQVFFQTJHbURQZjFuWS91KzhTQXgzNTlqXH\
JcbmxHamYxVkZWSFVzdjRpZDROTWdZVURkM2pmS2ZMVmNtclNBNE5KdFM4N05wQm\
VuQS9WTW1YNlRSZFJzZG5XV2FcclxuR2plSml5YkhOdlBmK0Y4TWFBSnM3YUxRcj\
Z3elNwTjhYaEV6Z3ZMYTZBK2FqRnovbXlGY0hjRmVaMXV4WTBjaVxyXG5jb0tDS1\
hlZ0xaaG93N0g0eS9lSUd5a0szN2w3aEVmQmNWL1RVOVhnYzFtTk9INjNCQzVkYT\
l4NXNtTkY3WUdvXHJcbkllOXdCeXRMVHcweC9QMTVJT25IaUpsbVdueDFJVWlnUD\
RPenNoenpTbUVpcGFGdmhsWG03MWNDS0V3ZmwybmpcclxuSU1nUFovSndmTWJoa0\
R1VE5GcEUvd1lFWEU4RzNPUk5QUWIvVmE5V3AwN1Q1WW1iaW1MK1VOV0NFdmdGUW\
1IYlxyXG5Pd0lEQVFBQlxyXG4tLS0tLUVORCBQVUJMSUMgS0VZLS0tLS1cclxuIi\
wic3ViUm9sZXMiOlsiUmVnaXN0cmFyIl19.OaTSaO8mPjsCuenpCY250sW7AqQTm\
G-xGz2TCE06C4uErCADObLJUvVIZ5QsLdHN6LbynRoZ488QZYWttA9VQg`;

const tokenWithVotingAdministratorRole =
`eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWJJZCI6IjEiLCJzdWIiOi\
JhZG1pbiIsImV4cGlyYXRpb25UaW1lIjoxNTQ2MzY1Njk5LCJzdWJSb2xlcyI6Wy\
JWb3RpbmdfQWRtaW5pc3RyYXRvciJdfQ.5somytS1iEl7U1zVwoPf6R5K3fIz7mZ\
onY49ijhhIhTwf8aDgOb7RMi6yY_TxMqtAqtyP7xI2pdC5Q9Hm9wB4Q`;

const tokenWithZeroVotingsAsTrustee =
`eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWJJZCI6IjEiLCJzdWIiOi\
JhZG1pbiIsImV4cGlyYXRpb25UaW1lIjoxNTQ2MzY1Njk5LCJudW1iZXJPZlZvdG\
luZ3NXaXRoVHJ1c3RlZVJvbGUiOiIwIiwic3ViUm9sZXMiOlsiVm90aW5nX0FkbW\
luaXN0cmF0b3IiXX0.2yLgmFcpg7ekdQjWMJoB4Z3ZTfEqVMHDvxAqznfI3WrS0t\
ShLA_mwxc3moQGROR--iIT8ILhisF70n0EU9mFcA`;

const tokenWithOneVotingAsTrustee =
`eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWJJZCI6IjEiLCJzdWIiOi\
JhZG1pbiIsImV4cGlyYXRpb25UaW1lIjoxNTQ2MzY1Njk5LCJudW1iZXJPZlZvdG\
luZ3NXaXRoVHJ1c3RlZVJvbGUiOiIxIiwic3ViUm9sZXMiOlsiVm90aW5nX0FkbW\
luaXN0cmF0b3IiXX0.CC8Km74eLxoQbtO-iKiFrMUnJsUDbv6XDbkbqTgI5vOcbG\
wrs0QacjRYdGB7xFtv8alRAs4qtLUhT_RnIOCrxQ`;

describe('CurrentUserImpl', () => {


    beforeEach(() => {
        storageService = new MockStorageService();
        jwtTokenDecoderService = new JwtTokenDecoderServiceImpl();
        currentUserService = new CurrentUserServiceImpl(storageService, jwtTokenDecoderService);
      });

    it('should be created', () => {
        expect(currentUserService).toBeTruthy();
      });

    it('if token available, should return true', () => {
        spyOn(storageService, 'get').and.returnValue(Maybe.fromData(tokenForJohnDoe));
        expect(currentUserService.isCurrentUserLoggedIn()).toBeTruthy();
    });

    it('if token not available return false', () => {
        spyOn(storageService, 'get').and.returnValue(Maybe.fromEmpty());
        expect(currentUserService.isCurrentUserLoggedIn()).toBeFalsy();
    });

    it('should be able to return the public key if there is one', () => {
        spyOn(storageService, 'get').and.returnValue(Maybe.fromData(tokenForJohnDoeWithPublicKey));
        expect(currentUserService.getPublicKey().getData()).toBe(publicKey);
    });

    it('should be able to return the current voter id', () => {
        spyOn(storageService, 'get').and.returnValue(Maybe.fromData(tokenForJohnDoeWithPublicKey));
        expect(currentUserService.getUserId().getData()).toBe(currentVoterId);
    });

    it('should be able to detect missing current voter id', () => {
        spyOn(storageService, 'get').and.returnValue(Maybe.fromData(tokenWithoutId));
        expect(currentUserService.getUserId().isEmpty()).toBeTruthy();
    });

    it('should be able to detect invalid current voter id', () => {
        spyOn(storageService, 'get').and.returnValue(Maybe.fromData(tokenWithInvalidId));
        expect(currentUserService.getUserId().isEmpty()).toBeTruthy();
    });

    it('should be able to return the current voter name', () => {
        spyOn(storageService, 'get').and.returnValue(Maybe.fromData(tokenForJohnDoeWithPublicKey));
        expect(currentUserService.getVoterName().getData()).toBe(currentVoterName);
    });

    it('should return empty if key is not specified', () => {
        spyOn(storageService, 'get').and.returnValue(Maybe.fromData(tokenForJohnDoe));
        expect(currentUserService.getPublicKey().isEmpty()).toBeTruthy();
    });

    it('should be able to get roles of voter if empty', () => {
        spyOn(storageService, 'get').and.returnValue(Maybe.fromData(tokenForJohnDoe));
        expect(currentUserService.getRoles().isEmpty()).toBeTruthy();
    });

    it('should be able to get roles of voter if roles there', () => {
        spyOn(storageService, 'get').and.returnValue(Maybe.fromData(tokenForAdminWithRoles));
        expect(currentUserService.getRoles().getData()).toEqual(['Administrator', 'Powervoter']);
    });

    it('should return false voter not admin', () => {
        spyOn(storageService, 'get').and.returnValue(Maybe.fromData(tokenForJohnDoe));
        expect(currentUserService.isCurrentUserAdmin()).toBeFalsy();
    });

    it('should return true if voter is admin', () => {
        spyOn(storageService, 'get').and.returnValue(Maybe.fromData(tokenForAdminWithRoles));
        expect(currentUserService.isCurrentUserAdmin()).toBeTruthy();
    });

    it('should return false voter not powervoter', () => {
        spyOn(storageService, 'get').and.returnValue(Maybe.fromData(tokenForJohnDoe));
        expect(currentUserService.isCurrentUserRegistrar()).toBeFalsy();
    });

    it('should return true if voter is powervoter', () => {
        spyOn(storageService, 'get').and.returnValue(Maybe.fromData(tokenWithRegistrarRole));
        expect(currentUserService.isCurrentUserRegistrar()).toBeTruthy();
    });

    it('should return false if voter not voting administrator', () => {
        spyOn(storageService, 'get').and.returnValue(Maybe.fromData(tokenForJohnDoe));
        expect(currentUserService.isCurrentUserVotingAdministrator()).toBeFalsy();
    });

    it('should return true if voter is voting administrator', () => {
        spyOn(storageService, 'get').and.returnValue(Maybe.fromData(tokenWithVotingAdministratorRole));
        expect(currentUserService.isCurrentUserVotingAdministrator()).toBeTruthy();
    });

    it('should return not trustee at the moment if token has 0 votings as trustee', () => {
        spyOn(storageService, 'get').and.returnValue(Maybe.fromData(tokenWithZeroVotingsAsTrustee));
        expect(currentUserService.isCurrentUserTrusteeAtTheMoment()).toBeFalsy();
    });

    it('should return trustee at the moment if token has 1 votings as trustee', () => {
        spyOn(storageService, 'get').and.returnValue(Maybe.fromData(tokenWithOneVotingAsTrustee));
        expect(currentUserService.isCurrentUserTrusteeAtTheMoment()).toBeTruthy();
    });

});
