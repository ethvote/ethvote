import { MathService } from '../../../app/services/math/math.service';
import { MathServiceImpl } from '../../../app/services/math/mathImpl.service';
import { Polynom } from '../../../app/data/polynom';


describe('MathServiceImpl', () => {

    let service: MathService;
    const BigInteger = require('jsbn').BigInteger;

    beforeEach(() => {
        service = new MathServiceImpl();
    });

    it('positive number is recognized positive', () => {
        const number = '1238907';
        expect(service.isPositive(number)).toBeTruthy();
    });

    it('positive number is recognized positive', () => {
        const number = '-2';
        expect(service.isPositive(number)).toBeFalsy();
    });

    it('random number creation works', () => {
        const upperBound = '3246123418631206';
        const randomNumber = service.generateRandom(upperBound);
        expect(isLarger(upperBound, randomNumber)).toBeTruthy();
    });

    it('random number from one bytes works', () => {
        const numberOfBytes = 1;
        const randomNumber = service.generateNBytesRandomNumber(numberOfBytes);
        const random: number = +randomNumber;

        expect(random).toBeLessThan(256);
    });

    it('random number from multiple bytes works', () => {
        const numberOfBytes = 3;

        const randomNumber = service.generateNBytesRandomNumber(numberOfBytes);
        const random: number = +randomNumber;
        expect(random).toBeLessThan(16777216);
    });

    it('to bitlength works', () => {
        const max13BitNumber = '8191';
        const min14BitNumber = '8192';

        const bitLengthOne = service.toBitLength(max13BitNumber);
        const bitLengthTwo = service.toBitLength(min14BitNumber);
        expect(bitLengthOne).toBe(13);
        expect(bitLengthTwo).toBe(14);
    });

    it('to bytelength works', () => {
        const max2ByteNumber = '65535';
        const min3ByteNumber = '65536';

        const byteLengthOne = service.toByteLengthRoundedUp(max2ByteNumber);
        const byteLengthTwo = service.toByteLengthRoundedUp(min3ByteNumber);
        expect(byteLengthOne).toBe(2);
        expect(byteLengthTwo).toBe(3);
    });

    it('polynom creation works', () => {
        const numberOfExponents = 7;
        const upperBound = '46812315645616';
        const polynomial: Polynom = service.generateRandomPolynomial(numberOfExponents, upperBound);
        expect(polynomial.getCoefficients().length).toBe(numberOfExponents);
        for (let i = 0; i < numberOfExponents; i++) {
            expect(isLarger(upperBound, polynomial.getCoefficients()[i])).toBeTruthy();
        }
    });

    it('polynom evaluation works', () => {
        const coefficients = ['27', '42', '13', '52'];
        const polynomial: Polynom = new Polynom(coefficients);
        const res = service.evaluatePolynomial(polynomial, '7');
        const expectedResult = '18794';
        expect(res).toBe(expectedResult);
    });

    it('power with modulo works', () => {
        const base = '45';
        const exponent = '7';
        const mod = '132';
        const res = service.powerWithModulo(base, exponent, mod);
        const expectedResult = '45';
        expect(res).toBe(expectedResult);
    });

    it('multiply all with modulo works', () => {
        const res = service.multiplyAllWithModulo(['5', '7', '1', '13'], '100');
        expect(res).toBe('55');
    });

    it('multiply with modulo works', () => {
        const factor1 = '37';
        const factor2 = '201';
        const mod = '215';
        const res = service.multiplyWithModulo(factor1, factor2, mod);
        const expectedResult = '127';
        expect(res).toBe(expectedResult);
    });

    it('multiply works', () => {
        const factor1 = '37';
        const factor2 = '201';
        const res = service.multiply(factor1, factor2);
        const expectedResult = '7437';
        expect(res).toBe(expectedResult);
    });

    it('addition works', () => {
        const summand1 = '37';
        const summand2 = '201';
        const res = service.add(summand1, summand2);
        const expectedResult = '238';
        expect(res).toBe(expectedResult);
    });

    it('addition with modulo works', () => {
        const summand1 = '37';
        const summand2 = '201';
        const mod = '200';
        const res = service.addWithModulo(summand1, summand2, mod);
        const expectedResult = '38';
        expect(res).toBe(expectedResult);
    });

    it('add all with modulo works', () => {
        const res = service.addAllWithModulo(['42', '27', '5', '0', '69'], '100');
        const expectedResult = '43';
        expect(res).toBe(expectedResult);
    });

    it('subtract  works', () => {
        const subtly = '5';
        const subtle = '7';
        const res = service.subtract(subtly, subtle);
        const expectedResult = '-2';
        expect(res).toBe(expectedResult);
    });

    it('subtract with modulo works', () => {
        const subtly = '5';
        const subtle = '7';
        const mod = '42';
        const res = service.subtractWithModulo(subtly, subtle, mod);
        const expectedResult = '40';
        expect(res).toBe(expectedResult);
    });

    it('divide with works', () => {
        const dividend = '15';
        const divisor = '3';
        const res = service.divide(dividend, divisor);
        const expectedResult = '5';
        expect(res).toBe(expectedResult);
    });

    it('multinverse with works', () => {
        const i = '7';
        const n = '20';
        const res = service.modInverse(i, n);
        const expectedResult = '3';
        expect(res).toBe(expectedResult);
    });

    it('modulo works', () => {
        const i = '27';
        const mod = '5';
        const rest = service.mod(i, mod);
        const expectedResult = '2';
        expect(rest).toBe(expectedResult);
    });

    it('absolute value works', () => {
        const i = '-27';
        const rest = service.abs(i);
        const expectedResult = '27';
        expect(rest).toBe(expectedResult);
    });

    it('hex to number works', () => {
        const hexNumber = 'DEADC0DE';
        const number = service.hexToNumber(hexNumber);
        const expectedResult = '3735929054';
        expect(number).toBe(expectedResult);
    });

    function isLarger(i: string, j: string): boolean {
        const compare = new BigInteger(i).compareTo(new BigInteger(j));
        return service.isPositive(compare.toString());
    }

});
