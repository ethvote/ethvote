import { Maybe } from 'src/app/utils/maybe.utils';
import { AsymmetricKey } from './../../../app/data/asymmetricKey';
import { CryptoService } from 'src/app/services/crypto/crypto.service';
import { CalculationParams } from 'src/app/data/calculationParams';

export class NoopCryptoService implements CryptoService {

    validateKeyPair(key: AsymmetricKey): boolean {
        return false;
    }

    generateKeyPair(): Promise<Maybe<AsymmetricKey>> {
        return null;
    }

    signSHA256(privateKeyPem: string, message: string): Maybe<string> {
        return null;
    }

    verifySHA256Signature(publicKeyPem: string, message: string, signature: string) {
        return false;
    }

    pemToSHA256Fingerprint(pem: string): string {
        return null;
    }

    generateCalculationParams(): Promise<Maybe<CalculationParams>> {
        return null;
    }

    encrypt(publicKeyPem: string, message: string): string {
        return null;
    }

    decrypt(privateKeyPem: string, cipherText: string): Maybe<string> {
        return null;
    }

    hashSHA256(message: string): string {
        return null;
    }

}
