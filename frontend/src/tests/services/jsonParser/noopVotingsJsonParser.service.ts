import { Maybe } from './../../../app/utils/maybe.utils';
import { VotingsJsonParser } from './../../../app/services/jsonParser/votingsJsonParser.service';
import { VotingsArray } from '../../../app/models/votings/votingsArray.json.model';
import {VotingOverview} from '../../../app/models/votings/votingOverview.json.model';

export class NoopVotingJsonParser implements VotingsJsonParser {

    parseJson(s: string): Maybe<VotingsArray> {
        return Maybe.fromEmpty();
    }

    parseSingleVoting(jsonString: string): Maybe<VotingOverview> {
        return undefined;
    }
}
