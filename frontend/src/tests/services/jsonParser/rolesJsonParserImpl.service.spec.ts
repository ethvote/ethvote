import {Maybe} from '../../../app/utils/maybe.utils';
import {RolesJsonParserImpl} from '../../../app/services/jsonParser/rolesJsonParserImpl.service';
import {Roles} from '../../../app/models/roles/roles.model';
import {VotersWithRoles} from '../../../app/models/roles/votersWithRoles.model';

describe('RolesJsonParserImpl', () => {

    let service: RolesJsonParserImpl;
    const validAvailableRolesObject = {
        roles:
            [
                'role1',
                'role2',
                'role3'
            ]
    };

    const emptyRoles = {
      roles: []
    };

    const validVoterWithRolesObject = {
        voters:
            [
                {roles: [], name: 'u1', id: 1},
                {roles: ['Powervoter', 'Administrator'], name: 'u2', id: 2}
            ]
    };

    beforeEach(() => {
        service = new RolesJsonParserImpl();
    });

    it('should be able to parse valid json empty roles', () => {
        const jsonString = JSON.stringify(emptyRoles);

        const parsedMaybeObject: Maybe<Roles> = service.parseAvailableRolesJson(jsonString);
        expect(parsedMaybeObject.getData().getRoles()).toEqual([]);
    });

    it('should be able to parse valid json object', () => {
        const jsonString = JSON.stringify(validAvailableRolesObject);

        const parsedMaybeObject: Maybe<Roles> = service.parseAvailableRolesJson(jsonString);
        expect(parsedMaybeObject.getData().getRoles()).toEqual(['role1', 'role2', 'role3']);
    });

    it('invalid json should return empty', () => {
        const parsedMaybeObject: Maybe<Roles> = service.parseAvailableRolesJson('invalid');
        expect(parsedMaybeObject.isEmpty).toBeTruthy();
    });

    it('should be able to parse user with roles', () => {
        const jsonString = JSON.stringify(validVoterWithRolesObject);

        const parsedMaybeObject: Maybe<VotersWithRoles> = service.parseVotersWithRoles(jsonString);
        expect(parsedMaybeObject.getData().getVoters().length).toBe(2);

        expect(parsedMaybeObject.getData().getVoters()[0].getId()).toBe(1);
        expect(parsedMaybeObject.getData().getVoters()[0].getName()).toBe('u1');
        expect(parsedMaybeObject.getData().getVoters()[0].getRoles()).toEqual([]);

        expect(parsedMaybeObject.getData().getVoters()[1].getId()).toBe(2);
        expect(parsedMaybeObject.getData().getVoters()[1].getName()).toBe('u2');
        expect(parsedMaybeObject.getData().getVoters()[1].getRoles()).toEqual(['Powervoter', 'Administrator']);
    });

});
