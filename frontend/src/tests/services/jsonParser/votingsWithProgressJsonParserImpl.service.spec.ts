import { Maybe } from '../../../app/utils/maybe.utils';
import {VotingsWithProgressArray} from '../../../app/models/votings/votingsWithProgressArray.json.model';
import {VotingsWithProgressJsonParser} from '../../../app/services/jsonParser/votingsWithProgressJsonParser.service';
import {VotingsWithProgressJsonParserImpl} from '../../../app/services/jsonParser/votingsWithProgressJsonParserImpl.service';


describe('VotingsWithProgressJsonParserImpl', () => {

    let service: VotingsWithProgressJsonParser;
    let validJsonObject;

    beforeEach(() => {
        service = new VotingsWithProgressJsonParserImpl();

        validJsonObject = {
                votings:
                    [
                        {
                            phase: 0,
                            options:
                                [
                                    {optionPrimeNumber: 2, optionName: 'Option 1'},
                                    {optionPrimeNumber: 3, optionName: 'Option 2'}
                                ],
                            description: 'Description',
                            id: 3,
                            title: 'Title',
                            transactionId: 'transId',
                            timeVotingIsFinished: -1
                        }
                    ],
                votingsVoterProgress:
                    [
                        {
                            participatedInDecryption: false,
                            phaseNumber: 0,
                            votingId: 3,
                            createdSecretShares:  false,
                            voted: true,
                            usesCustomKey: true
                        }
                    ]
            };
    });

    it('should be able to parser valid json', () => {
        const parsedMaybeObject: Maybe<VotingsWithProgressArray> = service.parseJson(JSON.stringify(validJsonObject));
        const voting = parsedMaybeObject.getData().getVotings()[0];
        const votingProgress = parsedMaybeObject.getData().getVotingsVoterProgress()[0];

        expect(parsedMaybeObject.getData().getVotings().length).toBe(1);
        expect(parsedMaybeObject.getData().getVotingsVoterProgress().length).toBe(1);

        expect(voting.getTitle()).toBe('Title');
        expect(voting.getDescription()).toBe('Description');
        expect(voting.getId()).toBe(3);
        expect(voting.getPhaseNumber()).toBe(0);
        expect(voting.getOptions().length).toBe(2);
        expect(voting.getOptions()[0].getOptionName()).toBe('Option 1');
        expect(voting.getOptions()[1].getOptionName()).toBe('Option 2');
        expect(voting.getOptions()[0].getOptionPrimeNumber()).toBe(2);
        expect(voting.getOptions()[1].getOptionPrimeNumber()).toBe(3);
        expect(voting.getTransactionId()).toBe('transId');
        expect(voting.getTimeVotingIsFinished()).toBe(-1);

        expect(votingProgress.getPhaseNumber()).toBe(0);
        expect(votingProgress.getVotingId()).toBe(3);
        expect(votingProgress.getParticipatedInDecryption()).toBeFalsy();
        expect(votingProgress.getVoted()).toBeTruthy();
        expect(votingProgress.getCreatedSecretShares()).toBeFalsy();
        expect(votingProgress.getUsesCustomKey()).toBeTruthy();
    });

});
