import { UserPublicKeyArray } from 'src/app/models/userPublicKey/userPublicKeyArray.model';
import { UserPublicKeyJsonParserImpl } from './../../../app/services/jsonParser/userPublicKeyJsonParserImpl.service';
import { Maybe } from '../../../app/utils/maybe.utils';
import { UserPublicKeyJsonParser } from 'src/app/services/jsonParser/userPublicKeyJsonParser.service';


describe('UserPublicKeyJsonParserImpl', () => {

    let service: UserPublicKeyJsonParser;

    beforeEach(() => {
        service = new UserPublicKeyJsonParserImpl();
      });

    it('throws parsing error if votings array is missing', () => {
        const jsonObject: object = {};
        const jsonString = JSON.stringify(jsonObject);

        const parsedMaybeObject: Maybe<UserPublicKeyArray> = service.parseJson(jsonString);
        expect(parsedMaybeObject.isEmpty()).toBeTruthy();
    });

    it('should be able to parse empty object', () => {
        const jsonObject: object = {voters: []};
        const jsonString = JSON.stringify(jsonObject);

        const parsedMaybeObject: Maybe<UserPublicKeyArray> = service.parseJson(jsonString);
        expect(parsedMaybeObject.isEmpty()).toBeFalsy();

        const parsedObject: UserPublicKeyArray = parsedMaybeObject.getData();
        expect(JSON.stringify(jsonObject)).toBe(JSON.stringify(parsedObject));
        expect(parsedObject.getUserPublicKeys().length).toBe(0);
    });

    it('should return empty if not valid json', () => {
        const parsedMaybeObject: Maybe<UserPublicKeyArray> = service.parseJson('notjson');
        expect(parsedMaybeObject.isEmpty()).toBeTruthy();
    });

    it('throws parsing error if username is missing', () => {
        const jsonObject: object = {voters: [{ publicKey: 'key', id: 42}]};
        const jsonString = JSON.stringify(jsonObject);

        const parsedMaybeObject: Maybe<UserPublicKeyArray> = service.parseJson(jsonString);
        expect(parsedMaybeObject.isEmpty()).toBeTruthy();
    });

    it('throws parsing error if publicKey is missing', () => {
        const jsonObject: object = {voters: [{ username: 'username', id: 42}]};
        const jsonString = JSON.stringify(jsonObject);

        const parsedMaybeObject: Maybe<UserPublicKeyArray> = service.parseJson(jsonString);
        expect(parsedMaybeObject.isEmpty()).toBeTruthy();
    });

    it('throws parsing error if id is missing', () => {
        const jsonObject: object = {voters: [{ publicKey: 'key', username: 'username'}]};
        const jsonString = JSON.stringify(jsonObject);

        const parsedMaybeObject: Maybe<UserPublicKeyArray> = service.parseJson(jsonString);
        expect(parsedMaybeObject.isEmpty()).toBeTruthy();
    });

    it('can parse valid object', () => {
        const jsonObject: object = {voters: [
            {name: 'u1', publicKey: 'k1', id: 27},
            {name: 'u2', publicKey: 'k2', id: 42},
            {name: 'u3', publicKey: 'k3', id: 23},
        ]};
        const jsonString = JSON.stringify(jsonObject);

        const parsedMaybeObject: Maybe<UserPublicKeyArray> = service.parseJson(jsonString);
        expect(parsedMaybeObject.isEmpty()).toBeFalsy();
        expect(parsedMaybeObject.getData().getUserPublicKeys().length).toBe(3);
        expect(parsedMaybeObject.getData().getUserPublicKeys()[0].getName()).toBe('u1');
        expect(parsedMaybeObject.getData().getUserPublicKeys()[0].getPublicKey()).toBe('k1');
        expect(parsedMaybeObject.getData().getUserPublicKeys()[0].getId()).toBe(27);
        expect(parsedMaybeObject.getData().getUserPublicKeys()[1].getName()).toBe('u2');
        expect(parsedMaybeObject.getData().getUserPublicKeys()[1].getPublicKey()).toBe('k2');
        expect(parsedMaybeObject.getData().getUserPublicKeys()[1].getId()).toBe(42);
        expect(parsedMaybeObject.getData().getUserPublicKeys()[2].getName()).toBe('u3');
        expect(parsedMaybeObject.getData().getUserPublicKeys()[2].getPublicKey()).toBe('k3');
        expect(parsedMaybeObject.getData().getUserPublicKeys()[2].getId()).toBe(23);
    });

    it('should be able to parse actual public key', () => {
        const publicKey = '-----BEGIN PUBLIC KEY-----\r\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxUEwYbLRZDpfyXvV2Y3B\r\nZzS8MN3MfbM+hyRsBSgy6yXhNED9wWKRVqBC9jhxbkY+YTJrFrbsLDPvOI7a8H1S\r\nJRfRSgb+YhnPYLWnhffZR2JEWsthCmtU4BN5d7uJv6NyIV3GXktFJceHbwRj0u8o\r\n9w5eag7kVpoVruAsdXcG0SKlR3GEuZd8GQX3pqL+19nD0X9PhwC9K5Ne8KwSvMTX\r\nYi04eMaChhu7MFy8kubfIEaaSuziF5omxEfvE18i+nYAkHB2fIiCg/ziIrRrP8WZ\r\nN003B4FCbxs8dpY2Nz+PVYtz0nMgz18ElJ1MmzrXRFB/FzGvb/YxjR9+flfxJqR2\r\nWwIDAQAB\r\n-----END PUBLIC KEY-----\r\n';
        const username = 'John Doe';
        const id = 27;

        const jsonObject: object = {voters: [
            {name: username, publicKey: publicKey, id: id},
        ]};
        const jsonString = JSON.stringify(jsonObject);

        const parsedMaybeObject: Maybe<UserPublicKeyArray> = service.parseJson(jsonString);
        expect(parsedMaybeObject.isEmpty()).toBeFalsy();
        expect(parsedMaybeObject.getData().getUserPublicKeys().length).toBe(1);
        expect(parsedMaybeObject.getData().getUserPublicKeys()[0].getName()).toBe(username);
        expect(parsedMaybeObject.getData().getUserPublicKeys()[0].getPublicKey()).toBe(publicKey);
        expect(parsedMaybeObject.getData().getUserPublicKeys()[0].getId()).toBe(id);
      });
});
