import { NoopLocalStorageService } from './../../localStorage/noopLocalStorage.service';
import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LocalStorageService } from '../../../../app/services/localStorage/localStorage.service';
import {RefreshTokenInterceptor} from '../../../../app/services/communication/refreshTokenInterceptor/refreshTokenInterceptor';
import {SubscriberService} from '../../../../app/services/subscription/subscriber.service';
import {SubscriberServiceImpl} from '../../../../app/services/subscription/subscriberImpl.service';

describe('RefreshTokenInterceptor', () => {

    let httpMock: HttpTestingController;
    let httpClient: HttpClient;
    let noopStorageService: LocalStorageService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule
            ],
            providers: [
                { provide: HTTP_INTERCEPTORS, useClass: RefreshTokenInterceptor, multi: true },
                { provide: LocalStorageService, useClass: NoopLocalStorageService},
                { provide: SubscriberService, useClass: SubscriberServiceImpl}
            ],
        });

        httpMock = TestBed.get(HttpTestingController);
        httpClient = TestBed.get(HttpClient);
        noopStorageService = TestBed.get(LocalStorageService);
    });

    it('should store new token if there is one', async() => {
        spyOn(noopStorageService, 'upsert');
        const dummyToken = 'dummyToken';
        httpClient.get('/dummyUrl').subscribe();
        const req = httpMock.expectOne('/dummyUrl');
        req.flush({}, { status: 200, statusText: 'Commitments are stored', headers: {RefreshToken: dummyToken}} );

        expect(noopStorageService.upsert).toHaveBeenCalledWith('token', dummyToken);
    });

    it('should not store null token', async() => {
        spyOn(noopStorageService, 'upsert');
        const dummyToken = 'dummyToken';
        httpClient.get('/dummyUrl').subscribe();
        const req = httpMock.expectOne('/dummyUrl');
        req.flush({}, { status: 200, statusText: 'Commitments are stored'} );

        expect(noopStorageService.upsert).toHaveBeenCalledTimes(0);
    });

});
