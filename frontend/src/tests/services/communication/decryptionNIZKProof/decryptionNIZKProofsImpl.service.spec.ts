import { TestBed } from '@angular/core/testing';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import {DecryptionNIZKProofService} from '../../../../app/services/communication/decryptionNIZKProof/decryptionNIZKProofs.service';
import {DecryptionNIZKProofServiceImpl} from '../../../../app/services/communication/decryptionNIZKProof/decryptionNIZKProofsImpl.service';
import {DecryptionNIZKProof} from '../../../../app/models/decryptionNIZKProof/decryptionNIZKProof';

describe('DecryptionNIZKProofService', () => {

    let decryptionNIZKProofService: DecryptionNIZKProofService;
    let httpMock: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule
            ],
            providers: [
                { provide: DecryptionNIZKProofService , useClass: DecryptionNIZKProofServiceImpl },
            ],
        });

        httpMock = TestBed.get(HttpTestingController);
        decryptionNIZKProofService = TestBed.get(DecryptionNIZKProofService);

    });

    it('should be created', async() => {
        expect(decryptionNIZKProofService).toBeTruthy();
    });

    it('should return false if server returned an error', async() => {
        const voteId = 27;
        const voterId = 42;
        const proof = new DecryptionNIZKProof().setA('a').setB('b').setR('r').setSignature('sign');

        decryptionNIZKProofService.sendNIZKProofs(voteId, voterId, proof).subscribe(
            successful => {
                expect(successful.getFirst()).toBeFalsy();
                expect(successful.getSecond()).toBe('Dummy error');
            });

        const req = httpMock.expectOne('/api/votings/' + voteId + '/decryptionZeroKnowledgeProofs/voters/' + voterId);
        expect(req.request.method).toEqual('POST');
        expect(req.request.body).toBe('{"a":"a","b":"b","r":"r","signature":"sign"}');
        req.flush('Dummy error', { status: 400, statusText: 'Server error' } );
    });

    it('should return false if server returned an error', async() => {
        const voteId = 27;
        const voterId = 42;
        const proof = new DecryptionNIZKProof().setA('a').setB('b').setR('r').setSignature('sign');

        decryptionNIZKProofService.sendNIZKProofs(voteId, voterId, proof).subscribe(
            successful => {
                expect(successful.getFirst()).toBeTruthy();
            });

        const req = httpMock.expectOne('/api/votings/' + voteId + '/decryptionZeroKnowledgeProofs/voters/' + voterId);
        expect(req.request.method).toEqual('POST');
        expect(req.request.body).toBe('{"a":"a","b":"b","r":"r","signature":"sign"}');
        req.flush({}, { status: 201, statusText: 'Created' } );
    });

});
