import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed, async } from '@angular/core/testing';

import {DecryptionFactorsService} from '../../../../app/services/communication/decryptionFactors/decryptionFactors.service';
import {DecryptionFactorsServiceImpl} from '../../../../app/services/communication/decryptionFactors/decryptionFactorsImpl.service';
import {DecryptionFactorsJsonParser} from '../../../../app/services/jsonParser/decryptionFactorsJsonParser.service';
import {DecryptionFactorsJsonParserImpl} from '../../../../app/services/jsonParser/decryptionFactorsJsonParserImpl.service';
import {SingleDecryptionFactor} from '../../../../app/models/decryptionFactors/singleDecryptionFactor.model';

describe('DecryptionFactorsService', () => {

    const votingId = 42;
    const voterId = 27;
    const decryptionFactorValue = 'd1';
    let decryptionFactor: SingleDecryptionFactor;

    let decryptionFactorService: DecryptionFactorsService;
    let httpMock: HttpTestingController;

    const validObject = {
            'decryptionFactors':
                [
                    {'fromVoterPosition': 0, 'fromVoterId': 27, 'decryptionFactor': 'd1', 'fromVoterName': 'v1', 'signature': 'sign1'},
                    {'fromVoterPosition': 1, 'fromVoterId': 42, 'decryptionFactor': 'd2', 'fromVoterName': 'v2', 'signature': 'sign2'},
                    {'fromVoterPosition': 2, 'fromVoterId': 79, 'decryptionFactor': 'd3', 'fromVoterName': 'v3', 'signature': 'sign3'}
                ]
        };

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule
            ],
            providers: [
                { provide: DecryptionFactorsService , useClass: DecryptionFactorsServiceImpl },
                { provide: DecryptionFactorsJsonParser , useClass: DecryptionFactorsJsonParserImpl },
            ],
        });

        decryptionFactor = new SingleDecryptionFactor()
            .setDecryptionFactor(decryptionFactorValue);

        httpMock = TestBed.get(HttpTestingController);
        decryptionFactorService = TestBed.get(DecryptionFactorsService);
    });

    it('should be created', () => {
        expect(decryptionFactorService).toBeTruthy();
    });

    it('should return false if return code is 200 (valid answer is 201)', async(() => {
        decryptionFactorService.storeDecryptionFactor(votingId, voterId, decryptionFactor).subscribe(ret => expect(ret.getFirst()).toBeFalsy());
        const req = httpMock.expectOne(r => r.method === 'POST' && r.url === '/api/votings/' + votingId + '/decryptionFactors/voters/' + voterId);
        req.flush({}, { status: 200, statusText: 'Ok' } );
    }));

    it('should return true if return code is 200 (valid answer is 201)', async(() => {
        decryptionFactorService.storeDecryptionFactor(votingId, voterId, decryptionFactor).subscribe(ret => expect(ret.getFirst()).toBeTruthy());
        const req = httpMock.expectOne(r => r.method === 'POST' && r.url === '/api/votings/' + votingId + '/decryptionFactors/voters/' + voterId);
        req.flush({}, { status: 201, statusText: 'Created' } );
        expect(req.request.body).toBe( '{"decryptionFactor":"d1"}');

    }));

    it('should return the right data if asked for all keys', async() => {
        decryptionFactorService.getAllDecryptionFactors(votingId).subscribe(data => {
            expect(data.getData().getDecryptionFactors().length).toBe(3);

            expect(data.getData().getDecryptionFactors()[0].getFromVoterId()).toBe(27);
            expect(data.getData().getDecryptionFactors()[0].getFromVoterName()).toBe('v1');
            expect(data.getData().getDecryptionFactors()[0].getFromVoterPosition()).toBe(0);
            expect(data.getData().getDecryptionFactors()[0].getDecryptionFactor()).toBe('d1');
            expect(data.getData().getDecryptionFactors()[0].getSignature()).toBe('sign1');

            expect(data.getData().getDecryptionFactors()[1].getFromVoterId()).toBe(42);
            expect(data.getData().getDecryptionFactors()[1].getFromVoterName()).toBe('v2');
            expect(data.getData().getDecryptionFactors()[1].getFromVoterPosition()).toBe(1);
            expect(data.getData().getDecryptionFactors()[1].getDecryptionFactor()).toBe('d2');
            expect(data.getData().getDecryptionFactors()[1].getSignature()).toBe('sign2');

            expect(data.getData().getDecryptionFactors()[2].getFromVoterId()).toBe(79);
            expect(data.getData().getDecryptionFactors()[2].getFromVoterName()).toBe('v3');
            expect(data.getData().getDecryptionFactors()[2].getFromVoterPosition()).toBe(2);
            expect(data.getData().getDecryptionFactors()[2].getDecryptionFactor()).toBe('d3');
            expect(data.getData().getDecryptionFactors()[2].getSignature()).toBe('sign3');
        });
        const mockAnswer = JSON.stringify(validObject);
        const req = httpMock.expectOne(r => r.method === 'GET' && r.url === '/api/votings/' + votingId + '/decryptionFactors');
        req.flush(JSON.parse(mockAnswer), { status: 200, statusText: 'Ok' });
    });

    it('should return empty right data if asked for all keys', async() => {
        decryptionFactorService.getAllDecryptionFactors(votingId).subscribe(data => expect(data.isEmpty()).toBeTruthy());
        const mockAnswer = JSON.stringify(validObject);
        const req = httpMock.expectOne(r => r.method === 'GET' && r.url === '/api/votings/' + votingId + '/decryptionFactors');
        req.flush(JSON.parse(mockAnswer), { status: 500, statusText: 'Ok' });
    });

});
