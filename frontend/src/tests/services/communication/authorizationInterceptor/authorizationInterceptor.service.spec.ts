import { Maybe } from './../../../../app/utils/maybe.utils';
import { NoopLocalStorageService } from './../../localStorage/noopLocalStorage.service';
import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
// tslint:disable-next-line
import { AuthorizationInterceptor } from './../../../../app/services/communication/authorizationInterceptor/authorizationInterceptor.service';
import { HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LocalStorageService } from '../../../../app/services/localStorage/localStorage.service';

describe('AuthorizationInterceptor', () => {

    let httpMock: HttpTestingController;
    let httpClient: HttpClient;
    let noopStorageService: LocalStorageService;

    beforeEach(() => {
        TestBed.configureTestingModule({
        imports: [
            HttpClientTestingModule
        ],
        providers: [
            { provide: HTTP_INTERCEPTORS, useClass: AuthorizationInterceptor, multi: true },
            { provide: LocalStorageService, useClass: NoopLocalStorageService }
          ],
      });

      httpMock = TestBed.get(HttpTestingController);
      httpClient = TestBed.get(HttpClient);
      noopStorageService = TestBed.get(LocalStorageService);
    });

    it('should append token if it is available', async() => {
        const token = 'thisIsAtoken';
        const expected = 'Bearer ' + token;
        spyOn(noopStorageService, 'get').and.returnValue(Maybe.fromData(token));

        httpClient.get('/dummyUrl').subscribe();
        const req = httpMock.expectOne('/dummyUrl');
        expect(req.request.method).toEqual('GET');
        expect(req.request.headers.getAll('Authorization')[0]).toBe(expected);
    });

    it('should just forward request if no token available', async() => {
        spyOn(noopStorageService, 'get').and.returnValue(Maybe.fromEmpty());

        httpClient.get('/dummyUrl').subscribe();
        const req = httpMock.expectOne('/dummyUrl');
        expect(req.request.method).toEqual('GET');
        expect(req.request.headers.getAll('Authorization')).toBeFalsy();
    });

});
