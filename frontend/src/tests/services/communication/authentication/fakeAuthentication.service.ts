import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Maybe } from './../../../../app/utils/maybe.utils';
import { AuthenticationService } from './../../../../app/services/communication/authentication/authentication.service';
import { User } from '../../../../app/models/user.model';

@Injectable()
export class FakeAuthenticationService implements AuthenticationService {

    constructor(private validUsername: string, private validPassword: string, private tokenValue: string) {
    }

    authenticate(username: string, password: string): Observable<Maybe<User>> {
        if ( username === this.validUsername && password === this.validPassword) {
            return new Observable((observer) => {
                observer.next(Maybe.fromData(new User(username, this.tokenValue)));
                observer.complete();
            });
        }
        else {
            return new Observable((observer) => {
                observer.next(Maybe.fromEmpty());
            });
        }
    }
}
