import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {TestBed} from '@angular/core/testing';
import {CommitmentsService} from '../../../../app/services/communication/commitments/commitments.service';
import {SingleCommitment} from '../../../../app/models/commitments/singleCommitment.model';
import {CommitmentsServiceImpl} from '../../../../app/services/communication/commitments/commitmentsImpl.service';
import {CommitmentFromVoter} from '../../../../app/models/commitments/commitmentFromVoter.model';
import {CommitmentJsonParser} from '../../../../app/services/jsonParser/commitmentJsonParser.service';
import {CommitmentJsonParserImpl} from '../../../../app/services/jsonParser/commitmentJsonParserImpl.service';

describe('CommitmentsServiceImpl', () => {

    let commitmentService: CommitmentsService;
    let httpMock: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule
            ],
            providers: [
                { provide: CommitmentsService , useClass: CommitmentsServiceImpl },
                { provide: CommitmentJsonParser, useClass: CommitmentJsonParserImpl }
            ],
        });

        httpMock = TestBed.get(HttpTestingController);
        commitmentService = TestBed.get(CommitmentsService);

    });

    it('should send data to the right address and return true if successful', async() => {
        const votingId = 42;

        const voterId = 27;
        const voterName = 'Luke';

        const commitment1 = new SingleCommitment().setCoefficientNumber(0).setCommitment('c1');
        const commitment2 = new SingleCommitment().setCoefficientNumber(1).setCommitment('c2');
        const commitment3 = new SingleCommitment().setCoefficientNumber(2).setCommitment('c3');

        const commitments = new CommitmentFromVoter().setCommitments([commitment1, commitment2, commitment3]);

        commitmentService.storeCommitments(votingId, voterId, commitments).subscribe(s => expect(s).toBeTruthy());

        const req = httpMock.expectOne('/api/votings/' + votingId + '/commitments/voters/' + voterId);
        expect(req.request.method).toEqual('POST');
        req.flush({}, { status: 201, statusText: 'Commitments are stored' } );
        expect(req.request.body).toBe( '{"numberOfCommitments":3,"commitments":[{"commitment":"c1","coefficientNumber":0},{"commitment":"c2","coefficientNumber":1},{"commitment":"c3","coefficientNumber":2}]}');
    });

    it('should return false if something went wrong', async() => {
        const votingId = 42;

        const voterId = 27;

        const commitment1 = new SingleCommitment().setCoefficientNumber(0).setCommitment('c1');
        const commitment2 = new SingleCommitment().setCoefficientNumber(1).setCommitment('c2');
        const commitment3 = new SingleCommitment().setCoefficientNumber(2).setCommitment('c3');

        const commitments = new CommitmentFromVoter().setCommitments([commitment1, commitment2, commitment3]);

        commitmentService.storeCommitments(votingId, voterId, commitments).subscribe(s => expect(s.getFirst()).toBeFalsy());

        const req = httpMock.expectOne('/api/votings/' + votingId + '/commitments/voters/' + voterId);

        expect(req.request.method).toEqual('POST');
        req.flush({}, { status: 500, statusText: 'Internal server error' } );
    });

});
