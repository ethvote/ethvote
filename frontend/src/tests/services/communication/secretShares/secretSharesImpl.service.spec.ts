import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {TestBed} from '@angular/core/testing';
import {SecretSharesService} from '../../../../app/services/communication/secretShares/secretShares.service';
import {SecretSharesServiceImpl} from '../../../../app/services/communication/secretShares/secretSharesImpl.service';
import {SecretSharesFromVoter} from '../../../../app/models/secretShares/secretSharesFromVoter.model';
import {SingleSecretShareFromVoter} from '../../../../app/models/secretShares/singleSecretShareFromVoter.model';
import {SecretSharesForVoterJsonParser} from '../../../../app/services/jsonParser/secretSharesForVoterJsonParser.service';
import {NoopSecretSharesForVoterJsonParser} from '../../jsonParser/noopSecretSharesForVoterJsonParser.service';
import {SecretSharesForVoter} from '../../../../app/models/secretShares/secretSharesForVoter.model';
import {Maybe} from '../../../../app/utils/maybe.utils';

describe('SecretSharesServiceImpl', () => {

    let secretShareService: SecretSharesService;
    let noopParser: SecretSharesForVoterJsonParser;
    let httpMock: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule
            ],
            providers: [
                { provide: SecretSharesService , useClass: SecretSharesServiceImpl},
                { provide: SecretSharesForVoterJsonParser, useClass: NoopSecretSharesForVoterJsonParser},
            ],
        });

        httpMock = TestBed.get(HttpTestingController);
        secretShareService = TestBed.get(SecretSharesService);
        noopParser = TestBed.get(SecretSharesForVoterJsonParser);

    });

    it('should send data to the right address and return true if successful', async() => {
        const votingId = 42;

        const voterId = 2;
        const voterName = 'Luke';

        const singleShare1 = new SingleSecretShareFromVoter(1, 'voter1', ['s1', 's2', 's3'], 'sign0');
        const singleShare2 = new SingleSecretShareFromVoter(2, 'voter2', ['s4', 's5', 's6'], 'sign1');
        const singleShare3 = new SingleSecretShareFromVoter(3, 'voter3', ['s7', 's8', 's9'], 'sign2');

        const share = new SecretSharesFromVoter(voterId, voterName);
        share.addSecretShare(singleShare1)
            .addSecretShare(singleShare2)
            .addSecretShare(singleShare3);

        secretShareService.storeSecretShares(votingId, voterId, share).subscribe(s => expect(s.getFirst()).toBeTruthy());

        const req = httpMock.expectOne('/api/votings/' + votingId + '/secretShares');
        expect(req.request.method).toEqual('POST');
        req.flush({}, { status: 201, statusText: 'Secret shares are stored' } );
        expect(req.request.body).toBe('{"fromVoterId":2,"fromVoterName":"Luke","secretShares":[{"forVoterId":1,"forVoterName":"voter1","secretShare":["s1","s2","s3"],"signature":"sign0"},{"forVoterId":2,"forVoterName":"voter2","secretShare":["s4","s5","s6"],"signature":"sign1"},{"forVoterId":3,"forVoterName":"voter3","secretShare":["s7","s8","s9"],"signature":"sign2"}]}');
    });

    it('should return false if something went wrong', async() => {
        const votingId = 42;

        const voterId = 2;
        const voterName = 'Luke';

        const singleShare1 = new SingleSecretShareFromVoter(1, 'voter1', ['s1', 's2', 's3'], 'sign0');
        const singleShare2 = new SingleSecretShareFromVoter(2, 'voter2', ['s4', 's5', 's6'], 'sign1');
        const singleShare3 = new SingleSecretShareFromVoter(3, 'voter3', ['s7', 's8', 's9'], 'sign2');

        const share = new SecretSharesFromVoter(voterId, voterName);
        share.addSecretShare(singleShare1)
            .addSecretShare(singleShare2)
            .addSecretShare(singleShare3);

        secretShareService.storeSecretShares(votingId, voterId, share).subscribe(s => expect(s.getFirst()).toBeFalsy());

        const req = httpMock.expectOne('/api/votings/' + votingId + '/secretShares');
        expect(req.request.method).toEqual('POST');
        req.flush({}, { status: 500, statusText: 'Internal server error' } );
        expect(req.request.body).toBe('{"fromVoterId":2,"fromVoterName":"Luke","secretShares":[{"forVoterId":1,"forVoterName":"voter1","secretShare":["s1","s2","s3"],"signature":"sign0"},{"forVoterId":2,"forVoterName":"voter2","secretShare":["s4","s5","s6"],"signature":"sign1"},{"forVoterId":3,"forVoterName":"voter3","secretShare":["s7","s8","s9"],"signature":"sign2"}]}');

    });

    it('should return empty object if response status is not ok', async() => {
        const votingsId = 42;
        const voterId = 27;
        secretShareService .getAllSharesForVoter(votingsId, voterId).subscribe(maybeSecretShares => expect(maybeSecretShares.isEmpty()).toBeTruthy());
        const req = httpMock.expectOne('/api/votings/' + votingsId + '/secretShares/forVoters/' + voterId);
        expect(req.request.method).toEqual('GET');
        req.flush({}, { status: 500, statusText: 'Internal server error' });
    });

    it('should forward data that parser provided', async() => {
        const votingsId = 42;
        const voterId = 27;
        const secretShares = new SecretSharesForVoter();

        spyOn(noopParser, 'parseJson').and.returnValue(Maybe.fromData(secretShares));
        secretShareService.getAllSharesForVoter(votingsId, voterId)
            .subscribe(maybeSecretShares => expect(maybeSecretShares.getData()).toBe(secretShares));

        const req = httpMock.expectOne('/api/votings/' + votingsId + '/secretShares/forVoters/' + voterId);

        req.flush({}, { status: 200, statusText: 'DummyText' });
    });

});
