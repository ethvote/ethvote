import { SaveFileServiceImpl } from './../../../app/services/saveFile/saveFileImpl.service';
import { TestBed } from '@angular/core/testing';

import { SaveFileService } from 'src/app/services/saveFile/saveFile.service';

describe('SaveFileService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  let saveFileService: SaveFileService;
  saveFileService = new SaveFileServiceImpl();

  it('should be created', () => {
    const service: SaveFileService = TestBed.get(SaveFileService);
    expect(service).toBeTruthy();
  });

});
