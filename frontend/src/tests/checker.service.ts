export class Checker {
    static isEqual(o1: Object, o2: Object): Boolean {
        return JSON.stringify(o1) === JSON.stringify(o2);
    }
}
